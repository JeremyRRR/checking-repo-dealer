$(document).ready(function() {
	$(".narrow").attr("style","opacity:.5; box-shadow:0; ");
	$(".baseselect").attr("style","opacity:.5; box-shadow:0; ");
	$(".medium").attr("style","opacity:1;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);" );
	$('.navbar-nav').on('shown.bs.tab', 'a', function (e) {
    if (e.relatedTarget) {
        $(e.relatedTarget).removeClass('active');
    }
})
$('#template_form').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) ==""){

           swal("Thank You!", "New Template Created.", "success");

		}else if($.trim(responseText) !=""){
			// sweetAlert("Oops...", "Error:"+responseText, "error");

			 swal({
  title: "Error!",
  text: "Error:"+responseText,
  html: true
});

		}

		}

    });

	$("#border_color").spectrum({
    showPaletteOnly: true,
    showPalette:true,
    color: '#ccc',
    palette: [
        ['#2E3193', '#000000', '#00ACF1', '#1473DB', '#EF1716'],
        ['#F89400', '#FFF300', '#00A64C', '#939598']
    ],
	change: function(color) {
        $("#border_color2").val(color.toHexString());
	if($("#template_width").val()=="narrow"){
		$(".standard_preview").attr("style", "background:no-repeat url(https://d1ja3ktoroz7zi.cloudfront.net/standard_"+($("#template_layout").val()).toLowerCase()+"_"+((color.toHexString()).replace('#','')).toLowerCase()+".png); background-size: 210px 560px;");
		//alert((color.toHexString()).replace('#',''));
   }else{
	   $(".standard_preview").attr("style", "background:no-repeat url(https://d1ja3ktoroz7zi.cloudfront.net/"+($("#template_width").val()).toLowerCase()+"_"+($("#template_layout").val()).toLowerCase()+"_"+((color.toHexString()).replace('#','')).toLowerCase()+".png); background-size: 210px 560px;");
   }
//	if($("#template_width").val()=="narrow"){
//		$(".narrow_preview").attr("style", "background:no-repeat url(assets/template/"+$("#template_width").val()+"_"+$("#template_layout").val()+"_"+(color.toHexString()).replace('#','')+".png); background-size: 210px 560px;");
		//alert((color.toHexString()).replace('#',''));
//    }
	}
});
$("#background_color").spectrum({
    showPalette:true,
    color: '#555',
    palette: [
        ['#2E3193', '#000000', '#00ACF1', '#1473DB', '#EF1716'],
        ['#F89400', '#FFF300', '#00A64C', '#939598', '#FFFFFF']
    ],
	change: function(color) {
        $("#background_color2").val(color.toHexString());
		if($("#template_width").val()=="standard"){
		$(".standard_preview").css("background-color", color.toHexString());
		//alert((color.toHexString()).replace('#',''));
    }
	if($("#template_width").val()=="narrow"){
		$(".standard_preview").css("background-color", color.toHexString());
		//alert((color.toHexString()).replace('#',''));
    }
    }
	
});
$( "#vifont" ).change(function() {
  $(".vinformation").removeClass("font_MED");
  $(".vinformation").removeClass("font_SM");
  $(".vinformation").removeClass("font_LG");
  $(".vinformation").addClass("font_"+$(this).val());
});
$( "#address" ).change(function() {
	
	var text=$(this).val();
	var text=text.replace(/<br\s*\/?>/mg,"\n")
  $(".daddress").html(text);
});
$( ".aoptions" ).change(function() {
  $("."+$(this).attr("fs-id")).removeClass("font_MED");
  $("."+$(this).attr("fs-id")).removeClass("font_SM");
  $("."+$(this).attr("fs-id")).removeClass("font_LG");
  $("."+$(this).attr("fs-id")).removeClass("font_XL");
  $("."+$(this).attr("fs-id")).removeClass("font_XXL");
  $("."+$(this).attr("fs-id")).addClass("font_"+$(this).val());
});
$('.boldcheck').click(function(){
   if(this.checked == true) {
        $("."+$(this).attr("b-id")).addClass("bold");
    } else {
       $("."+$(this).attr("b-id")).removeClass("bold");
   }
});
$('.isoptions').click(function(){
	$('.isoptions').removeClass("active");
	$(this).addClass("active");
	$('.infographic .stock_image').attr('src',$(this).attr("oi-image"));
	$('#isoptions').val($(this).attr("oi-img"));

        //$("."+$(this).attr("b-id")).addClass("bold");
});
	$(".step1").show();
    $(".step2a").hide();
	$(".step2b").hide();
	$(".step2c").hide();
	$(".step3").hide();
	$(".step4").hide();
	$(".step5").hide();
	$(".step6").hide();
	$(".step7").hide();
	$(".isoptions").attr("style","display:none");
var hidenshow="."+$("#template_layout").val()+"."+$("#template_width").val()+".isoptions";
$( hidenshow ).removeAttr( "style" );
});
function stepup(value){
	if(value=="step1"){
	if($("#template_name").val()==""){
		sweetAlert("Oops...", "You can not begin without template name!", "error");
	}else{
		$(".step1").hide();
		$(".step2a").show();
		step("step2");
		$(".step").html("Step 2a");
			$(".step-title").html("Base Template Width ");
	}
	}else if(value=="step2a"){
		$(".step2a").hide();
		$(".step2b").show();
		$(".step").html("Step 2b");
			$(".step-title").html("Base Template Layout");
	}else if(value=="step2b"){
		$(".step2b").hide();
		$(".step2c").show();
		$(".step").html("Step 2c");
			$(".step-title").html("Base Template Color");
	}else if(value=="step2c"){
		$(".step2c").hide();
		$(".step3").show();
		step("step3");
	}else if(value=="step3"){
		$(".step3").hide();
		$(".step4").show();
		step("step4");
	}else if(value=="step4"){
		$(".step4").hide();
		$(".step5").show();
		step("step5");
	}else if(value=="step5"){
		$(".step5").hide();
		$(".step6").show();
		step("step6");
	}else if(value=="step6"){
		$(".step6").hide();
		$(".step7").show();
		step("step7");
	}
}
function stepdown(value){
	if(value=="step1"){
		$(".step1").show();
		$(".step2a").hide();
		step("step1");
	}else if(value=="step2a"){
		$(".step2a").show();
		$(".step2b").hide();
		$(".step").html("Step 2a");
			$(".step-title").html("Base Template Width ");
	}else if(value=="step2b"){
		$(".step2b").show();
		$(".step2c").hide();
		$(".step").html("Step b");
			$(".step-title").html("Base Template Layout");
	}else if(value=="step2c"){
		$(".step2c").show();
		$(".step3").hide();
		step("step2");
		$(".step").html("Step 2c");
			$(".step-title").html("Base Template Color");
	}else if(value=="step3"){
		$(".step3").show();
		$(".step4").hide();
		step("step3");
	}else if(value=="step4"){
		$(".step4").show();
		$(".step5").hide();
		step("step4");
	}else if(value=="step5"){
		$(".step5").show();
		$(".step6").hide();
		step("step5");
	}else if(value=="step6"){
		$(".step6").show();
		$(".step7").hide();
		step("step6");
	}
}
function step(value){
		$(".step-tab").hide();
		$("."+value).show();
		$(".btn-circle").removeClass("btn-success");
		$(".btn-circle").addClass("btn-default");
		$("#"+value).removeClass("btn-default");
		$("#"+value).addClass("btn-success");
		if(value=="step1"){
			$(".step").html("Step 1");
			$(".step-title").html("Name Your Template");
		}else if(value=="step3"){
			$(".step").html("Step 3");
			$(".step-title").html("Dealer information ");
		}else if(value=="step4"){
			$(".step").html("Step 4");
			$(".step-title").html("Infographic ");
		}else if(value=="step5"){
			$(".step").html("Step 5");
			$(".step-title").html("Vehicle Information");
		}else if(value=="step6"){
			$(".step").html("Step 6");
			$(".step-title").html("Option Information");
		}else if(value=="step7"){
			$(".step").html("Step 7");
			$(".step-title").html("Save Template");
		}
		
}
function ucword(str){
    str = str.toLowerCase().replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g, function(replace_latter) { 
        return replace_latter.toUpperCase();
    });  //Can use also /\b[a-z]/g
    return str;  //First letter capital in each word
}

function widthSelect(value){
	if(value=="width"){
		$(".narrow").attr("style","opacity:.5; box-shadow:0; ");
		$(".width").attr("style","opacity:1;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);" );
		$("#template_width").val("standard");
		 var brtext="";
		$.each(JSON.parse($("#border_json").val()), function(i, v) {
    if (v.bg_base == ucword($("#template_width").val()) && v.bg_layout == ucword($("#template_layout").val())) {
       brtext=brtext+'<div class="colorid" onClick=brcolor("'+v.bg_color+'") style="background-color:#'+v.bg_color+'"></div>';
    }
});
$(".mycolor").html(brtext);
	}else{
		$(".width").attr("style","opacity:.5; box-shadow:0; ");
		$(".narrow").attr("style","opacity:1;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);");
		$("#template_width").val("narrow");
		var brtext="";
		$.each(JSON.parse($("#border_json").val()), function(i, v) {
    if (v.bg_base == ucword($("#template_width").val()) && v.bg_layout == ucword($("#template_layout").val())) {
       brtext=brtext+'<div class="colorid" onClick=brcolor("'+v.bg_color+'") style="background-color:#'+v.bg_color+'"></div>';
    }
});
$(".mycolor").html(brtext);
	}
	$('#background_image').val(($("#template_width").val()).toLowerCase()+"_"+($("#template_layout").val()).toLowerCase()+"_"+(($("#border_color2").val()).replace('#','')).toLowerCase()+".png");
	$(".isoptions").attr("style","display:none");
var hidenshow="."+$("#template_layout").val()+"."+$("#template_width").val()+".isoptions";
$( hidenshow ).removeAttr( "style" );
}
function baseSelect(value){
		$(".baseselect").attr("style","opacity:.5; box-shadow:0; ");
		$("."+value).attr("style","opacity:1;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);" );
		$(".standard_preview").removeClass($("#template_layout").val()+"T");
		$("#template_layout").val(value);
		$(".standard_preview").addClass(value+"T");
		$(".standard_preview").attr("style", "background:no-repeat url(https://d1ja3ktoroz7zi.cloudfront.net/"+($("#template_width").val()).toLowerCase()+"_"+($("#template_layout").val()).toLowerCase()+"_"+(($("#border_color2").val()).replace('#','')).toLowerCase()+".png); background-size: 210px 560px; background-color:"+$("#background_color2").val());
		if(value=="small"){
			if($('#isoptions').val()==""){
			$('.infographic .stock_image').attr('src',"https://d1xlji8qxtrdmo.cloudfront.net/1508539198.png");
			}
		}else if(value=="medium"){
			if($('#isoptions').val()==""){
			$('.infographic .stock_image').attr('src',"https://d1xlji8qxtrdmo.cloudfront.net/1490978725.png");
			}
		}else if(value=="large"){
			if($('#isoptions').val()==""){
			$('.infographic .stock_image').attr('src',"https://d1xlji8qxtrdmo.cloudfront.net/1508790038.png");
			}
		}else if(value=="top"){
			if($('#isoptions').val()==""){
			$('.infographic .stock_image').attr('src',"https://d1xlji8qxtrdmo.cloudfront.net/1508790038.png");
			}
		}
		var brtext="";
		$.each(JSON.parse($("#border_json").val()), function(i, v) {
    if (v.bg_base == ucword($("#template_width").val()) && v.bg_layout == ucword($("#template_layout").val())) {
       brtext=brtext+'<div class="colorid" onClick=brcolor("'+v.bg_color+'") style="background-color:#'+v.bg_color+'"></div>';
    }
});
$(".mycolor").html(brtext);
$('#background_image').val(($("#template_width").val()).toLowerCase()+"_"+($("#template_layout").val()).toLowerCase()+"_"+(($("#border_color2").val()).replace('#','')).toLowerCase()+".png");
$(".isoptions").attr("style","display:none");
var hidenshow="."+$("#template_layout").val()+"."+$("#template_width").val()+".isoptions";
$( hidenshow ).removeAttr( "style" );
}
function logoloader(filephoto){
	var imageLoader = document.getElementById('filePhoto'+filephoto);
    imageLoader.addEventListener('change', handleImage, false);

function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        
        $('.uploader'+filephoto+' img').attr('src',event.target.result);
		if(filephoto==""){
		$('.logo').html("<img src='"+event.target.result+"'>");
		}else if(filephoto=="2"){
		//$('.infographic').html("<img src='"+event.target.result+"' class='stock_image'>");
		$('.infographic .stock_image').attr('src',event.target.result);
		}
    }
    reader.readAsDataURL(e.target.files[0]);
}
}

function logoloader2(filephoto){
	var imageLoader = document.getElementById('background_upload');
    imageLoader.addEventListener('change', handleImage, false);

function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        
        $('.background_uploader img').attr('src',event.target.result);
		$(".standard_preview").attr("style", "background:no-repeat url("+event.target.result+"); background-size: 210px 560px;");
		$("#border_color2").val("");
		
    }
    reader.readAsDataURL(e.target.files[0]);
}
}

function showVi(element, status){
if(status=="0"){
	$("."+element).hide();
}else{
	$("."+element).show();
}
}

function changetext(element){
	//alert(element);
	$("#"+element).html($("#"+element+"desc").val());
}
function brcolor(color){
	
        $("#border_color2").val(color);
	if($("#template_width").val()=="narrow"){
		$(".standard_preview").attr("style", "background:no-repeat url(https://d1ja3ktoroz7zi.cloudfront.net/standard_"+($("#template_layout").val()).toLowerCase()+"_"+((color).replace('#','')).toLowerCase()+".png); background-size: 210px 560px; background-color:"+$("#background_color2").val());
		//alert((color.toHexString()).replace('#',''));
		$('#background_image').val(($("#template_width").val()).toLowerCase()+"_"+($("#template_layout").val()).toLowerCase()+"_"+((color).replace('#','')).toLowerCase()+".png");
		$('#background_upload').val(null);
		$('.background_uploader img').attr("src","assets/builder/test.png");
   }else{
	   $(".standard_preview").attr("style", "background:no-repeat url(https://d1ja3ktoroz7zi.cloudfront.net/"+($("#template_width").val()).toLowerCase()+"_"+($("#template_layout").val()).toLowerCase()+"_"+((color).replace('#','')).toLowerCase()+".png); background-size: 210px 560px; background-color:"+$("#background_color2").val());
	   $('#background_image').val(($("#template_width").val()).toLowerCase()+"_"+($("#template_layout").val()).toLowerCase()+"_"+((color).replace('#','')).toLowerCase()+".png");
	   $('#background_upload').val(null);
	   $('.background_uploader img').attr("src","assets/builder/test.png");
   }
//	if($("#template_width").val()=="narrow"){
//		$(".narrow_preview").attr("style", "background:no-repeat url(assets/template/"+$("#template_width").val()+"_"+$("#template_layout").val()+"_"+(color.toHexString()).replace('#','')+".png); background-size: 210px 560px;");
		//alert((color.toHexString()).replace('#',''));
//    }
	
}
/*var imageLoader = document.getElementById('filePhoto2');
    imageLoader.addEventListener('change', handleImage, false);

function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        
        $('.uploader2 img').attr('src',event.target.result);
		alert("shawon2");
    }
    reader.readAsDataURL(e.target.files[0]);
}*/
$( "#qr_title" ).change(function() {
  $(".qr_title").html($(this).val());
});

$( "#qr_text" ).change(function() {
  $(".qr_text").html($(this).val());
});
$( "#qr_text2" ).change(function() {
  $(".qr_text2").html($(this).val());
});
$( "#qr_footer" ).change(function() {
  var text2=$(this).val();
	var text2=text2.replace(/<br\s*\/?>/mg,"\n")
  $(".qr_footer").html(text2);
});
$( "#bar_title" ).change(function() {
  $(".bar_title").html($(this).val());
});

$( "#bar_text" ).change(function() {
  $(".bar_text").html($(this).val());
});
$( "#bar_footer" ).change(function() {
  var text1=$(this).val();
	var text1=text1.replace(/<br\s*\/?>/mg,"\n")
  $(".bar_footer").html(text1);
});

 $('input[type=radio][name=stock_select]').change(function() {
	
        if (this.value == 'qr_code') {
			 if($("#template_layout").val()!="small"){
            $(".stockin").attr("style","display:none");
			$(".medium_qr_code").removeAttr("style");
			 }
        }
        else if (this.value == 'logo_upload') {
            $(".stockin").attr("style","display:none");
			$(".stock_image").removeAttr("style");
        }else if (this.value == 'default_image') {
             $(".stockin").attr("style","display:none");
			$(".stock_image").removeAttr("style")
        }else if (this.value == 'barcode') {
			 if($("#template_layout").val()!="small"){
            $(".stockin").attr("style","display:none");
			$(".medium_bar_code").removeAttr("style");
			 }
        }
    });