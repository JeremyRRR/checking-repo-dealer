$(document).ready(function() {
$('#VIN_FORM').ajaxForm({

    // target identifies the element(s) to update with the server response

    // success identifies the function to invoke when the server response

    // has been received; here we apply a fade-in effect to the new content

    dataType:  'json',
    error: function(XMLHttpRequest, textStatus, errorThrown) {
		//Call vin audit if EDMUNDS failed to fetch
        $.ajax({

            url: "assets/ajax/vin_query.php",
            type: "post",
            data:$("#VIN_FORM" ).serialize(),
            dataType: 'json',
            cache:false,
            success: function(html){
				if(html.message === 'invalid'){

							swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});

						}else{
				var valid=html.VIN["@attributes"].Status;
//if VINQUERY succeed
                        if(valid=="SUCCESS"){

                            if(typeof html.VIN.Vehicle[0] === 'undefined'){

                                var ISN=$('#inputStockNumber').val();

                                var IVN=$('#inputVINNumber').val();

                                var InputPrice=$('#InputPrice').val();

                                $('#inputStockNumber2').val(ISN);

                                $('#inputVINNumber2').val(IVN);

                                $('#inputPrice2').val(InputPrice);

                                var year=html.VIN.Vehicle["@attributes"].Model_Year;

                                var make=html.VIN.Vehicle["@attributes"].Make;

                                var model=html.VIN.Vehicle["@attributes"].Model;

                                //var transmission=html.VIN.Vehicle["@attributes"].Transmission;

                                //var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

                                var style=html.VIN.Vehicle.Item[5]["@attributes"].Value;

                                var trim=html.VIN.Vehicle.Item[3]["@attributes"].Value;

                                var engine=html.VIN.Vehicle.Item[6]["@attributes"].Value;

                                $('#inputYear').val(year);

                                $('#inputMake').val(make);

                                $('#inputModel').val(model);

                                $('#inputTrim').val(trim);

                                $('#inputStyle').val(style);

                                //$('#inputTransmission').val(transmission);

                                //$('#inputDrivetrain').val(drivetrain);

                                $('#inputEngine').val(engine);

                                $('#VIN_FORM').hide();

                                $('#vehicle_form').show();

                                var d = new Date();

                                var curr_date = d.getDate();


                                var curr_month = d.getMonth()+1;

                                var curr_year = d.getFullYear();

                                var ddate=curr_year + "-" + curr_month + "-" + curr_date;

                                $('#inputDate').val(ddate);

                            }else{

                                var ISN=$('#inputStockNumber').val();

                                var IVN=$('#inputVINNumber').val();

                                var InputPrice=$('#InputPrice').val();

                                $('#inputStockNumber2').val(ISN);

                                $('#inputVINNumber2').val(IVN);

                                $('#inputPrice2').val(InputPrice);

                                var year=html.VIN.Vehicle[0]["@attributes"].Model_Year;

                                var make=html.VIN.Vehicle[0]["@attributes"].Make;

                                var model=html.VIN.Vehicle[0]["@attributes"].Model;

                                //var transmission=html.VIN.Vehicle["@attributes"].Transmission;

                                //var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

                                var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;

                                var trim=html.VIN.Vehicle[0].Item[3]["@attributes"].Value;

                                $('#inputTrim').val(trim);
                                $('#exttrim').hide();
                                if(typeof html.VIN.Vehicle[1]!=="undefined"){
                                    $('#inputTrim').val("");
                                    $('#inputTrim').attr("placeholder", "Choose Trim");
                                    $('select#extrim').append($('<option>', {value:"", text:"Choose Other"}));
                                    $('#extrim').show();
                                    $('#extrim').html("");
                                    $('#extrim').val("");
                                    $('#extrim').attr("placeholder", "Choose Color");
                                    //var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
                                    var firstArray=html.VIN.Vehicle;
                                    for(var i = 0, len = html.VIN.Vehicle.length; i < len; i += 1) {
                                        // you access the elements
                                        var mp=i;
                                        if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value === 'undefined'){
                                            break;
                                        }else{
                                            if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value!="+"){
                                                //var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
                                                var inputtrim=html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value;
                                                inputtrim=inputtrim.replace("+", "");
                                                $('select#extrim').append($('<option>', {value:inputtrim, text:inputtrim}));
                                                if(mp==0){
                                                    $("#inputTrim").val(inputtrim);
                                                }
                                            }
                                        }

                                    }
                                    $('select#extrim').append($('<option>', {value:"Unknown", text:"Unknown"}));
                                }


                                var engine=html.VIN.Vehicle[0].Item[6]["@attributes"].Value;

                                $('#inputYear').val(year);

                                $('#inputMake').val(make);

                                $('#inputModel').val(model);

                                $('#inputTrim').val(trim);

                                $('#inputStyle').val(style);

                                //$('#inputTransmission').val(transmission);

                                //$('#inputDrivetrain').val(drivetrain);

                                $('#inputEngine').val(engine);

                                $('#VIN_FORM').hide();

                                $('#vehicle_form').show();

                                var d = new Date();

                                var curr_date = d.getDate();

                                var curr_month = d.getMonth()+1;

                                var curr_year = d.getFullYear();

                                var ddate=curr_year + "-" + curr_month + "-" + curr_date;

                                $('#inputDate').val(ddate);

                            }
						

                        }else{
							//if VINQUERY Failed
			$.ajax({

            url: "assets/ajax/vin_audit.php",
            type: "post",
            data:$("#VIN_FORM" ).serialize(),
            dataType: 'json',
            cache:false,
            success: function(html){	
				if(html.success==1){
           // var valid=html.vin;
            var vinin=$('#inputVINNumber').val().toUpperCase();

            var ISN=$('#inputStockNumber').val();

            var IVN=$('#inputVINNumber').val();

            var InputPrice=$('#InputPrice').val();

            $('#inputStockNumber2').val(ISN);

            $('#inputVINNumber2').val(IVN);

            $('#inputPrice2').val(InputPrice);
            if(typeof html.attributes.Year!=="undefined"){
                    var year=html.attributes.Year;
                    $('#inputYear').val(year);
            }
            if(typeof html.attributes.Make!=="undefined"){
                    var make=html.attributes.Make;
                    $('#inputMake').val(make);
            }
            if(typeof html.attributes.Model!=="undefined"){

                    var model=html.attributes.Model;
                    $('#inputModel').val(model);
            }
            if(typeof html.attributes.transmissionType!=="undefined"){

                    var transmission=html.attributes.transmissionType;
                    $('#inputTransmission').val(transmission);
            }
            if(typeof html.attributes.DrivenWheels!=="undefined"){

                var drivetrain=html.attributes.DrivenWheels;
                $('#inputDrivetrain').val(drivetrain);
            }
            if(typeof html.attributes.VehicleStyle!=="undefined"){

                    var style=html.attributes.VehicleStyle;
                    $('#inputStyle').val(style);
            }
			if(typeof html.attributes.Trim!=="undefined"){

                    var trim=html.attributes.Trim;
                    $('#inputTrim').val(trim);
            }
            if(typeof html.attributes.EngineSize!=="undefined"){

                    var engine=html.attributes.EngineSize;
                    $('#inputEngine').val(engine);
            }

            if(typeof html.attributes.HighwayMileage!=="undefined"){
                    var mpg_highway=html.attributes.HighwayMileage;
                    $('#inputMPGHigh').val(mpg_highway);
            }
            if(typeof  html.attributes.CityMileage!=="undefined"){
                    var mpg_city=html.attributes.CityMileage;
                    $('#inputMPGCity').val(mpg_city);
            }
            if(typeof html.attributes.Engine!=="undefined"){
                    var engine_type=html.attributes.Engine;
                    $('#inputEnginetype').val(engine_type);
            }
            if(typeof html.attributes.Doors!=="undefined"){
                var doors=html.attributes.Doors;
                $('#inputDoors').val(doors);
            }

            $('#VIN_FORM').hide();

            $('#vehicle_form').show();

            var d = new Date();

            var curr_date = d.getDate();

            var curr_month = d.getMonth()+1;

            var curr_year = d.getFullYear();

            var ddate=curr_year + "-" + curr_month + "-" + curr_date;

            $('#inputDate').val(ddate);
			}else{
				// JavaScript Document
                        swal({
                                title: "VIN not recognized",
                                text: "Uh Oh! Looks like that VIN is not yet in the decoder database1",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Add this VIN",
                                closeOnConfirm: false
                            },
                            function(){
                                swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success");
                                var values2="vin="+$('#inputVINNumber').val();
                                $.ajax({
                                    url: "assets/ajax/not-found.php",

                                    type: "get",

                                    data: values2,

                                    cache:false,

                                    success: function(html){
                                    }
                                });
                                var ISN=$('#inputStockNumber').val();

                                var IVN=$('#inputNumber').val();

                                var InputPrice=$('#InputPrice').val();

                                $('#inputStockNumber2').val(ISN);

                                var inputVINNumber=$('#inputVINNumber').val();

                                $('#inputVINNumber2').val(inputVINNumber);

                                $('#inputNumber2').val(IVN);

                                $('#inputPrice2').val(InputPrice);

                                $('#VIN_FORM').hide();

                                $('#vehicle_form').show();
                            });

                    
			}
			}
			});
						}
			}
			
						
				}

        });

    },

    success: function(html) {

        if(html.years === undefined || html.years === null){
			//if EDMUNDS failed
			
		//Call vin audit if EDMUNDS failed to fetch
        $.ajax({

            url: "assets/ajax/vin_query.php",
            type: "post",
            data:$("#VIN_FORM" ).serialize(),
            dataType: 'json',
            cache:false,
            success: function(html){
				if(html.message === 'invalid'){

							swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});

						}else{
				var valid=html.VIN["@attributes"].Status;
//if VINQUERY succeed
                        if(valid=="SUCCESS"){

                            if(typeof html.VIN.Vehicle[0] === 'undefined'){

                                var ISN=$('#inputStockNumber').val();

                                var IVN=$('#inputVINNumber').val();

                                var InputPrice=$('#InputPrice').val();

                                $('#inputStockNumber2').val(ISN);

                                $('#inputVINNumber2').val(IVN);

                                $('#inputPrice2').val(InputPrice);

                                var year=html.VIN.Vehicle["@attributes"].Model_Year;

                                var make=html.VIN.Vehicle["@attributes"].Make;

                                var model=html.VIN.Vehicle["@attributes"].Model;

                                //var transmission=html.VIN.Vehicle["@attributes"].Transmission;

                                //var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

                                var style=html.VIN.Vehicle.Item[5]["@attributes"].Value;

                                var trim=html.VIN.Vehicle.Item[3]["@attributes"].Value;

                                var engine=html.VIN.Vehicle.Item[6]["@attributes"].Value;

                                $('#inputYear').val(year);

                                $('#inputMake').val(make);

                                $('#inputModel').val(model);

                                $('#inputTrim').val(trim);

                                $('#inputStyle').val(style);

                                //$('#inputTransmission').val(transmission);

                                //$('#inputDrivetrain').val(drivetrain);

                                $('#inputEngine').val(engine);

                                $('#VIN_FORM').hide();

                                $('#vehicle_form').show();

                                var d = new Date();

                                var curr_date = d.getDate();


                                var curr_month = d.getMonth()+1;

                                var curr_year = d.getFullYear();

                                var ddate=curr_year + "-" + curr_month + "-" + curr_date;

                                $('#inputDate').val(ddate);

                            }else{

                                var ISN=$('#inputStockNumber').val();

                                var IVN=$('#inputVINNumber').val();

                                var InputPrice=$('#InputPrice').val();

                                $('#inputStockNumber2').val(ISN);

                                $('#inputVINNumber2').val(IVN);

                                $('#inputPrice2').val(InputPrice);

                                var year=html.VIN.Vehicle[0]["@attributes"].Model_Year;

                                var make=html.VIN.Vehicle[0]["@attributes"].Make;

                                var model=html.VIN.Vehicle[0]["@attributes"].Model;

                                //var transmission=html.VIN.Vehicle["@attributes"].Transmission;

                                //var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

                                var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;

                                var trim=html.VIN.Vehicle[0].Item[3]["@attributes"].Value;

                                $('#inputTrim').val(trim);
                                $('#exttrim').hide();
                                if(typeof html.VIN.Vehicle[1]!=="undefined"){
                                    $('#inputTrim').val("");
                                    $('#inputTrim').attr("placeholder", "Choose Trim");
                                    $('select#extrim').append($('<option>', {value:"", text:"Choose Other"}));
                                    $('#extrim').show();
                                    $('#extrim').html("");
                                    $('#extrim').val("");
                                    $('#extrim').attr("placeholder", "Choose Color");
                                    //var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
                                    var firstArray=html.VIN.Vehicle;
                                    for(var i = 0, len = html.VIN.Vehicle.length; i < len; i += 1) {
                                        // you access the elements
                                        var mp=i;
                                        if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value === 'undefined'){
                                            break;
                                        }else{
                                            if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value!="+"){
                                                //var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
                                                var inputtrim=html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value;
                                                inputtrim=inputtrim.replace("+", "");
                                                $('select#extrim').append($('<option>', {value:inputtrim, text:inputtrim}));
                                                if(mp==0){
                                                    $("#inputTrim").val(inputtrim);
                                                }
                                            }
                                        }

                                    }
                                    $('select#extrim').append($('<option>', {value:"Unknown", text:"Unknown"}));
                                }


                                var engine=html.VIN.Vehicle[0].Item[6]["@attributes"].Value;

                                $('#inputYear').val(year);

                                $('#inputMake').val(make);

                                $('#inputModel').val(model);

                                $('#inputTrim').val(trim);

                                $('#inputStyle').val(style);

                                //$('#inputTransmission').val(transmission);

                                //$('#inputDrivetrain').val(drivetrain);

                                $('#inputEngine').val(engine);

                                $('#VIN_FORM').hide();

                                $('#vehicle_form').show();

                                var d = new Date();

                                var curr_date = d.getDate();

                                var curr_month = d.getMonth()+1;

                                var curr_year = d.getFullYear();

                                var ddate=curr_year + "-" + curr_month + "-" + curr_date;

                                $('#inputDate').val(ddate);

                            }

                        }else{
							//if VINQUERY Failed
			$.ajax({

            url: "assets/ajax/vin_audit.php",
            type: "post",
            data:$("#VIN_FORM" ).serialize(),
            dataType: 'json',
            cache:false,
            success: function(html){	
				if(html.success==1){
           // var valid=html.vin;
            var vinin=$('#inputVINNumber').val().toUpperCase();

            var ISN=$('#inputStockNumber').val();

            var IVN=$('#inputVINNumber').val();

            var InputPrice=$('#InputPrice').val();

            $('#inputStockNumber2').val(ISN);

            $('#inputVINNumber2').val(IVN);

            $('#inputPrice2').val(InputPrice);
            if(typeof html.attributes.Year!=="undefined"){
                    var year=html.attributes.Year;
                    $('#inputYear').val(year);
            }
            if(typeof html.attributes.Make!=="undefined"){
                    var make=html.attributes.Make;
                    $('#inputMake').val(make);
            }
            if(typeof html.attributes.Model!=="undefined"){

                    var model=html.attributes.Model;
                    $('#inputModel').val(model);
            }
            if(typeof html.attributes.transmissionType!=="undefined"){

                    var transmission=html.attributes.transmissionType;
                    $('#inputTransmission').val(transmission);
            }
            if(typeof html.attributes.DrivenWheels!=="undefined"){

                var drivetrain=html.attributes.DrivenWheels;
                $('#inputDrivetrain').val(drivetrain);
            }
            if(typeof html.attributes.VehicleStyle!=="undefined"){

                    var style=html.attributes.VehicleStyle;
                    $('#inputStyle').val(style);
            }
			if(typeof html.attributes.Trim!=="undefined"){

                    var trim=html.attributes.Trim;
                    $('#inputTrim').val(trim);
            }
            if(typeof html.attributes.EngineSize!=="undefined"){

                    var engine=html.attributes.EngineSize;
                    $('#inputEngine').val(engine);
            }

            if(typeof html.attributes.HighwayMileage!=="undefined"){
                    var mpg_highway=html.attributes.HighwayMileage;
                    $('#inputMPGHigh').val(mpg_highway);
            }
            if(typeof  html.attributes.CityMileage!=="undefined"){
                    var mpg_city=html.attributes.CityMileage;
                    $('#inputMPGCity').val(mpg_city);
            }
            if(typeof html.attributes.Engine!=="undefined"){
                    var engine_type=html.attributes.Engine;
                    $('#inputEnginetype').val(engine_type);
            }
            if(typeof html.attributes.Doors!=="undefined"){
                var doors=html.attributes.Doors;
                $('#inputDoors').val(doors);
            }

            $('#VIN_FORM').hide();

            $('#vehicle_form').show();

            var d = new Date();

            var curr_date = d.getDate();

            var curr_month = d.getMonth()+1;

            var curr_year = d.getFullYear();

            var ddate=curr_year + "-" + curr_month + "-" + curr_date;

            $('#inputDate').val(ddate);
			}else{
				// JavaScript Document


                        swal({
                                title: "VIN not recognized",
                                text: "Uh Oh! Looks like that VIN is not yet in the decoder database1",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Add this VIN",
                                closeOnConfirm: false
                            },
                            function(){
                                swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success");
                                var values2="vin="+$('#inputVINNumber').val();
                                $.ajax({
                                    url: "assets/ajax/not-found.php",

                                    type: "get",

                                    data: values2,

                                    cache:false,

                                    success: function(html){
                                    }
                                });
                                var ISN=$('#inputStockNumber').val();

                                var IVN=$('#inputNumber').val();

                                var InputPrice=$('#InputPrice').val();

                                $('#inputStockNumber2').val(ISN);

                                var inputVINNumber=$('#inputVINNumber').val();

                                $('#inputVINNumber2').val(inputVINNumber);

                                $('#inputNumber2').val(IVN);

                                $('#inputPrice2').val(InputPrice);

                                $('#VIN_FORM').hide();

                                $('#vehicle_form').show();
                            });

                    
			}
			
			}
			});
						}
			
						}
				}

        });

    
			}else{
				//if EDMUNDS succeed
            var valid=html.vin;
            var vinin=$('#inputVINNumber').val().toUpperCase();

            var ISN=$('#inputStockNumber').val();

            var IVN=$('#inputVINNumber').val();

            var InputPrice=$('#InputPrice').val();

            $('#inputStockNumber2').val(ISN);

            $('#inputVINNumber2').val(IVN);

            $('#inputPrice2').val(InputPrice);
            if(typeof html.years[0]!=="undefined"){
                if(typeof html.years[0].year!=="undefined"){

                    var year=html.years[0].year;
                    $('#inputYear').val(year);
                }
            }
            if(typeof html.make!=="undefined"){
                if(typeof html.make.name!=="undefined"){

                    var make=html.make.name;
                    $('#inputMake').val(make);
                }
            }
            if(typeof html.model!=="undefined"){
                if(typeof html.model.name!=="undefined"){

                    var model=html.model.name;
                    $('#inputModel').val(model);
                }
            }
            if(typeof html.transmission!=="undefined"){
                if(typeof html.transmission.transmissionType!=="undefined"){

                    var transmission=html.transmission.transmissionType;
                    $('#inputTransmission').val(transmission);
                }
            }
            if(typeof html.drivenWheels!=="undefined"){

                var drivetrain=html.drivenWheels;
                $('#inputDrivetrain').val(drivetrain);
            }
            if(typeof html.categories!=="undefined"){
                if(typeof html.categories.vehicleStyle!=="undefined"){

                    var style=html.categories.vehicleStyle;
                    $('#inputStyle').val(style);
                }
            }
            if(typeof html.years[0]!=="undefined"){
                if(typeof html.years[0].styles!=="undefined"){
                    if(typeof html.years[0].styles[0]!=="undefined"){
                        if(typeof html.years[0].styles[0].trim!=="undefined"){

                            var trim=html.years[0].styles[0].trim;
                            $('#inputTrim').val(trim);
                            $('#exttrim').hide();
                            if(typeof html.years[0].styles[1]!=="undefined"){
                                $('#inputTrim').val("");
                                $('#inputTrim').attr("placeholder", "Choose Trim");
                                $('select#extrim').append($('<option>', {value:"", text:"Choose Other"}));
                                $('#extrim').show();
                                $('#extrim').html("");
                                $('#extrim').val("");
                                $('#extrim').attr("placeholder", "Choose Color");
                                //var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
                                var firstArray=html.years[0].styles;
                                for(var i = 0, len = html.years[0].styles.length; i < len; i += 1) {
                                    // you access the elements
                                    var mp=i;
                                    if(html.years[0].styles[eval(mp)].trim === 'undefined'){
                                        break;
                                    }else{
                                        if(html.years[0].styles[eval(mp)].trim!="+"){
                                            //var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
                                            var inputtrim=html.years[0].styles[eval(mp)].trim;
                                            inputtrim=inputtrim.replace("+", "");
                                            $('select#extrim').append($('<option>', {value:inputtrim, text:inputtrim}));
                                            if(mp==0){
                                                $("#inputTrim").val(inputtrim);
                                            }
                                        }
                                    }

                                }
                                $('select#extrim').append($('<option>', {value:"Unknown", text:"Unknown"}));
                            }
                        }
                    }
                }
            }
            if(typeof html.engine!=="undefined"){
                if(typeof html.engine.size!=="undefined"){

                    var engine=html.engine.size;
                    $('#inputEngine').val(engine);
                }
            }
            if(typeof html.colors!=="undefined"){
                if(typeof html.colors[0]!=="undefined"){
                    if(typeof html.colors[0].options[0]!=="undefined"){
                        if(typeof html.colors[0].options[0].name!=="undefined" && html.colors[0].category=="Interior"){
                            var int_color=html.colors[0].options[0].name;
                            $('#inputColor2').val(int_color);
                            $('#extcolor2').hide();
                            if(typeof html.colors[0].options[1]!=="undefined"){
                                $('#inputColor2').attr("placeholder", "Choose Color");
                                $('#inputColor2').val("");
                                $('select#extcolor2').append($('<option>', {value:"", text:"Choose Other"}));
                                $('#extcolor2').show();
                                $('#extcolor2').html("");
                                $('#extcolor2').val("");
                                $('#extcolor2').attr("placeholder", "Choose Color");
                                //var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
                                var firstArray=html.colors[0].options;
                                for(var i = 0, len = html.colors[0].options.length; i < len; i += 1) {
                                    // you access the elements
                                    var mp=i;
                                    if(html.colors[0].options[eval(mp)] === 'undefined'){
                                        break;
                                    }else{
                                        if(html.colors[0].options[eval(mp)].name!="+"){
                                            //var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
                                            var inputcolor=html.colors[0].options[eval(mp)].name;
                                            inputcolor=inputcolor.replace("+", "");
                                            $('select#extcolor2').append($('<option>', {value:inputcolor, text:inputcolor}));
                                            if(mp==0){
                                                $("#inputColor2").val(inputcolor);
                                            }
                                        }
                                    }
                                }
                                $('select#extcolor2').append($('<option>', {value:"Unknown", text:"Unknown"}));
                            }

                        }else if(typeof html.colors[0].options[0].name!=="undefined" && html.colors[0].category=="Exterior"){
                            var ext_color=html.colors[0].options[0].name;
                            $('#inputColor').val(ext_color);
                            $('#extcolor').hide();
                            if(typeof html.colors[0].options[1]!=="undefined"){
                                $('#inputColor').attr("placeholder", "Choose Color");
                                $('#inputColor').val("");
                                $('select#extcolor').append($('<option>', {value:"", text:"Choose Other"}));
                                $('#extcolor').show();
                                $('#extcolor').html("");
                                $('#extcolor').val("");
                                $('#extcolor').attr("placeholder", "Choose Color");
                                //var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
                                var firstArray=html.colors[0].options;
                                for(var i = 0, len = html.colors[0].options.length; i < len; i += 1) {
                                    // you access the elements
                                    var mp=i;
                                    if(html.colors[0].options[eval(mp)] === 'undefined'){
                                        break;
                                    }else{
                                        if(html.colors[0].options[eval(mp)].name!="+"){
                                            //var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
                                            var inputcolor=html.colors[0].options[eval(mp)].name;
                                            inputcolor=inputcolor.replace("+", "");
                                            $('select#extcolor').append($('<option>', {value:inputcolor, text:inputcolor}));
                                            if(mp==0){
                                                $("#inputColor").val(inputcolor);
                                            }
                                        }
                                    }
                                }
                                $('select#extcolor').append($('<option>', {value:"Unknown", text:"Unknown"}));
                            }

                        }
                    }
                }
            }

            if(typeof html.colors!=="undefined"){
                if(typeof html.colors[1]!=="undefined"){
                    if(typeof html.colors[1].options[0]!=="undefined"){
                        if(typeof html.colors[1].options[0].name!=="undefined" && html.colors[1].category=="Interior"){
                            var int_color=html.colors[1].options[0].name;
                            $('#inputColor2').val(int_color);
                            $('#extcolor2').hide();
                            if(typeof html.colors[1].options[1]!=="undefined"){
                                $('#inputColor2').attr("placeholder", "Choose Color");
                                $('#inputColor2').val("");
                                $('select#extcolor2').append($('<option>', {value:"", text:"Choose Other"}));
                                $('#extcolor2').show();
                                $('#extcolor2').html("");
                                $('#extcolor2').val("");
                                $('#extcolor2').attr("placeholder", "Choose Color");
                                //var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
                                var firstArray=html.colors[1].options;
                                for(var i = 0, len = html.colors[1].options.length; i < len; i += 1) {
                                    // you access the elements
                                    var mp=i;
                                    if(html.colors[1].options[eval(mp)] === 'undefined'){
                                        break;
                                    }else{
                                        if(html.colors[1].options[eval(mp)].name!="+"){
                                            //var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
                                            var inputcolor=html.colors[1].options[eval(mp)].name;
                                            inputcolor=inputcolor.replace("+", "");
                                            $('select#extcolor2').append($('<option>', {value:inputcolor, text:inputcolor}));
                                            if(mp==0){
                                                $("#inputColor2").val(inputcolor);
                                            }
                                        }
                                    }
                                }
                                $('select#extcolor2').append($('<option>', {value:"Unknown", text:"Unknown"}));
                            }

                        }else if(typeof html.colors[1].options[0].name!=="undefined" && html.colors[1].category=="Exterior"){
                            var ext_color=html.colors[1].options[0].name;
                            $('#inputColor').val(ext_color);
                            $('#extcolor').hide();
                            if(typeof html.colors[1].options[1]!=="undefined"){
                                $('#inputColor').attr("placeholder", "Choose Color");
                                $('#inputColor').val("");
                                $('select#extcolor').append($('<option>', {value:"", text:"Choose Other"}));
                                $('#extcolor').show();
                                $('#extcolor').html("");
                                $('#extcolor').val("");
                                $('#extcolor').attr("placeholder", "Choose Color");
                                //var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
                                var firstArray=html.colors[1].options;
                                for(var i = 0, len = html.colors[1].options.length; i < len; i += 1) {
                                    // you access the elements
                                    var mp=i;
                                    if(html.colors[1].options[eval(mp)] === 'undefined'){
                                        break;
                                    }else{
                                        if(html.colors[1].options[eval(mp)].name!="+"){
                                            //var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
                                            var inputcolor=html.colors[1].options[eval(mp)].name;
                                            inputcolor=inputcolor.replace("+", "");
                                            $('select#extcolor').append($('<option>', {value:inputcolor, text:inputcolor}));
                                            if(mp==0){
                                                $("#inputColor").val(inputcolor);
                                            }
                                        }
                                    }
                                }
                                $('select#extcolor').append($('<option>', {value:"Unknown", text:"Unknown"}));
                            }

                        }
                    }
                }
            }

            if(typeof html.MPG!=="undefined"){
                if(typeof html.MPG.highway!=="undefined"){
                    var mpg_highway=html.MPG.highway;
                    $('#inputMPGHigh').val(mpg_highway);
                }
            }
            if(typeof html.MPG!=="undefined"){
                if(typeof html.MPG.city!=="undefined"){
                    var mpg_city=html.MPG.city;
                    $('#inputMPGCity').val(mpg_city);
                }
            }
            if(typeof html.engine!=="undefined"){
                if(typeof html.engine.type!=="undefined"){
                    var engine_type=html.engine.type;
                    $('#inputEnginetype').val(engine_type);
                }
            }
            if(typeof html.numOfDoors!=="undefined"){
                var doors=html.numOfDoors;
                $('#inputDoors').val(doors);
            }

            $('#VIN_FORM').hide();

            $('#vehicle_form').show();

            var d = new Date();

            var curr_date = d.getDate();

            var curr_month = d.getMonth()+1;

            var curr_year = d.getFullYear();

            var ddate=curr_year + "-" + curr_month + "-" + curr_date;

            $('#inputDate').val(ddate);
			}

    }

});
});