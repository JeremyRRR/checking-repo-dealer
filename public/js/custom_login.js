// prepare the form when the DOM is ready 
$(document).ready(function() {
  $('.palert').hide();
    // bind form using ajaxForm  
      $('#loginform').ajaxForm({ 
        // target identifies the element(s) to update with the server response 
       
 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) {
        if(responseText=="no"){
            $('#lmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Invalid login details.</b></div>'); 
    }else if(responseText=="yes"){
             window.location="home.php";
    }else if(responseText=="inactive"){
           $('#lmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Dealer inactive. Please contact Customer Support at 801-415-9435.</b></div>'); 
    }else if(responseText=="suspended"){
           $('#lmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Your account has been suspended for a past due invoice. Please pay invoice or contact Customer Support at 801-415-9435</b></div>'); 
    }
    $('#loginform').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");
    }
    }); 
  
        $('#forgot_password').ajaxForm({ 
        // target identifies the element(s) to update with the server response 
       
 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) { 
    if(responseText=="no"){
            $('#smessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Email not found.</b></div>');
    }else if(responseText=="yes"){
        $("#myModal").modal("hide");
           swal("Password sent!", "Please check your email.", "success");
    }
    $('#forgot_password').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");
    }
    }); 
  $('canvas').css('width','100vw');
  $('canvas').css('height','100vh');
  $('canvas').css('position','absolute');
  $('canvas').css('top','0');
  $('canvas').css('left','0');
  $('canvas').css('z-index','1');
  $('div#app').css('width', '100vw');
  $('div#app').css('height', '100vh');
  $('div#app').css('z-index', '9');
  $('div#app').css('position', 'absolute');
});

$(function(){
    $('#watchvideo').click(function(){
        window.location='../../tutorial/'
    });
});
$(function(){
    $('#watchvideo2').click(function(){
        window.location='../../BuyersGuides/'
    });
});
$(function(){
    $('#sendmail').click(function(){
        window.location='mailto:'+$("#referEmail").val()+'?subject=Somebody wants to refer a great new service to you.&cc=referral@dealeraddendums.com&body=Hello, %0AI am using this great new program to create and manage my new Vehicle Addendums. You should give it a try, I think it will save you time and money, it does for me!%0AYou can sign up for free and they will even send you 50 free addendum labels just for giving the service a test drive.%0A%0AThe name of the service is DealerAddendums.com and here is the sign up link https://dealeraddendums.com/signup.php%0A%0A%0A%0AAfter you give it a try, let me know what you think.'
    });
});
function isIE () {
  var myNav = navigator.userAgent.toLowerCase();
  return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
if (isIE () && isIE () < 9) {
 swal("This service requires a newer version of Internet Explorer.", "It looks like you are using Internet Explorer 7, 8 or 9. In order to use all this sites functionality you will need to update your browser, or use Chrome or Firefox.", "success");
}