// -------------------------------------------------------------------

//Custom JS/jQuery

// -------------------------------------------------------------------
var VehicleTable;
var GetPrintTable;
var OptionTable;
var UserTable;
$(document).ready(function() {
$('.vehicle_multiple').addClass("disabled");
$('.vehicle_multiple').attr("disabled","disabled");
$('textarea').attr('autocomplete','off');
$('body').on('hidden', '.modal', function () {
  $(this).removeData('modal');
});

$('#addendum_print_modal').on('hidden.bs.modal', function() {

    $(this).removeData('modal');

	$('#info_print_modal').removeData('modal');

});
$('input[type=radio][name=addresschange]').change(function() {
        if (this.value == '2') {
            $("#newaddress").show();
        }
        else {
            $("#newaddress").hide();
        }
    });
	$('#guide_stock').change(function() {
        if (this.value == '0') {
            $(".guide_extra").hide();
        }
        else {
            $(".guide_extra").show();
        }
    });
$('.navbar-nav').on('shown.bs.tab', 'a', function (e) {
    if (e.relatedTarget) {
        $(e.relatedTarget).removeClass('active');
    }
})
$('#info_print_modal').on('hidden.bs.modal', function() {

    $(this).removeData('modal');

	$('#addendum_print_modal').removeData('modal');

});
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
  if(target=="#asis2"){
	  $("#dg2").val("asis");
	  $(".war_check2").show();
  }else if(target=="#implied2"){
	  $("#dg2").val("implied");
	  $(".war_check2").show();
  }else if(target=="#warranty2"){
	  $("#dg2").val("warranty");
	  $(".war_check2").show();
  }else if(target=="#warranty2_s"){
	  $("#dg2").val("warranty_s");
	  $(".war_check2").show();
  }else if(target=="#back_page_s"){
	  $(".war_check2").hide();
  }else if(target=="#asis"){
	  $(".war_check").show();
  }else if(target=="#implied"){
	  $(".war_check").show();
  }else if(target=="#warranty"){
	  $(".war_check").show();
  }else if(target=="#warranty_spanish"){
	  $(".war_check").show();
  }else if(target=="#back_page"){
	  $(".war_check").hide();
  }
});
	$('.datepicker').datetimepicker({
		format: 'YYYY-MM-DD',
		dayViewHeaderFormat:"YYYY-MM-DD"
            });
var screenWidth=$( document ).width();
if(screenWidth<=1024){
	VehicleTable=$('#VehicleTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/vehicle_table.php",
		"dom": '<"top">rt<"bottom"lip><"clear">',
		"pagingType": "simple",
		"search": {
    "regex": true
  },
		"order": [[ 8, "desc" ]],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return '<label class="ui-check m-a-0"><input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+row[0]+'><i class="dark-white"></i></label>';
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-info btn-xs" onClick="edtvehicle('+ row[0]+')">E</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='';
             }
                    return edtprint;
                },
				"className": "dt-head-right dt-body-right",
                "targets": -2,
                "sorting": false,
				"searchable": false
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[17]==0 || row[17]=="" || row[17]===null){
                    	return "0";
					 }else{
						return "$"+row[17];
					 }
                },
				"className": "dt-head-right dt-body-right",
                "targets": -3,
                "sorting": false,
				"searchable": false
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
					var medata='<button type="button" class="btn btn-success btn-xs ">Infosheet</button> <button type="button" class="btn btn-default btn-xs">Buyers Guide</button><button type="button" class="btn btn-default btn-xs">A</button> ';
					
					
						 if(row[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default btn-xs no-mobile" title="Buyer Guide"  onClick="printguide('+row[12]+')">B</button> ';

						 }else if(row[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success btn-xs no-mobile" title="Buyer Guide"  onClick="printguide('+row[12]+')">B</button> ';

						 }
					 if(row[11]=='Used'){



						 if(row[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default btn-xs no-mobile" title="Info Sheet"  onClick="printinfosheet('+row[12]+')">I</button> ';

						 }else if(row[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success btn-xs no-mobile" title="Info Sheet"  onClick="printinfosheet('+row[12]+')">I</button> ';

						 }

					 }else{


						 var printed3='<button disabled type="button" class="btn btn-fake btn-xs no-mobile">I</button> ';

					 }

					 if(row[10]=='1'){

						 var printed1='<button type="button" class="btn btn-xs print_vehicle btn-success"  title="Addendum" onClick="printvehicle('+row[12]+')">A</button> ';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle btn-xs btn-default"  title="Addendum" onClick="printvehicle('+row[12]+')">A</button> ';

					 }
					 if($("#inputSMS1").val()!="" && $("#inputSMS").val()!=""){
					 
					  if(row[16]=='1'){

						 var printedsms='<button type="button" class="btn btn-xs print_sms btn-success no-mobile"  title="SMS" onClick="printsms('+row[12]+')">S</button> ';

					 }else{

						 var printedsms='<button type="button" class="btn print_sms btn-xs btn-default no-mobile"  title="SMS" onClick="printsms('+row[12]+')">S</button> ';

					 }
					 }else{
						 if(row[16]=='1'){

						 var printedsms='<button disabled type="button" class="btn btn-xs print_sms btn-success no-mobile"  title="SMS" onClick="printsms('+row[12]+')">S</button> ';

					 }else{

						 var printedsms='<button disabled type="button" class="btn print_sms btn-xs btn-default no-mobile"  title="SMS" onClick="printsms('+row[12]+')">S</button> ';

					 }
					 }

                 return printedsms+printed2+printed3+printed1 
					
                },
                "targets": -1,
                "sorting": false,
				"searchable": false
            },
		
		{ "visible": true,  "targets": [ 1 ] },
		{ "visible": true,  "targets": [ 2 ] },
		{ "visible": true,  "targets": [ 3 ] },
		{ "visible": true,  "targets": [ 4 ] },
		{ "visible": true,  "targets": [ 5 ] },
		{ "visible": true,"searchable": false, "targets": [ 6 ] },
		{ "visible": true,"searchable": false, "targets": [ 7 ] },
		{ "visible": true,"searchable": false, "targets": [ 8 ] },
		{ "visible": true,"searchable": false, "targets": [ 9 ] }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'v'+aData[0]);
		if(aData[17] === undefined || aData[17] == null || aData[17].length <= 0 || aData[17]=="0" || aData[17]=="0.00" || aData[17]=="0.0" || aData[17]=="" || $.isNumeric( aData[17] )==false){
		$(nRow).attr('style', 'color:red');
		}
    }
		
    } );

	
	$('.vehiclesearchm').keyup(function(){
		   var key = $(this).val();
 key=key.replace(",", "|"); 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";

    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	VehicleTable.search(key).draw();
	})
	$('.vehiclesearch').keyup(function(){
   var key = $(this).val();
 key=key.replace(/,/g, "|");
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";

    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	if($("input[name=stock_vin]:checked").val()=="stock"){
	VehicleTable.columns([1]).search(regExp, true, false).draw();
	}else if($("input[name=stock_vin]:checked").val()=="vin"){
	VehicleTable.columns([2]).search(regExp, true, false).draw();
	}
	
	
})

$("#stock_vin3").click(function(){
   var key = $(".vehiclesearchm").val();
 key=key.replace(",", "|");
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";
    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	if($("input[name=stock_vinm]:checked").val()=="stock"){
	VehicleTable.column(2).search("", false, false).draw();
	VehicleTable.column(1).search(regExp, true, false).draw();
	}
});
$("#stock_vin4").click(function(){
   var key = $(".vehiclesearchm").val();
 key=key.replace(",", "|"); 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";
    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	if($("input[name=stock_vinm]:checked").val()=="vin"){
	VehicleTable.column(1).search("", false, false).draw();
	VehicleTable.column(2).search(regExp, true, false).draw();
	}
});

}else{
	
	VehicleTable=$('#VehicleTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/vehicle_table.php",
		"dom": '<"top">rt<"bottom"lip><"clear">',
		"cache": false,
		"order": [[ 8, "desc" ]],
		"search": {
    "regex": true
  },
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return '<label class="ui-check m-a-0"><input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+row[0]+'><i class="dark-white"></i></label>';
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
				"searchable": false
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-info btn-xs" onClick="edtvehicle('+ row[0]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='';
             }
                    return edtprint;
                },
				"className": "dt-head-right dt-body-right",
                "targets": -2,
                "sorting": false,
				"searchable": false
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[17]==0 || row[17]=="" || row[17]===null){
                    	return "0";
					 }else{
						return "$"+row[17];
					 }
                },
				"className": "dt-head-right dt-body-right",
                "targets": -3,
                "sorting": false,
				"searchable": false
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
					var medata='<button type="button" class="btn btn-success btn-xs ">Infosheet</button> <button type="button" class="btn btn-default btn-xs">Buyers Guide</button><button type="button" class="btn btn-default btn-xs">Addendum</button> ';
					
					
						 if(row[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default btn-xs no-mobile"  onClick="printguide('+row[12]+')">Buyers Guide</button> ';

						 }else if(row[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success btn-xs no-mobile"  onClick="printguide('+row[12]+')">Buyers Guide</button> ';

						 }
					 if(row[11]=='Used'){



						 if(row[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default btn-xs no-mobile"  onClick="printinfosheet('+row[12]+')">Info Sheet</button> ';

						 }else if(row[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success btn-xs no-mobile"  onClick="printinfosheet('+row[12]+')">Info Sheet</button> ';

						 }

					 }else{


						 var printed3='<button disabled type="button" class="btn btn-fake btn-xs no-mobile">Info Sheet</button> ';

					 }

					 if(row[10]=='1'){

						 var printed1='<button type="button" class="btn btn-xs print_vehicle btn-success"  onClick="printvehicle('+row[12]+')">Addendum</button> ';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle btn-xs btn-default" onClick="printvehicle('+row[12]+')">Addendum</button> ';

					 }
					 if($("#inputSMS1").val()!="" && $("#inputSMS").val()!=""){
					  if(row[16]=='1'){

						 var printedsms='<button type="button" class="btn btn-xs print_sms btn-success no-mobile" onClick="printsms('+row[12]+')">SMS</button> ';

					 }else{

						 var printedsms='<button type="button" class="btn print_sms btn-xs btn-default no-mobile" onClick="printsms('+row[12]+')">SMS</button> ';

					 }
					 }else{
						 if(row[16]=='1'){

						 var printedsms='<button disabled  title="Please contact DealerAddendums to enable this feature (801-415-9435)" type="button" class="btn btn-xs print_sms btn-success no-mobile" onClick="printsms('+row[12]+')">SMS</button> ';

					 }else{

						 var printedsms='<button disabled  title="Please contact DealerAddendums to enable this feature (801-415-9435)" type="button" class="btn print_sms btn-xs btn-default no-mobile" onClick="printsms('+row[12]+')">SMS</button> ';

					 }
					 }

                 return printedsms+printed2+printed3+printed1 
					
                },
				"className": "text-right",
                "targets": -1,
                "sorting": false,
				"searchable": false
            },{ "searchable": false, "targets": [ 6 ] },
		{ "searchable": false, "targets": [ 7 ] },
		{ "searchable": false, "targets": [ 8 ] },
		{ "searchable": false, "targets": [ 9 ] }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'v'+aData[0]);
		if(aData[17] === undefined || aData[17] == null || aData[17].length <= 0 || aData[17]=="0" || aData[17]=="0.00" || aData[17]=="0.0" || aData[17]=="" || $.isNumeric( aData[17] )==false){
		$(nRow).attr('style', 'color:red');
		}
    }
		
    } );

	$('.vehiclesearch2').keyup(function(){
	var key = $(this).val();
		if(key.indexOf(',') > -1)
{
 var matchvin = key.split(',')
 if(matchvin[0].length==17){
	  key=key.replace(",", "|"); 
    var regExp = "."
    if (key)
    regExp = "^\\s*" + key + "\\s*$";
	VehicleTable.column(1).search("", true, false).draw();
	VehicleTable.column(2).search(regExp, true, false).draw();
 }else{
	key=key.replace(",", "|"); 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";
	VehicleTable.column(2).search("", true, false).draw();
	VehicleTable.column(1).search(regExp, true, false).draw();
 }
}else{
		  key=key.replace(",", "|"); 
    var regExp = ""
    if (key)
       regExp =key;
	VehicleTable.search(regExp, false, false).draw();
}
})
	$('.vehiclesearch').keyup(function(){
   var key = $(this).val();
   if(key.length<9){
 key=key.replace(/,/g, "|") 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";

    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	VehicleTable.search(key).draw();
	$(".btnsearch").attr("style","display:none");
   }else{
	   $(".btnsearch").removeAttr("style");
   }
})

$(".btnsearch").click(function(){
   var key = $('.vehiclesearch').val();
      
 key=key.replace(/,/g, "|") 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";

    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	VehicleTable.search(key).draw();
});
$(".btnsearchm").click(function(){
   var key = $('.vehiclesearchm').val();
      
 key=key.replace(/,/g, "|") 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";

    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	VehicleTable.search(key).draw();
});
	$('.vehiclesearchm').keyup(function(){
   var key = $(this).val();
   if(key.length<9){
 key=key.replace(/,/g, "|") 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";

    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	VehicleTable.search(key).draw();
	$(".btnsearchm").attr("style","display:none");
   }else{
	   $(".btnsearchm").removeAttr("style");
   }
})

$("#stock_vin1").click(function(){
   var key = $(".vehiclesearch").val();
 key=key.replace(",", "|"); 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";
    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	if($("input[name=stock_vin]:checked").val()=="stock"){
	VehicleTable.column(2).search("", false, false).draw();
	VehicleTable.column(1).search(regExp, true, false).draw();
	}
});
$("#stock_vin2").click(function(){
   var key = $(".vehiclesearch").val();
 key=key.replace(",", "|"); 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";
    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	if($("input[name=stock_vin]:checked").val()=="vin"){
	VehicleTable.column(1).search("", false, false).draw();
	VehicleTable.column(2).search(regExp, true, false).draw();
	}
});

$("#stock_vin3").click(function(){
   var key = $(".vehiclesearch").val();
 key=key.replace(",", "|"); 
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";
    //VehicleTable.columns([1],[2],[3],[4],[5],[6]).search(regExp, true).draw();
	if($("input[name=stock_vin]:checked").val()=="common"){
	VehicleTable.column(1).search("", false, false).draw();
	VehicleTable.column(2).search("", false, false).draw();
	VehicleTable.search(key, false, false).draw();
	}
});

}
if(screenWidth<=1024){
	OptionTable=$('#OptionTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/option_table.php",
		"dom": '<"top">rt<"bottom"ip><"clear">',
		"aaSorting": [],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[1];
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[2];
                },
                "targets"  : 1,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[20];
                },
                "targets"  : 2,
                "sorting": false,
				"orderable": false,
            },{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					var model=row[15];
	 if(row[16]!=""){
		 model+=', '+row[16];
	 }
	 if(row[17]!=""){
		 model+=', '+row[17];
	 }
	 if(row[18]!=""){
		 model+=', '+row[18];
	 }
	 if(row[21]!=""){

		 model+='...';

	 }
                 return model;
                },
                "targets"  : 3,
                "sorting": false,
				"orderable": false,
            
				},{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 var style=row[4];
	 if(row[5]!=""){
		 style+=', '+row[5];
	 }
	 if(row[6]!=""){
		 style+=', '+row[6];
	 }
	 if(row[7]!=""){
		 style+=', '+row[7];
	 }
	 
                 return   style;
                },
                "targets"  : 4,
                "sorting": false,
				"orderable": false,
            
				},
		
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[19]<0){
						 var mydprice=Math.abs(row[19]);
						 return   "-$"+mydprice;
					 }else{
						  return   "$"+row[19];
					 }
                 },
                "targets"  : 5,
                "sorting": false,
				"orderable": false,
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
                 return   '<button type="button" class="btn btn-danger btn-xs no-mobile no-small" onClick="addendumdel('+row[14]+')">Delete</button> <button type="button" class="btn btn-info btn-xs" id="'+row[14]+'" onClick="addendumedit('+row[14]+')">Edit</button>'
					},
                "targets"  : 6,
                "sorting": false,
				"orderable": false,
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'ad'+aData[0]);
    }
		
    } );
}else{
	OptionTable=$('#OptionTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/option_table.php",
		"dom": '<"top">rt<"bottom"ip><"clear">',
		"aaSorting": [],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[1];
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[2];
                },
                "targets"  : 1,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[20];
                },
                "targets"  : 2,
                "sorting": false,
				"orderable": false,
            },{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					var model=row[15];
	 if(row[16]!=""){
		 model+=', '+row[16];
	 }
	 if(row[17]!=""){
		 model+=', '+row[17];
	 }
	 if(row[18]!=""){
		 model+=', '+row[18];
	 }
	 if(row[21]!=""){

		 model+='...';

	 }
                 return model;
                },
                "targets"  : 3,
                "sorting": false,
				"orderable": false,
            
				},{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 var style=row[4];
	 if(row[5]!=""){
		 style+=', '+row[5];
	 }
	 if(row[6]!=""){
		 style+=', '+row[6];
	 }
	 if(row[7]!=""){
		 style+=', '+row[7];
	 }
	 
                 return   style;
                },
                "targets"  : 4,
                "sorting": false,
				"orderable": false,
            
				},
		
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[19]<0){
						 var mydprice=Math.abs(row[19]);
						 return   "-$"+mydprice;
					 }else{
						  return   "$"+row[19];
					 }
                 },
                "targets"  : 5,
                "sorting": false,
				"orderable": false,
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
                 return   '<button type="button" class="btn btn-danger btn-xs no-mobile no-small" onClick="addendumdel('+row[14]+')">Delete</button> <button type="button" class="btn btn-info btn-xs" id="'+row[14]+'" onClick="addendumedit('+row[14]+')">Edit</button>' 
					},
                "targets"  : 6,
                "sorting": false,
				"orderable": false,
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'ad'+aData[0]);
    }
		
    } );
}


if(screenWidth<=1024){
	groupTable=$('#groupTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/option_group.php",
		"dom": '<"top">rt<"bottom"ip><"clear">',
		"order": [],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[1];
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[2];
				 var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
                },
                "targets"  : 1,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[2];
                },
                "targets"  : 2,
                "sorting": false,
				"orderable": false,
            },{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					var model=row[15];
	 if(row[16]!=""){
		 model+=', '+row[16];
	 }
	 if(row[17]!=""){
		 model+=', '+row[17];
	 }
	 if(row[18]!=""){
		 model+=', '+row[18];
	 }
	 if(row[21]!=""){

		 model+='...';

	 }
                 return model;
                },
                "targets"  : 3,
                "sorting": false,
				"orderable": false,
            
				},{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 var style=row[4];
	 if(row[5]!=""){
		 style+=', '+row[5];
	 }
	 if(row[6]!=""){
		 style+=', '+row[6];
	 }
	 if(row[7]!=""){
		 style+=', '+row[7];
	 }
	 
                 return   style;
                },
                "targets"  : 4,
                "sorting": false,
				"orderable": false,
            
				},
		
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[19]<0){
						 var mydprice=Math.abs(row[19]);
						 return   "-$"+mydprice;
					 }else{
						  return   "$"+row[19];
					 }
                 },
                "targets"  : 5,
                "sorting": false,
				"orderable": false,
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
                 return   '<button type="button" class="btn btn-danger btn-xs no-mobile no-small" onClick="addendumdel('+row[14]+')">Delete</button> <button type="button" class="btn btn-info btn-xs" id="'+row[14]+'" onClick="addendumedit('+row[14]+')">Edit</button>'
					},
                "targets"  : 6,
                "sorting": false,
				"orderable": false,
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'og'+aData[0]);
    }
		
    } );
}else{
	groupTable=$('#groupTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/option_group.php",
		"dom": '<"top">rt<"bottom"ip><"clear">',
		"order": [],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[1];
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
                },
                "targets"  : 1,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[20];
                },
                "targets"  : 2,
                "sorting": false,
				"orderable": false,
            },{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					var model=row[15];
	 if(row[16]!=""){
		 model+=', '+row[16];
	 }
	 if(row[17]!=""){
		 model+=', '+row[17];
	 }
	 if(row[18]!=""){
		 model+=', '+row[18];
	 }
	 if(row[21]!=""){

		 model+='...';

	 }
                 return model;
                },
                "targets"  : 3,
                "sorting": false,
				"orderable": false,
            
				},{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 var style=row[4];
	 if(row[5]!=""){
		 style+=', '+row[5];
	 }
	 if(row[6]!=""){
		 style+=', '+row[6];
	 }
	 if(row[7]!=""){
		 style+=', '+row[7];
	 }
	 
                 return   style;
                },
                "targets"  : 4,
                "sorting": false,
				"orderable": false,
            
				},
		
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[19]<0){
						 var mydprice=Math.abs(row[19]);
						 return   "-$"+mydprice;
					 }else{
						  return   "$"+row[19];
					 }
                 },
                "targets"  : 5,
                "sorting": false,
				"orderable": false,
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
                 return   '<button type="button" class="btn btn-danger btn-xs no-mobile no-small" onClick="groupdel('+row[14]+')">Delete</button> <button type="button" class="btn btn-info btn-xs" id="'+row[14]+'" onClick="groupedit('+row[14]+')">Edit</button>' 
					},
                "targets"  : 6,
                "sorting": false,
				"orderable": false,
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'og'+aData[0]);
    }
		
    } );
}

	
if(screenWidth<=1024){
	UserTable=$('#UserTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/user_table.php",
		"dom": '<"top">rt<"bottom"ip><"clear">',
		"order": [],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[1];
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[2];
                },
                "targets"  : 1,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[3];
                },
                "targets"  : 2,
                "sorting": false,
				"orderable": false,
            },{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[4];
                },
                "targets"  : 3,
                "sorting": false,
				"orderable": false,
            
				},
		
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[5];
                },
                "targets"  : 4,
                "sorting": false,
				"orderable": false,
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
                 return   '<button type="button" class="btn btn-primary btn-xs" onclick="sendpassword('+row[6]+')">Send</button> <button type="button" class="btn btn-info btn-xs no-mobile"  onclick="useredit('+row[6]+')">Edit</button> <button type="button" class="btn btn-danger btn-xs no-mobile" onclick="userdelete('+row[6]+')">Delete</button>' 
					},
                "targets"  : 5,
                "sorting": false,
				"orderable": false,
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'u'+aData[6]);
    }
		
    } );
}else{
	
	UserTable=$('#UserTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/user_table.php",
		"dom": '<"top">rt<"bottom"ip><"clear">',
		"order": [],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[1];
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[2];
                },
                "targets"  : 1,
                "sorting": false,
				"orderable": false,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[3];
                },
                "targets"  : 2,
                "sorting": false,
				"orderable": false,
            },{
				
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[4];
                },
                "targets"  : 3,
                "sorting": false,
				"orderable": false,
            
				},
		
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return row[5];
                },
                "targets"  : 4,
                "sorting": false,
				"orderable": false,
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
                 return   '<button type="button" class="btn btn-primary btn-xs" onclick="sendpassword('+row[6]+')">Send Password</button> <button type="button" class="btn btn-info btn-xs no-mobile"  onclick="useredit('+row[6]+')">Edit</button> <button type="button" class="btn btn-danger btn-xs no-mobile" onclick="userdelete('+row[6]+')">Delete</button>' 
					},
                "targets"  : 5,
                "sorting": false,
				"orderable": false,
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'u'+aData[6]);
    }
		
    } );

}

    // bind form using ajaxForm  
$.ajax({

			    	url: "assets/ajax/pay_due.php",
			    	type: "get",
					dataType: 'json',
					cache:false,
			      	success: function(html){
						if(html.status=="due"){
							$(".pdinv").html(html.invoice_id);
							$(".pddays").html(html.invoice_days);
							$("#payduemodal").modal("show");
						}
					}

		});
	    $('#account_settings').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

			sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 


		}else if($.trim(responseText) =="yes"){

					VehicleTable.ajax.reload( null, false);

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					 

				 $("#settings").modal('hide');	
			 swal("Thank You!", "Information successfully updated.", "success")
		}else{
					VehicleTable.ajax.reload( null, false);

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					 

					
             $("#settings").modal('hide');	
			 swal("Thank You!", "Information successfully updated.", "success");
			 
var ppimage="https://d1xlji8qxtrdmo.cloudfront.net/"+$.trim(responseText);

			 $('#inputprofile').attr( 'src',ppimage);

			 $('#profilePicture').val(responseText);
		
		}

		}

    }); 

		    $('#order_label').ajaxForm({ 
			 beforeSubmit : function(arr, $form, options){
             $("#order").modal('hide');
          },

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else if($.trim(responseText) =="yes"){
			 swal("Thank You!", "We have received your order and will be shipping your labels ASAP.", "success")

		}else if($.trim(responseText) =="free"){

            var url="https://www.dodson-group.com/pressure-sensitive-laser-printer-labels";

			var win=window.open(url, '_blank');

			if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();

			}

		}

		}

    });
	
	$('#sms_settings').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else if($.trim(responseText) =="yes"){
				$("#sms_modal").modal('hide');
			 swal("Thank You!", "SMS settings updated.", "success")

		}else{
			$('#smslogo').attr( 'src',"https://d1xlji8qxtrdmo.cloudfront.net/"+$.trim(responseText));
			$("#sms_modal").modal('hide');
			 swal("Thank You!", "SMS settings updated.", "success")
		}

		}

    });  

	

		$('#buyers_guide').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 

		}else if($.trim(responseText) =="yes"){
			$("#buyer_guide_settings").modal('hide');	
			 swal("Thank You!", "Buyer buyers guide successfully updated.", "success")

			 $(".btndb").html("Close");

		}

		}

    }); 

	

	

		    $('#update_user').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){
            sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 
		}else if($.trim(responseText) =="yes"){
			$('#user_edit').modal('hide');
			swal("Thank You!", "Information successfully updated.", "success")
			UserTable.ajax.reload();
		}else{
			$("#inputprofile2").attr("src","https://d1xlji8qxtrdmo.cloudfront.net/"+responseText);
			$('#user_edit').modal('hide');
			swal("Thank You!", "Information successfully updated.", "success")
			UserTable.ajax.reload();
		}
		}
    }); 

		$('#dealer_profile').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
 				$("#profile").modal("hide");
			 swal("Thank You!", "Information successfully updated.", "success");
			
			 location.reload();

		}

		}

    });

	

			$('#addendum_setting_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
			$.ajax({
			    	url: "assets/ajax/get_background.php?form=new_as",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".file_c1bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_BG);
						$(".file_c1qrbg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_QR_BG);
						$(".file_c1smsbg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_SMS_BG);
						$(".file_c1vinbg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_VIN_BG);
						$(".file_c2bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM2_BG);
						$(".file_c3bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM3_BG);
						$(".file_c5bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM5_BG);
						$(".file_c6bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM6_BG);
						$(".file_c7bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM7_BG);
						$(".file_c7nbg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM7N_BG);
						$(".file_c2qrbg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM2_QR_BG);
						$(".file_c2nmbg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM2_NM_BG);
						$(".file_c4bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM4_BG);
						$(".file_nc1bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].NARROW_CUSTOM1_BG);
						$(".file_nc2bg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].NARROW_CUSTOM2_BG);
						$(".file_wholesalebg").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].WHOLESALE_BG);
						}
					}
			});
			$('#addendum_settings').modal('hide');

             swal("Thank You!", "Information successfully updated.", "success");

			if($(".uselect").val()=="EPA Template"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});
                $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Branded"){

				 $(".accheck").removeAttr("style");                $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Savings"){

				 $(".accheck").removeAttr("style");                
                 $(".dbscheck").removeAttr("style");

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
                 $(".dbscheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Simple"){

				 $(".accheck").removeAttr("style");                $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="PrePrinted"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Wide"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Narrow"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT Narrow Series S"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Two Up"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="QR Label"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});
				 $(".nbccheck").removeAttr("style");
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1" || $(".uselect").val()=="Custom1p"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").removeAttr("style");
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Custom 1"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").removeAttr("style");
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Custom 2" || $(".uselect").val()=="Narrow Custom 2p"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc2bg").removeAttr("style");
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 SMS"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").removeAttr("style");
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 VIN"||$(".uselect").val()=="Custom1 VIN No MSRP"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").removeAttr("style");
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").removeAttr("style");
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").removeAttr("style");
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom3"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").removeAttr("style");
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom5"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c5bg").removeAttr("style");
				 $(".c3bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom6"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").removeAttr("style");
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").removeAttr("style");
				 $(".total_adds").removeAttr("style");
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom7"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").removeAttr("style");
				  $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").removeAttr("style");
				 $(".total_adds").removeAttr("style");
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom7 Narrow"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").removeAttr("style");
				 $(".c6check").removeAttr("style");
				 $(".total_adds").removeAttr("style");
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom4"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Wholesale"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uwholesalebg").removeAttr("style");
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").removeAttr("style");
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 No MSRP"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else{

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }

		}else if($.trim(responseText) =="Invalid Request"){

			sweetAlert("Oops...", "Invalid Request.", "error");
	image="https://d1xlji8qxtrdmo.cloudfront.net/"+$.trim(responseText);

			 $('#inputlogo').attr( 'src',image);

			 $('#logofile').val(responseText);

			 if(($(".uselect").val()=="Narrow Branded")||($(".uselect").val()=="Standard Branded")||($(".uselect").val()=="Dealer Branded Savings")||($(".uselect").val()=="Dealer Branded Simple")||($(".uselect").val()=="Dealer Branded Custom QR")||($(".uselect").val()=="Narrow Branded Custom QR")||($(".uselect").val()=="Narrow Custom 2" || $(".uselect").val()=="Narrow Custom 2p")||($(".uselect").val()=="Dealer Branded with VIN Barcode")){

				 $(".lgcheck").removeAttr("style");

			 }else{

				 $(".lgcheck").css({'display' : 'none'});

			 }
			 $('#addendum_settings').modal('hide');

			 swal("Thank You!", "Information successfully updated.", "success");

		}

		}

    });
	$('#sms_style').change(function(){

		  if($("#sms_style").val()=="SMS"){
				 $(".smscheck").css({'display' : 'none'});
				 $(".smscolor").removeAttr("style");
				 $(".smsc1bg").css({'display' : 'none'});
			 }else if($("#sms_style").val()=="SMS Custom 1"){
				 $(".smscheck").removeAttr("style");
				 $(".smscolor").css({'display' : 'none'});
				 $(".smsc1bg").removeAttr("style");
			 }
	});

	$('.uselect').change(function(){

		  if($(".uselect").val()=="EPA Template"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Branded"){

				 $(".accheck").removeAttr("style");                $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Savings"){

				 $(".accheck").removeAttr("style");                $(".dbscheck").removeAttr("style");
				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Simple"){

				 $(".accheck").removeAttr("style");                $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="PrePrinted"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Wide"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Narrow"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT Narrow Series S"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Two Up"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="QR Label"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});
				 $(".nbccheck").removeAttr("style");
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1" || $(".uselect").val()=="Custom1p"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").removeAttr("style");
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Custom 1"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").removeAttr("style");
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Custom 2" || $(".uselect").val()=="Narrow Custom 2p"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc2bg").removeAttr("style");
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 SMS"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").removeAttr("style");
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 VIN"||$(".uselect").val()=="Custom1 VIN No MSRP"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").removeAttr("style");
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").removeAttr("style");
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom3"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").removeAttr("style");
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom5"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").removeAttr("style");
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom6"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").removeAttr("style");
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").removeAttr("style");
				 $(".total_adds").removeAttr("style");
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom7"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").removeAttr("style");
				  $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").removeAttr("style");
				 $(".total_adds").removeAttr("style");
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom7 Narrow"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").removeAttr("style");
				 $(".c6check").removeAttr("style");
				 $(".total_adds").removeAttr("style");
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom4"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				 $(".c4bg").removeAttr("style");
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Wholesale"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				 $(".wholesalebg").removeAttr("style");
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c4bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 QR"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").removeAttr("style");
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 No MSRP"){

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }else{

				 $(".accheck").css({'display' : 'none'});                 $(".dbscheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c6bg").css({'display' : 'none'});
				 $(".c7bg").css({'display' : 'none'});
				 $(".c7nbg").css({'display' : 'none'});
				 $(".c6check").css({'display' : 'none'});
				 $(".total_adds").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});
				 $(".nc2bg").css({'display' : 'none'});

			 }

	});





	

			$('#used_addendum_setting_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 

		}else if($.trim(responseText) =="yes"){
$.ajax({
			    	url: "assets/ajax/get_background.php?form=used_as",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".ufile_c1bg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_BG);
						$(".ufile_c1qrbg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_QR_BG);
						$(".ufile_c1smsbg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_SMS_BG);
						$(".ufile_c1vinbg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM1_VIN_BG);
						$(".ufile_c2bg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM2_BG);
						$(".ufile_c3bg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM3_BG);
						$(".ufile_c2qrbg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM2_QR_BG);
						$(".ufile_c2nmbg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM2_NM_BG);
						$(".ufile_c4bg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].CUSTOM4_BG);
						$(".ufile_nc1bg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].NARROW_CUSTOM1_BG);
						$(".ufile_wholesalebg1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].WHOLESALE_BG);
						}
					}
			});
			$('#used_addendum_settings').modal('hide');
              swal("Thank You!", "Information successfully updated.", "success");

			  if($(".uuselect").val()=="EPA Template"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Branded"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Savings"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").removeAttr("style"); 

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Simple"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Used"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="PrePrinted"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Wide"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Narrow"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT Narrow Series S"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Two Up"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="QR Label"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});
				 

				 $(".umrcheck").removeAttr("style");
				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1" || $(".uuselect").val()=="Custom1p"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").removeAttr("style");
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 1"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").removeAttr("style");
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 2" || $(".uuselect").val()=="Narrow Custom 2p"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc2bg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 SMS"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 VIN"||$(".uuselect").val()=="Custom1 VIN No MSRP"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").removeAttr("style");
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").removeAttr("style");
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom3"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").removeAttr("style");
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom5"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom6"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").removeAttr("style");
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom7"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").removeAttr("style");
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom7 Narrow"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7bg").removeAttr("style");
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom4"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Wholesale"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uwholesalebg").removeAttr("style");
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").removeAttr("style");
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 No MSRP"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").removeAttr("style");

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else{

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }

		}else if($.trim(responseText) =="Invalid Request"){
			 sweetAlert("Oops...", "Invalid Request.", "error");
		}else{

			image="https://d1xlji8qxtrdmo.cloudfront.net/"+$.trim(responseText);

			 $('#uinputlogo').attr( 'src',image);

			 if(($(".uuselect").val()=="Narrow Branded")||($(".uuselect").val()=="Standard Branded")||($(".uuselect").val()=="Dealer Branded Simple")||($(".uuselect").val()=="Dealer Branded Savings")||($(".uuselect").val()=="Standard Used")||($(".uuselect").val()=="Dealer Branded Custom QR")||($(".uuselect").val()=="Narrow Branded Custom QR")||($(".uuselect").val()=="Narrow Custom 2" || $(".uuselect").val()=="Narrow Custom 2p")||($(".uuselect").val()=="Dealer Branded with VIN Barcode")){

				 $(".ulgcheck").removeAttr("style");

			 }else{

				 $(".ulgcheck").css({'display' : 'none'});

			 }
				$('#used_addendum_settings').modal('hide');
			 swal("Thank You!", "Information successfully updated.", "success");

		}

		}

    });

	$('.uuselect').change(function(){

		 if($(".uuselect").val()=="EPA Template"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Branded"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Savings"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").removeAttr("style"); 

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Simple"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Used"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="PrePrinted"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Wide"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Narrow"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT Narrow Series S"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Two Up"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="QR Label"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});
				 

				 $(".umrcheck").removeAttr("style");
				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1" || $(".uuselect").val()=="Custom1p"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").removeAttr("style");
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 1"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").removeAttr("style");
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 2" || $(".uuselect").val()=="Narrow Custom 2p"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc2bg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 SMS"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 VIN"||$(".uuselect").val()=="Custom1 VIN No MSRP"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").removeAttr("style");
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").removeAttr("style");
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom3"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").removeAttr("style");
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom5"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom6"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").removeAttr("style");
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom7"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7nbg").removeAttr("style");
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom7 Narrow"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").removeAttr("style");
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom4"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Wholesale"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uwholesalebg").removeAttr("style");
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").removeAttr("style");
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 No MSRP"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").removeAttr("style");

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else{

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }

	});



			$('#infosheet_setting_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
			$.ajax({
			    	url: "assets/ajax/get_background.php?form=info",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".info_narrow").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_NARROW_BG);
						$(".info_narrow_np").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_NARROW_NP_BG);
						$(".info_dc1").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC1_BG);
						$(".info_dc1c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC1C_BG);
						$(".info_dc2").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC2_BG);
						$(".info_dc2c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC2C_BG);
						$(".info_dc3").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC3_BG);
                            $(".info_dc4").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC4_BG);
						$(".info_dc3c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC3C_BG);
						$(".info_pc4").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_PURE4_BG);
						}
					}
			});
			$('#infosheet_settings').modal('hide');

              swal("Thank You!", "Information successfully updated.", "success");

			 if($(".isselect").val()=="Dealer Custom" || $(".isselect").val()=="Dealer Custom NP" || $(".isselect").val()=="Standard 4" || $(".isselect").val()=="Info Book"){

				 $(".ischeck").removeAttr("style");

			 }else{

				 $(".ischeck").css({'display' : 'none'});

			 }

			  if($(".isselect").val()=="Pure Cars 2" || $(".isselect").val()=="Pure Cars 3" || $(".isselect").val()=="Pure Cars 1" || $(".isselect").val()=="Pure Cars 4"){

				 $(".pccheck").removeAttr("style");

			 }else{

				 $(".pccheck").css({'display' : 'none'});

			 }
			  if($(".isselect").val()=="Pure Cars 4"){

				  $(".ispc4bg").removeAttr("style");

			 }else{

				 $(".ispc4bg").css({'display' : 'none'});
				 

			 }

			  if($(".isselect").val()=="Narrow Standard"){

				 $(".nscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nsnpcheck").css({'display' : 'none'});

			 }else if($(".isselect").val()=="Narrow Standard NP"){

				 $(".nsnpcheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nscheck").css({'display' : 'none'});

			 }else{

				  $(".nscheck").css({'display' : 'none'});
				  $(".nsnpcheck").css({'display' : 'none'});

				  $(".iqrselect").removeAttr("style");

			 }
			 
			 if($(".isselect").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 2 Certified"){

				 $(".isdc2ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc2ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 1"){

				 $(".isdc1check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 2"){

				 $(".isdc2check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc2check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 3"){

				 $(".isdc3check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc3check").css({'display' : 'none'});


			 }
            if($(".isselect").val()=="Dealer Custom 4"){

                $(".isdc4check").removeAttr("style");

                $(".iqrselect").css({'display' : 'none'});

            }else{

                $(".isdc4check").css({'display' : 'none'});


            }
            if($(".isselect").val()=="Dealer Custom 4 NP" || $(".isselect").val()=="Dealer Custom 4"){

                $(".isdc4check").removeAttr("style");

                $(".iqrselect").css({'display' : 'none'});

            }else{

                $(".isdc4check").css({'display' : 'none'});

            }

			 

		}else if($.trim(responseText) =="Invalid Request"){

			 sweetAlert("Oops...", "Invalid Request.", "error");

		}else{

			image="https://d1xlji8qxtrdmo.cloudfront.net/"+$.trim(responseText);

			 $('#inputlogoiss').attr( 'src',image);

			 if($(".isselect").val()=="Dealer Custom" || $(".isselect").val()=="Dealer Custom NP" || $(".isselect").val()=="Standard 4" || $(".isselect").val()=="Info Book"){

				 $(".isscheck").removeAttr("style");

			 }else{

				 $(".isscheck").css({'display' : 'none'});

			 }

			  if($(".isselect").val()=="Pure Cars 2" || $(".isselect").val()=="Pure Cars 3" || $(".isselect").val()=="Pure Cars 1" || $(".isselect").val()=="Pure Cars 4"){

				 $(".pccheck").removeAttr("style");

			 }else{

				 $(".pccheck").css({'display' : 'none'});

			 }
			  if($(".isselect").val()=="Pure Cars 4"){

				  $(".ispc4bg").removeAttr("style");

			 }else{

				 $(".ispc4bg").css({'display' : 'none'});
				 

			 }

			  if($(".isselect").val()=="Narrow Standard"){

				 $(".nscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nsnpcheck").css({'display' : 'none'});

			 }else if($(".isselect").val()=="Narrow Standard NP"){

				 $(".nsnpcheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nscheck").css({'display' : 'none'});

			 }else{

				  $(".nscheck").css({'display' : 'none'});
				  $(".nsnpcheck").css({'display' : 'none'});

				  $(".iqrselect").removeAttr("style");

			 }
			 if($(".isselect").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 2 Certified"){

				 $(".isdc2ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc2ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 1"){

				 $(".isdc1check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 2"){

				 $(".isdc2check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc2check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 3"){

				 $(".isdc3check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc3check").css({'display' : 'none'});


			 }
            if($(".isselect").val()=="Dealer Custom 4"){

                $(".isdc4check").removeAttr("style");

                $(".iqrselect").css({'display' : 'none'});

            }else{

                $(".isdc4check").css({'display' : 'none'});


            }
            if($(".isselect").val()=="Dealer Custom 4 NP" || $(".isselect").val()=="Dealer Custom 4"){

                $(".isdc4check").removeAttr("style");

                $(".iqrselect").css({'display' : 'none'});

            }else{

                $(".isdc4check").css({'display' : 'none'});


            }
			 $('#infosheet_settings').modal('hide');

			 swal("Thank You!", "Information successfully updated.", "success");

		}

		}

    });
	
	$('#infosheet_setting_form_c').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
			$.ajax({
			    	url: "assets/ajax/get_background.php?form=info_c",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".info_narrow_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_NARROW_BG);
						$(".info_narrow_np_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_NARROW_NP_BG);
						$(".info_dc1_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC1_BG);
						$(".info_dc1c_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC1C_BG);
						$(".info_dc2_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC2_BG);
						$(".info_dc2c_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC2C_BG);
						$(".info_dc3c_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC3C_BG);
                            $(".info_dc4c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_DC4_BG);
						$(".info_pc4_c").attr("href","https://d1ja3ktoroz7zi.cloudfront.net/"+mydata[x].INFO_PURE4_BG);
						}
					}
			});
			$('#infosheet_settings_c').modal('hide');

             swal("Thank You!", "Information successfully updated.", "success");

			 if($(".isselectc").val()=="Dealer Custom" || $(".isselectc").val()=="Dealer Custom NP" || $(".isselectc").val()=="Standard 4" || $(".isselectc").val()=="Info Book"){

				 $(".ischeckc").removeAttr("style");

			 }else{

				 $(".ischeckc").css({'display' : 'none'});

			 }

			  if($(".isselectc").val()=="Pure Cars 2" || $(".isselectc").val()=="Pure Cars 3" || $(".isselectc").val()=="Pure Cars 1" || $(".isselectc").val()=="Pure Cars 4"){

				 $(".pccheckc").removeAttr("style");

			 }else{

				 $(".pccheckc").css({'display' : 'none'});

			 }
			  if($(".isselectc").val()=="Pure Cars 4"){

				  $(".ispc4cbg").removeAttr("style");

			 }else{
				 $(".ispc4cbg").css({'display' : 'none'});
			 }

			  if($(".isselectc").val()=="Narrow Standard"){

				 $(".nscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nsnpcheckc").css({'display' : 'none'});

			 }else if($(".isselectc").val()=="Narrow Standard NP"){

				 $(".nsnpcheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nscheckc").css({'display' : 'none'});

			 }else{

				  $(".nscheckc").css({'display' : 'none'});
				  $(".nsnpcheckc").css({'display' : 'none'});

				  $(".iqrselectc").removeAttr("style");

			 }
			 if($(".isselectc").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 2 Certified"){

				 $(".isdc2ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc2ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 1"){

				 $(".isdc1checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1checkc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 2"){

				 $(".isdc2checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc2checkc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 3"){

				 $(".isdc3checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc3checkc").css({'display' : 'none'});


			 }
            if($(".isselectc").val()=="Dealer Custom 4"){

                $(".isdc4checkc").removeAttr("style");

                $(".iqrselectc").css({'display' : 'none'});

            }else{

                $(".isdc4checkc").css({'display' : 'none'});


            }
            if($(".isselectc").val()=="Dealer Custom 4 NP"){

                $(".isdc4checkc").removeAttr("style");

                $(".iqrselectc").css({'display' : 'none'});

            }else{

                $(".isdc4checkc").css({'display' : 'none'});


            }

			 

		}else if($.trim(responseText) =="Invalid Request"){

			 sweetAlert("Oops...", "Invalid Request.", "error"); 

		}else{

			image="https://d1xlji8qxtrdmo.cloudfront.net/"+$.trim(responseText);

			 $('#inputlogoissc').attr( 'src',image);

			 if($(".isselectc").val()=="Dealer Custom" || $(".isselectc").val()=="Dealer Custom NP" || $(".isselectc").val()=="Standard 4" || $(".isselectc").val()=="Info Book"){

				 $(".isscheckc").removeAttr("style");

			 }else{

				 $(".isscheckc").css({'display' : 'none'});

			 }

			  if($(".isselectc").val()=="Pure Cars 2" || $(".isselectc").val()=="Pure Cars 3" || $(".isselectc").val()=="Pure Cars 1" || $(".isselectc").val()=="Pure Cars 4"){

				 $(".pccheckc").removeAttr("style");

			 }else{

				 $(".pccheckc").css({'display' : 'none'});

			 }
			 if($(".isselectc").val()=="Pure Cars 4"){

				  $(".ispc4cbg").removeAttr("style");

			 }else{

				 $(".ispc4cbg").css({'display' : 'none'});
				 

			 }

			  if($(".isselectc").val()=="Narrow Standard"){

				 $(".nscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nsnpcheckc").css({'display' : 'none'});

			 }else if($(".isselectc").val()=="Narrow Standard NP"){

				 $(".nsnpcheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nscheckc").css({'display' : 'none'});

			 }else{

				  $(".nscheckc").css({'display' : 'none'});
				  $(".nsnpcheckc").css({'display' : 'none'});

				  $(".iqrselectc").removeAttr("style");

			 }
			 if($(".isselectc").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 2 Certified"){

				 $(".isdc2ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc2ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 1"){

				 $(".isdc1checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1checkc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 2"){

				 $(".isdc2checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc2checkc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 3"){

				 $(".isdc3checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc3checkc").css({'display' : 'none'});


			 }
            if($(".isselectc").val()=="Dealer Custom 4"){

                $(".isdc4checkc").removeAttr("style");

                $(".iqrselectc").css({'display' : 'none'});

            }else{

                $(".isdc4checkc").css({'display' : 'none'});


            }
            if($(".isselectc").val()=="Dealer Custom 4 NP"){

                $(".isdc4checkc").removeAttr("style");

                $(".iqrselectc").css({'display' : 'none'});

            }else{

                $(".isdc4checkc").css({'display' : 'none'});


            }
			 $('#infosheet_settings_c').modal('hide');

			 swal("Thank You!", "Information successfully updated.", "success");

		}

		}

    });

	$('.isselect').change(function(){

		if($(".isselect").val()=="Dealer Custom" || $(".isselect").val()=="Dealer Custom NP" || $(".isselect").val()=="Standard 4" || $(".isselect").val()=="Info Book"){

				 $(".ischeck").removeAttr("style");

			 }else{

				 $(".ischeck").css({'display' : 'none'});

			 }

			  if($(".isselect").val()=="Pure Cars 2" || $(".isselect").val()=="Pure Cars 3" || $(".isselect").val()=="Pure Cars 1" || $(".isselect").val()=="Pure Cars 4"){

				 $(".pccheck").removeAttr("style");

			 }else{

				 $(".pccheck").css({'display' : 'none'});

			 }
			 if($(".isselect").val()=="Pure Cars 4"){

				  $(".ispc4bg").removeAttr("style");

			 }else{

				 $(".ispc4bg").css({'display' : 'none'});
				 

			 }

			 if($(".isselect").val()=="Narrow Standard"){

				 $(".nscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nsnpcheck").css({'display' : 'none'});

			 }else if($(".isselect").val()=="Narrow Standard NP"){

				 $(".nsnpcheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nscheck").css({'display' : 'none'});

			 }else{

				  $(".nscheck").css({'display' : 'none'});
				  $(".nsnpcheck").css({'display' : 'none'});

				  $(".iqrselect").removeAttr("style");

			 }
			
			 if($(".isselect").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 2 Certified"){

				 $(".isdc2ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc2ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 1"){

				 $(".isdc1check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 2"){

				 $(".isdc2check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc2check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 3"){

				 $(".isdc3check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc3check").css({'display' : 'none'});


			 }
        if($(".isselect").val()=="Dealer Custom 4"){

            $(".isdc4check").removeAttr("style");

            $(".iqrselect").css({'display' : 'none'});

        }else{

            $(".isdc4check").css({'display' : 'none'});


        }
        if($(".isselect").val()=="Dealer Custom 4 NP" || $(".isselect").val()=="Dealer Custom 4"){

            $(".isdc4check").removeAttr("style");

            $(".iqrselect").css({'display' : 'none'});

        }else{

            $(".isdc4check").css({'display' : 'none'});


        }

			

	});
	
	$('.isselectc').change(function(){

		if($(".isselectc").val()=="Dealer Custom" || $(".isselectc").val()=="Dealer Custom NP" || $(".isselectc").val()=="Standard 4" || $(".isselectc").val()=="Info Book"){

				 $(".ischeckc").removeAttr("style");

			 }else{

				 $(".ischeckc").css({'display' : 'none'});

			 }

			  if($(".isselectc").val()=="Pure Cars 2" || $(".isselectc").val()=="Pure Cars 3" || $(".isselectc").val()=="Pure Cars 1" || $(".isselectc").val()=="Pure Cars 4"){

				 $(".pccheckc").removeAttr("style");

			 }else{

				 $(".pccheckc").css({'display' : 'none'});

			 }
			 if($(".isselectc").val()=="Pure Cars 4"){
				  $(".ispc4cbg").removeAttr("style");
			 }else{
				 $(".ispc4cbg").css({'display' : 'none'});
				 
			 }

			  if($(".isselectc").val()=="Narrow Standard"){

				 $(".nscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nsnpcheckc").css({'display' : 'none'});

			 }else if($(".isselectc").val()=="Narrow Standard NP"){

				 $(".nsnpcheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nscheckc").css({'display' : 'none'});

			 }else{

				  $(".nscheckc").css({'display' : 'none'});
				  $(".nsnpcheckc").css({'display' : 'none'});

				  $(".iqrselectc").removeAttr("style");

			 }
			 if($(".isselectc").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 2 Certified"){

				 $(".isdc2ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc2ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 1"){

				 $(".isdc1checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1checkc").css({'display' : 'none'});

			 }
			 if($(".isselectc").val()=="Dealer Custom 2"){

				 $(".isdc2checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc2checkc").css({'display' : 'none'});

			 }
			  if($(".isselectc").val()=="Dealer Custom 3"){

            $(".isdc3checkc").removeAttr("style");

            $(".iqrselectc").css({'display' : 'none'});

        }else{

            $(".isdc3checkc").css({'display' : 'none'});

        }
        if($(".isselectc").val()=="Dealer Custom 4"){

            $(".isdc4checkc").removeAttr("style");

            $(".iqrselectc").css({'display' : 'none'});

        }else{

            $(".isdc4checkc").css({'display' : 'none'});

        }
        if($(".isselectc").val()=="Dealer Custom 4 NP"){

            $(".isdc4checkc").removeAttr("style");

            $(".iqrselectc").css({'display' : 'none'});

        }else{

            $(".isdc4checkc").css({'display' : 'none'});

        }


			

	});

	

	$('#add_options').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

			

		var id=$.trim(responseText);

		$('#add_option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,	

			      	success: function(html){
						$('#item_name').html(html);
						GetPrintTable.ajax.reload();
						}

				});

				

      				}

    			});


				
			}

    });

	

		$('#option_edit_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

			

		var id=$.trim(responseText);

		$('#add_option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

					$('#item_name').html(html);
						GetPrintTable.ajax.reload();
					$('#pdesc').html("");
					document.getElementById("option_edit_form").reset();
					$('#print_option_edit').modal('hide');	

					}

				});

				

      				}

    			});

				

	

			}

    });



	

	

		 $('#add_addendum').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 
		}else if($.trim(responseText) =="yes"){

             swal("Thank You!", "Information successfully updated.", "success");

			 $('#add_addendum').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

			$("#inputstyle2").select2("val", "- NONE");

	$("#inputmodel2").select2("val", "-NONE");
	$('#separator_above1').prop('checked', false);
	$('#separator_below1').prop('checked', false);

	OptionTable.ajax.reload( null, false);
	$("#addendum_add").modal("hide");
	$.ajax({

			    	url: "assets/ajax/get_options.php",

			    	type: "get",

					cache:false,

			      	success: function(option){
						$('#eginputdesc').html(option).change();
						$('#inputgroupdesc').html(option).change();
						$('#agdesc').html(option).change();
						$('#pidesc').html(option).change();
					}
	});

	$('a[href="#defaults-overview"]').tab('show');

		}

		}

    });
	
	
	$('#group_add').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 
		}else if($.trim(responseText) =="yes"){

             swal("Thank You!", "Information successfully updated.", "success");

			 $('#group_add').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

			$("#inputgroupstyle2").select2("val", "- NONE");

	$("#inputgroupmodel2").select2("val", "-NONE");
	$("#inputgroupdesc").select2("val", "");

	groupTable.ajax.reload();
	
	$("#add_group").modal("hide");

	//$('a[href="#defaults-overview"]').tab('show');

		}

		}

    });

	

		$('#vehicle_edit').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
			$("#vehicle_edit_form").modal("hide");
             swal("Thank You!", "Information successfully updated.", "success");

			  VehicleTable.ajax.reload( null, false );

      		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  

					$.ajax({

			    	url: "assets/ajax/get_model.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						$('#inputmodel2').html(html);

					}

					});

					$.ajax({

			    	url: "assets/ajax/get_style.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						$('#inputstyle2').html(html);

					}

					});

   $('#vehicle_edit_form').modal('hide');

		}

		}

    });

	

			$('#update_addendum').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
			$('#addendum_edit').modal('hide');

            swal("Thank You!", "Information successfully updated.", "success");

	OptionTable.ajax.reload( null, false);
	$("#eadescription").html("");
	$.ajax({
			    	url: "assets/ajax/get_options.php",

			    	type: "get",

					cache:false,

			      	success: function(option){
						$('#eginputdesc').html(option).change();
						$('#inputgroupdesc').html(option).change();
						$('#agdesc').html(option).change();
						$('#pidesc').html(option).change();
					}
	});

			 

		}

		}

    });

	$('#update_group').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
			$('#group_edit').modal('hide');

            swal("Thank You!", "Information successfully updated.", "success");

	groupTable.ajax.reload();

			 

		}

		}

    });


	$('#vehicle_form').hide();


	

	$('#option_add_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {	

		var id=$.trim(responseText);

		$('#add_option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);
						GetPrintTable.ajax.reload();

   $('#option_add_form').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

					$('#print_option_edit').modal('hide');	

					}

				});

				

      				}

    			});

 $('#print_option_add').modal('hide');

		}

	});

	$('#group_add_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {	

		var id=$.trim(responseText);

		$('#add_group_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);
						GetPrintTable.ajax.reload();

   $('#group_add_form').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

					$('#print_group_edit').modal('hide');	

					}

				});

				

      				}

    			});

 $('#print_group_add').modal('hide');

		}

	});

		$('#add_user').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

           sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="username"){

           sweetAlert("Oops...", "This username was taken. Please try different username.", "error");

		}else if($.trim(responseText) =="yes"){

			 swal("Thank You!", "New User Created.", "success");

			 $('#add_user').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

			 UserTable.ajax.reload();

    $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});
					$("#add_user").attr("style","display:none");
		}

		}

    });

	

			$('#VEHICLE_INPUT').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

		 dataType:  'json',

        success: function(html) { 

				VehicleTable.ajax.reload( null, false);

   $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					



					$.ajax({

			    	url: "assets/ajax/get_style.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						$('#inputstyle2').html(html);

					}

					});

		if(html.type=="savevehicle"){
			
			 swal("Thank You!", "New Vehicle Added.", "success");

		$('#VIN_FORM').show();

		$('#vehicle_form').hide();

		 $('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('a[href="#vehicle-overview"]').tab('show');

		}else if(html.type=="savennew"){

             swal("Thank You!", "New Vehicle Added.", "success");

		$('#VIN_FORM').show();

		$('#vehicle_form').hide();

		 $('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		}else if(html.type=="savenprint"){

		$('#VIN_FORM').show();

		$('#vehicle_form').hide();

		 $('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $(".print_addendum").attr("id",html.id);

		$(".add_new_option").attr("id",html.id);
		$(".add_new_group").attr("id",html.id);

		$('#add_option_id').val(html.id);
		$('#add_group_id').val(html.id);

		$('#option_id').val(html.id);


			var values = "id="+html.id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);
						
						$('#item_name').html(html);
						
						if(GetPrintTable != null){
							if($("#initiate").val()!="1"){
							 GetPrintTable=$('#GetPrintTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/get_print_table.php",
		"dom": '',
		"order": [],
		"columnDefs": [ 
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];

                 
				},
                "targets": 0,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					if(row[4]=="2"){
					var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
					}else{
						return row[2];
					}

                 
				},
                "targets": 1,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[3]<0){

						 var mydprice=Math.abs(row[3]);

						 return   "-$"+mydprice;

					 }else if(row[3]=="np"){

						 return   row[3];

					 }else{

						  return   "$"+row[3];

					 }

                 
				},
				"className": "dt-head-right dt-body-right",
                "targets": 2,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
				 	if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+row[0]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
						if(row[4]=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pdefault('+row[0]+')">Edit</button>';
						}else{
						var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pgroup('+row[0]+')">Edit</button>';
						}
				 	}
return deet },
				"className": "dt-head-right dt-body-right",
                "targets": 3,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'pa'+aData[0]);
    }
		
    } );
							}
			
						GetPrintTable.destroy();
						
						}
 
 GetPrintTable=$('#GetPrintTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/get_print_table.php",
		"dom": '',
		"order": [],
		"columnDefs": [ 
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];

                 
				},
                "targets": 0,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					if(row[4]=="2"){
					var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
					}else{
						return row[2];
					}
					alert(row[4]);

                 
				},
                "targets": 1,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[3]<0){

						 var mydprice=Math.abs(row[3]);

						 return   "-$"+mydprice;

					 }else if(row[3]=="np"){

						 return   row[3];

					 }else{

						  return   "$"+row[3];

					 }

                 
				},
				"className": "dt-head-right dt-body-right",
                "targets": 2,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
				 	if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+row[0]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
						if(row[4]=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pdefault('+row[0]+')">Edit</button>';
						}else{
						var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pgroup('+row[0]+')">Edit</button>';
						}
				 		
				 	}
return deet },
				"className": "dt-head-right dt-body-right",
                "targets": 3,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'pa'+aData[0]);
    }
		
    } );
	$("#initiate").val("1");
						
						
						
						}

				});

				

      				}

    			});

				$('a[href="#edit-options"]').tab('show');

		}

		

		}

    });

	$('.add_new_option').click(function(){

		var id=$(this).attr('id');

		$("#add_option_id").val(id);

		$("#print_option_add").modal("show");

	});
	$('.add_new_group').click(function(){

		var id=$(this).attr('id');

		$("#add_group_id").val(id);

		$("#print_group_add").modal("show");

	});
	$('.add-option').click(function(){
$("#addendum_add").modal("show");
	});
	$("#sortable").sortable({
        update: function (event, ui) {
            var order = $(this).sortable('serialize');
        }
    }).disableSelection();
	$("#sortable2").sortable({
        update: function (event, ui) {
            var order = $(this).sortable('serialize');
        }
    }).disableSelection();
	$('.re-order-form').click(function(){
		var r = $("#sortable").sortable("toArray");
		var values = "vin="+$('.vvin').html()+"&data="+r;
		$.ajax({

			    	url: "assets/ajax/save_sortable.php",

			    	type: "post",

			    	data: values,

					cache:false,

			      	success: function(html){
						$("#re-order").modal("hide");
						
						//$('#item_name').html(html);
						
						if(GetPrintTable != null){
							if($("#initiate").val()!="1"){
							 GetPrintTable=$('#GetPrintTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/get_print_table.php",
		"dom": '',
		"order": [],
		"columnDefs": [ 
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];

                 
				},
                "targets": 0,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					if(row[4]=="2"){
					var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
					}else{
						return row[2];
					}
					alert(row[4]);

                 
				},
                "targets": 1,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[3]<0){

						 var mydprice=Math.abs(row[3]);

						 return   "-$"+mydprice;

					 }else if(row[3]=="np"){

						 return   row[3];

					 }else{

						  return   "$"+row[3];

					 }

                 
				},
				"className": "dt-head-right dt-body-right",
                "targets": 2,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
				 	if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+row[0]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
						if(row[4]=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pdefault('+row[0]+')">Edit</button>';
						}else{
						var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pgroup('+row[0]+')">Edit</button>';
						}
				 		
				 	}
return deet },
				"className": "dt-head-right dt-body-right",
                "targets": 3,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'pa'+aData[0]);
    }
		
    } );
							}
			
						GetPrintTable.destroy();
						
						}
 
 GetPrintTable=$('#GetPrintTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/get_print_table.php",
		"dom": '',
		"order": [],
		"columnDefs": [ 
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];

                 
				},
                "targets": 0,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					if(row[4]=="2"){
					var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
					}else{
						return row[2];
					}
					alert(row[4]);

                 
				},
                "targets": 1,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[3]<0){

						 var mydprice=Math.abs(row[3]);

						 return   "-$"+mydprice;

					 }else if(row[3]=="np"){

						 return   row[3];

					 }else{

						  return   "$"+row[3];

					 }

                 
				},
				"className": "dt-head-right dt-body-right",
                "targets": 2,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
				 	if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+row[0]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
						if(row[4]=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pdefault('+row[0]+')">Edit</button>';
						}else{
						var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pgroup('+row[0]+')">Edit</button>';
						}
				 		
				 	}
return deet },
				"className": "dt-head-right dt-body-right",
                "targets": 3,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'pa'+aData[0]);
    }
		
    } );
	$("#initiate").val("1");
						
						
						
						}
					});

	})
	$('.re-order-form2').click(function(){
		var r = $("#sortable2").sortable("toArray");
		var values = "vin="+$('.vvin').html()+"&data="+r;
		$.ajax({

			    	url: "assets/ajax/save_sortable_d.php",

			    	type: "post",

			    	data: values,

					cache:false,

			      	success: function(html){
						$("#re-order2").modal("hide");
						OptionTable.ajax.reload( null, false);
						}
					});

	})
	$('.re-order').click(function(){
		var values = "vin="+$('.vvin').html();

		   			$.ajax({

			    	url: "assets/ajax/get_sortable.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){
						var sorthtml="";
						for (var i = 0, len = html.length; i < len; i++) {
       						 sorthtml+='<li class="ui-state-default" id="'+html[i].add_id+'">'+html[i].item_name+'</li>';
    					}
						$('#sortable').html(sorthtml);
					}
					});
$("#re-order").modal("show");
	});
	$('.re-order2').click(function(){
		var values = "vin="+$('.vvin').html();

		   			$.ajax({

			    	url: "assets/ajax/get_sortable_d.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){
						var sorthtml="";
						for (var i = 0, len = html.length; i < len; i++) {
       						 sorthtml+='<li class="ui-state-default" id="'+html[i].AD_ID+'">'+html[i].ITEM_NAME+'</li>';
    					}
						$('#sortable2').html(sorthtml);
					}
					});
$("#re-order2").modal("show");
	});
	$('.add-group').click(function(){
$("#add_group").modal("show");
	});
	$('.cancel-add-option').click(function(){
$("#add_addendum").attr("style","display:none");
	});
	$('.cancel-add-group').click(function(){
$("#add_group").attr("style","display:none");
	});
	
	$('.add-user').click(function(){
$("#add_user").removeAttr("style");
	});
	$('.cancel-add-user').click(function(){
$("#add_user").attr("style","display:none");
	});

		$('.vehicle_reset').click(function(){

				$('#VIN_FORM').show();

				$('#vehicle_form').hide();

				$('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

				$('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

				$('a[href="#vehicle-overview"]').tab('show');

		});

	

		$('.add_addendum_reset').click(function(){

				$('#add_addendum').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

					$("#inputstyle2 option:selected").removeAttr("selected");

	$("#inputmodel2 option:selected").removeAttr("selected");

				$('a[href="#vehicle-overview"]').tab('show');

		});

		$('.add_user_reset').click(function(){

				$('#add_user').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

				$('#inputUsertype').prop('selectedIndex',0);

				$('a[href="#vehicle-overview"]').tab('show');

		});

		

	$('.btn-refresh').click(function(){

		location.reload();

	});

	$('.print_addendum').click(function() {

		$.ajax({

			    	url: "assets/ajax/addendum_check.php",

			    	type: "get",

					cache:false,

			      	success: function(addendum_check){
$("#addcheck").val(addendum_check);

					if($.trim(addendum_check)=="QR Label"){

					var id=$(".print_addendum").attr('id');
		var url="assets/print/multiprint.php?print%5B0%5D="+id+"&uid="+Date.now();

		//var url="assets/print/print.php?vid="+id+"&uid="+Date.now();

		$(".final_print").val(url);

		$("#spv").val(id);

		$("#som").val("single");

		$("#qr_label").html('<div class="qr_label1">&nbsp;</div><div class="qr_label2"><strong>You are printing 1 QR Label.<br><br>Start Printing Here:</strong><label><input type="radio" name="qr_label" onclick="qr_label();" value="1"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="2"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="3"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="4"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="5"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="6"/><div>&nbsp;</div></label></div>');

		$(".qr_label1").load(url);

		$('#addendum_print_modal').modal({

  keyboard: false

},"show");

					}else if($.trim(addendum_check)=="Dealer Branded Savings"){

			 var cdjrprice = prompt("Please enter CDJR price", "");
    if (cdjrprice != null) {
        var id=$(".print_addendum").attr('id');
		var url="assets/print/multiprint.php?print%5B0%5D="+id+"&uid="+Date.now()+"&cdjr="+cdjrprice;

		//var url="assets/print/print.php?vid="+id+"&uid="+Date.now();

		$(".final_print").val(url);

		$("#spv").val(id);
$("#cdjrcheck").val(cdjrprice);

		$("#som").val("single");
		$('#addendum_print_modal').modal({
  keyboard: false
},"show");
		$("#my_print_modal").attr("src", url);
    }		

		

					

					}else{

		var id=$(".print_addendum").attr('id');
		var url="assets/print/multiprint.php?print%5B0%5D="+id+"&uid="+Date.now();

		//var url="assets/print/print.php?vid="+id+"&uid="+Date.now();

		$(".final_print").val(url);

		$("#spv").val(id);

		$("#som").val("single");
		$('#addendum_print_modal').modal({
  keyboard: false
},"show");
		$("#my_print_modal").attr("src", url);

					}

					}

		});



//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});



	$('.final_print').click(function() {

		if($("#som").val()=="single"){

		var id=$("#spv").val();

		var values="id="+id+"&type=single";

		}else if($("#som").val()=="multiprint"){

			var id=$("#multipleinput").val();

		var values=id+"&type=multiprint";

		}

			 $.ajax({

			    	url: "assets/ajax/print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					VehicleTable.ajax.reload( null, false);

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					 

					if($("#som").val()=="single"){

	var url=$(".final_print").val()+"&type=final_print";

					}else if($("#som").val()=="multiprint"){

	var url=$(".final_print").val()+"&type=multiprint";

					}

	var time1=Math.round(new Date().getTime() / 1000);

    //var myWindow=window.open(url,time1,'');

	$("a#ahref").attr("href", url)

	$('a#ahref')[0].click();

	$("a#ahref").trigger( "click" );

					}

					});	





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});

$('.print-ipdf').click(function() {
var html = '<div id="load"></div>';
        $('body').append(html);
		if($("#iom").val()=="single"){

		var id=$("#ipv").val();

		var values="id="+id+"&type=single&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}else if($("#iom").val()=="multiprint"){

			var id=$("#ipv").val();

		var values=id+"&type=multiprint&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}

		$.ajax({

			

			    	url: "assets/ajax/info_print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					VehicleTable.ajax.reload( null, false);

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					 

					var printstart=$("#printstart").val();

							if($("#iom").val()=="single"){

	var url="assets/print/info_print_download.php?printstart="+printstart+"&vid="+$("#ipv").val()+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}else if($("#iom").val()=="multiprint"){

	var url="assets/print/info-multiprint-download.php?"+$("#ipv").val()+"&type=multiprint&printstart="+printstart+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}

					$("#info_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari1-' + randid;
  $(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
	 setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
		 setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){
setTimeout(function(){ $('#load').remove(); }, 6000);
				$("#popupblock").modal("show");

			}else{
				 setTimeout(function(){ $('#load').remove(); }, 6000);
				win.focus();
				
			}
}



					}

					

		});

						





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

});

	$('.print-pdf').click(function() {
var html = '<div id="load"></div>';
        $('body').append(html);
		if($("#som").val()=="single"){

		var id=$("#spv").val();

		var printstart=$("#printstart").val();

		var values="id="+id+"&type=single&printstart="+printstart;

		}else if($("#som").val()=="multiprint"){

		var id=$("#multipleinput").val();

		var values=id+"&type=multiprint&printstart="+printstart;

		}

			 $.ajax({

			    	url: "assets/ajax/print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					VehicleTable.ajax.reload( null, false);

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);
				

				var printstart=$("#printstart").val();

				$("#printstart").val("1");

				if($("#som").val()=="single"){

if($("#addcheck").val()=="Dealer Branded Savings"){
	//var url="assets/print/single-download.php?vid="+$("#spv").val()+"&dtype=single&printstart="+printstart;
	var url="assets/print/muliple-download.php?print%5B0%5D="+$("#spv").val()+"&type=multiprint"+"&printstart="+printstart+"&cdjr="+$("#cdjrcheck").val();
}else{
var url="assets/print/muliple-download.php?print%5B0%5D="+$("#spv").val()+"&type=multiprint"+"&printstart="+printstart;
}

	//var url="assets/print/single-download.php?vid="+$("#spv").val()+"&printstart="+printstart;
	//var url="assets/print/single-download.php?print%5B0%5D="+$("#spv").val()+"&printstart="+printstart;

					}else if($("#som").val()=="multiprint"){
if($("#addcheck").val()=="Dealer Branded Savings"){
	//var url="assets/print/single-download.php?vid="+$("#spv").val()+"&dtype=single&printstart="+printstart;
	var url="assets/print/muliple-download.php?"+$("#multipleinput").val()+"&type=multiprint"+"&printstart="+printstart+"&cdjr="+$("#cdjrcheck").val();
}else{
var url="assets/print/muliple-download.php?"+$("#multipleinput").val()+"&type=multiprint"+"&printstart="+printstart;
}
	

					}

					$("#addendum_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

	var time1=Math.round(new Date().getTime() / 1000);
	
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari2-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
	setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
		setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');
}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){
setTimeout(function(){ $('#load').remove(); }, 6000);
				$("#popupblock").modal("show");

			}else{
setTimeout(function(){ $('#load').remove(); }, 6000);
				win.focus();
				
			}
}

	//



//document.getElementById('pdfDocument').setAttribute('src',"https://dev.dealeraddendums.com/app/"+url);
//var e=document.getElementById('pdfDocument');
//$('#pdfDocument').load(function(){
    // Operate upon the SVG DOM here
   //var x = document.getElementById('pdfDocument');
       // this.print();
		//this.unbind('load');
//}).attr('src', "https://dev.dealeraddendums.com/app/"+url);
	 
	//

					

					}

					});

					  



					}

					});	





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});


$('#download_guide').click(function() {

			$('#buyers_guide_print').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){				

        

	VehicleTable.ajax.reload( null, false);
if($("#pbgcheck").val()=="multiprint"){
var url="assets/print/guide-multiple-download-spanish.php?bgtype=print&"+$("#multipleinput").val();
	}else{
var url="assets/print/guide-download-spanish.php?bgtype=print&vid="+$("#pbgvid").val();
	}


$("#buyer_guide_print").modal("hide");
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari3-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

		}

    });

});


$('.download-sms').click(function() {
if($("#smsom").val()=="multiprint"){

		var id=$("#multipleinput").val();

		var values=id+"&type=multiprint";

		}else{
		var id=$("#smspv").val();

		var values="id="+id;
		}
		
			 $.ajax({

			    	url: "assets/ajax/sms_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					VehicleTable.ajax.reload( null, false);
					$.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					
	if($("#smsom").val()=="multiprint"){
		var url="assets/print/sms-multiple-download.php?"+$("#multipleinput").val()+"&type=download";
		}else{
		var url="assets/print/sms-download.php?vid="+$("#smspv").val()+"&type=download";
		}
			
		$("#sms_print_modal").modal("hide");


var time1=Math.round(new Date().getTime() / 1000);

	//var win=window.open(url, "", "directories=no,menubar=yes,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
window.location=url;
}

					});
					}

	});





		//$("#addendum_print_modal").modal("show");

//window.open('assets/import/print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;
	

});

$('.print-sms').click(function() {
if($("#smsom").val()=="multiprint"){

		var id=$("#multipleinput").val();

		var values=id+"&type=multiprint";

		}else{
		var id=$("#smspv").val();

		var values="id="+id;
		}
		
			 $.ajax({

			    	url: "assets/ajax/sms_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					VehicleTable.ajax.reload( null, false);
	if($("#smsom").val()=="multiprint"){
		var url="assets/print/sms-multiple-download.php?"+$("#multipleinput").val()+"&type=print";
		}else{
		var url="assets/print/sms-download.php?vid="+$("#smspv").val()+"&type=print";
		}
					
					$("#sms_print_modal").modal("hide");

var time1=Math.round(new Date().getTime() / 1000);

var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari4-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

	}

	});





		//$("#addendum_print_modal").modal("show");

//window.open('assets/import/print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;
	

});

$('#print_guide').click(function() {

			$('#buyers_guide_print').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){				

        

	VehicleTable.ajax.reload( null, false);
	if($("#pbgcheck").val()=="multiprint"){
        var url="assets/print/guide-multiple-download.php?bgtype=print&"+$("#multipleinput").val();
	}else{
		var url="assets/print/guide-download.php?bgtype=print&vid="+$("#pbgvid").val();
	}

			$("#buyer_guide_print").modal("hide");

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari4-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

		}

    });

});

 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

    var target = $(e.target).attr("href");

	if(target=="#asis2"){

		$(".download_guide2").removeAttr("style");
		$(".print_guide2").removeAttr("style");

	}else if(target=="#implied2"){

	$(".download_guide2").removeAttr("style");
	$(".print_guide2").removeAttr("style");

	}else if(target=="#warranty2"){

	$(".download_guide2").removeAttr("style");
	$(".print_guide2").removeAttr("style");

	}else if(target=="#warranty2_s"){

	$(".download_guide2").removeAttr("style");
	$(".print_guide2").removeAttr("style");

	}else if(target=="#back_page_s"){

	$(".download_guide2").attr("style","visibility:hidden");
	$(".print_guide2").attr("style","visibility:hidden");

	}

    })

$('#print_backpage').click(function() {

var bpaddress=$("#bpaddress").val();

var bpdealer=$("#bpdealer").val();

var bpcomplaints=$("#bpcomplaints").val();

 var data = $("#buyers_guide" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari5-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});


$('#print_backpage_p').click(function() {

var bpaddress=$("#bpaddress_p").val();

var bpdealer=$("#bpdealer_p").val();

var bpcomplaints=$("#bpcomplaints_p").val();

 var data = $("#buyers_guide_print" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari5-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});

$('#download_backpage').click(function() {

var bpaddress=$("#bpaddress").val();

var bpdealer=$("#bpdealer").val();

var bpcomplaints=$("#bpcomplaints").val();

 var data = $("#buyers_guide" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){	

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download-spanish.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari6-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});

$('#download_backpage_p').click(function() {

var bpaddress=$("#bpaddress_p").val();

var bpdealer=$("#bpdealer_p").val();

var bpcomplaints=$("#bpcomplaints_p").val();

 var data = $("#buyers_guide_print" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){	

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download-spanish.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari6-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "https://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});

$('#save_guide').click(function() {

			$('#buyers_guide_print').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){
			$("#buyer_guide_print").modal("hide");

             swal("Thank You!", "Buyers guide successfully updated.", "success");

			 $(".btnbgp").html("Close");

		}

		}

});

});



$('.download-ipdf').click(function() {

		if($("#iom").val()=="single"){

		var id=$("#ipv").val();

		var values="id="+id+"&type=single&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}else if($("#iom").val()=="multiprint"){

			var id=$("#ipv").val();

		var values=id+"&type=multiprint&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}

		$.ajax({

			

			    	url: "assets/ajax/info_print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					VehicleTable.ajax.reload( null, false);

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);



					var printstart=$("#printstart").val();

							if($("#iom").val()=="single"){

	var url="assets/print/info_print_download.php?dtype=single&printstart="+printstart+"&vid="+$("#ipv").val()+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}else if($("#iom").val()=="multiprint"){

	var url="assets/print/info-multiprint-download.php?"+$("#ipv").val()+"&type=multiprint&dtype=single&printstart="+printstart+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}

					$("#info_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

	var time1=Math.round(new Date().getTime() / 1000);

	window.location=url;

					
					}

					});

					

					



					}

					

		});

						





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

});

	$('.download-pdf').click(function() {

		if($("#som").val()=="single"){

		var id=$("#spv").val();

		var values="id="+id+"&type=single";

		}else if($("#som").val()=="multiprint"){

			var id=$("#multipleinput").val();

		var values=id+"&type=multiprint";

		}

			 $.ajax({

			    	url: "assets/ajax/print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					VehicleTable.ajax.reload( null, false);

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);



					var printstart=$("#printstart").val();

					$("#printstart").val("1");

					if($("#som").val()=="single"){
if($("#addcheck").val()=="Dealer Branded Savings"){
	//var url="assets/print/single-download.php?vid="+$("#spv").val()+"&dtype=single&printstart="+printstart;
	var url="assets/print/muliple-download.php?print%5B0%5D="+$("#spv").val()+"&type=multiprint&dtype=multiple"+"&printstart="+printstart+"&cdjr="+$("#cdjrcheck").val();
}else{
var url="assets/print/muliple-download.php?print%5B0%5D="+$("#spv").val()+"&type=multiprint&dtype=multiple"+"&printstart="+printstart;
}
					}else if($("#som").val()=="multiprint"){
if($("#addcheck").val()=="Dealer Branded Savings"){
	//var url="assets/print/single-download.php?vid="+$("#spv").val()+"&dtype=single&printstart="+printstart;
	var url="assets/print/muliple-download.php?"+$("#multipleinput").val()+"&type=multiprint&dtype=multiple&printstart="+printstart+"&cdjr="+$("#cdjrcheck").val();
}else{
var url="assets/print/muliple-download.php?"+$("#multipleinput").val()+"&type=multiprint&dtype=multiple&printstart="+printstart;
}

	
					}
					$("#addendum_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

var time1=Math.round(new Date().getTime() / 1000);

	//var win=window.open(url, "", "directories=no,menubar=yes,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
window.location=url;
					
					}

					});

					 

					

					

					
	

	}

	});





		//$("#addendum_print_modal").modal("show");

//window.open('assets/import/print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});





	   $('#vehicle_check').change(function() {   

        if (this.checked) {

          $('.delcheck').prop("checked", true);

	$('.vehicle_multiple').removeClass("disabled");
	$('.vehicle_multiple').removeAttr("disabled");

        }else {

            $('.delcheck').removeAttr("checked");

	$('.vehicle_multiple').addClass("disabled");
	$('.vehicle_multiple').attr("disabled","disabled");


        }

    });

	

	 $('#DataTables_Table_3_next').click(function() {   



            $('#vehicle_check').removeAttr("checked");

			$('.delcheck').removeAttr("checked");

			$('.vehicle_multiprint').removeAttr("disabled");

			$('.vehicle_multidelete').removeAttr("disabled");

   

    });

		 $('#DataTables_Table_3_previous').click(function() {   



            $('#vehicle_check').removeAttr("checked");

			$('.delcheck').removeAttr("checked");

			$('.vehicle_multidelete').removeAttr("disabled");

			$('.vehicle_multiprint').removeAttr("disabled");

   

    });

	$('.multiselect').each(function(){ 

    var select = $(this), values = {};    

    $('option',select).each(function(i, option){

        values[option.value] = option.selected;        

    }).click(function(event){        

        values[this.value] = !values[this.value];

        $('option',select).each(function(i, option){            

            option.selected = values[option.value];        

        });    

    });

});

/*$(".pick-a-color").pickAColor({

			  showSpectrum            : true,

				showSavedColors         : true,

				saveColorsPerElement    : true,

				fadeMenuToggle          : true,

				showAdvanced			: true,

				showBasicColors         : true,

				showHexInput            : true,

				allowBlank				: true,

				inlineDropdown			: true

});*/

if ( $(window).width() < 1024) {   

 $(".ipadprint").addClass("offset3");

 $(".ipadnew").removeClass("span2");

 $(".ipadnew").addClass("span3");

 $(".ipadlist").removeClass("span2");

 $(".ipadlist").addClass("span3");

 $(".ipaddetails").removeClass("span3");

 $(".ipaddetails").addClass("span5");

}

$(window).resize(function() {

if ( $(window).width() < 1024) {   

 $(".ipadprint").addClass("offset3");

 $(".ipadnew").removeClass("span2");

 $(".ipadnew").addClass("span3");

 $(".ipadlist").removeClass("span2");

 $(".ipadlist").addClass("span3");

 $(".ipaddetails").removeClass("span3");

 $(".ipaddetails").addClass("span5");

}

else if ( $(window).width() >= 1024) {

   $(".ipadprint").removeClass("offset3");

 $(".ipadnew").addClass("span2");

 $(".ipadnew").removeClass("span3");

 $(".ipadlist").addClass("span2");

 $(".ipadlist").removeClass("span3");

 $(".ipaddetails").addClass("span3");

 $(".ipaddetails").removeClass("span5");

}

});
$('#extrim').change(function() {  
if($('#extrim').val()==""){
	$("#inputTrim").val();
	$("#inputTrim").focus();
}else if($('#extrim').val()=="Unknown"){
	$('#extrim').attr("placeholder", "Choose Trim");
	$("#inputTrim").val();
}else{
	$("#inputTrim").val($('#extrim').val());
	$("#inputTrim").focus();
}
});

$('#extcolor').change(function() {  
if($('#extcolor').val()==""){
	$("#inputColor").val();
	$("#inputColor").focus();
}else if($('#extcolor').val()=="Unknown"){
	$('#extcolor').attr("placeholder", "Choose Trim");
	$("#inputColor").val();
}else{
	$("#inputColor").val($('#extcolor').val());
	$("#inputColor").focus();
}
});

$('#extcolor2').change(function() {  
if($('#extcolor2').val()==""){
	$("#inputColor2").val();
	$("#inputColor2").focus();
}else if($('#extcolor2').val()=="Unknown"){
	$('#extcolor2').attr("placeholder", "Choose Trim");
	$("#inputColor2").val();
}else{
	$("#inputColor2").val($('#extcolor2').val());
	$("#inputColor2").focus();
}
});


});

function decodeHTMLEntities(text) {

    var entities = [

        ['apos', '\''],

        ['amp', '&'],

        ['lt', '<'],

        ['gt', '>']

    ];



    for (var i = 0, max = entities.length; i < max; ++i) 

        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);



    return text;

}


function vehicle_multiple(){
	
			var vehicle_multiselect=$( ".vehicle_multiselect option:selected" ).text();
			if(vehicle_multiselect=="Delete"){

		$("#multiaction").val("multidelete");

var r=confirm("Do you really want to delete selected entry(s) ?");

if (r==true)

  {

	  var data = $("#multi_delete" ).serialize();
	  data=data+"&multi=multidelete";

 $.ajax({

			    	url: "assets/ajax/vehicle_delete.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

						

		if($.trim(responseText) =="no"){

            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");

		}else if($.trim(responseText) =="yes"){

			swal("Thank You!", "Selected vehicle deleted.", "success");
			 VehicleTable.ajax.reload( null, false);

    $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					 

		}

		            $('.delcheck').removeAttr("checked");

	$('.vehicle_multiple').addClass("disabled");
	$('.vehicle_multiple').attr("disabled","disabled");


						}

					});

  }
		}else if(vehicle_multiselect=="Print Addendum"){
			

		$("#multiaction").val("multiprint");

			  var data = $("#multi_delete" ).serialize();
			  data=data+"&multi=multiprint";

 $.ajax({

			    	url: "assets/ajax/vehicle_delete.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

						var values="type=multiple&"+$.trim(responseText);

		$.ajax({

			    	url: "assets/ajax/get_free_cap.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="ok"){		

				  $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  
		$("#multipleinput").val($.trim(responseText));

		$.ajax({

			    	url: "assets/ajax/addendum_check.php",

			    	type: "get",

					cache:false,

			      	success: function(addendum_check){

					if($.trim(addendum_check)=="QR Label"){

					var id=$("#multipleinput").val();

		var url="assets/print/multiprint.php?"+id+"&uid="+Date.now();;

		$(".final_print").val(url);

		$("#spv").val(id);

		$("#som").val("multiprint");

		$("#qr_label").html('<div class="qr_label1">&nbsp;</div><div class="qr_label2"><strong><span class="countingadd"></span><br><br>Start Printing Here:</strong><label><input type="radio" name="qr_label" onclick="qr_label();" value="1"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="2"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="3"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="4"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="5"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="6"/><div>&nbsp;</div></label></div>');

		var len = $("input[name='delcheck[]']:checked").length;

		$(".countingadd").html("You are printing "+len+" QR Label.");
		VehicleTable.ajax.reload( null, false);

		$(".qr_label1").load(url);

		$('#addendum_print_modal').modal({

  keyboard: false

},"show");

					}else{

						var id=$("#multipleinput").val();

		var url="assets/print/multiprint.php?"+id+"&uid="+Date.now();

		$(".final_print").val(url);

		$("#mpv").val(id);

		$("#som").val("multiprint");
		VehicleTable.ajax.reload( null, false);

		$('#addendum_print_modal').modal({

  keyboard: false

},"show");
$("#my_print_modal").attr("src", url);
}

					}

		});

//window.open('multiprint.php?'+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

						}else if(html=="no"){


							sweetAlert("Oops...", "I'm afraid you have reached your printing or time limit for this free account. Please upgrade now or call support at 801-415-9435 to request an extension.", "error"); 
							

						}

					}

					});

						}

		 });

	
		}else if(vehicle_multiselect=="Print SMS"){
		$("#multiaction").val("multiprint");

			  var data = $("#multi_delete" ).serialize();
			  data=data+"&multi=multiprint";

 $.ajax({

			    	url: "assets/ajax/vehicle_delete.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

						var values="type=multiple&"+$.trim(responseText);

		$.ajax({

			    	url: "assets/ajax/get_free_cap.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="ok"){		

		$("#multipleinput").val($.trim(responseText));

		$.ajax({

			    	url: "assets/ajax/addendum_check.php",

			    	type: "get",

					cache:false,

			      	success: function(addendum_check){


						var id=$("#multipleinput").val();
		var url="assets/print/sms_multiprint.php?"+id+"&uid="+Date.now();
		$(".final_print").val(url);
		$("#mpv").val(id);
		$("#smsom").val("multiprint");

		$('#sms_print_modal').modal({

  keyboard: false

},"show");

$("#my_sms_modal").attr("src", url);

					}

		});

//window.open('multiprint.php?'+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

						}else if(html=="no"){


							sweetAlert("Oops...", "I'm afraid you have reached your printing or time limit for this free account. Please upgrade now or call support at 801-415-9435 to request an extension.", "error"); 
							

						}

					}

					});

						}

		 });

	
		}else if(vehicle_multiselect=="Print Buyer Guide"){
		$("#multiaction").val("multiprint");
		$("#pbgcheck").val("multiprint");
			  var data = $("#multi_delete" ).serialize();
			  data=data+"&multi=multiprint";

 $.ajax({

			    	url: "assets/ajax/vehicle_delete.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

						var values="type=multiple&"+$.trim(responseText);

		$.ajax({

			    	url: "assets/ajax/get_free_cap.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="ok"){
		$("#multipleinput").val($.trim(responseText));
		$("#pbgmultiple").val($.trim(responseText));
		$("#pbgvid").val("No");

		$(".btnbgp").html("Cancel");

		var values="vid=No";

		$.ajax({

			    	url: "assets/ajax/print_guide.php",

			    	type: "get",

					data: values,

					cache:false,

					dataType: 'json',

			      	success: function(html){

						$('#buyer_guide_print').modal('show');

						for (var x = 0; x < html.length; x++) {

				if(html[x].DEFAULT_GUIDE=="asis"){

					$(".dgasis").html("Yes");

					$(".dgimplied").html("No");

					$(".dgwarranty").html("No");

					$(".dgspanish").html("No");

					$('.nav-tabs a[href="#asis2"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}else if(html[x].DEFAULT_GUIDE=="implied"){

					$(".dgasis").html("No");

					$(".dgimplied").html("Yes");

					$(".dgwarranty").html("No");

					$(".dgspanish").html("No");

					$('.nav-tabs a[href="#implied2"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}else if(html[x].DEFAULT_GUIDE=="warranty"){

					$(".dgasis").html("No");

					$(".dgimplied").html("No");

					$(".dgwarranty").html("Yes");

					$(".dgspanish").html("No");

					$('.nav-tabs a[href="#warranty2"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}else if(html[x].DEFAULT_GUIDE=="warranty_spanish"){

					$(".dgasis").html("No");

					$(".dgimplied").html("No");

					$(".dgwarranty").html("No");

					$(".dgspanish").html("Yes");

					$('.nav-tabs a[href="#warranty2_s"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}

				if(html[x].SC_ASIS=="1"){

					$("[name='sc_asis2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_asis2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_IMPLIED=="1"){

					$("[name='sc_implied2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_implied2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_WARRANTY=="1"){

					$("[name='sc_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_warranty2']").bootstrapSwitch('setState', false);

				}
				
				if(html[x].M_WARRANTY=="1"){

					$("[name='M_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='M_warranty2']").bootstrapSwitch('setState', false);

				}
				
				if(html[x].UV_WARRANTY=="1"){

					$("[name='UV_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='UV_warranty2']").bootstrapSwitch('setState', false);

				}
				
				if(html[x].OUV_WARRANTY=="1"){

					$("[name='OUV_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='OUV_warranty2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_WARRANTY_S=="1"){

					$("[name='sc_warranty2_s']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_warranty2_s']").bootstrapSwitch('setState', false);

				}

				if(html[x].WARRANTY_TYPE=="FULL"){

					$("[name='warrantytype2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='warrantytype2']").bootstrapSwitch('setState', false);

				}

				if(html[x].WARRANTY_TYPE_S=="FULL"){

					$("[name='warrantytype2_s']").bootstrapSwitch('setState', true);

				}else{

					$("[name='warrantytype2_s']").bootstrapSwitch('setState', false);

				}

				$("#inputparts2").val(html[x].PARTS);

				$("#inputlabor2").val(html[x].LABOR);
				$("#bpphone_p").val(html[x].PHONE);
				$("#bpemail_p").val(html[x].EMAIL);
				$("#bpcomplaints_p").val(html[x].COMPLAINTS);
				var sco2=html[x].SYSTEMS_COVERED.replace(/(<br ?\/?>)*/g,"");
				$("#systemscovered2").html(sco2).text
				var dur2=html[x].DURATION.replace(/(<br ?\/?>)*/g,"");

				//$('#eadescription').html(pdesc).text();
				$("#duration2").html(dur2).text;

				$("#inputparts2_s").val(html[x].PARTS_S);

				$("#inputlabor2_s").val(html[x].LABOR_S);
				var sco2s=html[x].SYSTEMS_COVERED_S.replace(/(<br ?\/?>)*/g,"");
				$("#systemscovered2_s").html(sco2s).text;
				var dur2s=html[x].DURATION_S.replace(/(<br ?\/?>)*/g,"");
				$("#duration2_s").html(dur2s).text;

            }

					}

		});

	}else if(html=="no"){


							sweetAlert("Oops...", "I'm afraid you have reached your printing or time limit for this free account. Please upgrade now or call support at 801-415-9435 to request an extension.", "error"); 
							

						}

					}

					});

						}

		 });

	
		}else if(vehicle_multiselect=="Print Infosheet"){
		$("#iom").val("multiprint");
		$("#wed").val("");
		$("#wed").hide();
		$("#infotype").val("standard");
		$("#multiaction").val("multiprint");
			  var data = $("#multi_delete" ).serialize();
			  data=data+"&multi=multiprint";

 $.ajax({

			    	url: "assets/ajax/vehicle_delete.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

						var values="type=multiple&"+$.trim(responseText);

		$.ajax({

			    	url: "assets/ajax/get_free_cap.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="ok"){		

		$("#multipleinput").val($.trim(responseText));

					var id=$("#multipleinput").val();
		//var url="assets/print/info_multiprint.php?"+id+"&uid="+Date.now();
		//$(".final_print").val(url);
		//$("#mpv").val(id);
		//$("#smsom").val("multiprint");

		//var url="assets/print/info_multiprint.php?"+id+"&INFO_TYPE=standard&uid="+Date.now();

		$("#ipv").val(id);
		$("#istandard").trigger("click");
		$('#info_print_modal').modal({

  keyboard: false

},"show");

$("#my_info_modal").attr("src", url);

//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;
			


//window.open('multiprint.php?'+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

						}else if(html=="no"){


							sweetAlert("Oops...", "I'm afraid you have reached your printing or time limit for this free account. Please upgrade now or call support at 801-415-9435 to request an extension.", "error"); 
							

						}

					}

					});

						}

		 });

			}
		
}
	function edtvehicle(veid){

var id=veid;

	$('#vehicle_edit_form').modal('show');

	

  				/* get some values from elements on the page: */

   				var values = "veid="+id+"&option=get_vehicle";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/vehicle_edit.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#edtid').val(html[x].id);

				$('#edtStockNumber').val(html[x].STOCK_NUMBER);

				$('#edtVINNumber').val(html[x].VIN_NUMBER);

				$('#edtPrice').val(html[x].MSRP);

				$('#edtYear').val(html[x].YEAR);

				$('#edtMake').val(html[x].MAKE);

				$('#edtMileage').val(html[x].MILEAGE);

				$('#edtTrim').val(html[x].TRIM);

				$('#edtModel').val(html[x].MODEL);

				$('#edtStyle').val(html[x].BODYSTYLE);

				$('#edtColor').val(html[x].EXT_COLOR);

				$('#edtTransmission').val(html[x].TRANSMISSION);

				$('#edtEngine').val(html[x].ENGINE);

				$('#edtDrivetrain').val(html[x].DRIVETRAIN);
				$('#edtColor2').val(html[x].INT_COLOR);
				$('#edtDoors').val(html[x].DOORS);
				$('#edtEnginetype').val(html[x].FUEL);
				$('#edtMPGHigh').val(html[x].HMPG);
				$('#edtMPGCity').val(html[x].CMPG);

				$('#edtDate').val(html[x].DATE_IN_STOCK);

				$('#edtOption').val(html[x].OPTIONS);
				$('#INSP_NUMB').val(html[x].INSP_NUMB);

				

				$('#edtDescription').html(decodeHTMLEntities(html[x].DESCRIPTION));

//$("select#edtStyle option").each(function() { this.selected = (this.text == html[x].BODYSTYLE); });

//$("select#edtModel option").each(function() { this.selected = (this.text == html[x].MODEL); });

$("select#edtVehicletype option").each(function() { this.selected = (this.text == html[x].NEW_USED); });

            }// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

		

			

	}

	

	function addendumedit(adid){

var id=adid;

	$('#addendum_edit').modal('show');

	$("#eainputstyle option:selected").removeAttr("selected");

	$("#eainputmodel option:selected").removeAttr("selected");

  				/* get some values from elements on the page: */

   				var values = "adid="+id+"&option=get_addendum";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/addendum_edit.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache: false,

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#eaid').val(html[x].AD_ID);
				$('#eaprice').val(html[x].PRICE);

				$('#inputeAdtype').val(html[x].AD_TYPE);
				var pname = html[x].ITEM_NAME.replace(/(<br ?\/?>)*/g,"");

				$('#eaitemname').val($('<div>').html(pname).text());
				if(html[x].SEPARATOR_BELOW=="0"){
					$('#separator_below2').prop('checked', false);
				}else if(html[x].SEPARATOR_BELOW=="1"){
					$('#separator_below2').prop('checked', true);
				}
				
				if(html[x].SEPARATOR_ABOVE=="0"){
					$('#separator_above2').prop('checked', false);
				}else if(html[x].SEPARATOR_ABOVE=="1"){
					$('#separator_above2').prop('checked', true);
				}

				var pdesc = html[x].ITEM_DESCRIPTION.replace(/(<br ?\/?>)*/g,"");

				$('#eadescription').html(pdesc).text();

				var mystyle="";

					for(i=0;i<=7;i++){

					var selectval='html[x].BODY_STYLE_'+i;

					 var valum=eval(selectval);

					 if(i==1){

						 var separate="";

					 }else{

						 var separate=", ";

					 }

					 if((typeof(valum) != 'undefined')){

						 if(valum!=""){

					 var mystyle=mystyle+separate+'"'+valum+'"';

						 }

					 }

					}

					var mystyle="["+mystyle+"]";

					$("#eainputstyle").val(eval(mystyle)).trigger("change");

					var mymodel="";

					for(i=0;i<=15;i++){

						var selectval='html[x].MODEL_'+i;

						

					 var valum=eval(selectval);

					 if(i==1){

						 var separate="";

					 }else{

						 var separate=", ";

					 }

					 if((typeof(valum) != 'undefined')){

						 if(valum!=""){

					 var mymodel=mymodel+separate+'"'+valum+'"';
					 

						 }

					 }

					}

					var mymodel="["+mymodel+"]";

					$("#eainputmodel").val(eval(mymodel)).trigger("change");

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

		

			

	}

	function groupedit(adid){
	var id=adid;
	$('#group_edit').modal('show');

	$("#eginputstyle option:selected").removeAttr("selected");

	$("#eginputmodel option:selected").removeAttr("selected");

  				/* get some values from elements on the page: */

   				var values = "adid="+id+"&option=get_group";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/group_edit.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache: false,

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#egid').val(html[x].AD_ID);

				$('#egprice').val(html[x].PRICE);

				$('#inputeAdtypeg').val(html[x].AD_TYPE);
				var pname = html[x].ITEM_NAME.replace(/(<br ?\/?>)*/g,"");

				$('#egitemname').val($('<div>').html(pname).text());
				if(html[x].SEPARATOR_BELOW=="0"){
					$('#separator_below2g').prop('checked', false);
				}else if(html[x].SEPARATOR_BELOW=="1"){
					$('#separator_below2g').prop('checked', true);
				}
				
				if(html[x].SEPARATOR_ABOVE=="0"){
					$('#separator_above2g').prop('checked', false);
				}else if(html[x].SEPARATOR_ABOVE=="1"){
					$('#separator_above2g').prop('checked', true);
				}
				var egdesc='[';
				var s = html[x].ITEM_DESCRIPTION;
    var match = s.split(',')
    for (var a in match)
    {
		var matchvalue=match[a];
		matchvalue=matchvalue.replace(/"/g, '\\"');
        egdesc =egdesc+'"'+matchvalue+'",';
    }
	egdesc=egdesc.replace(/,\s*$/, "");
	egdesc=egdesc+']';
				$("#eginputdesc").val(eval(egdesc)).trigger("change");

				var mystyle="";

					for(i=0;i<=7;i++){

					var selectval='html[x].BODY_STYLE_'+i;

					 var valum=eval(selectval);

					 if(i==1){

						 var separate="";

					 }else{

						 var separate=", ";

					 }

					 if((typeof(valum) != 'undefined')){

						 if(valum!=""){

					 var mystyle=mystyle+separate+'"'+valum+'"';

						 }

					 }

					}

					var mystyle="["+mystyle+"]";

					$("#eginputstyle").val(eval(mystyle)).trigger("change");

					var mymodel="";

					for(i=0;i<=15;i++){

						var selectval='html[x].MODEL_'+i;

						

					 var valum=eval(selectval);

					 if(i==1){

						 var separate="";

					 }else{

						 var separate=", ";

					 }

					 if((typeof(valum) != 'undefined')){

						 if(valum!=""){

					 var mymodel=mymodel+separate+'"'+valum+'"';

						 }

					 }

					}

					var mymodel="["+mymodel+"]";

					$("#eginputmodel").val(eval(mymodel)).trigger("change");

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

		

			

	}

function vehicle(veid){

var id=veid;

var r=confirm("Do you really want to delete this entry ?");

if (r==true)

  {

	 var values1 = "veid="+id+"&option=vehicle";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="no"){
						sweetAlert("Oops...", "Something unexpected happened. Please try again", "error"); 	
			      	} 

					else{

					var duser=html+"tr";

					$("#"+duser).remove();

					 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  

					}

					}

				});  

  }

	}

	

	function addendumdel(adid){

var id=adid;

var r=confirm("Do you really want to delete this entry ?");

if (r==true)

  {

	 var values1 = "adid="+id+"&option=ad";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="no"){

						sweetAlert("Oops...", "Something unexpected happened. Please try again", "error"); 

			      	} 

					else{

					var duser="ad"+html;

					$("#"+duser).remove();

					}

					}

				});  

  }

	}

	function groupdel(adid){

var id=adid;

var r=confirm("Do you really want to delete this entry ?");

if (r==true)

  {

	 var values1 = "adid="+id+"&option=ad";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="no"){

						sweetAlert("Oops...", "Something unexpected happened. Please try again", "error"); 

			      	} 

					else{

					var duser="og"+html;

					$("#"+duser).remove();

					}

					}

				});  

  }

	}

	

	function sendpassword(id){

		var id=id;

	 var values1 = "cuid="+id+"&option=send_password";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/send_password.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

					if(html=="yes"){
						 swal("Thank You!", "Password Successfully sent!", "success");
			      	} 

					else{
						sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 

					}

					}

				}); 

	}

	

	function useredit(id){

		var id=id;

	$('#user_edit').modal('show');

	

  				/* get some values from elements on the page: */

   				var values = "cuid="+id+"&option=get_user";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/get_user.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#ename').val(html[x].NAME);

				$('#eid').val(html[x].USER_ID);

				$('#eusername').val(html[x].USERNAME);

				$('#eemail').val(html[x].EMAIL);
				if(html[x].USER_IMAGE==""){
					
					$('#profilePicture2').val("https://placehold.it/100x100");
				}else{
				$('#profilePicture2').val(html[x].USER_IMAGE);
				}
				
				$('#inputprofile2').attr("src","https://d1xlji8qxtrdmo.cloudfront.net/"+html[x].USER_IMAGE);
				
				$('#vehicle_status2').val(html[x].ACTIVE_INACTIVE);

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

	}

	

	function userdelete(id){

		var id=id;

var r=confirm("Do you really want to delete this user?");

if (r==true)

  {

	 var values1 = "cuid="+id+"&option=user";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="no"){

						sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 

			      	} 

					else{

					var duser="u"+html;

					$("#"+duser).remove();

					 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					}

					}

				});

  }

	}

	function printguide(vid){
		$("#pbgmultiple").val("");
		$("#pbgcheck").val("");

		$("#pbgvid").val(vid);

		$(".btnbgp").html("Cancel");

		var values="vid="+vid;

		$.ajax({

			    	url: "assets/ajax/print_guide.php",

			    	type: "get",

					data: values,

					cache:false,

					dataType: 'json',

			      	success: function(html){

						$('#buyer_guide_print').modal('show');

						for (var x = 0; x < html.length; x++) {

				if(html[x].DEFAULT_GUIDE=="asis"){

					$(".dgasis").html("Yes");

					$(".dgimplied").html("No");

					$(".dgwarranty").html("No");

					$(".dgspanish").html("No");

					$('.nav-tabs a[href="#asis2"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}else if(html[x].DEFAULT_GUIDE=="implied"){

					$(".dgasis").html("No");

					$(".dgimplied").html("Yes");

					$(".dgwarranty").html("No");

					$(".dgspanish").html("No");

					$('.nav-tabs a[href="#implied2"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}else if(html[x].DEFAULT_GUIDE=="warranty"){

					$(".dgasis").html("No");

					$(".dgimplied").html("No");

					$(".dgwarranty").html("Yes");

					$(".dgspanish").html("No");

					$('.nav-tabs a[href="#warranty2"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}else if(html[x].DEFAULT_GUIDE=="warranty_spanish"){

					$(".dgasis").html("No");

					$(".dgimplied").html("No");

					$(".dgwarranty").html("No");

					$(".dgspanish").html("Yes");

					$('.nav-tabs a[href="#warranty2_s"]').tab('show');
					$('#dg2').val(html[x].DEFAULT_GUIDE);

				}

				if(html[x].SC_ASIS=="1"){

					$("[name='sc_asis2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_asis2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_IMPLIED=="1"){

					$("[name='sc_implied2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_implied2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_WARRANTY=="1"){

					$("[name='sc_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_warranty2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_WARRANTY_S=="1"){

					$("[name='sc_warranty2_s']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_warranty2_s']").bootstrapSwitch('setState', false);

				}

				if(html[x].WARRANTY_TYPE=="FULL"){

					$("[name='warrantytype2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='warrantytype2']").bootstrapSwitch('setState', false);

				}

				if(html[x].WARRANTY_TYPE_S=="FULL"){

					$("[name='warrantytype2_s']").bootstrapSwitch('setState', true);

				}else{

					$("[name='warrantytype2_s']").bootstrapSwitch('setState', false);

				}
				if(html[x].M_WARRANTY=="1"){

					$("[name='M_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='M_warranty2']").bootstrapSwitch('setState', false);

				}
				
				if(html[x].UV_WARRANTY=="1"){

					$("[name='UV_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='UV_warranty2']").bootstrapSwitch('setState', false);

				}
				
				if(html[x].OUV_WARRANTY=="1"){

					$("[name='OUV_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='OUV_warranty2']").bootstrapSwitch('setState', false);

				}

				$("#inputparts2").val(html[x].PARTS);
				$("#bpphone_p").val(html[x].PHONE);
				$("#bpemail_p").val(html[x].EMAIL);
				$("#bpcomplaints_p").val(html[x].COMPLAINTS);

				$("#inputlabor2").val(html[x].LABOR);
				var sco2=html[x].SYSTEMS_COVERED.replace(/(<br ?\/?>)*/g,"");
				$("#systemscovered2").html(sco2).text
				var dur2=html[x].DURATION.replace(/(<br ?\/?>)*/g,"");

				//$('#eadescription').html(pdesc).text();
				$("#duration2").html(dur2).text;

				$("#inputparts2_s").val(html[x].PARTS_S);

				$("#inputlabor2_s").val(html[x].LABOR_S);
				var sco2s=html[x].SYSTEMS_COVERED_S.replace(/(<br ?\/?>)*/g,"");
				$("#systemscovered2_s").html(sco2s).text;
				var dur2s=html[x].DURATION_S.replace(/(<br ?\/?>)*/g,"");
				$("#duration2_s").html(dur2s).text;

            }

					}

		});

	}

	function printinfosheet(vid){

		var id=vid;

		var url="assets/print/info_print.php?vid="+id+"&INFO_TYPE=standard&uid="+Date.now();

		$("#ipv").val(id);

		$("#iom").val("single");
		$("#wed").val("");
		$("#wed").hide();
		$("#istandard").trigger("click");
		$("#infotype").val("standard");

		$('#info_print_modal').modal({

  keyboard: false

},"show");

$("#my_info_modal").attr("src", url);

//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;





;}


function printsms(smsid){
$("#smsom").val("single");
		var smsid=smsid;

		var url="assets/print/sms_print.php?smsid="+smsid+"&uid="+Date.now();

		$("#smspv").val(smsid);

		$('#sms_print_modal').modal({

  keyboard: false

},"show");

$("#my_sms_modal").attr("src", url);

//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;
;}

function infotype(intype){
			if(intype=="certified"){
				$("#wed").show();
				$("#infotype").val("certified");
			}else{
				$("#wed").hide();
				$("#infotype").val("standard");
			}
			if($("#iom").val()=="single"){
		var url="assets/print/info_print.php?vid="+$("#ipv").val()+"&INFO_TYPE="+intype+"&uid="+Date.now();
			}else if($("#iom").val()=="multiprint"){
		var url="assets/print/info_multiprint.php?"+$("#ipv").val()+"&INFO_TYPE="+intype+"&uid="+Date.now();
			}

		//$('#info_print_modal .modal-body').load(url,function(e){$('#info_print_modal').modal('show');});
		$("#my_info_modal").attr("src", url);



//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;





;}


	function new_used1(myval){
		

		var values="myval="+myval;

		$.ajax({

			    	url: "assets/ajax/new_used.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="yes"){

							$.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});	

							VehicleTable.ajax.reload( null, false);
							}

					}

		});

		

	}

	function printvehicle(id){

		var values="type=single";

		$.ajax({

			    	url: "assets/ajax/get_free_cap.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="ok"){

		$(".print_addendum").attr("id",id);

		$(".add_new_option").attr("id",id);
		$(".add_new_group").attr("id",id);

		$('#add_option_id').val(id);
		$('#add_group_id').val(id);

		$('#option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,
						

			      	success: function(html){
						$('#item_name').html(html);
						
						if(GetPrintTable != null){
							if($("#initiate").val()!="1"){
							 GetPrintTable=$('#GetPrintTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/get_print_table.php",
		"dom": '',
		"order": [],
		"columnDefs": [ 
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];

                 
				},
                "targets": 0,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					if(row[4]=="2"){
					var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
					}else{
						return row[2];
					}
					alert(row[4]);

                 
				},
                "targets": 1,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[3]<0){

						 var mydprice=Math.abs(row[3]);

						 return   "-$"+mydprice;

					 }else if(row[3]=="np"){

						 return   row[3];

					 }else{

						  return   "$"+row[3];

					 }

                 
				},
				"className": "dt-head-right dt-body-right",
                "targets": 2,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
				 	if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+row[0]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
						if(row[4]=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pdefault('+row[0]+')">Edit</button>';
						}else{
						var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pgroup('+row[0]+')">Edit</button>';
						}
				 		
				 	}
return deet },
				"className": "dt-head-right dt-body-right",
                "targets": 3,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'pa'+aData[0]);
    }
		
    } );
							}
			
						GetPrintTable.destroy();
						
						}
 
 GetPrintTable=$('#GetPrintTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "assets/ajax/get_print_table.php",
		"dom": '',
		"order": [],
		"columnDefs": [ 
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];

                 
				},
                "targets": 0,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					if(row[4]=="2"){
					var formattedString = row[2].split(",").join("<br />")
                 return formattedString;
					}else{
						return row[2];
					}
					alert(row[4]);

                 
				},
                "targets": 1,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 if(row[3]<0){

						 var mydprice=Math.abs(row[3]);

						 return   "-$"+mydprice;

					 }else if(row[3]=="np"){

						 return   row[3];

					 }else{

						  return   "$"+row[3];

					 }

                 
				},
				"className": "dt-head-right dt-body-right",
                "targets": 2,
                "sorting": false
            },
		{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
				 	if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+row[0]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
						if(row[4]=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pdefault('+row[0]+')">Edit</button>';
						}else{
						var deet='<button type="button" class="btn btn-danger delete_vehicle btn-xs" onClick="delete_pdefault('+row[0]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle btn-xs" id="'+row[0]+'" onClick="edit_pgroup('+row[0]+')">Edit</button>';
						}
				 		
				 	}
return deet },
				"className": "dt-head-right dt-body-right",
                "targets": 3,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', 'pa'+aData[0]);
    }
		
    } );
	$("#initiate").val("1");
						
						
						}

				});

				

      				}

    			});

				$('a[href="#edit-options"]').tab('show');

						}else if(html=="no"){


							sweetAlert("Oops...", "I'm afraid you have reached your printing or time limit for this free account. Please upgrade now or call support at 801-415-9435 to request an extension.", "error"); 

						}

					}

		});



	}
	
	
	
	

	setInterval(function(){

		$.ajax({

			    	url: "assets/ajax/check_cookie.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						if(html=="yes"){

							window.location.replace("index.php");

						}else if(html=="55"){
							$('#timeoutmodal').modal('show');
						}else if(html=="60"){
							window.location="?logout=logout";
						}

					}

		});

		}, 10000);
		function stay_here(){
			$.ajax({

			    	url: "assets/ajax/check_cookie.php?stay=1",

			    	type: "get",

					cache:false,

			      	success: function(html){
						$('#timeoutmodal').modal('hide');
						}

		});
		}
		function delete_pdefault(id){

		var id=id;

var r=confirm("Do you really want to delete this option?");

if (r==true)

  {

	  var values1 = "id="+id+"&option=defaults";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					dataType: 'json',

					cache:false,

			      	success: function(html){

		

		var id=html;

		$('#add_option_id').val(id);
		$('#add_group_id').val(id);	

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);
				

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
						$('#item_name').html(html);
						GetPrintTable.ajax.reload();
						}

				});

				

      				}

    			});

				$('a[href="#edit-options"]').tab('show');

	

						}

				});

  }

	}

	

	function edit_pdefault(id){
		var id=id;

	$('#print_option_edit').modal('show');
	$('.group_show').attr("style","display:none");
	$('.group_hide').removeAttr("style");

	

  				/* get some values from elements on the page: */

   				var values = "id="+id+"&option=get_option";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/get_option_addendum.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',
					cache: false,

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {
				var pname = html[x].item_name.replace(/(<br ?\/?>)*/g,"");
				$('#pitemname').val($('<div>').html(pname).text());

				var pdesc = html[x].description.replace(/(<br ?\/?>)*/g,"");
				$('#pdesc').html("");
				

				$('#pdesc').html(pdesc).text();

				$('#pprice').val(html[x].item_price);

				$('#pvehicleid').val(html[x].vehicle_id);
				
				if(html[x].SEPARATOR_BELOW=="0"){
					$('#separator_below').prop('checked', false);
				}else if(html[x].SEPARATOR_BELOW=="1"){
					$('#separator_below').prop('checked', true);
				}
				
				if(html[x].SEPARATOR_ABOVE=="0"){
					$('#separator_above').prop('checked', false);
				}else if(html[x].SEPARATOR_ABOVE=="1"){
					$('#separator_above').prop('checked', true);
				}

				$('#edit_option_id').val(html[x].add_id);
				$('#edit_type').val(html[x].OG_OR_AD);
				$('#spaces').val(html[x].SEPARATOR_SPACES);

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

	}

	function edit_pgroup(id){
		var id=id;

	$('#print_option_edit').modal('show');
	$('.group_hide').attr("style","display:none");
	$('.group_show').removeAttr("style");

  				/* get some values from elements on the page: */

   				var values = "id="+id+"&option=get_option";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/get_option_addendum.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',
					cache: false,

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {
				var pname = html[x].item_name.replace(/(<br ?\/?>)*/g,"");
				$('#pitemname').val($('<div>').html(pname).text());
				var egdesc='[';
				var s = html[x].description;
    var match = s.split(',')
    for (var a in match)
    {
        egdesc =egdesc+'"'+match[a]+'",';
    }
	egdesc=egdesc.replace(/,\s*$/, "");
	egdesc=egdesc+']';
	$('#pidesc').html("");
				$("#pidesc").val(eval(egdesc)).trigger("change");

				$('#pprice').val(html[x].item_price);

				$('#pvehicleid').val(html[x].vehicle_id);
				
				if(html[x].SEPARATOR_BELOW=="0"){
					$('#separator_below').prop('checked', false);
				}else if(html[x].SEPARATOR_BELOW=="1"){
					$('#separator_below').prop('checked', true);
				}
				
				if(html[x].SEPARATOR_ABOVE=="0"){
					$('#separator_above').prop('checked', false);
				}else if(html[x].SEPARATOR_ABOVE=="1"){
					$('#separator_above').prop('checked', true);
				}

				$('#edit_option_id').val(html[x].add_id);
				$('#edit_type').val(html[x].OG_OR_AD);

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

	}

	function delcheck(){

		

	if($(".delcheck:checked").length > 0)

{

	$('.vehicle_multiple').removeClass("disabled");
	$('.vehicle_multiple').removeAttr("disabled");




}

else

{

$('.vehicle_multiple').addClass("disabled");
$('.vehicle_multiple').attr("disabled","disabled");


}

	 

	}

	function qr_label(){

	$("#printstart").val($("input[name=qr_label]:checked").val());

	}
	function price_help(){
	swal({
  title: "Here's a trick!",
  text: "Write:<br /><b>NC</b> to print <b>No Charge</b><br /><b>NP</b> to print Nothing<br /><b>FR</b> to print <b>Free</b><br /><b>IN</b> or <b>inc</b> to print <b>Included</b>",
  html: true
});
	}

	function copy_addendum(){

	$("#used_addendum_setting_form input[name=text_color]").val($("#addendum_setting_form input[name=text_color]").val() );

	$("#acselect2").val($(".acselect option:selected").val() );

	$("#barselect2").val($(".barselect option:selected").val() );
	$("#total_addsu").val($(".total_adds option:selected").val() );

	$("#qrselect2").val($(".qrselect option:selected").val() );
	$(".font-size2").val($(".font_size1 option:selected").val());
	
	$("#unbcselect").val($(".unbcselect option:selected").val() );

	$("#used_addendum_setting_form input[name=background_color]").val($("#addendum_setting_form input[name=background_color]").val() );

	$("#used_addendum_setting_form input[name=slogan]").val($("#addendum_setting_form input[name=slogan]").val() );

	$("#used_addendum_setting_form input[name=qr_address]").val($("#addendum_setting_form input[name=qr_address]").val() );

	$("#used_addendum_setting_form input[name=qr_title]").val($("#addendum_setting_form input[name=qr_title]").val() );

	$("#used_addendum_setting_form input[name=qr_desc]").val($("#addendum_setting_form input[name=qr_desc]").val() );

	$("#used_addendum_setting_form input[name=left_margin]").val($("#addendum_setting_form input[name=left_margin]").val() );

	$("#used_addendum_setting_form input[name=right_margin]").val($("#addendum_setting_form input[name=right_margin]").val() );

	$("#used_addendum_setting_form input[name=top_margin]").val($("#addendum_setting_form input[name=top_margin]").val() );

	$("#used_addendum_setting_form input[name=bottom_margin]").val($("#addendum_setting_form input[name=bottom_margin]").val() );

	$("#used_addendum_setting_form input[name=adjustment]").val($("#addendum_setting_form input[name=adjustment]").val() );

	$('#uinputlogo').attr( 'src',"https://d1xlji8qxtrdmo.cloudfront.net/"+$("#logofile").val());

	$('#logofile2').val($("#logofile").val());
	
	$('#ufile_c1bg1').val($("#file_c1bg").val());
	
	$('#ufile_c1qrbg1').val($("#file_c1qrbg").val());
	
	$('#ufile_c2bg1').val($("#file_c2bg").val());
	$('#ufile_c3bg1').val($("#file_c3bg").val());
	$('#ufile_c5bg1').val($("#file_c5bg").val());
	$('#ufile_nc1bg1').val($("#file_nc1bg").val());
	$('#ufile_nc2bg1').val($("#file_nc2bg").val());
	
	$('#ufile_c2qrbg1').val($("#file_c2qrbg").val());
	$('#ufile_file_wholesalebg1').val($("#file_wholesalebg").val());
	
	$(".uuselect").val($(".uselect option:selected").val() );

	

		

		 if($(".uselect").val()=="EPA Template"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Branded"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Savings"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").removeAttr("style"); 

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Simple"){

				 $(".uaccheck").removeAttr("style");                  $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Used"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="PrePrinted"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Wide"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Narrow"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT Narrow Series S"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Two Up"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="QR Label"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});
				 

				 $(".umrcheck").removeAttr("style");
				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1" || $(".uselect").val()=="Custom1p"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").removeAttr("style");
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 1"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").removeAttr("style");
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 2" || $(".uuselect").val()=="Narrow Custom 2p"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc2bg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 SMS"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 VIN"||$(".uselect").val()=="Custom1 VIN No MSRP"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").removeAttr("style");
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").removeAttr("style");
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom3"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").removeAttr("style");
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom5"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom6"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").removeAttr("style");
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom7"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").removeAttr("style");
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom7 Narrow"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").removeAttr("style");
				 $(".uc6check").removeAttr("style");
				 $(".total_addsu").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom4"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Wholesale"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uwholesalebg").removeAttr("style");
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 QR"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").removeAttr("style");
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 No MSRP"){

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").removeAttr("style");

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else{

				 $(".uaccheck").css({'display' : 'none'}); $(".udbscheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".unc2bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc6bg").css({'display' : 'none'});
				 $(".uc7bg").css({'display' : 'none'});
				 $(".uc7nbg").css({'display' : 'none'});
				 $(".uc6check").css({'display' : 'none'});
				 $(".total_addsu").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }


			 $("#used_addendum_setting_form input[name=text_color]").next('div.btn-group').find('.color-preview.current-color').css('background-color', '#' + $("#addendum_setting_form input[name=text_color]").val());

			 $("#used_addendum_setting_form input[name=background_color]").next('div.btn-group').find('.color-preview.current-color').css('background-color', '#' + $("#addendum_setting_form input[name=background_color]").val());

	

	}

	function apply_adjustment(){

		var vid=$("#option_id").val();

		var adj_value=$(".amsrp").val();

		var values="vid="+vid+"&adjust_value="+adj_value;

		$.ajax({

			    	url: "assets/ajax/apply_adjustment.php",

			    	type: "post",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=='yes'){

		var values = "id="+$("#option_id").val();;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

					}

					});

						}else{

							sweetAlert("Oops...", "Something is wrong. Please try again.", "error"); 

						}

					}

		});

	}
	function printme(){
	window.frames['myframe'].print();
}
function pay_due_cookie(){
	$.ajax({

			    	url: "assets/ajax/pay_due_cookie.php",
			    	type: "get",
					dataType: 'json',
					cache:false,
			      	success: function(html){}

		});
}
function printunprint(id){
    var values="myval="+id;

		$.ajax({

			    	url: "assets/ajax/print_unprint.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="yes"){
        VehicleTable.ajax.reload( null, false);
							
							}

					}

		});
}