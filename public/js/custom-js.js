// -------------------------------------------------------------------

//Custom JS/jQuery

// -------------------------------------------------------------------

$(document).ready(function() {



$('body').on('hidden', '.modal', function () {

  $(this).removeData('modal');

});

$('#addendum_print_modal').on('hidden', function() {

    $(this).removeData('modal');

	$('#info_print_modal').removeData('modal');

});

$('#info_print_modal').on('hidden', function() {

    $(this).removeData('modal');

	$('#addendum_print_modal').removeData('modal');

});

    $(".datepicker").datepicker({format:'yyyy-mm-dd'});

    // bind form using ajaxForm  
$.ajax({

			    	url: "assets/ajax/pay_due.php",
			    	type: "get",
					dataType: 'json',
					cache:false,
			      	success: function(html){
						if(html.status=="due"){
							$(".pdinv").html(html.invoice_id);
							$(".pddays").html(html.invoice_days);
							$("#payduemodal").modal("show");
						}
					}

		});
	    $('#account_settings').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#asmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>');

			$("#asmessage").fadeOut(3000, function() { $(this).html(""); });

		}else if($.trim(responseText) =="yes"){

			

					$('.data-table1').dataTable( {

						"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {


                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

             

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

					

             $('#asmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information successfully updated.</b></div>');

			 $("#asmessage").fadeOut(3000, function() { $(this).html(""); });

		}

		}

    }); 

		    $('#order_label').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else if($.trim(responseText) =="yes"){
$("#order").modal('hide');
			 swal("Thank You!", "We have received your order and will be shipping your labels ASAP.", "success")

		}else if($.trim(responseText) =="free"){

            var url="http://www.dodson-group.com/pressure-sensitive-laser-printer-labels";

			var win=window.open(url, '_blank');

			if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();

			}

		}

		}

    }); 

	

		$('#buyers_guide').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#bgsmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

			$("#bgsmessage").fadeOut(3000, function() { $(this).html(""); });

		}else if($.trim(responseText) =="yes"){

             $('#bgsmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Buyer buyers guide successfully updated.</b></div>');

			 $("#bgsmessage").fadeOut(3000, function() { $(this).html(""); });

			 $(".btndb").html("Close");

		}

		}

    }); 

	

	

		    $('#update_user').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#uumessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

		}else if($.trim(responseText) =="yes"){

             $('#uumessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information successfully updated.</b></div>');

			 

	$('.data-table3').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'u'+aData[6]);

    },

					"sAjaxSource": "assets/ajax/users_overview.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

					

                 return   '<button type="button" class="btn btn-primary " onclick="sendpassword('+oObj.aData[6]+')">Send Password</button><button type="button" class="btn btn-info"  onclick="useredit('+oObj.aData[6]+')">Edit</button><button type="button" class="btn btn-danger" onclick="userdelete('+oObj.aData[6]+')">Delete</button>' }, "aTargets": [ 6 ]}

 

 ]

				} );

				

  var oTable = $('.data-table3').dataTable();

   oTable.fnSort( [[0,'desc']] );

$('#user_edit').modal('hide');

		}

		}

    }); 

		$('#dealer_profile').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#dpmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

		}else if($.trim(responseText) =="yes"){

             $('#dpmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information successfully updated.</b></div>');
			 $("#profile").modal("hide");
			 location.reload();

		}

		}

    });

	

			$('#addendum_setting_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#assmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

			$("#assmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else if($.trim(responseText) =="yes"){
			$.ajax({
			    	url: "assets/ajax/get_background.php?form=new_as",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".file_c1bg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_BG);
						$(".file_c1qrbg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_QR_BG);
						$(".file_c1smsbg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_SMS_BG);
						$(".file_c1vinbg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_VIN_BG);
						$(".file_c2bg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM2_BG);
						$(".file_c3bg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM3_BG);
						$(".file_c5bg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM5_BG);
						$(".file_c2qrbg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM2_QR_BG);
						$(".file_c2nmbg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM2_NM_BG);
						$(".file_c4bg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM4_BG);
						$(".file_nc1bg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].NARROW_CUSTOM1_BG);
						$(".file_wholesalebg").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].WHOLESALE_BG);
						}
					}
			});

             $('#assmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#assmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

			if($(".uselect").val()=="EPA Template"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Branded"){

				 $(".accheck").removeAttr("style");

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Simple"){

				 $(".accheck").removeAttr("style");

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="PrePrinted"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Wide"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Narrow"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Two Up"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="QR Label"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});
				 $(".nbccheck").removeAttr("style");
				 $(".nc1bg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").removeAttr("style");
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Custom 1"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").removeAttr("style");

			 }else if($(".uselect").val()=="Custom1 SMS"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").removeAttr("style");
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 VIN"||$(".uselect").val()=="Custom1 VIN No MSRP"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").removeAttr("style");
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").removeAttr("style");
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").removeAttr("style");
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom3"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").removeAttr("style");
				 $(".c5bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom5"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c5bg").removeAttr("style");
				 $(".c3bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom4"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").removeAttr("style");
				  $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Wholesale"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				  $(".wholesalebg").removeAttr("style");
				  $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="SMS"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				  $(".wholesalebg").css({'display' : 'none'});
				  $(".smscolor").removeAttr("style");
				 $(".msrp2").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").removeAttr("style");
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 No MSRP"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else{

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }

		}else if($.trim(responseText) =="Invalid Request"){

			 $('#assmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Invalid Request.</b></div>'); 

			 $("#assmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else{

			image="http://addendum-images.s3.amazonaws.com/"+$.trim(responseText);

			 $('#inputlogo').attr( 'src',image);

			 $('#logofile').val(responseText);

			 if(($(".uselect").val()=="Narrow Branded")||($(".uselect").val()=="Standard Branded")||($(".uselect").val()=="Dealer Branded Simple")||($(".uselect").val()=="Dealer Branded Custom QR")||($(".uselect").val()=="SMS")||($(".uselect").val()=="Narrow Branded Custom QR")||($(".uselect").val()=="Dealer Branded with VIN Barcode")){

				 $(".lgcheck").removeAttr("style");

			 }else{

				 $(".lgcheck").css({'display' : 'none'});

			 }

			 $('#assmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#assmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}

		}

    });

	$('.uselect').change(function(){

		  if($(".uselect").val()=="EPA Template"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Branded"){

				 $(".accheck").removeAttr("style");

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Simple"){

				 $(".accheck").removeAttr("style");

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="PrePrinted"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Wide"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Narrow"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Two Up"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="QR Label"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").removeAttr("style");

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});
				 $(".nbccheck").removeAttr("style");
				 $(".nc1bg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded Custom QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").removeAttr("style");

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").removeAttr("style");

				 $(".qrdcheck").removeAttr("style");

				 $(".bqccheck").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").removeAttr("style");
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Custom 1"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").removeAttr("style");

			 }else if($(".uselect").val()=="Custom1 SMS"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").removeAttr("style");
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 VIN"||$(".uselect").val()=="Custom1 VIN No MSRP"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").removeAttr("style");
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").removeAttr("style");
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom3"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").removeAttr("style");
				 $(".c5bg").css({'display' : 'none'});
				 $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom5"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").removeAttr("style");
				 $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom4"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				 $(".c4bg").removeAttr("style");
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Wholesale"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				  $(".wholesalebg").removeAttr("style");
				  $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="SMS"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").removeAttr("style");

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				  $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				  $(".wholesalebg").css({'display' : 'none'});
				  $(".smscolor").removeAttr("style");
				 $(".msrp2").css({'display' : 'none'});
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 QR"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").removeAttr("style");
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 No MSRP"){

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").removeAttr("style");

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").removeAttr("style");

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }else{

				 $(".accheck").css({'display' : 'none'});

				 $(".tccheck").css({'display' : 'none'});

				 $(".bccheck").css({'display' : 'none'});

				 $(".lgcheck").css({'display' : 'none'});

				 $(".slcheck").css({'display' : 'none'});

				 $(".qrcheck").css({'display' : 'none'});

				 $(".qrtcheck").css({'display' : 'none'});

				 $(".qrdcheck").css({'display' : 'none'});

				 $(".bqccheck").css({'display' : 'none'});

				 $(".baraccheck").css({'display' : 'none'});
				 $(".c1bg").css({'display' : 'none'});
				 $(".c1qrbg").css({'display' : 'none'});
				 $(".c1smsbg").css({'display' : 'none'});
				 $(".c1vinbg").css({'display' : 'none'});
				 $(".c2bg").css({'display' : 'none'});
				 $(".c3bg").css({'display' : 'none'});
				 $(".c5bg").css({'display' : 'none'});
				  $(".c4bg").css({'display' : 'none'});
				 $(".wholesalebg").css({'display' : 'none'});
				 $(".smscolor").css({'display' : 'none'});
				 $(".msrp2").removeAttr("style");
				 $(".c2qrbg").css({'display' : 'none'});
				 $(".c2nmbg").css({'display' : 'none'});

				 $(".mrcheck").removeAttr("style");
				 $(".nbccheck").css({'display' : 'none'});
				 $(".nc1bg").css({'display' : 'none'});

			 }

	});





	

			$('#used_addendum_setting_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#uassmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

			$("#uassmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else if($.trim(responseText) =="yes"){
$.ajax({
			    	url: "assets/ajax/get_background.php?form=used_as",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".ufile_c1bg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_BG);
						$(".ufile_c1qrbg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_QR_BG);
						$(".ufile_c1smsbg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_SMS_BG);
						$(".ufile_c1vinbg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM1_VIN_BG);
						$(".ufile_c2bg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM2_BG);
						$(".ufile_c3bg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM3_BG);
						$(".ufile_c2qrbg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM2_QR_BG);
						$(".ufile_c2nmbg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM2_NM_BG);
						$(".ufile_c4bg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].CUSTOM4_BG);
						$(".ufile_nc1bg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].NARROW_CUSTOM1_BG);
						$(".ufile_wholesalebg1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].WHOLESALE_BG);
						}
					}
			});
             $('#uassmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#uassmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

			  if($(".uuselect").val()=="EPA Template"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Branded"){

				 $(".uaccheck").removeAttr("style");

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Simple"){

				 $(".uaccheck").removeAttr("style");

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Used"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="PrePrinted"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Wide"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Narrow"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Two Up"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="QR Label"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});
				 

				 $(".umrcheck").removeAttr("style");
				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").removeAttr("style");
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 1"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").removeAttr("style");
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 SMS"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 VIN"||$(".uuselect").val()=="Custom1 VIN No MSRP"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").removeAttr("style");
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").removeAttr("style");
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom3"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").removeAttr("style");
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom5"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom4"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Wholesale"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uwholesalebg").removeAttr("style");
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="SMS"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").removeAttr("style");
				 $(".umsrp2").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").removeAttr("style");
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 No MSRP"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").removeAttr("style");

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else{

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }

		}else if($.trim(responseText) =="Invalid Request"){

			 $('#uassmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Invalid Request.</b></div>'); 

			 $("#uassmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else{

			image="http://addendum-images.s3.amazonaws.com/"+$.trim(responseText);

			 $('#uinputlogo').attr( 'src',image);

			 if(($(".uuselect").val()=="Narrow Branded")||($(".uuselect").val()=="Standard Branded")||($(".uuselect").val()=="Dealer Branded Simple")||($(".uuselect").val()=="Standard Used")||($(".uuselect").val()=="SMS")||($(".uuselect").val()=="Dealer Branded Custom QR")||($(".uuselect").val()=="Narrow Branded Custom QR")||($(".uuselect").val()=="Dealer Branded with VIN Barcode")){

				 $(".ulgcheck").removeAttr("style");

			 }else{

				 $(".ulgcheck").css({'display' : 'none'});

			 }

			 $('#uassmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#uassmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}

		}

    });

	$('.uuselect').change(function(){

		 if($(".uuselect").val()=="EPA Template"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Branded"){

				 $(".uaccheck").removeAttr("style");

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Simple"){

				 $(".uaccheck").removeAttr("style");

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard Used"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="PrePrinted"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Wide"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="AAT-Narrow"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Standard"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Two Up"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="QR Label"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});
				 

				 $(".umrcheck").removeAttr("style");
				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").removeAttr("style");
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 1"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").removeAttr("style");
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 SMS"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom1 VIN"||$(".uuselect").val()=="Custom1 VIN No MSRP"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").removeAttr("style");
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").removeAttr("style");
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom3"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").removeAttr("style");
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom5"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom4"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Wholesale"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uwholesalebg").removeAttr("style");
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="SMS"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").removeAttr("style");
				 $(".umsrp2").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").removeAttr("style");
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Custom2 No MSRP"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").removeAttr("style");

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else{

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }

	});



			$('#infosheet_setting_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#issmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

			$("#issmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else if($.trim(responseText) =="yes"){
			$.ajax({
			    	url: "assets/ajax/get_background.php?form=info",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".info_narrow").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_NARROW_BG);
						$(".info_narrow_np").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_NARROW_NP_BG);
						$(".info_dc1").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_DC1_BG);
						$(".info_dc1c").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_DC1C_BG);
						$(".info_dcs").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_DCS_BG);
						$(".info_pc4").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_PURE4_BG);
						}
					}
			});

             $('#issmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#issmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

			 if($(".isselect").val()=="Dealer Custom" || $(".isselect").val()=="Dealer Custom NP" || $(".isselect").val()=="Standard 4" || $(".isselect").val()=="Info Book"){

				 $(".ischeck").removeAttr("style");

			 }else{

				 $(".ischeck").css({'display' : 'none'});

			 }

			  if($(".isselect").val()=="Pure Cars 2" || $(".isselect").val()=="Pure Cars 3" || $(".isselect").val()=="Pure Cars 1" || $(".isselect").val()=="Pure Cars 4"){

				 $(".pccheck").removeAttr("style");

			 }else{

				 $(".pccheck").css({'display' : 'none'});

			 }
			  if($(".isselect").val()=="Pure Cars 4"){

				  $(".ispc4bg").removeAttr("style");

			 }else{

				 $(".ispc4bg").css({'display' : 'none'});
				 

			 }

			  if($(".isselect").val()=="Narrow Standard"){

				 $(".nscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nsnpcheck").css({'display' : 'none'});

			 }else if($(".isselect").val()=="Narrow Standard NP"){

				 $(".nsnpcheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nscheck").css({'display' : 'none'});

			 }else{

				  $(".nscheck").css({'display' : 'none'});
				  $(".nsnpcheck").css({'display' : 'none'});

				  $(".iqrselect").removeAttr("style");

			 }
			 
			 if($(".isselect").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 1"){

				 $(".isdc1check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom SMS"){

				 $(".isdcscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdcscheck").css({'display' : 'none'});


			 }

			 

		}else if($.trim(responseText) =="Invalid Request"){

			 $('#issmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Invalid Request.</b></div>'); 

			 $("#issmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else{

			image="http://addendum-images.s3.amazonaws.com/"+$.trim(responseText);

			 $('#inputlogoiss').attr( 'src',image);

			 if($(".isselect").val()=="Dealer Custom" || $(".isselect").val()=="Dealer Custom NP" || $(".isselect").val()=="Standard 4" || $(".isselect").val()=="Info Book"){

				 $(".isscheck").removeAttr("style");

			 }else{

				 $(".isscheck").css({'display' : 'none'});

			 }

			  if($(".isselect").val()=="Pure Cars 2" || $(".isselect").val()=="Pure Cars 3" || $(".isselect").val()=="Pure Cars 1" || $(".isselect").val()=="Pure Cars 4"){

				 $(".pccheck").removeAttr("style");

			 }else{

				 $(".pccheck").css({'display' : 'none'});

			 }
			  if($(".isselect").val()=="Pure Cars 4"){

				  $(".ispc4bg").removeAttr("style");

			 }else{

				 $(".ispc4bg").css({'display' : 'none'});
				 

			 }

			  if($(".isselect").val()=="Narrow Standard"){

				 $(".nscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nsnpcheck").css({'display' : 'none'});

			 }else if($(".isselect").val()=="Narrow Standard NP"){

				 $(".nsnpcheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nscheck").css({'display' : 'none'});

			 }else{

				  $(".nscheck").css({'display' : 'none'});
				  $(".nsnpcheck").css({'display' : 'none'});

				  $(".iqrselect").removeAttr("style");

			 }
			 if($(".isselect").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 1"){

				 $(".isdc1check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom SMS"){

				 $(".isdcscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdcscheck").css({'display' : 'none'});


			 }

			 $('#issmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#issmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}

		}

    });
	
	$('#infosheet_setting_form_c').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#isscmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

			$("#isscmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else if($.trim(responseText) =="yes"){
			$.ajax({
			    	url: "assets/ajax/get_background.php?form=info_c",
			    	type: "post",
					dataType: 'json',
					cache:false,
			      	success: function(mydata){
						for (var x = 0; x < mydata.length; x++) {
						$(".info_narrow_c").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_NARROW_BG);
						$(".info_narrow_np_c").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_NARROW_NP_BG);
						$(".info_dc1_c").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_DC1_BG);
						$(".info_dcs_c").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_DCS_BG);
						$(".info_dc1c_c").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_DC1C_BG);
						$(".info_pc4_c").attr("href","http://addendum-backgrounds.s3.amazonaws.com/"+mydata[x].INFO_PURE4_BG);
						}
					}
			});

             $('#isscmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#isscmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

			 if($(".isselectc").val()=="Dealer Custom" || $(".isselectc").val()=="Dealer Custom NP" || $(".isselectc").val()=="Standard 4" || $(".isselectc").val()=="Info Book"){

				 $(".ischeckc").removeAttr("style");

			 }else{

				 $(".ischeckc").css({'display' : 'none'});

			 }

			  if($(".isselectc").val()=="Pure Cars 2" || $(".isselectc").val()=="Pure Cars 3" || $(".isselectc").val()=="Pure Cars 1" || $(".isselectc").val()=="Pure Cars 4"){

				 $(".pccheckc").removeAttr("style");

			 }else{

				 $(".pccheckc").css({'display' : 'none'});

			 }
			  if($(".isselectc").val()=="Pure Cars 4"){

				  $(".ispc4cbg").removeAttr("style");

			 }else{
				 $(".ispc4cbg").css({'display' : 'none'});
			 }

			  if($(".isselectc").val()=="Narrow Standard"){

				 $(".nscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nsnpcheckc").css({'display' : 'none'});

			 }else if($(".isselectc").val()=="Narrow Standard NP"){

				 $(".nsnpcheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nscheckc").css({'display' : 'none'});

			 }else{

				  $(".nscheckc").css({'display' : 'none'});
				  $(".nsnpcheckc").css({'display' : 'none'});

				  $(".iqrselectc").removeAttr("style");

			 }
			 if($(".isselectc").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 1"){

				 $(".isdc1checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1checkc").css({'display' : 'none'});


			 }
			 
			 if($(".isselectc").val()=="Dealer Custom SMS"){

				 $(".isdcscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdcscheckc").css({'display' : 'none'});


			 }

			 

		}else if($.trim(responseText) =="Invalid Request"){

			 $('#isscmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Invalid Request.</b></div>'); 

			 $("#isscmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}else{

			image="http://addendum-images.s3.amazonaws.com/"+$.trim(responseText);

			 $('#inputlogoissc').attr( 'src',image);

			 if($(".isselectc").val()=="Dealer Custom" || $(".isselectc").val()=="Dealer Custom NP" || $(".isselectc").val()=="Standard 4" || $(".isselectc").val()=="Info Book"){

				 $(".isscheckc").removeAttr("style");

			 }else{

				 $(".isscheckc").css({'display' : 'none'});

			 }

			  if($(".isselectc").val()=="Pure Cars 2" || $(".isselectc").val()=="Pure Cars 3" || $(".isselectc").val()=="Pure Cars 1" || $(".isselectc").val()=="Pure Cars 4"){

				 $(".pccheckc").removeAttr("style");

			 }else{

				 $(".pccheckc").css({'display' : 'none'});

			 }
			 if($(".isselectc").val()=="Pure Cars 4"){

				  $(".ispc4cbg").removeAttr("style");

			 }else{

				 $(".ispc4cbg").css({'display' : 'none'});
				 

			 }

			  if($(".isselectc").val()=="Narrow Standard"){

				 $(".nscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nsnpcheckc").css({'display' : 'none'});

			 }else if($(".isselectc").val()=="Narrow Standard NP"){

				 $(".nsnpcheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nscheckc").css({'display' : 'none'});

			 }else{

				  $(".nscheckc").css({'display' : 'none'});
				  $(".nsnpcheckc").css({'display' : 'none'});

				  $(".iqrselectc").removeAttr("style");

			 }
			 if($(".isselectc").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 1"){

				 $(".isdc1checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1checkc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom SMS"){

				 $(".isdcscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdcscheckc").css({'display' : 'none'});


			 }

			 $('#isscmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information Successfully updated.</b></div>');

			 $("#isscmessage").fadeOut(3000, function() { $(this).html(""); }).fadeIn(200);

		}

		}

    });

	$('.isselect').change(function(){

		if($(".isselect").val()=="Dealer Custom" || $(".isselect").val()=="Dealer Custom NP" || $(".isselect").val()=="Standard 4" || $(".isselect").val()=="Info Book"){

				 $(".ischeck").removeAttr("style");

			 }else{

				 $(".ischeck").css({'display' : 'none'});

			 }

			  if($(".isselect").val()=="Pure Cars 2" || $(".isselect").val()=="Pure Cars 3" || $(".isselect").val()=="Pure Cars 1" || $(".isselect").val()=="Pure Cars 4"){

				 $(".pccheck").removeAttr("style");

			 }else{

				 $(".pccheck").css({'display' : 'none'});

			 }
			 if($(".isselect").val()=="Pure Cars 4"){

				  $(".ispc4bg").removeAttr("style");

			 }else{

				 $(".ispc4bg").css({'display' : 'none'});
				 

			 }

			 if($(".isselect").val()=="Narrow Standard"){

				 $(".nscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nsnpcheck").css({'display' : 'none'});

			 }else if($(".isselect").val()=="Narrow Standard NP"){

				 $(".nsnpcheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});
				 $(".nscheck").css({'display' : 'none'});

			 }else{

				  $(".nscheck").css({'display' : 'none'});
				  $(".nsnpcheck").css({'display' : 'none'});

				  $(".iqrselect").removeAttr("style");

			 }
			
			 if($(".isselect").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheck").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom 1"){

				 $(".isdc1check").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdc1check").css({'display' : 'none'});


			 }
			 if($(".isselect").val()=="Dealer Custom SMS"){

				 $(".isdcscheck").removeAttr("style");

				 $(".iqrselect").css({'display' : 'none'});

			 }else{

				  $(".isdcscheck").css({'display' : 'none'});


			 }

			

	});
	
	$('.isselectc').change(function(){

		if($(".isselectc").val()=="Dealer Custom" || $(".isselectc").val()=="Dealer Custom NP" || $(".isselectc").val()=="Standard 4" || $(".isselectc").val()=="Info Book"){

				 $(".ischeckc").removeAttr("style");

			 }else{

				 $(".ischeckc").css({'display' : 'none'});

			 }

			  if($(".isselectc").val()=="Pure Cars 2" || $(".isselectc").val()=="Pure Cars 3" || $(".isselectc").val()=="Pure Cars 1" || $(".isselectc").val()=="Pure Cars 4"){

				 $(".pccheckc").removeAttr("style");

			 }else{

				 $(".pccheckc").css({'display' : 'none'});

			 }
			 if($(".isselectc").val()=="Pure Cars 4"){
				  $(".ispc4cbg").removeAttr("style");
			 }else{
				 $(".ispc4cbg").css({'display' : 'none'});
				 
			 }

			  if($(".isselectc").val()=="Narrow Standard"){

				 $(".nscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nsnpcheckc").css({'display' : 'none'});

			 }else if($(".isselectc").val()=="Narrow Standard NP"){

				 $(".nsnpcheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});
				 $(".nscheckc").css({'display' : 'none'});

			 }else{

				  $(".nscheckc").css({'display' : 'none'});
				  $(".nsnpcheckc").css({'display' : 'none'});

				  $(".iqrselectc").removeAttr("style");

			 }
			 if($(".isselectc").val()=="Dealer Custom 1 Certified"){

				 $(".isdc1ccheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1ccheckc").css({'display' : 'none'});


			 }
			 if($(".isselectc").val()=="Dealer Custom 1"){

				 $(".isdc1checkc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdc1checkc").css({'display' : 'none'});

			 }
			 if($(".isselectc").val()=="Dealer Custom SMS"){

				 $(".isdcscheckc").removeAttr("style");

				 $(".iqrselectc").css({'display' : 'none'});

			 }else{

				  $(".isdcscheckc").css({'display' : 'none'});

			 }

			

	});

	

	$('#add_options').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

			

		var id=$.trim(responseText);

		$('#add_option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);

						

		$('.data-table4').dataTable({

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'pa'+aData[4]);

    },

					"sAjaxSource": "assets/ajax/get_print_table.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[3]<0){

						 var mydprice=Math.abs(oObj.aData[3]);

						 return   "-$"+mydprice;

					 }else if(oObj.aData[3]=="np"){

						 return   oObj.aData[3];

					 }else{

						  return   "$"+oObj.aData[3];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {
				 	if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}
return deet }, "aTargets": [ 4 ]}
                 //return   '<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>' }, "aTargets": [ 4 ]}

 ]

				} );

				

  var oTable = $('.data-table4').dataTable();

   oTable.fnSort( [[0,'asc']] );

						

					}

				});

				

      				}

    			});

				

	

			}

    });

	

		$('#option_edit_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

			

		var id=$.trim(responseText);

		$('#add_option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);

						

		$('.data-table4').dataTable({

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'pa'+aData[4]);

    },

					"sAjaxSource": "assets/ajax/get_print_table.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[3]<0){

						 var mydprice=Math.abs(oObj.aData[3]);

						 return   "-$"+mydprice;

					 }else if(oObj.aData[3]=="np"){

						 return   oObj.aData[3];

					 }else{

						  return   "$"+oObj.aData[3];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

                 
if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}
return deet }, "aTargets": [ 4 ]}
                 //return   '<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>' }, "aTargets": [ 4 ]}

 ]

				} );

				

  var oTable = $('.data-table4').dataTable();

   oTable.fnSort( [[0,'asc']] );

					$('#print_option_edit').modal('hide');	

					}

				});

				

      				}

    			});

				

	

			}

    });



	

	

		 $('#add_addendum').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('.aamessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

		}else if($.trim(responseText) =="yes"){

             $('.aamessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information successfully updated.</b></div>');

			 $('#add_addendum').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

			$("#inputstyle2").select2("val", "- NONE");

	$("#inputmodel2").select2("val", "-NONE");

	$('.data-table2').dataTable( {

		"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'ad'+aData[14]);

    },

					"sAjaxSource": "assets/ajax/addendum_defaults.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[20] }},

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

	 var model=oObj.aData[15];

	 if(oObj.aData[16]!=""){

		 model+=', '+oObj.aData[16];

	 }

	 if(oObj.aData[17]!=""){

		 model+=', '+oObj.aData[17];

	 }

	 if(oObj.aData[18]!=""){

		 model+=', '+oObj.aData[18];

	 }
	 if(oObj.aData[21]!=""){

		 model+='...';

	 }
	 

                 return   model }},				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

	 var style=oObj.aData[4];

	 if(oObj.aData[5]!=""){

		 style+=', '+oObj.aData[5];

	 }

	 if(oObj.aData[6]!=""){

		 style+=', '+oObj.aData[6];

	 }

	 if(oObj.aData[7]!=""){

		 style+=', '+oObj.aData[7];

	 }

	 if(oObj.aData[8]!=""){

		 style+=', '+oObj.aData[8];

	 }

	 if(oObj.aData[9]!=""){

		 style+=', '+oObj.aData[9];

	 }

	 if(oObj.aData[10]!=""){

		 style+=', '+oObj.aData[10];

	 }

	 if(oObj.aData[11]!=""){

		 style+=', '+oObj.aData[11];

	 }

	 if(oObj.aData[12]!=""){

		 style+=', '+oObj.aData[12];

	 }

	 if(oObj.aData[13]!=""){

		 style+=', '+oObj.aData[13];

	 }

                 return   style }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[19]<0){

						 var mydprice=Math.abs(oObj.aData[19]);

						 return   "-$"+mydprice;

					 }else{

						  return   "$"+oObj.aData[19];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

                 return   '<button type="button" class="btn btn-danger" onClick="addendumdel('+oObj.aData[14]+')">Delete</button><button type="button" class="btn btn-info" id="'+oObj.aData[14]+'" onClick="addendumedit('+oObj.aData[14]+')">Edit</button>' }, "aTargets": [ 4 ]}

 

 ]

				} );

				

  var oTable = $('.data-table2').dataTable();

   oTable.fnSort( [[0,'asc']] );

	$('a[href="#defaults-overview"]').tab('show');

		}

		}

    });

	

		$('#vehicle_edit').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#evmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

		}else if($.trim(responseText) =="yes"){

             $('#evmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information successfully updated.</b></div>');

			  $('.data-table1').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

  var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

      		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

					$.ajax({

			    	url: "assets/ajax/get_model.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						$('#inputmodel2').html(html);

					}

					});

					$.ajax({

			    	url: "assets/ajax/get_style.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						$('#inputstyle2').html(html);

					}

					});

   $('#vehicle_edit_form').modal('hide');

		}

		}

    });

	

			$('#update_addendum').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#eamessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

		}else if($.trim(responseText) =="yes"){

             $('#eamessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Information successfully updated.</b></div>');

	$('.data-table2').dataTable( {

		"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'ad'+aData[14]);

    },

					"sAjaxSource": "assets/ajax/addendum_defaults.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[20] }},

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

	 var model=oObj.aData[15];

	 if(oObj.aData[16]!=""){

		 model+=', '+oObj.aData[16];

	 }

	 if(oObj.aData[17]!=""){

		 model+=', '+oObj.aData[17];

	 }

	 if(oObj.aData[18]!=""){

		 model+=', '+oObj.aData[18];

	 }
	if(oObj.aData[21]!=""){

		 model+='...';

	 }

                 return   model }},				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

	 var style=oObj.aData[4];

	 if(oObj.aData[5]!=""){

		 style+=', '+oObj.aData[5];

	 }

	 if(oObj.aData[6]!=""){

		 style+=', '+oObj.aData[6];

	 }

	 if(oObj.aData[7]!=""){

		 style+=', '+oObj.aData[7];

	 }

	 if(oObj.aData[8]!=""){

		 style+=', '+oObj.aData[8];

	 }

	 if(oObj.aData[9]!=""){

		 style+=', '+oObj.aData[9];

	 }

	 if(oObj.aData[10]!=""){

		 style+=', '+oObj.aData[10];

	 }

	 if(oObj.aData[11]!=""){

		 style+=', '+oObj.aData[11];

	 }

	 if(oObj.aData[12]!=""){

		 style+=', '+oObj.aData[12];

	 }

	 if(oObj.aData[13]!=""){

		 style+=', '+oObj.aData[13];

	 }

                 return   style }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[19]<0){

						 var mydprice=Math.abs(oObj.aData[19]);

						 return   "-$"+mydprice;

					 }else{

						  return   "$"+oObj.aData[19];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

                 return   '<button type="button" class="btn btn-danger" onClick="addendumdel('+oObj.aData[14]+')">Delete</button><button type="button" class="btn btn-info" id="'+oObj.aData[14]+'" onClick="addendumedit('+oObj.aData[14]+')">Edit</button>' }, "aTargets": [ 4 ]}

 

 ]

				} );

				

  var oTable = $('.data-table2').dataTable();

   oTable.fnSort( [[0,'asc']] );

			 $('#addendum_edit').modal('hide');

		}

		}

    });

	

	$('#vehicle_form').hide();

	$('#VIN_FORM').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

		 dataType:  'json',

        success: function(html) {

						if(html.status!=='undefined' && html.errorType=='INCORRECT_PARAMS'){

							swal({   
  title: "Invalid VIN Number",   
  text: "VIN must contain only 17 letters and numbers. Last five digits must be numeric and should not contain I,O or Q",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});

				//var r=confirm("I'm sorry that VIN number ["+$('#inputVINNumber').val()+"] is not recognized by the decoder. Click 'Cancel' to edit and try again, or click 'OK' to proceed and bypass the decoder and add the vehicle. We will be notified and add this VIN series to this decoder ASAP.");
						}else if(html.status!=='undefined' && html.errorType=='RESOURCE_NOT_FOUND'){
		$.ajax({

			    	url: "assets/ajax/vin_old.php",

			    	type: "post",

					data:$("#VIN_FORM" ).serialize(),
					dataType: 'json',

					cache:false,

			      	success: function(html){

						

						if(html.message === 'invalid'){

							swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});

						}else{

							var valid=html.VIN["@attributes"].Status;

			if(valid=="SUCCESS"){

				if(typeof html.VIN.Vehicle[0] === 'undefined'){

				var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputVINNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				$('#inputVINNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice);

				var year=html.VIN.Vehicle["@attributes"].Model_Year;

				var make=html.VIN.Vehicle["@attributes"].Make;

				var model=html.VIN.Vehicle["@attributes"].Model;

				//var transmission=html.VIN.Vehicle["@attributes"].Transmission;

				//var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

				var style=html.VIN.Vehicle.Item[5]["@attributes"].Value;

				var trim=html.VIN.Vehicle.Item[3]["@attributes"].Value;

				var engine=html.VIN.Vehicle.Item[6]["@attributes"].Value;

				$('#inputYear').val(year);

				$('#inputMake').val(make);

				$('#inputModel').val(model);

				$('#inputTrim').val(trim);

				$('#inputStyle').val(style);

				//$('#inputTransmission').val(transmission);

				//$('#inputDrivetrain').val(drivetrain);

				$('#inputEngine').val(engine);

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();

var d = new Date();

var curr_date = d.getDate();

var curr_month = d.getMonth()+1;

var curr_year = d.getFullYear();

var ddate=curr_year + "-" + curr_month + "-" + curr_date;

$('#inputDate').val(ddate);	

				}else{

					var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputVINNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				$('#inputVINNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice);

				var year=html.VIN.Vehicle[0]["@attributes"].Model_Year;

				var make=html.VIN.Vehicle[0]["@attributes"].Make;

				var model=html.VIN.Vehicle[0]["@attributes"].Model;

				//var transmission=html.VIN.Vehicle["@attributes"].Transmission;

				//var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

				var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;

				var trim=html.VIN.Vehicle[0].Item[3]["@attributes"].Value;
				
				$('#inputTrim').val(trim);
				$('#exttrim').hide();
				if(typeof html.VIN.Vehicle[1]!=="undefined"){
					$('#inputTrim').val("");
					$('#inputTrim').attr("placeholder", "Choose Trim");
					$('select#extrim').append($('<option>', {value:"", text:"Choose Other"}));
					$('#extrim').show();
					$('#extrim').html("");
					$('#extrim').val("");
					$('#extrim').attr("placeholder", "Choose Color");
				//var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
				var firstArray=html.VIN.Vehicle;
for(var i = 0, len = html.VIN.Vehicle.length; i < len; i += 1) {
    // you access the elements
					var mp=i;
					if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value === 'undefined'){
						break;
					}else{
						if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value!="+"){
				//var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
				var inputtrim=html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value;
				inputtrim=inputtrim.replace("+", "");
				$('select#extrim').append($('<option>', {value:inputtrim, text:inputtrim}));
				if(mp==0){
					$("#inputTrim").val(inputtrim);
				}
						}
					}
					
				}
				$('select#extrim').append($('<option>', {value:"Unknown", text:"Unknown"}));
				}
				

				var engine=html.VIN.Vehicle[0].Item[6]["@attributes"].Value;

				$('#inputYear').val(year);

				$('#inputMake').val(make);

				$('#inputModel').val(model);

				$('#inputTrim').val(trim);

				$('#inputStyle').val(style);

				//$('#inputTransmission').val(transmission);

				//$('#inputDrivetrain').val(drivetrain);

				$('#inputEngine').val(engine);

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();

var d = new Date();

var curr_date = d.getDate();

var curr_month = d.getMonth()+1;

var curr_year = d.getFullYear();

var ddate=curr_year + "-" + curr_month + "-" + curr_date;

$('#inputDate').val(ddate);	

				}

			}else if(valid=="FAILED"){swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});}else{swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});}

						}

			}

    });
	}else if(html.status!=='undefined' && (html.errorType=='Not Found' || html.errorType=='REQUEST_ERROR')){
		$.ajax({

			    	url: "assets/ajax/vin_old.php",

			    	type: "post",

					data:$("#VIN_FORM" ).serialize(),
					dataType: 'json',

					cache:false,

			      	success: function(html){

						

						if(html.message === 'invalid'){

							swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});

						}else{

							var valid=html.VIN["@attributes"].Status;

			if(valid=="SUCCESS"){

				if(typeof html.VIN.Vehicle[0] === 'undefined'){

				var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputVINNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				$('#inputVINNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice);

				var year=html.VIN.Vehicle["@attributes"].Model_Year;

				var make=html.VIN.Vehicle["@attributes"].Make;

				var model=html.VIN.Vehicle["@attributes"].Model;

				//var transmission=html.VIN.Vehicle["@attributes"].Transmission;

				//var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

				var style=html.VIN.Vehicle.Item[5]["@attributes"].Value;

				var trim=html.VIN.Vehicle.Item[3]["@attributes"].Value;

				var engine=html.VIN.Vehicle.Item[6]["@attributes"].Value;

				$('#inputYear').val(year);

				$('#inputMake').val(make);

				$('#inputModel').val(model);

				$('#inputTrim').val(trim);

				$('#inputStyle').val(style);

				//$('#inputTransmission').val(transmission);

				//$('#inputDrivetrain').val(drivetrain);

				$('#inputEngine').val(engine);

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();

var d = new Date();

var curr_date = d.getDate();

var curr_month = d.getMonth()+1;

var curr_year = d.getFullYear();

var ddate=curr_year + "-" + curr_month + "-" + curr_date;

$('#inputDate').val(ddate);	

				}else{

					var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputVINNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				$('#inputVINNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice);

				var year=html.VIN.Vehicle[0]["@attributes"].Model_Year;

				var make=html.VIN.Vehicle[0]["@attributes"].Make;

				var model=html.VIN.Vehicle[0]["@attributes"].Model;

				//var transmission=html.VIN.Vehicle["@attributes"].Transmission;

				//var drivetrain=html.VIN.Vehicle["@attributes"].Drivetrain;

				var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;

				var trim=html.VIN.Vehicle[0].Item[3]["@attributes"].Value;
				
				$('#inputTrim').val(trim);
				$('#exttrim').hide();
				if(typeof html.VIN.Vehicle[1]!=="undefined"){
					$('#inputTrim').val("");
					$('#inputTrim').attr("placeholder", "Choose Trim");
					$('select#extrim').append($('<option>', {value:"", text:"Choose Other"}));
					$('#extrim').show();
					$('#extrim').html("");
					$('#extrim').val("");
					$('#extrim').attr("placeholder", "Choose Color");
				//var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
				var firstArray=html.VIN.Vehicle;
for(var i = 0, len = html.VIN.Vehicle.length; i < len; i += 1) {
    // you access the elements
					var mp=i;
					if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value === 'undefined'){
						break;
					}else{
						if(html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value!="+"){
				//var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
				var inputtrim=html.VIN.Vehicle[eval(mp)].Item[3]["@attributes"].Value;
				inputtrim=inputtrim.replace("+", "");
				$('select#extrim').append($('<option>', {value:inputtrim, text:inputtrim}));
				if(mp==0){
					$("#inputTrim").val(inputtrim);
				}
						}
					}
					
				}
				$('select#extrim').append($('<option>', {value:"Unknown", text:"Unknown"}));
				}
				

				var engine=html.VIN.Vehicle[0].Item[6]["@attributes"].Value;

				$('#inputYear').val(year);

				$('#inputMake').val(make);

				$('#inputModel').val(model);

				$('#inputTrim').val(trim);

				$('#inputStyle').val(style);

				//$('#inputTransmission').val(transmission);

				//$('#inputDrivetrain').val(drivetrain);

				$('#inputEngine').val(engine);

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();

var d = new Date();

var curr_date = d.getDate();

var curr_month = d.getMonth()+1;

var curr_year = d.getFullYear();

var ddate=curr_year + "-" + curr_month + "-" + curr_date;

$('#inputDate').val(ddate);	

				}

			}else if(valid=="FAILED"){swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});}else{swal({   
  title: "VIN not recognized",   
  text: "Uh Oh! Looks like that VIN is not yet in the decoder database",   
  type: "warning",   
  showCancelButton: true,   
  confirmButtonColor: "#DD6B55",   
  confirmButtonText: "Add this VIN",   
  closeOnConfirm: false 
}, 
function(){
  swal("OK!", "We are checking the vin and if valid we'll get it added to the database.", "success"); 
  var values2="vin="+$('#inputVINNumber').val();
  $.ajax({
			    	url: "assets/ajax/not-found.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){
					}
					});
  var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				var inputVINNumber=$('#inputVINNumber').val();

				$('#inputVINNumber2').val(inputVINNumber);

				$('#inputNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice); 

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();
});}

						}

			}

    });
	}else{

				var valid=html.vin;
				var vinin=$('#inputVINNumber').val().toUpperCase();

				var ISN=$('#inputStockNumber').val();

				var IVN=$('#inputVINNumber').val();

				var InputPrice=$('#InputPrice').val();

				$('#inputStockNumber2').val(ISN);

				$('#inputVINNumber2').val(IVN);

				$('#inputPrice2').val(InputPrice);
				if(typeof html.years[0]!=="undefined"){
				if(typeof html.years[0].year!=="undefined"){

				var year=html.years[0].year;
				$('#inputYear').val(year);
				}
				}
				if(typeof html.make!=="undefined"){
				if(typeof html.make.name!=="undefined"){

				var make=html.make.name;
				$('#inputMake').val(make);
				}
				}
				if(typeof html.model!=="undefined"){
				if(typeof html.model.name!=="undefined"){

				var model=html.model.name;
				$('#inputModel').val(model);
				}
				}
				if(typeof html.transmission!=="undefined"){
				if(typeof html.transmission.transmissionType!=="undefined"){

				var transmission=html.transmission.transmissionType;
				$('#inputTransmission').val(transmission);
				}
				}
				if(typeof html.drivenWheels!=="undefined"){

				var drivetrain=html.drivenWheels;
				$('#inputDrivetrain').val(drivetrain);
				}
				if(typeof html.categories!=="undefined"){
				if(typeof html.categories.vehicleStyle!=="undefined"){

				var style=html.categories.vehicleStyle;
				$('#inputStyle').val(style);
				}
				}
				if(typeof html.years[0]!=="undefined"){
					if(typeof html.years[0].styles!=="undefined"){
				if(typeof html.years[0].styles[0]!=="undefined"){
				if(typeof html.years[0].styles[0].trim!=="undefined"){

				var trim=html.years[0].styles[0].trim;
				$('#inputTrim').val(trim);
				$('#exttrim').hide();
				if(typeof html.years[0].styles[1]!=="undefined"){
					$('#inputTrim').val("");
					$('#inputTrim').attr("placeholder", "Choose Trim");
					$('select#extrim').append($('<option>', {value:"", text:"Choose Other"}));
					$('#extrim').show();
					$('#extrim').html("");
					$('#extrim').val("");
					$('#extrim').attr("placeholder", "Choose Color");
				//var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
				var firstArray=html.years[0].styles;
for(var i = 0, len = html.years[0].styles.length; i < len; i += 1) {
    // you access the elements
					var mp=i;
					if(html.years[0].styles[eval(mp)].trim === 'undefined'){
						break;
					}else{
						if(html.years[0].styles[eval(mp)].trim!="+"){
				//var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
				var inputtrim=html.years[0].styles[eval(mp)].trim;
				inputtrim=inputtrim.replace("+", "");
				$('select#extrim').append($('<option>', {value:inputtrim, text:inputtrim}));
				if(mp==0){
					$("#inputTrim").val(inputtrim);
				}
						}
					}
					
				}
				$('select#extrim').append($('<option>', {value:"Unknown", text:"Unknown"}));
				}
				}
				}
				}
				}
				if(typeof html.engine!=="undefined"){
				if(typeof html.engine.size!=="undefined"){

				var engine=html.engine.size;
				$('#inputEngine').val(engine);
				}
				}
				if(typeof html.colors!=="undefined"){
				if(typeof html.colors[0]!=="undefined"){
				if(typeof html.colors[0].options[0]!=="undefined"){
				if(typeof html.colors[0].options[0].name!=="undefined" && html.colors[0].category=="Interior"){
				var int_color=html.colors[0].options[0].name;
				$('#inputColor2').val(int_color);
				$('#extcolor2').hide();
				if(typeof html.colors[0].options[1]!=="undefined"){
					$('#inputColor2').attr("placeholder", "Choose Color");
					$('#inputColor2').val("");
					$('select#extcolor2').append($('<option>', {value:"", text:"Choose Other"}));
					$('#extcolor2').show();
					$('#extcolor2').html("");
					$('#extcolor2').val("");
					$('#extcolor2').attr("placeholder", "Choose Color");
				//var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
				var firstArray=html.colors[0].options;
for(var i = 0, len = html.colors[0].options.length; i < len; i += 1) {
    // you access the elements
					var mp=i;
					if(html.colors[0].options[eval(mp)] === 'undefined'){
						break;
					}else{
						if(html.colors[0].options[eval(mp)].name!="+"){
				//var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
				var inputcolor=html.colors[0].options[eval(mp)].name;
				inputcolor=inputcolor.replace("+", "");
				$('select#extcolor2').append($('<option>', {value:inputcolor, text:inputcolor}));
				if(mp==0){
					$("#inputColor2").val(inputcolor);
				}
						}
					}
				}
				$('select#extcolor2').append($('<option>', {value:"Unknown", text:"Unknown"}));
				}
				
				}else if(typeof html.colors[0].options[0].name!=="undefined" && html.colors[0].category=="Exterior"){
				var ext_color=html.colors[0].options[0].name;
				$('#inputColor').val(ext_color);
				$('#extcolor').hide();
				if(typeof html.colors[0].options[1]!=="undefined"){
					$('#inputColor').attr("placeholder", "Choose Color");
					$('#inputColor').val("");
					$('select#extcolor').append($('<option>', {value:"", text:"Choose Other"}));
					$('#extcolor').show();
					$('#extcolor').html("");
					$('#extcolor').val("");
					$('#extcolor').attr("placeholder", "Choose Color");
				//var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
				var firstArray=html.colors[0].options;
for(var i = 0, len = html.colors[0].options.length; i < len; i += 1) {
    // you access the elements
					var mp=i;
					if(html.colors[0].options[eval(mp)] === 'undefined'){
						break;
					}else{
						if(html.colors[0].options[eval(mp)].name!="+"){
				//var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
				var inputcolor=html.colors[0].options[eval(mp)].name;
				inputcolor=inputcolor.replace("+", "");
				$('select#extcolor').append($('<option>', {value:inputcolor, text:inputcolor}));
				if(mp==0){
					$("#inputColor").val(inputcolor);
				}
						}
					}
				}
				$('select#extcolor').append($('<option>', {value:"Unknown", text:"Unknown"}));
				}
				
				}
				}
				}
				}

				if(typeof html.colors!=="undefined"){
				if(typeof html.colors[1]!=="undefined"){
				if(typeof html.colors[1].options[0]!=="undefined"){
				if(typeof html.colors[1].options[0].name!=="undefined" && html.colors[1].category=="Interior"){
				var int_color=html.colors[1].options[0].name;
				$('#inputColor2').val(int_color);
				$('#extcolor2').hide();
				if(typeof html.colors[1].options[1]!=="undefined"){
					$('#inputColor2').attr("placeholder", "Choose Color");
					$('#inputColor2').val("");
					$('select#extcolor2').append($('<option>', {value:"", text:"Choose Other"}));
					$('#extcolor2').show();
					$('#extcolor2').html("");
					$('#extcolor2').val("");
					$('#extcolor2').attr("placeholder", "Choose Color");
				//var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
				var firstArray=html.colors[1].options;
for(var i = 0, len = html.colors[1].options.length; i < len; i += 1) {
    // you access the elements
					var mp=i;
					if(html.colors[1].options[eval(mp)] === 'undefined'){
						break;
					}else{
						if(html.colors[1].options[eval(mp)].name!="+"){
				//var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
				var inputcolor=html.colors[1].options[eval(mp)].name;
				inputcolor=inputcolor.replace("+", "");
				$('select#extcolor2').append($('<option>', {value:inputcolor, text:inputcolor}));
				if(mp==0){
					$("#inputColor2").val(inputcolor);
				}
						}
					}
				}
				$('select#extcolor2').append($('<option>', {value:"Unknown", text:"Unknown"}));
				}
				
				}else if(typeof html.colors[1].options[0].name!=="undefined" && html.colors[1].category=="Exterior"){
				var ext_color=html.colors[1].options[0].name;
				$('#inputColor').val(ext_color);
				$('#extcolor').hide();
				if(typeof html.colors[1].options[1]!=="undefined"){
					$('#inputColor').attr("placeholder", "Choose Color");
					$('#inputColor').val("");
					$('select#extcolor').append($('<option>', {value:"", text:"Choose Other"}));
					$('#extcolor').show();
					$('#extcolor').html("");
					$('#extcolor').val("");
					$('#extcolor').attr("placeholder", "Choose Color");
				//var style=html.VIN.Vehicle[0].Item[5]["@attributes"].Value;
				var firstArray=html.colors[1].options;
for(var i = 0, len = html.colors[1].options.length; i < len; i += 1) {
    // you access the elements
					var mp=i;
					if(html.colors[1].options[eval(mp)] === 'undefined'){
						break;
					}else{
						if(html.colors[1].options[eval(mp)].name!="+"){
				//var trim=html.VIN.Vehicle[i].Item[3]["@attributes"].Value;
				var inputcolor=html.colors[1].options[eval(mp)].name;
				inputcolor=inputcolor.replace("+", "");
				$('select#extcolor').append($('<option>', {value:inputcolor, text:inputcolor}));
				if(mp==0){
					$("#inputColor").val(inputcolor);
				}
						}
					}
				}
				$('select#extcolor').append($('<option>', {value:"Unknown", text:"Unknown"}));
				}
				
				}
				}
				}
						}

				if(typeof html.MPG!=="undefined"){
				if(typeof html.MPG.highway!=="undefined"){
				var mpg_highway=html.MPG.highway;
				$('#inputMPGHigh').val(mpg_highway);
				}
				}
				if(typeof html.MPG!=="undefined"){
				if(typeof html.MPG.city!=="undefined"){
				var mpg_city=html.MPG.city;
				$('#inputMPGCity').val(mpg_city);
				}
				}
				if(typeof html.engine!=="undefined"){
				if(typeof html.engine.type!=="undefined"){
				var engine_type=html.engine.type;
				$('#inputEnginetype').val(engine_type);
				}
				}
				if(typeof html.numOfDoors!=="undefined"){
				var doors=html.numOfDoors;
				$('#inputDoors').val(doors);
				}

				$('#VIN_FORM').hide();

				$('#vehicle_form').show();

var d = new Date();

var curr_date = d.getDate();

var curr_month = d.getMonth()+1;

var curr_year = d.getFullYear();

var ddate=curr_year + "-" + curr_month + "-" + curr_date;

$('#inputDate').val(ddate);	

				


						}

			}

    });

	

	$('#option_add_form').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {	

		var id=$.trim(responseText);

		$('#add_option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);

						

		$('.data-table4').dataTable({

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'pa'+aData[4]);

    },

					"sAjaxSource": "assets/ajax/get_print_table.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[3]<0){

						 var mydprice=Math.abs(oObj.aData[3]);

						 return   "-$"+mydprice;

					 }else if(oObj.aData[3]=="np"){

						 return   oObj.aData[3];

					 }else{

						  return   "$"+oObj.aData[3];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}else {
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}
return deet }, "aTargets": [ 4 ]}
                 //return   '<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>' }, "aTargets": [ 4 ]}

 ]

				} );

				

  var oTable = $('.data-table4').dataTable();

   oTable.fnSort( [[0,'asc']] );

   $('#option_add_form').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

					$('#print_option_edit').modal('hide');	

					}

				});

				

      				}

    			});

 $('#print_option_add').modal('hide');

		}

	});

	

		$('#create_user').ajaxForm({ 

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) { 

		if($.trim(responseText) =="no"){

            $('#cumessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

		}else if($.trim(responseText) =="yes"){

             $('#cumessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>New User Created.</b></div>');

			 $('#create_user').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

			 	$('.data-table3').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'u'+aData[6]);

    },

					"sAjaxSource": "assets/ajax/users_overview.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

					

                 return   '<button type="button" class="btn btn-primary " onclick="sendpassword('+oObj.aData[6]+')">Send Password</button><button type="button" class="btn btn-info"  onclick="useredit('+oObj.aData[6]+')">Edit</button><button type="button" class="btn btn-danger" onclick="userdelete('+oObj.aData[6]+')">Delete</button>' }, "aTargets": [ 6 ]}

 

 ]

				} );

				

  var oTable = $('.data-table3').dataTable();

   oTable.fnSort( [[0,'desc']] );

    $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

				

   $('a[href="#user-overview"]').tab('show');

		}

		}

    });

	

			$('#VEHICLE_INPUT').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

		 dataType:  'json',

        success: function(html) { 

				 $('.data-table1').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

  var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

   $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

					$.ajax({

			    	url: "assets/ajax/get_model.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						$('#inputmodel2').html(html);

					}

					});

					$.ajax({

			    	url: "assets/ajax/get_style.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						$('#inputstyle2').html(html);

					}

					});

		if(html.type=="savevehicle"){

             $('#vimessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>New Vehicle Added.</b></div>');

		$('#VIN_FORM').show();

		$('#vehicle_form').hide();

		 $('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('a[href="#vehicle-overview"]').tab('show');

		}else if(html.type=="savennew"){

             $('#vimessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>New Vehicle Added.</b></div>');

		$('#VIN_FORM').show();

		$('#vehicle_form').hide();

		 $('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		}else if(html.type=="savenprint"){

		$('#VIN_FORM').show();

		$('#vehicle_form').hide();

		 $('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

		 $(".print_addendum").attr("id",html.id);

		$(".add_new_option").attr("id",html.id);

		$('#add_option_id').val(html.id);

		$('#option_id').val(html.id);

		 $('#option_id').val(html.id);

			var values = "id="+html.id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);

						

		$('.data-table4').dataTable({

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'pa'+aData[4]);

    },

					"sAjaxSource": "assets/ajax/get_print_table.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[3]<0){

						 var mydprice=Math.abs(oObj.aData[3]);

						 return   "-$"+mydprice;

					 }else if(oObj.aData[3]=="np"){

						 return   oObj.aData[3];

					 }else{

						  return   "$"+oObj.aData[3];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}else {
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}
return deet }, "aTargets": [ 4 ]}

 ]

				} );

				

  var oTable = $('.data-table4').dataTable();

   oTable.fnSort( [[0,'asc']] );

						

					}

				});

				

      				}

    			});

				$('a[href="#edit-options"]').tab('show');

		}

		

		}

    });

	$('.add_new_option').click(function(){

		var id=$(this).attr('id');

		$("#add_option_id").val(id);

		$("#print_option_add").modal("show");

	});

		$('.vehicle_reset').click(function(){

				$('#VIN_FORM').show();

				$('#vehicle_form').hide();

				$('#VEHICLE_INPUT').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

				$('#VIN_FORM').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

				$('a[href="#vehicle-overview"]').tab('show');

		});

	

		$('.add_addendum_reset').click(function(){

				$('#add_addendum').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

					$("#inputstyle2 option:selected").removeAttr("selected");

	$("#inputmodel2 option:selected").removeAttr("selected");

				$('a[href="#vehicle-overview"]').tab('show');

		});

		$('.add_user_reset').click(function(){

				$('#add_user').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");

				$('#inputUsertype').prop('selectedIndex',0);

				$('a[href="#vehicle-overview"]').tab('show');

		});

		

		$('.vehicle_multidelete').click(function() {

		$("#multiaction").val("multidelete");

var r=confirm("Do you really want to delete selected entry(s) ?");

if (r==true)

  {

	  var data = $("#multi_delete" ).serialize();

 $.ajax({

			    	url: "assets/ajax/vehicle_delete.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

						

		if($.trim(responseText) =="no"){

            $('#vemessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 

		}else if($.trim(responseText) =="yes"){

			$('#vemessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Selected vehicle deleted.</b></div>'); 

            

			 $('.data-table1').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

  var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

    $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

		}

		            $('.delcheck').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

						}

					});

  }

		});

	

	

	$('.vehicle_multiprint').click(function() {

		$("#multiaction").val("multiprint");

			  var data = $("#multi_delete" ).serialize();

 $.ajax({

			    	url: "assets/ajax/vehicle_delete.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

						var values="type=multiple&"+$.trim(responseText);

		$.ajax({

			    	url: "assets/ajax/get_free_cap.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="ok"){		

				  $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

		$("#multipleinput").val($.trim(responseText));

		$.ajax({

			    	url: "assets/ajax/addendum_check.php",

			    	type: "get",

					cache:false,

			      	success: function(addendum_check){

					if($.trim(addendum_check)=="QR Label"){

					var id=$("#multipleinput").val();

		var url="assets/print/multiprint.php?"+id+"&uid="+Date.now();;

		$(".final_print").val(url);

		$("#spv").val(id);

		$("#som").val("multiprint");

		$("#qr_label").html('<div class="qr_label1">&nbsp;</div><div class="qr_label2"><strong><span class="countingadd"></span><br><br>Start Printing Here:</strong><label><input type="radio" name="qr_label" onclick="qr_label();" value="1"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="2"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="3"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="4"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="5"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="6"/><div>&nbsp;</div></label></div>');

		var len = $("input[name='delcheck[]']:checked").length;

		$(".countingadd").html("You are printing "+len+" QR Label.");

		$(".qr_label1").load(url);

		$('#addendum_print_modal').modal({

  keyboard: false

},"show");

					}else{

						var id=$("#multipleinput").val();

		var url="assets/print/multiprint.php?"+id+"&uid="+Date.now();

		$(".final_print").val(url);

		$("#mpv").val(id);

		$("#som").val("multiprint");

		$('#addendum_print_modal').modal({

  keyboard: false,

  remote:url

},"show");}

					}

		});

//window.open('multiprint.php?'+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

						}else if(html=="no"){

							alert("I'm afraid you have reached your printing limit for this free account. Please upgrade now.");

						}

					}

					});

						}

		 });

	});

	

	$('.btn-refresh').click(function(){

		location.reload();

	});

	$('.print_addendum').click(function() {

		$.ajax({

			    	url: "assets/ajax/addendum_check.php",

			    	type: "get",

					cache:false,

			      	success: function(addendum_check){

					if($.trim(addendum_check)=="QR Label"){

					var id=$(".print_addendum").attr('id');

		var url="assets/print/print.php?vid="+id+"&uid="+Date.now();

		$(".final_print").val(url);

		$("#spv").val(id);

		$("#som").val("single");

		$("#qr_label").html('<div class="qr_label1">&nbsp;</div><div class="qr_label2"><strong>You are printing 1 QR Label.<br><br>Start Printing Here:</strong><label><input type="radio" name="qr_label" onclick="qr_label();" value="1"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="2"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="3"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="4"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="5"/><div>&nbsp;</div></label><label><input type="radio" name="qr_label" onclick="qr_label();" value="6"/><div>&nbsp;</div></label></div>');

		$(".qr_label1").load(url);

		$('#addendum_print_modal').modal({

  keyboard: false

},"show");

					}else{

		var id=$(".print_addendum").attr('id');

		var url="assets/print/print.php?vid="+id+"&uid="+Date.now();

		$(".final_print").val(url);

		$("#spv").val(id);

		$("#som").val("single");

		$('#addendum_print_modal').modal({

  keyboard: false,

  remote:url

},"show");

					}

					}

		});



//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});



	$('.final_print').click(function() {

		if($("#som").val()=="single"){

		var id=$("#spv").val();

		var values="id="+id+"&type=single";

		}else if($("#som").val()=="multiprint"){

			var id=$("#multipleinput").val();

		var values=id+"&type=multiprint";

		}

			 $.ajax({

			    	url: "assets/ajax/print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					$('.data-table1').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

					if($("#som").val()=="single"){

	var url=$(".final_print").val()+"&type=final_print";

					}else if($("#som").val()=="multiprint"){

	var url=$(".final_print").val()+"&type=multiprint";

					}

	var time1=Math.round(new Date().getTime() / 1000);

    //var myWindow=window.open(url,time1,'');

	$("a#ahref").attr("href", url)

	$('a#ahref')[0].click();

	$("a#ahref").trigger( "click" );

					}

					});	





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});

$('.print-ipdf').click(function() {
var html = '<div id="load"></div>';
        $('body').append(html);
		if($("#iom").val()=="single"){

		var id=$("#ipv").val();

		var values="id="+id+"&type=single&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}else if($("#iom").val()=="multiprint"){

			var id=$("#ipv").val();

		var values=id+"&type=multiprint&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}

		$.ajax({

			

			    	url: "assets/ajax/info_print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					$('.data-table1').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

					var printstart=$("#printstart").val();

							if($("#iom").val()=="single"){

	var url="assets/print/info_print_download.php?printstart="+printstart+"&vid="+$("#ipv").val()+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}else if($("#iom").val()=="multiprint"){

	var url="assets/print/muliple-download.php?"+$("#ipv").val()+"&type=multiprint&printstart="+printstart+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}

					$("#info_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari1-' + randid;
  $(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
	 setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
		 setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){
setTimeout(function(){ $('#load').remove(); }, 6000);
				$("#popupblock").modal("show");

			}else{
				 setTimeout(function(){ $('#load').remove(); }, 6000);
				win.focus();
				
			}
}



					}

					

		});

						





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

});

	$('.print-pdf').click(function() {
var html = '<div id="load"></div>';
        $('body').append(html);
		if($("#som").val()=="single"){

		var id=$("#spv").val();

		var printstart=$("#printstart").val();

		var values="id="+id+"&type=single&printstart="+printstart;

		}else if($("#som").val()=="multiprint"){

		var id=$("#multipleinput").val();

		var values=id+"&type=multiprint&printstart="+printstart;

		}

			 $.ajax({

			    	url: "assets/ajax/print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					$('.data-table1').dataTable( {

						"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

				var printstart=$("#printstart").val();

				$("#printstart").val("1");

				if($("#som").val()=="single"){

	var url="assets/print/single-download.php?vid="+$("#spv").val()+"&printstart="+printstart;

					}else if($("#som").val()=="multiprint"){

	var url="assets/print/muliple-download.php?"+$("#multipleinput").val()+"&type=multiprint"+"&printstart="+printstart;

					}

					$("#addendum_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

	var time1=Math.round(new Date().getTime() / 1000);
	
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari2-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
	setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
		setTimeout(function(){ $('#load').remove(); }, 6000);
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');
}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){
setTimeout(function(){ $('#load').remove(); }, 6000);
				$("#popupblock").modal("show");

			}else{
setTimeout(function(){ $('#load').remove(); }, 6000);
				win.focus();
				
			}
}

	//



//document.getElementById('pdfDocument').setAttribute('src',"http://dev.dealeraddendums.com/app/"+url);
//var e=document.getElementById('pdfDocument');
//$('#pdfDocument').load(function(){
    // Operate upon the SVG DOM here
   //var x = document.getElementById('pdfDocument');
       // this.print();
		//this.unbind('load');
//}).attr('src', "http://dev.dealeraddendums.com/app/"+url);
	 
	//

					}

					});



					}

					});	





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});


$('#download_guide').click(function() {

			$('#buyers_guide_print').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

		if($.trim(responseText) =="no"){

            $('#bgpmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>');

		}else if($.trim(responseText) =="yes"){				

        

	$('.data-table1').dataTable( {

						"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

var url="assets/print/guide-download-spanish.php?bgtype=print&vid="+$("#pbgvid").val();

$("#buyer_guide_print").modal("hide");
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari3-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

		}

    });

});

$('#print_guide').click(function() {

			$('#buyers_guide_print').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

		if($.trim(responseText) =="no"){

            $('#bgpmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>');

		}else if($.trim(responseText) =="yes"){				

        

	$('.data-table1').dataTable( {

						"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

        var url="assets/print/guide-download.php?bgtype=print&vid="+$("#pbgvid").val();

			$("#buyer_guide_print").modal("hide");

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari4-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

		}

    });

});

 $('a[data-toggle="tab"]').on('shown', function (e) {

    var target = $(e.target).attr("href");

	if(target=="#asis2"){

		$(".download_guide2").removeAttr("style");
		$(".print_guide2").removeAttr("style");

	}else if(target=="#implied2"){

	$(".download_guide2").removeAttr("style");
	$(".print_guide2").removeAttr("style");

	}else if(target=="#warranty2"){

	$(".download_guide2").removeAttr("style");
	$(".print_guide2").removeAttr("style");

	}else if(target=="#warranty2_s"){

	$(".download_guide2").removeAttr("style");
	$(".print_guide2").removeAttr("style");

	}else if(target=="#back_page_s"){

	$(".download_guide2").attr("style","visibility:hidden");
	$(".print_guide2").attr("style","visibility:hidden");

	}

    })

$('#print_backpage').click(function() {

var bpaddress=$("#bpaddress").val();

var bpdealer=$("#bpdealer").val();

var bpcomplaints=$("#bpcomplaints").val();

 var data = $("#buyers_guide" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari5-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});


$('#print_backpage_p').click(function() {

var bpaddress=$("#bpaddress_p").val();

var bpdealer=$("#bpdealer_p").val();

var bpcomplaints=$("#bpcomplaints_p").val();

 var data = $("#buyers_guide_print" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari5-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});

$('#download_backpage').click(function() {

var bpaddress=$("#bpaddress").val();

var bpdealer=$("#bpdealer").val();

var bpcomplaints=$("#bpcomplaints").val();

 var data = $("#buyers_guide" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){	

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download-spanish.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari6-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});

$('#download_backpage_p').click(function() {

var bpaddress=$("#bpaddress_p").val();

var bpdealer=$("#bpdealer_p").val();

var bpcomplaints=$("#bpcomplaints_p").val();

 var data = $("#buyers_guide_print" ).serialize();

 $.ajax({

			    	url: "assets/ajax/savebackpage.php",

			    	type: "post",

					data:data,

					cache:false,

			      	success: function(responseText){	

		if($.trim(responseText) =="yes"){

			var url="assets/print/backpage-download-spanish.php?type=print";

		var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var dhost = location.hostname;
var randid = Math.floor(Math.random() * 101);
var uframe='goinOnaSafari6-' + randid;
$(".iframe").html('<iframe src="" height="0" width="0" id="'+uframe+'" style="visibility:hidden"></iframe>');
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
{   $('#'+uframe).load(function(){
this.contentWindow.focus();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else if(isChrome){
	$('#'+uframe).load(function(){
this.contentWindow.focus();
this.contentWindow.print();
$(this).unbind('load');}).attr('src', "http://"+dhost+"/app/"+url);
}else{
	var win=window.open(url, "", "directories=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
	if(win==null){

				$("#popupblock").modal("show");

			}else{

				win.focus();
				
			}
}

		}

					}

					});

});

$('#save_guide').click(function() {

			$('#buyers_guide_print').ajaxForm({

        // target identifies the element(s) to update with the server response 

        // success identifies the function to invoke when the server response 

        // has been received; here we apply a fade-in effect to the new content 

        success: function(responseText) {

		if($.trim(responseText) =="no"){

            $('#bgpmessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>');

			$("#bgpmessage").fadeOut(3000, function() { $(this).html(""); });

		}else if($.trim(responseText) =="yes"){

             $('#bgpmessage').html('<div class="alert alert-success palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Buyers guide successfully updated.</b></div>');

			 $("#bgpmessage").fadeOut(3000, function() { $(this).html(""); });

			 $(".btnbgp").html("Close");

		}

		}

});

});



$('.download-ipdf').click(function() {

		if($("#iom").val()=="single"){

		var id=$("#ipv").val();

		var values="id="+id+"&type=single&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}else if($("#iom").val()=="multiprint"){

			var id=$("#ipv").val();

		var values=id+"&type=multiprint&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

		}

		$.ajax({

			

			    	url: "assets/ajax/info_print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					$('.data-table1').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);
					var printstart=$("#printstart").val();

							if($("#iom").val()=="single"){

	var url="assets/print/info_print_download.php?dtype=single&printstart="+printstart+"&vid="+$("#ipv").val()+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}else if($("#iom").val()=="multiprint"){

	var url="assets/print/muliple-download.php?"+$("#ipv").val()+"&type=multiprint&dtype=single&printstart="+printstart+"&wed="+$("#wed").val()+"&INFO_TYPE="+$("#infotype").val();

					}

					$("#info_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

	var time1=Math.round(new Date().getTime() / 1000);

	window.location=url;

					}

					});

					



					}

					

		});

						





		//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;

});

	$('.download-pdf').click(function() {

		if($("#som").val()=="single"){

		var id=$("#spv").val();

		var values="id="+id+"&type=single";

		}else if($("#som").val()=="multiprint"){

			var id=$("#multipleinput").val();

		var values=id+"&type=multiprint";

		}

			 $.ajax({

			    	url: "assets/ajax/print_status.php",

			    	type: "get",

					cache:false,

					data:values,

			      	success: function(stats_list){

					$('.data-table1').dataTable( {

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );

               $('.delcheck').removeAttr("checked");

			   $('#vehicle_check').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

         		 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);
					var printstart=$("#printstart").val();

					$("#printstart").val("1");

					if($("#som").val()=="single"){

	var url="assets/print/single-download.php?vid="+$("#spv").val()+"&dtype=single&printstart="+printstart;
					}else if($("#som").val()=="multiprint"){

	var url="assets/print/muliple-download.php?"+$("#multipleinput").val()+"&type=multiprint&dtype=multiple&printstart="+printstart;
					}
					$("#addendum_print_modal").modal("hide");

					$('a[href="#vehicle-overview"]').tab('show');

var time1=Math.round(new Date().getTime() / 1000);

	//var win=window.open(url, "", "directories=no,menubar=yes,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,top=0,location=no");
window.location=url;
					}

					});

					

					

					
	

	}

	});





		//$("#addendum_print_modal").modal("show");

//window.open('assets/import/print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;



});





	   $('#vehicle_check').change(function() {   

        if (this.checked) {

          $('.delcheck').prop("checked", true);

	$('.vehicle_multiprint').removeAttr("disabled");

	$('.vehicle_multidelete').removeAttr("disabled");

        }else {

            $('.delcheck').removeAttr("checked");

	$('.vehicle_multiprint').attr("disabled","disabled");

	$('.vehicle_multidelete').attr("disabled","disabled");

        }

    });

	

	 $('#DataTables_Table_3_next').click(function() {   



            $('#vehicle_check').removeAttr("checked");

			$('.delcheck').removeAttr("checked");

			$('.vehicle_multiprint').removeAttr("disabled");

			$('.vehicle_multidelete').removeAttr("disabled");

   

    });

		 $('#DataTables_Table_3_previous').click(function() {   



            $('#vehicle_check').removeAttr("checked");

			$('.delcheck').removeAttr("checked");

			$('.vehicle_multidelete').removeAttr("disabled");

			$('.vehicle_multiprint').removeAttr("disabled");

   

    });

	$('.multiselect').each(function(){ 

    var select = $(this), values = {};    

    $('option',select).each(function(i, option){

        values[option.value] = option.selected;        

    }).click(function(event){        

        values[this.value] = !values[this.value];

        $('option',select).each(function(i, option){            

            option.selected = values[option.value];        

        });    

    });

});

$(".pick-a-color").pickAColor({

			  showSpectrum            : true,

				showSavedColors         : true,

				saveColorsPerElement    : true,

				fadeMenuToggle          : true,

				showAdvanced			: true,

				showBasicColors         : true,

				showHexInput            : true,

				allowBlank				: true,

				inlineDropdown			: true

});

if ( $(window).width() < 1024) {   

 $(".ipadprint").addClass("offset3");

 $(".ipadnew").removeClass("span2");

 $(".ipadnew").addClass("span3");

 $(".ipadlist").removeClass("span2");

 $(".ipadlist").addClass("span3");

 $(".ipaddetails").removeClass("span3");

 $(".ipaddetails").addClass("span5");

}

$(window).resize(function() {

if ( $(window).width() < 1024) {   

 $(".ipadprint").addClass("offset3");

 $(".ipadnew").removeClass("span2");

 $(".ipadnew").addClass("span3");

 $(".ipadlist").removeClass("span2");

 $(".ipadlist").addClass("span3");

 $(".ipaddetails").removeClass("span3");

 $(".ipaddetails").addClass("span5");

}

else if ( $(window).width() >= 1024) {

   $(".ipadprint").removeClass("offset3");

 $(".ipadnew").addClass("span2");

 $(".ipadnew").removeClass("span3");

 $(".ipadlist").addClass("span2");

 $(".ipadlist").removeClass("span3");

 $(".ipaddetails").addClass("span3");

 $(".ipaddetails").removeClass("span5");

}

});
$('#extrim').change(function() {  
if($('#extrim').val()==""){
	$("#inputTrim").val();
	$("#inputTrim").focus();
}else if($('#extrim').val()=="Unknown"){
	$('#extrim').attr("placeholder", "Choose Trim");
	$("#inputTrim").val();
}else{
	$("#inputTrim").val($('#extrim').val());
	$("#inputTrim").focus();
}
});

$('#extcolor').change(function() {  
if($('#extcolor').val()==""){
	$("#inputColor").val();
	$("#inputColor").focus();
}else if($('#extcolor').val()=="Unknown"){
	$('#extcolor').attr("placeholder", "Choose Trim");
	$("#inputColor").val();
}else{
	$("#inputColor").val($('#extcolor').val());
	$("#inputColor").focus();
}
});

$('#extcolor2').change(function() {  
if($('#extcolor2').val()==""){
	$("#inputColor2").val();
	$("#inputColor2").focus();
}else if($('#extcolor2').val()=="Unknown"){
	$('#extcolor2').attr("placeholder", "Choose Trim");
	$("#inputColor2").val();
}else{
	$("#inputColor2").val($('#extcolor2').val());
	$("#inputColor2").focus();
}
});


});

function decodeHTMLEntities(text) {

    var entities = [

        ['apos', '\''],

        ['amp', '&'],

        ['lt', '<'],

        ['gt', '>']

    ];



    for (var i = 0, max = entities.length; i < max; ++i) 

        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);



    return text;

}



	function edtvehicle(veid){

var id=veid;

	$('#vehicle_edit_form').modal('show');

	

  				/* get some values from elements on the page: */

   				var values = "veid="+id+"&option=get_vehicle";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/vehicle_edit.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#edtid').val(html[x].id);

				$('#edtStockNumber').val(html[x].STOCK_NUMBER);

				$('#edtVINNumber').val(html[x].VIN_NUMBER);

				$('#edtPrice').val(html[x].MSRP);

				$('#edtYear').val(html[x].YEAR);

				$('#edtMake').val(html[x].MAKE);

				$('#edtMileage').val(html[x].MILEAGE);

				$('#edtTrim').val(html[x].TRIM);

				$('#edtModel').val(html[x].MODEL);

				$('#edtStyle').val(html[x].BODYSTYLE);

				$('#edtColor').val(html[x].EXT_COLOR);

				$('#edtTransmission').val(html[x].TRANSMISSION);

				$('#edtEngine').val(html[x].ENGINE);

				$('#edtDrivetrain').val(html[x].DRIVETRAIN);
				$('#edtColor2').val(html[x].INT_COLOR);
				$('#edtDoors').val(html[x].DOORS);
				$('#edtEnginetype').val(html[x].FUEL);
				$('#edtMPGHigh').val(html[x].HMPG);
				$('#edtMPGCity').val(html[x].CMPG);

				$('#edtDate').val(html[x].DATE_IN_STOCK);

				$('#edtOption').val(html[x].OPTIONS);

				

				$('#edtDescription').html(decodeHTMLEntities(html[x].DESCRIPTION));

//$("select#edtStyle option").each(function() { this.selected = (this.text == html[x].BODYSTYLE); });

//$("select#edtModel option").each(function() { this.selected = (this.text == html[x].MODEL); });

$("select#edtVehicletype option").each(function() { this.selected = (this.text == html[x].NEW_USED); });

            }// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

		

			

	}

	

	function addendumedit(adid){

var id=adid;

	$('#addendum_edit').modal('show');

	$("#eainputstyle option:selected").removeAttr("selected");

	$("#eainputmodel option:selected").removeAttr("selected");

  				/* get some values from elements on the page: */

   				var values = "adid="+id+"&option=get_addendum";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/addendum_edit.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache: false,

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#eaid').val(html[x].AD_ID);

				$('#eaprice').val(html[x].PRICE);

				$('#inputeAdtype').val(html[x].AD_TYPE);
				var pname = html[x].ITEM_NAME.replace(/(<br ?\/?>)*/g,"");

				$('#eaitemname').val($('<div>').html(pname).text());
				if(html[x].SEPARATOR_BELOW=="0"){
					$('#separator_below2').prop('checked', false);
				}else if(html[x].SEPARATOR_BELOW=="1"){
					$('#separator_below2').prop('checked', true);
				}
				
				if(html[x].SEPARATOR_ABOVE=="0"){
					$('#separator_above2').prop('checked', false);
				}else if(html[x].SEPARATOR_ABOVE=="1"){
					$('#separator_above2').prop('checked', true);
				}

				var pdesc = html[x].ITEM_DESCRIPTION.replace(/(<br ?\/?>)*/g,"");

				$('#eadescription').html(pdesc).text();

				var mystyle="";

					for(i=0;i<=10;i++){

					var selectval='html[x].BODY_STYLE_'+i;

					 var valum=eval(selectval);

					 if(i==1){

						 var separate="";

					 }else{

						 var separate=", ";

					 }

					 if((typeof(valum) != 'undefined')){

						 if(valum!=""){

					 var mystyle=mystyle+separate+'"'+valum+'"';

						 }

					 }

					}

					var mystyle="["+mystyle+"]";

					$("#eainputstyle").val(eval(mystyle)).trigger("change");

					var mymodel="";

					for(i=0;i<=8;i++){

						var selectval='html[x].MODEL_'+i;

						

					 var valum=eval(selectval);

					 if(i==1){

						 var separate="";

					 }else{

						 var separate=", ";

					 }

					 if((typeof(valum) != 'undefined')){

						 if(valum!=""){

					 var mymodel=mymodel+separate+'"'+valum+'"';

						 }

					 }

					}

					var mymodel="["+mymodel+"]";

					$("#eainputmodel").val(eval(mymodel)).trigger("change");

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

		

			

	}



function vehicle(veid){

var id=veid;

var r=confirm("Do you really want to delete this entry ?");

if (r==true)

  {

	 var values1 = "veid="+id+"&option=vehicle";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="no"){

			        	alert("Something unexpected happened. Please try again");	

			      	} 

					else{

					var duser=html+"tr";

					$("#"+duser).remove();

					 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					  $.ajax({

			    	url: "assets/ajax/get_stats_list.php",

			    	type: "get",

					cache:false,

			      	success: function(stats_list){

				$('.todays_list').html(stats_list);

					}

					});

					}

					}

				});  

  }

	}

	

	function addendumdel(adid){

var id=adid;

var r=confirm("Do you really want to delete this entry ?");

if (r==true)

  {

	 var values1 = "adid="+id+"&option=ad";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="no"){

			        	alert("Something unexpected happened. Please try again");	

			      	} 

					else{

					var duser="ad"+html;

					$("#"+duser).remove();

					}

					}

				});  

  }

	}

	

	function sendpassword(id){

		var id=id;

	 var values1 = "cuid="+id+"&option=send_password";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/send_password.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="yes"){

			        	alert("Password Successfully sent!");	

			      	} 

					else{

					alert("Some unexpected happen. Please try again.");

					}

					}

				}); 

	}

	

	function useredit(id){

		var id=id;

	$('#user_edit').modal('show');

	

  				/* get some values from elements on the page: */

   				var values = "cuid="+id+"&option=get_user";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/get_user.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#ename').val(html[x].NAME);

				$('#eid').val(html[x].USER_ID);

				$('#eusername').val(html[x].USERNAME);

				$('#eemail').val(html[x].EMAIL);

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

	}

	

	function userdelete(id){

		var id=id;

var r=confirm("Do you really want to delete this user?");

if (r==true)

  {

	 var values1 = "cuid="+id+"&option=user";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					cache:false,

			      	success: function(html){

						if(html=="no"){

			        	alert("Something unexpected happened. Please try again");	

			      	} 

					else{

					var duser="u"+html;

					$("#"+duser).remove();

					 $.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});

					}

					}

				});

  }

	}

	function printguide(vid){

		$("#pbgvid").val(vid);

		$(".btnbgp").html("Cancel");

		var values="vid="+vid;

		$.ajax({

			    	url: "assets/ajax/print_guide.php",

			    	type: "get",

					data: values,

					cache:false,

					dataType: 'json',

			      	success: function(html){

						$('#buyer_guide_print').modal('show');

						for (var x = 0; x < html.length; x++) {

				if(html[x].DEFAULT_GUIDE=="asis"){

					$("#default_asis2").bootstrapSwitch('setState', true);

					$("#default_implied2").bootstrapSwitch('setState', false);

					$("#default_warranty2").bootstrapSwitch('setState', false);

					$("#default_warranty2_s").bootstrapSwitch('setState', false);

					$('.nav-tabs a[href="#asis2"]').tab('show');

				}else if(html[x].DEFAULT_GUIDE=="implied"){

					$("#default_asis2").bootstrapSwitch('setState', false);

					$("#default_implied2").bootstrapSwitch('setState', true);

					$("#default_warranty2").bootstrapSwitch('setState', false);

					$("#default_warranty2_s").bootstrapSwitch('setState', false);

					$('.nav-tabs a[href="#implied2"]').tab('show');

				}else if(html[x].DEFAULT_GUIDE=="warranty"){

					$("#default_asis2").bootstrapSwitch('setState', false);

					$("#default_implied2").bootstrapSwitch('setState', false);

					$("#default_warranty2").bootstrapSwitch('setState', true);

					$("#default_warranty2_s").bootstrapSwitch('setState', false);

					$('.nav-tabs a[href="#warranty2"]').tab('show');

				}else if(html[x].DEFAULT_GUIDE=="warranty_spanish"){

					$("#default_asis2").bootstrapSwitch('setState', false);

					$("#default_implied2").bootstrapSwitch('setState', false);

					$("#default_warranty2").bootstrapSwitch('setState', false);

					$("#default_warranty2_s").bootstrapSwitch('setState', true);

					$('.nav-tabs a[href="#warranty2_s"]').tab('show');

				}

				$("#asisLabel2 option[text='+html[x].LABEL_ASIS+']").attr("selected","selected");

				$("#impliedLabel2 option[text='+html[x].LABEL_IMPLIED+']").attr("selected","selected");

				$("#warrantyLabel2 option[text='+html[x].LABEL_WARRANTY+']").attr("selected","selected");

				$("#warrantyLabel2_s option[text='+html[x].LABEL_WARRANTY_S+']").attr("selected","selected");

				if(html[x].SC_ASIS=="1"){

					$("[name='sc_asis2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_asis2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_IMPLIED=="1"){

					$("[name='sc_implied2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_implied2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_WARRANTY=="1"){

					$("[name='sc_warranty2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_warranty2']").bootstrapSwitch('setState', false);

				}

				if(html[x].SC_WARRANTY_S=="1"){

					$("[name='sc_warranty2_s']").bootstrapSwitch('setState', true);

				}else{

					$("[name='sc_warranty2_s']").bootstrapSwitch('setState', false);

				}

				if(html[x].WARRANTY_TYPE=="FULL"){

					$("[name='warrantytype2']").bootstrapSwitch('setState', true);

				}else{

					$("[name='warrantytype2']").bootstrapSwitch('setState', false);

				}

				if(html[x].WARRANTY_TYPE_S=="FULL"){

					$("[name='warrantytype2_s']").bootstrapSwitch('setState', true);

				}else{

					$("[name='warrantytype2_s']").bootstrapSwitch('setState', false);

				}

				$("#inputparts2").val(html[x].PARTS);

				$("#inputlabor2").val(html[x].LABOR);
				var sco2=html[x].SYSTEMS_COVERED.replace(/(<br ?\/?>)*/g,"");
				$("#systemscovered2").html(sco2).text
				var dur2=html[x].DURATION.replace(/(<br ?\/?>)*/g,"");

				//$('#eadescription').html(pdesc).text();
				$("#duration2").html(dur2).text;

				$("#inputparts2_s").val(html[x].PARTS_S);

				$("#inputlabor2_s").val(html[x].LABOR_S);
				var sco2s=html[x].SYSTEMS_COVERED_S.replace(/(<br ?\/?>)*/g,"");
				$("#systemscovered2_s").html(sco2s).text;
				var dur2s=html[x].DURATION_S.replace(/(<br ?\/?>)*/g,"");
				$("#duration2_s").html(dur2s).text;

            }

					}

		});

	}

	function printinfosheet(vid){

		var id=vid;

		var url="assets/print/info_print.php?vid="+id+"&INFO_TYPE=standard&uid="+Date.now();

		$("#ipv").val(id);

		$("#iom").val("single");
		$("#wed").val("");
		$("#wed").hide();
		$("#istandard").trigger("click");
		$("#infotype").val("standard");

		$('#info_print_modal').modal({

  keyboard: false,

  remote:url

},"show");



//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;





;}

function infotype(intype){
			if(intype=="certified"){
				$("#wed").show();
				$("#infotype").val("certified");
			}else{
				$("#wed").hide();
				$("#infotype").val("standard");
			}
		var url="assets/print/info_print.php?vid="+$("#ipv").val()+"&INFO_TYPE="+intype+"&uid="+Date.now();

		$('#info_print_modal .modal-body').load(url,function(e){$('#info_print_modal').modal('show');});



//$("#addendum_print_modal").modal("show");

//window.open('print.php?vid='+id,'1370908909663','width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;





;}

	   function printunprint(myval){
	   	alert('asdf');
		var values="myval="+myval;

		$.ajax({

			    	url: "assets/ajax/print_unprint.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="yes"){$('.data-table1').dataTable( {

						"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );}

					}

		});

		

	}

	

	function new_used1(myval){

		var values="myval="+myval;

		$.ajax({

			    	url: "assets/ajax/new_used.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="yes"){

							$.ajax({

			    	url: "assets/ajax/get_stats.php",

			    	type: "get",

					dataType: 'json',

					cache:false,

			      	success: function(stats){

				$('.c_current_vehicles').html(stats.c_current_vehicles);

				$('.c_plus_today').html(stats.c_plus_today);

				$('.c_current_addendums').html(stats.c_current_addendums);

				$('.c_need_printing').html(stats.c_need_printing);

				$('.c_users').html(stats.c_users);

				$('.c_last_login').html(stats.c_last_login);

				$('.c_addendum_printed').html(stats.c_addendum_printed);

				$('.c_30_days').html(stats.c_30_days);

				$('.todays').html(stats.todays);

					}

					});	

							$('.data-table1').dataTable( {

						"sAjaxSource": "assets/ajax/vehicle_overview.php",

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', aData[12]+'tr');
		if(aData[16]=='0'||aData[16]==''||aData[16]==NULL||aData[16]=='NULL'){
		$(nRow).attr('style', 'color:red');
		}

    },

					"aoColumns": [

					{ "bSortable": false, "fnRender": function ( oObj ) {

					var cdata='<input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+oObj.aData[0]+'>'

                 return cdata;

				  }},

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[3] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[4] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[5] }},

				 { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[6] }},

				 { "sClass": "colors","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[7] }},

  				{ "sClass": "datein","bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[8] }},

				 {  "sClass": "edit_delete","bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="1"){
                 var edtprint='<button type="button" class="btn btn-danger delete_vehicle" onClick="vehicle('+oObj.aData[9]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>'
             }else if($("#ed_grant").val()=="0"){
                 var edtprint='<button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[9]+'" onClick="edtvehicle('+oObj.aData[9]+')">Edit</button>';
             }
             return edtprint }, "aTargets": [ 9 ]},

				 { "bSortable": false, "fnRender": function ( oObj ) {
						 if(oObj.aData[14]=='0'){

						 var printed2='<button type="button" class="btn btn-default"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }else if(oObj.aData[14]=='1'){

						 var printed2='<button type="button" class="btn btn-success"  onClick="printguide('+oObj.aData[12]+')">Buyers Guide</button>';

						 }
					 if(oObj.aData[11]=='Used'){



						 if(oObj.aData[15]=='0'){

						 var printed3='<button type="button" class="btn btn-default"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }else if(oObj.aData[15]=='1'){

						 var printed3='<button type="button" class="btn btn-success"  onClick="printinfosheet('+oObj.aData[12]+')">Info Sheet</button>';

						 }

					 }else{


						 var printed3='';

					 }

					 if(oObj.aData[10]=='1'){

						 var printed1='<button type="button" class="btn print_vehicle btn-success"  onClick="printvehicle('+oObj.aData[12]+')">Addendum</button>';

					 }else{

						 var printed1='<button type="button" class="btn print_vehicle" onClick="printvehicle('+oObj.aData[12]+')">Addendum</a>';

					 }

                 return printed2+printed3+printed1 }, "aTargets": [ 10 ]}

 

 ]

				} );

var oTable = $('.data-table1').dataTable();

   oTable.fnSort( [[8,'desc']] );}

					}

		});

		

	}

	function printvehicle(id){

		var values="type=single";

		$.ajax({

			    	url: "assets/ajax/get_free_cap.php",

			    	type: "get",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=="ok"){

		$(".print_addendum").attr("id",id);

		$(".add_new_option").attr("id",id);

		$('#add_option_id').val(id);

		$('#option_id').val(id);

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);

						

		$('.data-table4').dataTable({

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'pa'+aData[4]);

    },

					"sAjaxSource": "assets/ajax/get_print_table.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[3]<0){

						 var mydprice=Math.abs(oObj.aData[3]);

						 return   "-$"+mydprice;

					 }else if(oObj.aData[3]=="np"){

						 return   oObj.aData[3];

					 }else{

						  return   "$"+oObj.aData[3];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}
return deet }, "aTargets": [ 4 ]}
                 //return   '<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>' }, "aTargets": [ 4 ]}

 ]

				} );

				

  var oTable = $('.data-table4').dataTable();

   oTable.fnSort( [[0,'asc']] );

						

					}

				});

				

      				}

    			});

				$('a[href="#edit-options"]').tab('show');

						}else if(html=="no"){

							alert("I'm afraid you have reached your printing limit for this free account. Please upgrade now.");

						}

					}

		});



	}

	setInterval(function(){

		$.ajax({

			    	url: "assets/ajax/check_cookie.php",

			    	type: "get",

					cache:false,

			      	success: function(html){

						if(html=="yes"){

							window.location.replace("index.php");

						}else if(html=="55"){
							$('#timeoutmodal').modal('show');
						}else if(html=="60"){
							window.location="?logout=logout";
						}

					}

		});

		}, 10000);
		function stay_here(){
			$.ajax({

			    	url: "assets/ajax/check_cookie.php?stay=1",

			    	type: "get",

					cache:false,

			      	success: function(html){
						$('#timeoutmodal').modal('hide');
						}

		});
		}
		function delete_pdefault(id){

		var id=id;

var r=confirm("Do you really want to delete this option?");

if (r==true)

  {

	  var values1 = "id="+id+"&option=defaults";

	 

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/delete_function.php",

			    	type: "get",

			    	data: values1,

					dataType: 'json',

					cache:false,

			      	success: function(html){

		

		var id=html;

		$('#add_option_id').val(id);	

		var values = "id="+id;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

				var values2 = "id="+html.id;

				$.ajax({

			    	url: "assets/ajax/get_print_options.php",

			    	type: "get",

			    	data: values2,

					cache:false,

			      	success: function(html){

						$('#item_name').html(html);

						

		$('.data-table4').dataTable({

					"bRetrieve": true,

					"bProcessing": true,

					"bServerSide": true,

					"fnCreatedRow": function( nRow, aData, iDataIndex ) {

        $(nRow).attr('id', 'pa'+aData[4]);

    },

					"sAjaxSource": "assets/ajax/get_print_table.php",

					"aoColumns": [

 /*id*/          { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[1] }},

 

 /*name*/        

 /*phone#*/     { "bSortable": true, "fnRender": function ( oObj ) {

                 return   oObj.aData[2] }},

				 

 /*click me*/    { "bSortable": true, "fnRender": function ( oObj ) {

					 if(oObj.aData[3]<0){

						 var mydprice=Math.abs(oObj.aData[3]);

						 return   "-$"+mydprice;

					 }else if(oObj.aData[3]=="np"){

						 return   oObj.aData[3];

					 }else{

						  return   "$"+oObj.aData[3];

					 }

                 }},

				 { "bSortable": false, "fnRender": function ( oObj ) {

                 if($("#ed_grant").val()=="0"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button>';
				 	}else if($("#ed_grant").val()=="1"){
				 		var deet='<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>';
				 	}
return deet }, "aTargets": [ 4 ]}
                 //return   '<button type="button" class="btn btn-danger delete_vehicle" onClick="delete_pdefault('+oObj.aData[4]+')">Delete</button><button type="button" class="btn btn-info edit_vehicle" id="'+oObj.aData[4]+'" onClick="edit_pdefault('+oObj.aData[4]+')">Edit</button>' }, "aTargets": [ 4 ]}

 ]

				} );

				

  var oTable = $('.data-table4').dataTable();

   oTable.fnSort( [[0,'asc']] );

						

					}

				});

				

      				}

    			});

				$('a[href="#edit-options"]').tab('show');

	

						}

				});

  }

	}

	

	function edit_pdefault(id){

		

		var id=id;

	$('#print_option_edit').modal('show');

	

  				/* get some values from elements on the page: */

   				var values = "id="+id+"&option=get_option";

  				/* Send the data using post and put the results in a div */

   				$.ajax({

			    	url: "assets/ajax/get_option_addendum.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

			      	success: function(html){

					for (var x = 0; x < html.length; x++) {

				$('#pitemname').val(html[x].item_name);

				var pdesc = html[x].description.replace(/(<br ?\/?>)*/g,"");

				$('#pdesc').html(pdesc).text();

				$('#pprice').val(html[x].item_price);

				$('#pvehicleid').val(html[x].vehicle_id);
				
				if(html[x].SEPARATOR_BELOW=="0"){
					$('#separator_below').prop('checked', false);
				}else if(html[x].SEPARATOR_BELOW=="1"){
					$('#separator_below').prop('checked', true);
				}
				
				if(html[x].SEPARATOR_ABOVE=="0"){
					$('#separator_above').prop('checked', false);
				}else if(html[x].SEPARATOR_ABOVE=="1"){
					$('#separator_above').prop('checked', true);
				}

				$('#edit_option_id').val(html[x].add_id);

               // updateListing(data[x]);

            }

					// var uid2=$('#ufield1').val();

			

      				}   

    			}); 

	}

	function delcheck(){

		

	if($(".delcheck:checked").length > 0)

{

	$('.vehicle_multiprint').removeAttr("disabled");

	$('.vehicle_multidelete').removeAttr("disabled");



}

else

{

$('.vehicle_multiprint').attr("disabled","disabled");

$('.vehicle_multidelete').attr("disabled","disabled");

}

	 

	}

	function qr_label(){

	$("#printstart").val($("input[name=qr_label]:checked").val());

	}

	function copy_addendum(){

	$("#used_addendum_setting_form input[name=text_color]").val($("#addendum_setting_form input[name=text_color]").val() );

	$("#acselect2").val($(".acselect option:selected").val() );

	$("#barselect2").val($(".barselect option:selected").val() );

	$("#qrselect2").val($(".qrselect option:selected").val() );
	
	$("#unbcselect").val($(".unbcselect option:selected").val() );

	$("#used_addendum_setting_form input[name=background_color]").val($("#addendum_setting_form input[name=background_color]").val() );

	$("#used_addendum_setting_form input[name=slogan]").val($("#addendum_setting_form input[name=slogan]").val() );

	$("#used_addendum_setting_form input[name=qr_address]").val($("#addendum_setting_form input[name=qr_address]").val() );

	$("#used_addendum_setting_form input[name=qr_title]").val($("#addendum_setting_form input[name=qr_title]").val() );

	$("#used_addendum_setting_form input[name=qr_desc]").val($("#addendum_setting_form input[name=qr_desc]").val() );

	$("#used_addendum_setting_form input[name=left_margin]").val($("#addendum_setting_form input[name=left_margin]").val() );

	$("#used_addendum_setting_form input[name=right_margin]").val($("#addendum_setting_form input[name=right_margin]").val() );

	$("#used_addendum_setting_form input[name=top_margin]").val($("#addendum_setting_form input[name=top_margin]").val() );

	$("#used_addendum_setting_form input[name=bottom_margin]").val($("#addendum_setting_form input[name=bottom_margin]").val() );

	$("#used_addendum_setting_form input[name=adjustment]").val($("#addendum_setting_form input[name=adjustment]").val() );

	$('#uinputlogo').attr( 'src',"http://addendum-images.s3.amazonaws.com/"+$("#logofile").val());

	$('#logofile2').val($("#logofile").val());
	
	$('#ufile_c1bg1').val($("#file_c1bg").val());
	
	$('#ufile_c1qrbg1').val($("#file_c1qrbg").val());
	
	$('#ufile_c2bg1').val($("#file_c2bg").val());
	$('#ufile_c3bg1').val($("#file_c3bg").val());
	$('#ufile_c5bg1').val($("#file_c5bg").val());
	$('#ufile_nc1bg1').val($("#file_nc1bg").val());
	
	$('#ufile_c2qrbg1').val($("#file_c2qrbg").val());
	
	$(".uuselect").val($(".uselect option:selected").val() );

	

		

		 if($(".uselect").val()=="EPA Template"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Branded"){

				 $(".uaccheck").removeAttr("style");

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Simple"){

				 $(".uaccheck").removeAttr("style");

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard Used"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="PrePrinted"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Wide"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="AAT-Narrow"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Standard"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Two Up"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="QR Label"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").removeAttr("style");

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Dealer Branded with VIN Barcode"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});
				 

				 $(".umrcheck").removeAttr("style");
				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Narrow Branded Custom QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").removeAttr("style");

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").removeAttr("style");

				 $(".uqrdcheck").removeAttr("style");

				 $(".ubqccheck").removeAttr("style");

				 $(".umrcheck").removeAttr("style");

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").removeAttr("style");
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uuselect").val()=="Narrow Custom 1"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").removeAttr("style");
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 SMS"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 VIN"||$(".uselect").val()=="Custom1 VIN No MSRP"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").removeAttr("style");
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom1 QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").removeAttr("style");
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").removeAttr("style");
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom3"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").removeAttr("style");
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom5"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom4"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").removeAttr("style");
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Wholesale"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uwholesalebg").removeAttr("style");
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="SMS"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").removeAttr("style");

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").removeAttr("style");
				 $(".umsrp2").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 QR"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").removeAttr("style");
				 $(".uc2nmbg").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else if($(".uselect").val()=="Custom2 No MSRP"){

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").removeAttr("style");

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});
				 
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").removeAttr("style");

				 $(".umrcheck").removeAttr("style");
				 $(".unbccheck").css({'display' : 'none'});

			 }else{

				 $(".uaccheck").css({'display' : 'none'});

				 $(".utccheck").css({'display' : 'none'});

				 $(".ubccheck").css({'display' : 'none'});

				 $(".ulgcheck").css({'display' : 'none'});

				 $(".uslcheck").css({'display' : 'none'});

				 $(".uqrcheck").css({'display' : 'none'});

				 $(".uqrtcheck").css({'display' : 'none'});

				 $(".uqrdcheck").css({'display' : 'none'});

				 $(".ubqccheck").css({'display' : 'none'});

				 $(".ubaraccheck").css({'display' : 'none'});

				 $(".usucheck").css({'display' : 'none'});

				 $(".umrcheck").removeAttr("style");
				 $(".uc1bg").css({'display' : 'none'});
				 $(".uc1qrbg").css({'display' : 'none'});
				 $(".unc1bg").css({'display' : 'none'});
				 $(".uc1smsbg").css({'display' : 'none'});
				 $(".uc1vinbg").css({'display' : 'none'});
				 $(".uc2bg").css({'display' : 'none'});
				 $(".uc3bg").css({'display' : 'none'});
				 $(".uc5bg").css({'display' : 'none'});
				 $(".uc4bg").css({'display' : 'none'});
				 $(".uwholesalebg").css({'display' : 'none'});
				 $(".usmscolor").css({'display' : 'none'});
				 $(".umsrp2").removeAttr("style");
				 $(".uc2qrbg").css({'display' : 'none'});
				 $(".uc2nmbg").css({'display' : 'none'});
				 $(".unbccheck").css({'display' : 'none'});

			 }


			 $("#used_addendum_setting_form input[name=text_color]").next('div.btn-group').find('.color-preview.current-color').css('background-color', '#' + $("#addendum_setting_form input[name=text_color]").val());

			 $("#used_addendum_setting_form input[name=background_color]").next('div.btn-group').find('.color-preview.current-color').css('background-color', '#' + $("#addendum_setting_form input[name=background_color]").val());

	

	}

	function apply_adjustment(){

		var vid=$("#option_id").val();

		var adj_value=$(".amsrp").val();

		var values="vid="+vid+"&adjust_value="+adj_value;

		$.ajax({

			    	url: "assets/ajax/apply_adjustment.php",

			    	type: "post",

					data: values,

					cache:false,

			      	success: function(html){

						if(html=='yes'){

		var values = "id="+$("#option_id").val();;

		   			$.ajax({

			    	url: "assets/ajax/get_print_data.php",

			    	type: "get",

			    	data: values,

					dataType: 'json',

					cache:false,

			      	success: function(html){

					var vname=html.year+' '+html.make+' '+html.model;

				$('.vname').html(vname);

				$('.vstock').html(html.stock);

				$('.vvin').html(html.vin);

				$('.vprice').html(html.price);

				$('.vtotal').html(html.total_price);

				$('.voptions').html(html.options1);

				$('.amsrp').val(html.amsrp);

				$('.aprice').html(html.aprice);

					}

					});

						}else{

							alert("Something is wrong. Please try again.");

						}

					}

		});

	}
	function printme(){
	window.frames['myframe'].print();
}