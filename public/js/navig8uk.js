// jQuery.noConflict();
jQuery(document).ready(function($){
    var goafterimport="http://dealeraddendums.com/app/home.php#vehicle-overview";
    var colopts;
    $.ajax({
        url: "/app/assets/import/colopts.html",
        async: false,
        success: function(data) {
            colopts=data;
        }
    })
    $("#importexcel").on("click", function(event) {
        $("#txtpaste").val($("#txtexcelimport").val());
        var lines=$("#txtexcelimport").val().split("\n",7);
        var rows=new Array();
        var cols=new Array();
        var optstable="<table><tr>";
        for(var r=0;r<lines[0].split("\t").length;r++) {
            optstable=optstable+"<td>"+colopts+"</td>";
        }
        optstable=optstable+"</tr>";
        //$("#optstable").html(optstable);
        var table=optstable;
        for(var r=0;r<lines.length;r++) {
            table=table+"<tr>";
            var fields=lines[r].split("\t");
            for(var c=0;c<fields.length;c++) {
                var f=fields[c];
                if(f.length>13) {
                    f=f.substring(0,10)+"...";
                }
                table=table+"<td>"+f+"</td>";
            }
            table=table+"</tr>";
        }
        table=table+"</table>";
        
        $("#coltable").html(table);
        $("#chkommitfirst").prop("checked",false);
        $("#colchoice").show();
        return false;
    });
    $("#chkommitfirst").on("change", function(event) {
        if($(this).is(":checked")) {
            $("#coltable table tr").eq(1).css("color", "#ccc");
        }
        else {
            $("#coltable table tr").eq(1).css("color", "inherit");
        }
    });
    $("#colchoice").submit(function() {
        if(!verifyform()) {
            alert("You must include the following fields: Vin, Stock No, Year, Make, Model, MSRP!");
        }
        else {
            $("#colchoice").hide();
            $("#importprogress").show();
            
            var fieldArray=new Array(18);
            var i=0;
            $(".columnselect").each(function() {
                var fVal=$(this).val();
                if(fVal>0) {
                    fieldArray[fVal]=i;
                }
                i++;
            });
            var lines=$("#txtexcelimport").val().trim().split("\n");
            if($("#chkommitfirst").is(":checked")) {
                lines.shift();
            }
            var implength=lines.length;
            //alert($('input[name=importmethod]:checked').val()==1);
            if($('input[name=importmethod]:checked').val()==1) {
                $("#importstatus").html("<p>deleting previous records...</p>");
                var json={};
                json.dealerid=$("#dealerid").val().trim();
                $.ajax({
                    url: "/app/assets/import/clear.php",
                    async: false,
                    dataType: "json",
                    type: "POST",
                    data: json,
                    error: function(data) {
                        alert("AJAX Error! "+data);
                    },
                    success: function(data) {
                        if(data.result!="success") {
                            //Failed to delete records do something if required
                        }
                    }
                });
            }
            for(var i=0;i<implength;i++) {
                var json={};
                $("#importstatus").html("<p>importing "+(i+1)+" of "+implength+"</p>");
                p=Math.round(i/implength*100);
                $("#importprogress #progbar").css("width", p+"%");
                json.fieldArray=fieldArray;
                json.dealerid=$("#dealerid").val().trim();
                json.fields=lines[i].split("\t");
                //alert(JSON.stringify(json));
                $.ajax({
                    url: "/app/assets/import/addLine.php",
                    async: false,
                    dataType: "json",
                    type: "POST",
                    data: json,
                    error: function(data) {
                        alert("The AJAX Error! "+data);
                    },
                    success: function(data) {
                        //alert(JSON.stringify(data));
                        if(i===implength-1) {
                            window.location.reload();
                        }
                    }
                });
            }
        }
        return false;
    });
    $("#ccres").on("click", function() {
        $("#colchoice").hide();
        return false;
    });
    $("#colchoice").on("change", "select", function(event) {
        var prev=$(this).data("prev");
        var nocols=$("#coltable table tr").eq(1).children("td").length;
        var val=$(this).val();
        var bg="#ffffff";
        if($(this).val()==0) {
            bg="#efefef"
        }
        var col=$(this).parent().index();
        for(var a=1;a<8;a++) {
            $("#coltable table tr").eq(a).children("td").eq(col).css("background", bg);
        }
        for(a=0;a<nocols;a++) {
            if(prev>0) {
                $("#coltable table tr").eq(0).children("td").eq(a).children("select").eq(0).children("option").eq(prev).removeAttr("disabled");
            }
            if(val>0 && a!=col) {
                $("#coltable table tr").eq(0).children("td").eq(a).children("select").eq(0).children("option").eq(val).prop("disabled","disabled");
            }
        }
        $(this).data("prev",val);
    });
    var verifyform=function() {
        var nocols=$("#coltable table tr").eq(1).children("td").length;
        for(var a=1;a<7;a++) {
            var ok=false;
            for(var col=0;col<nocols;col++) {
                var colval=$("#coltable table tr").eq(0).children("td").eq(col).children("select").eq(0).data("prev");
                //alert("column "+col+"="+colval);
                if(a==colval) {
                    ok=true;
                    break;
                }
            }
        }
        return ok;
    }
});
