// -------------------------------------------------------------------
//Custom JS/jQuery
// -------------------------------------------------------------------
var DealerTable;
$(document).ready(function() {

	$('body').on('hidden', '.modal', function () {
  $(this).removeData('modal');
});
 $('.dealer_multidelete').addClass("disabled");
$('.dealer_multidelete').attr("disabled","disabled");


var screenWidth=$( document ).width();
if(screenWidth<=1024){
	
	DealerTable=$('#DealerTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "../assets/ajax/admin/dealer_table.php",
		"dom": '<"top">rt<"bottom"lip><"clear">',
		"search": {
    "regex": true
  },
		"order": [[ 0, "desc" ]],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return '<label class="ui-check m-a-0"><input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+row[0]+'><i class="dark-white"></i></label>';
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": true,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];
					},
                "targets": 1
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 return row[2];
					},
                "targets": 2
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[3];
					},
                "targets": 3
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[4];
					},
                "targets": 4
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[5];
					},
                "targets": 5
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[7];
					},
                "targets": 6,
                "sorting": false
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
					 if(row[6]=='Yes'){
						 var start1='<button type="button" class="btn pause_dealer btn-warning btn-xs"  onClick="pause_dealer('+row[0]+')">Pause</button>';
					 }else{
						 var start1='<button type="button" class="btn start_dealer btn-success btn-xs" onClick="start_dealer('+row[0]+')">Start</a>';
					 }
                 return   '<div style="text-align:right">'+start1+' <button type="button" class="btn btn-danger delete_dealer  btn-xs" onClick="delete_dealer('+row[0]+')">Delete</button> <button type="button" class="btn btn-info edit_dealer  btn-xs" id="'+row[0]+'" onClick="edtdealer('+row[0]+')">Edit</button> <button type="button" class="btn btn-success login_dealer  btn-xs" id="'+row[0]+'" onClick="login_dealer('+row[0]+')">Log In</button></div>';
					},
				"className": "text-right",
                "targets": 7,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', aData[0]+'tr');
    }
		
    } );

		$('.dealersearch').keyup(function(){
   var key = $(this).val();
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";
    	DealerTable.search(key).draw();
  
})


}else{
	
	DealerTable=$('#DealerTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "../assets/ajax/admin/dealer_table.php",
		"dom": '<"top">rt<"bottom"lip><"clear">',
		"search": {
    "regex": true
  },
		"order": [[ 0, "desc" ]],
		"columnDefs": [ {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                 return '<label class="ui-check m-a-0"><input name="delcheck[]" type="checkbox" onChange="delcheck();" class="delcheck" value='+row[0]+'><i class="dark-white"></i></label>';
                },
                "targets"  : 0,
                "sorting": false,
				"orderable": true,
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[1];
					},
                "targets": 1
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					 return row[2];
					},
                "targets": 2
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[3];
					},
                "targets": 3
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[4];
					},
                "targets": 4
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[5];
					},
                "targets": 5
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					return row[7];
					},
                "targets": 6,
                "sorting": false
            },{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
					
					 if(row[6]=='Yes'){
						 var start1='<button type="button" class="btn pause_dealer btn-warning btn-xs"  onClick="pause_dealer('+row[0]+')">Pause</button>';
					 }else{
						 var start1='<button type="button" class="btn start_dealer btn-success btn-xs" onClick="start_dealer('+row[0]+')">Start</a>';
					 }
                 return   '<div style="text-align:right">'+start1+' <button type="button" class="btn btn-danger delete_dealer  btn-xs" onClick="delete_dealer('+row[0]+')">Delete</button> <button type="button" class="btn btn-info edit_dealer  btn-xs" id="'+row[0]+'" onClick="edtdealer('+row[0]+')">Edit</button> <button type="button" class="btn btn-success login_dealer  btn-xs" id="'+row[0]+'" onClick="login_dealer('+row[0]+')">Log In</button></div>';
					},
				"className": "text-right",
                "targets": 7,
                "sorting": false
            }
		 ],
		 "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        $(nRow).attr('id', aData[0]+'tr');
    }
		
    } );

		$('.dealersearch').keyup(function(){
   var key = $(this).val();
    var regExp = "."
    if (key)
        regExp = "^\\s*" + key + "\\s*$";

    DealerTable.search(key).draw();
  
})


}




    // bind form using ajaxForm  
	    $('#account_settings').ajaxForm({ 
        // target identifies the element(s) to update with the server response 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) { 
		if(responseText=="no"){
           sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else if(responseText=="yes"){
			$("#settings").modal("hide");
             swal("Thank You!", "Information successfully updated.", "success")
		}
		}
    }); 
	
	 $('#template_backgrounds_form').ajaxForm({ 
        // target identifies the element(s) to update with the server response 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) { 
		if(responseText=="no"){
           sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else {
             swal("Thank You!", "Information successfully updated.", "success");
			 $(".tb_table").html(responseText);
		}
		}
    }); 
	
	$('#stock_images_form').ajaxForm({ 
        // target identifies the element(s) to update with the server response 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) { 
		if(responseText=="no"){
           sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else {
             swal("Thank You!", "Information successfully updated.", "success");
			 $(".si_table").html(responseText);
		}
		}
    }); 
	
		$('#keys').ajaxForm({
        // target identifies the element(s) to update with the server response 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) { 
		if(responseText=="no"){
            sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else if(responseText=="yes"){
             swal("Thank You!", "Information successfully updated.", "success")
			 $("#api_keys").modal("hide");
		}
		}
    });
	
			$('#create_dealer').ajaxForm({
        // target identifies the element(s) to update with the server response 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) { 
		if(responseText=="no"){
           sweetAlert("Oops...", "Something went wrong! Please try again.", "error"); 
		}else if(responseText=="yes"){
             swal("Thank You!", "Information successfully updated.", "success")
		$('#create_dealer').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		$('#account_type').prop('selectedIndex',0);
		DealerTable.ajax.reload();

		}
		}
    });
	
				$('#edit_dealer_form').ajaxForm({
        // target identifies the element(s) to update with the server response 
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        success: function(responseText) { 
		if(responseText=="no"){
           sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
		}else if(responseText=="yes"){
             swal("Thank You!", "Information successfully updated.", "success")
		$('#edit_dealer_form').closest('form').find("input[type=text], input[type=email], input[type=password], textarea").val("");
		$('#eaccount_type').prop('selectedIndex',0);
		DealerTable.ajax.reload();
$("#edit_dealer").modal("hide");
		}
		}
    });

	 $('#select_check').change(function() {
        if (this.checked) {
        $('.delcheck').prop("checked", true);
	$('.dealer_multidelete').removeAttr("disabled");
        }else {
            $('.delcheck').removeAttr("checked");
	$('.dealer_multidelete').attr("disabled","disabled");
        }
    });
	 $('#DataTables_Table_0_next').click(function() {   

            $('#dealer_check').removeAttr("checked");
			$('.delcheck').removeAttr("checked");
			$('.dealer_multidelete').removeAttr("disabled");
   
    });
		 $('#DataTables_Table_0_previous').click(function() {   

            $('#dealer_check').removeAttr("checked");
			$('.delcheck').removeAttr("checked");
			$('.dealer_multidelete').removeAttr("disabled");
   
    });

});
		$('.dealer_multidelete').click(function() {
		$("#multiaction").val("multidelete");
bootbox.confirm("Do you really want to delete selected entry(s) ?", function(result) {
if (result==true)
  {
	  var data = $("#multi_delete" ).serialize();
 $.ajax({
			    	url: "../assets/ajax/admin/dealer_delete.php",
			    	type: "post",
					data:data,
					cache:false,
			      	success: function(responseText){
						
		if(responseText=="no"){
            $('#vemessage').html('<div class="alert alert-danger palert"><button data-dismiss="alert" class="close" type="button">×</button><b>Something is wrong. Please try again.</b></div>'); 
		}else if(responseText=="yes"){
			swal("Thank You!", "Information successfully updated.", "success")
			
	DealerTable.ajax.reload();

			}
		            $('.delcheck').removeAttr("checked");
					$('#select_check').removeAttr("checked");
	 $('.dealer_multidelete').attr("disabled","disabled");
						}
					});
  }
});
		});
		
		function delete_dealer(did){
var id=did;
bootbox.confirm("Do you really want to delete selected entry ?", function(result) {
if (result==true)
  {
	 var values1 = "did="+id;
	 
  				/* Send the data using post and put the results in a div */
   				$.ajax({
			    	url: "../assets/ajax/admin/delete_function.php",
			    	type: "get",
			    	data: values1,
					cache:false,
			      	success: function(html){
						if(html=="no"){
			        	alert("Something unexpected happened. Please try again");	
			      	} 
					else{
					var duser=html+"tr";
					$("#"+duser).remove();
					}
					}
				});  
  }
  });
	}
	
			function pause_dealer(did){
var id=did;
bootbox.confirm("Do you really want to pause this dealer ?", function(result) {
if (result==true)
  {
	 var values1 = "did="+id;
	 
  				/* Send the data using post and put the results in a div */
   				$.ajax({
			    	url: "../assets/ajax/admin/pause_dealer.php",
			    	type: "get",
			    	data: values1,
					cache:false,
			      	success: function(html){
						if(html=="no"){
			        	sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
			      	} 
					else{
					DealerTable.ajax.reload();
    $('.dealer_multidelete').attr("disabled","disabled");
					}
					}
				});  
  }
  });
	}
function start_dealer(did){
var id=did;
bootbox.confirm("Do you really want to resume this dealer ?", function(result) {
if (result==true)
  {
	 var values1 = "did="+id;
	 
  				/* Send the data using post and put the results in a div */
   				$.ajax({
			    	url: "../assets/ajax/admin/resume_dealer.php",
			    	type: "get",
			    	data: values1,
					cache:false,
			      	success: function(html){
						if(html=="no"){
			        	sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
			      	} 
					else{
					DealerTable.ajax.reload();
   $('.dealer_multidelete').attr("disabled","disabled");
					}
					}
				});  
  }
  });
	}
	
function edtdealer(did){
var id=did;
$("#edit_dealer").modal("show");
  				/* get some values from elements on the page: */
   				var values = "did="+id;
  				/* Send the data using post and put the results in a div */
   				$.ajax({
			    	url: "../assets/ajax/admin/get_dealer.php",
			    	type: "get",
			    	data: values,
					dataType: 'json',
			      	success: function(html){
					for (var x = 0; x < html.length; x++) {
				$('#eid').val(html[x].ID);
				$('#edealer_name').val(html[x].DEALER_NAME);
				$('#edealer_id').val(html[x].DEALER_ID);
				$('#cedealer_id').val(html[x].DEALER_ID);
				$('#eusername').val(html[x].MSRP);
				$('#eaddress').val(html[x].DEALER_ADDRESS);
				$('#ecity').val(html[x].DEALER_CITY);
				$('#estate').val(html[x].DEALER_STATE);
				$('#ezip').val(html[x].DEALER_ZIP);
				$('#econtact').val(html[x].PRIMARY_CONTACT);
				$('#eemail').val(html[x].PRIMARY_CONTACT_EMAIL);
				$("#eaccount_type").val(html[x].ACCOUNT_TYPE);

            }// var uid2=$('#ufield1').val();
			
      				}   
    			}); 
}
	
	function login_dealer(id){
		var location="../index.php?dealer="+id;
		  var win=window.open(location, '_blank');
  win.focus();
		}

	function delcheck(){
       // alert($('input[name="delcheck[]"]:checked').length);
       // alert($('.delcheck :checked').size());
	if($('input[name="delcheck[]"]:checked').length > 0)
{
	$('.dealer_multidelete').removeClass("disabled");
    $('.dealer_multidelete').removeAttr("disabled","disabled");

}
else
{
$('.dealer_multidelete').addClass("disabled");
    $('.dealer_multidelete').Attr("disabled");
}
	 
	}
	function bgdelete(did,token){
var id=did;
bootbox.confirm("Do you really want to delete selected entry ?", function(result) {
if (result==true)
  {
	 var values1 = "id="+id+"&token="+token;
	 
  				/* Send the data using post and put the results in a div */
   				$.ajax({
			    	url: "../assets/ajax/admin/bg_delete.php",
			    	type: "get",
			    	data: values1,
					cache:false,
			      	success: function(html){
						if(html=="no"){
			        	alert("Something unexpected happened. Please try again");	
			      	} 
					else{
					var duser="bg"+did;
					$("#"+duser).remove();
					}
					}
				});  
  }
  });
	}
	
	function sidelete(did,token){
var id=did;
bootbox.confirm("Do you really want to delete selected entry ?", function(result) {
if (result==true)
  {
	 var values1 = "id="+id+"&token="+token;
	 
  				/* Send the data using post and put the results in a div */
   				$.ajax({
			    	url: "../assets/ajax/admin/si_delete.php",
			    	type: "get",
			    	data: values1,
					cache:false,
			      	success: function(html){
						if(html=="no"){
			        	alert("Something unexpected happened. Please try again");	
			      	} 
					else{
					var duser="si"+did;
					$("#"+duser).remove();
					}
					}
				});  
  }
  });
	}