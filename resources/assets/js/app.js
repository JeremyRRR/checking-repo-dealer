
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import Vuetify from 'vuetify'

Vue.use(Vuetify)

import VuejsDialog from "vuejs-dialog"

// Tell Vue to install the plugin.
Vue.use(VuejsDialog)

import jQuery from 'jquery'
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import HeaderNav from './components/header/header_nav.vue';
import HeaderInfo from './components/header/header_info.vue';
import forgetmodalform from './components/forget_password.vue';
import loginform from './components/login_form.vue';
import mobilenavheader from './components/dashboard_mobile_nav_tab.vue';
import dashboardTabMenu from './components/dashboard_tab_head.vue';
import dashboardTab from './DashboardTab.vue';
import OverviewTab from './components/dashboards/overview_tab.vue'; // OverviewTab in Dashboard
import AddNewVehicleTab from './components/dashboards/add_new_vehicle_tab.vue'; // Add New Vehicle(Dealer Inventory) in Dashboard
import ImportVehiclesTab from './components/dashboards/import_vehicles_tab.vue'; // Import Vehicles(Dealer Inventory) in Dashboard
import AdmOptionsTab from './components/dashboards/addendum_options_tab.vue'; // Addndum Options Tab in Dashboard
import OptionGroupsTab from './components/dashboards/option_groups_tab.vue'; // Options Group Tab in Dashboard
import UsersTab from './components/dashboards/users_tab.vue'; // OptionsTab in Dashboard
//Components in OverviewTab
import Printvehicle from './components/dashboards/overview_components/printvehicle.vue';
//Vue Component for templateBuilder
import templatebuilder from './components/templateBuilder.vue';
//Vue Google Map
import gmap from './components/gmap.vue';
//Vue Component for Upload Vehicle 
import uploadvphoto from './components/uploadVPhoto.vue';
// Admin portal
import DealerOverviewTab from './components/dashboards/dealer_overview_tab.vue'; // DealerOverviewTab in Dashboard
import AddNewDealerTab from './components/dashboards/add_new_dealer_tab.vue'; // DealerOverviewTab in Dashboard
import ImportDBTab from './components/dashboards/import_db.vue'; // DealerOverviewTab in Dashboard

if(jQuery('#app').length){
	new Vue({
		el: '#app',
		components:{
			forgetmodalform,
			loginform,
		}
	})	
}

if(jQuery('#template_body').length){
	new Vue({
		el: '#template_body',
		components:{
			templatebuilder
		}
	})	
}

if(jQuery('#map_body').length){
	new Vue({
		el: '#map_body',
		components:{
			gmap
		}
	})	
}

if(jQuery('#upload_photo_body').length){
	new Vue({
		el: '#upload_photo_body',
		components:{
			uploadvphoto
		}
	})	
}

if(jQuery('#app_body').length){
	const routes = [
		{
			name: 'Overview',
			path: '/allon/app/home/',
			components: {
				default: OverviewTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'DealerOverview',
			path: '/allon/app/home/dealer_overview',
			components: {
				default: DealerOverviewTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'AddNewDealer',
			path: '/allon/app/home/add_new_dealer',
			components: {
				default: AddNewDealerTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'ImportDB',
			path: '/allon/app/home/import_db',
			components: {
				default: ImportDBTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'AddNewVehicle',
			path: '/allon/app/home/add_new_vehicle/',
			components: {
				default: AddNewVehicleTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'ImportVehicles',
			path: '/allon/app/home/import_vehicles/',
			components: {
				default: ImportVehiclesTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'Options',
			path: '/allon/app/home/addendum_options/',
			components: {
				default: AdmOptionsTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'OptionGroups',
			path: '/allon/app/home/option_groups/',
			components: {
				default: OptionGroupsTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'Users',
			path: '/allon/app/home/users/',
			components: {
				default: UsersTab,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		},
		{
			name: 'Printvehicle',
			path: '/allon/app/home/printvehicle/:vid',
			components: {
				default: Printvehicle,
				headernav: HeaderNav,
				headerinfo: HeaderInfo,
				nav: dashboardTabMenu,
				mobilenav: mobilenavheader
			}
		}
	];
	
	const router = new VueRouter({ mode: 'history', routes: routes});
	new Vue(Vue.util.extend({ router }, dashboardTab)).$mount('#app_body');

}

