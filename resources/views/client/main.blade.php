<!DOCTYPE html>
<html lang="en">
<!-- DealerAddendums Inc. Patent Pending, All Rights Reserved 2016-->
	<head>
		<meta charset="utf-8" />
		<title>DealerAddendums Inc.</title>
		<meta name="description" content="DealerAddendums.com" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv='cache-control' content='no-cache'>
		<meta http-equiv='expires' content='0'>
		<meta http-equiv='pragma' content='no-cache'>
		<!-- for laravel csrf-token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- for ios 7 style, multi-resolution icon of 152x152 -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
		<link rel="icon" href="https://s3.amazonaws.com/addendum-websiteimages/favicon.ico">
		<link rel="apple-touch-icon" href="https://s3.amazonaws.com/addendum-websiteimages/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="https://s3.amazonaws.com/addendum-websiteimages/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="https://s3.amazonaws.com/addendum-websiteimages/apple-touch-icon-114x114.png">
		<meta name="apple-mobile-web-app-title" content="Dealeraddendums">
		<!-- for Chrome on Android, multi-resolution icon of 196x196 -->
		<meta name="mobile-web-app-capable" content="yes">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<!-- style -->
		<link rel="stylesheet" href="{{ asset('css/animate.css/animate.min.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('css/glyphicons/glyphicons.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('material-design-icons/material-design-icons.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('css/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />

		<!-- Vuetify CSS -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
		<link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
		
		<!-- build:css ../assets/styles/app.min.css -->
		<link rel="stylesheet" href="{{ asset('css/app_ori.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('css/datepicker.css') }}" type="text/css" />
		<link href="{{ asset('css/bootstrap-switch.css') }}" rel="stylesheet">
		<link href="{{ asset('css/select2.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" type="text/css" />
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css" type="text/css" />
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css">
		<link rel="stylesheet" href="{{ asset('css/pick-a-color-1.1.8.min.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('css/tooltips.css') }}" type="text/css" />

		<!-- jQuery -->
		<script src="{{ asset('libs/jquery/jquery/dist/jquery.js') }}"></script>

		<script type="text/javascript">
			/* it is used to do azax function in laravel. */
			$.ajaxSetup({
			  headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});
			var base_url = '{{ $base_url }}'; //Site Base URL
			window.laravel = <?php echo json_encode([
				'csrf_token' => csrf_token(),
				'base_url' => $base_url,
				'user' => $user,
				'dealer_info' => $dealer_info,
				'current_vehicle' => $current_vehicle,
				'plus_vehicle' => $plus_vehicle,
				'current_addendum' => $current_addendum,
				'need_addendum' => $need_addendum,
				'count_user' => $count_user,
				'last_login' => $last_login,
				'printed_addendum' => $printed_addendum,
				'todays' => $plus_vehicle,
				'last_30' => $last_30,
				'current_vehicle_c' => $current_vehicle_c,
				'count_dealer' => $count_dealer,
				'vlast_30' => $vlast_30,
				'setting_changed' => 0,
			]); ?>;
		</script>
	</head>

	<body>
		@yield('content')
		@include('client.partials.dashboard_footer')
	</body>
</html>