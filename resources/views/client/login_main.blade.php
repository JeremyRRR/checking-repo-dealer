<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Dealer Addendums Platform 4.0</title>
  <meta name="Dealer Addendums Platform App" />
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" /> -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for laravel csrf-token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="icon" href="https://s3.amazonaws.com/addendum-websiteimages/favicon.ico">
  <link rel="apple-touch-icon" href="https://s3.amazonaws.com/addendum-websiteimages/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="https://s3.amazonaws.com/addendum-websiteimages/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="https://s3.amazonaws.com/addendum-websiteimages/apple-touch-icon-114x114.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" href="https://s3.amazonaws.com/addendum-websiteimages/favicon.ico">
  
  <!-- style -->
  <link rel="stylesheet" href="{{ asset('css/animate.css/animate.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/glyphicons/glyphicons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('material-design-icons/material-design-icons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
  
  <!-- build:css ../css/app.min.css -->
  <link rel="stylesheet" href="{{ asset('css/app_ori.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/app_login.css') }}" type="text/css" />
  
  <!-- endbuild -->
  <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
  <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
  <!-- jQuery -->
  <script src="{{ asset('libs/jquery/jquery/dist/jquery.js') }}"></script>

  <script type="text/javascript">
      /* it is used to do ajax function in laravel. */
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
    var base_url = '{{$base_url}}'; //Site Base URL
    window.laravel = <?php echo json_encode([
        'csrf_token' => csrf_token(),
        'base_url' => $base_url,
      ]); ?>;
  </script>
</head>
<body>
   @yield('content')
   <!-- build:js scripts/app.html.js -->
  
    <!-- Bootstrap -->
    <script src="{{ asset('libs/jquery/tether/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>

    <!-- core -->
    <script src="{{ asset('js/app.js') }}"></script>
    @if(!$isIPhone)
      <script src="{{ asset('js/three.js') }}"></script>
    @endif
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom_login.js') }}"></script>
    <!-- endbuild -->
</body>
</html>