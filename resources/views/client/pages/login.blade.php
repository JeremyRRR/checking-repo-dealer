@extends('client.login_main')
@section('content')
<div class="app" id="app">
  <!-- Modal for forgot password START-->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <!-- Forget Password Form Vue Component -->
    <forgetmodalform></forgetmodalform>
  </div>
  <!-- Modal for forgot password END-->
  
  <!-- ############ LAYOUT START-->
  <div class="center-block w-xxl w-auto-xs p-y-md">
    <div class="navbar">
      <div class="pull-center">
        <a class="navbar-brand">
          <div> <img src="{{asset('/images/SVG/Login_Logo.svg')}}" height="35px" width="300px"></div>
       </a>
      </div>
    </div>

    <!-- Login Form Vue Component -->
    <loginform></loginform>  
    
    <div class="p-v-lg text-center">
      <div class="m-b"><a ui-sref="access.forgot-password" href="#myModal" class="text-primary _600"  data-toggle="modal">Forgot password?</a></div>
      <a href="/app/group" class="text-primary _600">Group Login</a> | <a href="/app/reseller" class="text-primary _600">Reseller Login</a>
      <div><h7 style="color:white;">Don't have an account?</h7> <a ui-sref="access.signup" href="/#signup" class="text-primary _600">Sign up</a></div>
    </div>

  </div>
  <!-- ############ LAYOUT END-->
</div>
@stop