 <!-- Footer -->
<div class="p-a p-x-md">
	<div class="row">
		<div class="col-sm-12 clearfix text-center">
			<p style="color:silver;"> DealerAddendums Inc.
				<a href="#" data-tooltip="{{ $latest_version_news }}" style="color:white">{{ $release_version }}</a>
				<br/>
			  	&copy; 
			  	<script type="text/javascript">
					//<![CDATA[
					var d = new Date()
					document.write(d.getFullYear())
					//]]>
				</script> 
			  	- All Rights Reserved -  **Patent Pending**
			</p>
		</div>
	</div>
</div>



<!-- / Footer --> 

<!-- Bootstrap --> 
<script src="{{ asset('libs/jquery/tether/dist/js/tether.min.js') }}"></script> 
<script src="{{ asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script> 
<!-- ajax --> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/moment.js') }}"></script> 
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script> 
<script src="{{ asset('js/app.js') }}"></script>

<!-- endbuild -->
