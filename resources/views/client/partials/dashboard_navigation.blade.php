<!--NAVIGATION-->
<div class="app-header black box-shadow">
  <div class="navbar"> 

    <!-- Logo --> 
    <a class="navbar-brand hidden-sm-down">
    <div ui-include="'assets/images/Logo_300x36.svg'"></div>
    <img src="https://d3manp75urydp4.cloudfront.net/Logo_300x36.png" alt="." class="hide"> </a><a class="navbar-brand">
    <div ui-include="'assets/images/Logo_300x36.svg'"></div>
    <img src="https://d3manp75urydp4.cloudfront.net/Logo_300x36.png" alt="."> </a> <a class="navbar-brand hidden-md-up">
    <div ui-include="'assets/images/icon.svg'"></div>
    <img src="https://d3manp75urydp4.cloudfront.net/icon.png" alt="." class="hide"> </a> 
    <!-- / Logo --> 
    
    <!-- Nabar right -->
    <ul class="nav navbar-nav pull-right">
      <li class="nav-item dropdown"> 
        <a href class="nav-link dropdown-toggle clear" data-toggle="dropdown">
          @if($dealer_info && $dealer_info->ACCOUNT_TYPE == 'Free')
            <span class="hidden-xs-down nav-text m-r-sm text-right"> 
              <span class="block l-h-1x _500">{{ $user->NAME }}</span>
              <small class="block l-h-1x text-muted">
                <i class="material-icons text-md">&#xe0af;</i>
                {{ $dealer_info?html_entity_decode($dealer_info->DEALER_NAME):'' }} - <strong style="color:#F60">Upgrade Account</strong>
              </small>
            </span>
          @else
            <span class="hidden-xs-down nav-text m-r-sm text-right">
              <span class="block l-h-1x _500">{{ $user->NAME }}</span>
              <small class="block l-h-1x text-muted">
                <i class="material-icons text-md">&#xe0af;</i> {{ $dealer_info?html_entity_decode($dealer_info->DEALER_NAME):'' }}
              </small>
            </span>
          @endif
          <span class="avatar w-32">
            <img class="avatarimg" data-name="{{ $user->NAME }}" src="https://d1xlji8qxtrdmo.cloudfront.net/{{ $user->USER_IMAGE }}" alt="..."> &nbsp;&nbsp; 
          </span> 
        </a>
        <div class="dropdown-menu pull-right dropdown-menu-scale">
          <a class="dropdown-item" data-toggle="modal" data-target="#settings">User Settings</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#profile">Dealer Profile
            @if($dealer_info && $dealer_info->ACCOUNT_TYPE == "Free")
            <strong style="color:#F60"> - Upgrade</strong>
            @endif
          </a>
        
          <a class="dropdown-item"  data-toggle="modal"  data-target="#addendum_settings">New Addendum Settings</a>
        
          <a class="dropdown-item"  data-toggle="modal"  data-target="#used_addendum_settings">Used Addendum Settings</a>

          <a class="dropdown-item"  data-toggle="modal"  data-target="#infosheet_settings">Standard Infosheet Settings</a>

          <a class="dropdown-item"  data-toggle="modal"  data-target="#infosheet_settings_c">Certified Infosheet Settings</a>
        
          <a class="dropdown-item"  data-toggle="modal"  data-target="#buyer_guide_settings">Buyers Guide Settings</a>

          <a class="dropdown-item"  data-toggle="modal"  data-target="#sms_modal">SMS Template Settings</a>

          <div class="dropdown-divider"></div>
          <a class="dropdown-item"  data-toggle="modal"  data-target="#reset_vehicle_options">Reset Vehicle Options</a>

          <div class="dropdown-divider"></div>
          <a class="dropdown-item"  data-toggle="modal"  data-target="#order">Order Supplies</a>

          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="template.php">Template Builder</a>
          <a class="dropdown-item" target="_blank" href="https://dealeraddendums.com/tutorial/">Help Video</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="?logout=logout">Logout</a> 
        </div>
      </li>
    </ul>
    <!-- / Navbar right --> 
    
  </div>
  <script type="text/javascript">
    //If User's avatar is null in DB, then initial User's avatar with first character of ther User's name
    <?php if($user->USER_IMAGE==""){ ?>
      $('.avatarimg').initial(); 
    <?php } ?>
  </script>
</div>
