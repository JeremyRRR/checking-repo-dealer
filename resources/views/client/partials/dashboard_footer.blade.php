 <!-- Footer -->
<div class="p-a p-x-md">
	<div class="row">
		<div class="col-sm-12 clearfix text-center">
			<p style="color:silver;"> DealerAddendums Inc.
				<a href="#" data-tooltip="{{ $latest_version_news }}" style="color:white">{{ $release_version }}</a>
				<br/>
			  	&copy; 
			  	<script type="text/javascript">
					//<![CDATA[
					var d = new Date()
					document.write(d.getFullYear())
					//]]>
				</script> 
			  	- All Rights Reserved -  **Patent Pending**
			</p>
		</div>
	</div>
</div>

<!-- Intercom API script -->
<script id="IntercomSettingsScriptTag">
    window.intercomSettings = {
      name: "<?php echo $user['NAME']; ?>",
      email: "<?php echo $user['EMAIL']; ?>",
      created_at: "<?php echo $user['USER_ID']; ?>",
      owner: "<?php echo $user['CREATOR_ID']; ?>",
      username: "<?php echo $user['USERNAME']; ?>",
      password: "<?php echo $user['PASSWORD']; ?>",
      plan: "<?php echo $dealer_info['ACCOUNT_TYPE']; ?>",
      "company": {
       "id": "<?php echo $dealer_info['DEALER_ID']; ?>",
       "name": "<?php echo $dealer_info['DEALER_NAME']; ?>",
       "created_at": "<?php echo $dealer_info['ID']; ?>",
       "plan": "<?php echo $dealer_info['ACCOUNT_TYPE']; ?>",
       "owner": "<?php echo $dealer_info['OWNER']; ?>",
       "30 day prints": "<?php echo $last_30['total']; ?>",
       "all prints": "<?php echo $printed_addendum['total']; ?>",
     },
     "user_hash": "<?php echo hash_hmac("sha256", $user["EMAIL"], "LpgODFHv3B_HVHRixEuOWFboBycM-kyweqxH_lNq"); ?>",
      app_id: "7cda8fc1bfea25032a2ba95d5d862ecb86f8e976"
    };
 	</script>
	<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://static.intercomcdn.com/intercom.v1.js';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<!-- Intercom API script -->

<!-- / Footer --> 

<!-- Bootstrap --> 
<script src="{{ asset('libs/jquery/tether/dist/js/tether.min.js') }}"></script> 
<script src="{{ asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script> 
<!-- ajax --> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/moment.js') }}"></script> 
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script> 
<script src="{{ asset('js/app.js') }}"></script>

<!-- endbuild -->
