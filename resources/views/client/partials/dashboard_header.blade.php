<div ui-view class="app-body" id="view"> 

    <!-- Statistics for xl and lg-->
    <div class="col-xs-12 col-sm-6 col-md-3 hidden-md-down">
      <div class="row">
        <div class="card card-block">
          <div class="pull-left m-r"> <img src="https://d3manp75urydp4.cloudfront.net/icon-car75.png" height="50em" width="50em"> </div>
          <div class="clear">
            <h5 class="card-title "><span class="c_current_vehicles">{{ $current_vehicle }}</span> Vehicles</h5>
            <p class="card-text ">+<span class="c_plus_today">{{ $plus_vehicle }}</span> TODAY</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 hidden-md-down">
      <div class="row">
        <div class="card card-block">
          <div class="pull-left m-r"> <img src="https://d3manp75urydp4.cloudfront.net/dark-icon75.png" height="50em" width="50em"> </div>
          <div class="clear">
            <h5 class="card-title "><span class="c_current_addendums">{{ $current_addendum }}</span> Current</h5>
            <p class="card-text "><span class="c_need_printing">{{ $need_addendum }}</span> UNPRINTED</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 hidden-md-down">
      <div class="row">
        <div class="card card-block">
          <div class="pull-left m-r"> <img src="https://d3manp75urydp4.cloudfront.net/user-icon75.png" height="50em" width="50em"> </div>
          <div class="clear">
            <h5 class="card-title "><span class="c_users">{{ $count_user }}</span> Users</h5>
            <p class="card-text ">LAST <span class="c_last_login">{{ $last_login }}</span></p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 hidden-md-down">
      <div class="row">
        <div class="card card-block">
          <div class="pull-left m-r"> <img src="https://d3manp75urydp4.cloudfront.net/printer-icon75.png" height="50em" width="50em"> </div>
          <div class="clear">
            <h5 class="card-title "><span class="c_addendum_printed">{{ $printed_addendum }}</span> Printed</h5>
            <p class="card-text muted "><span class="c_30_days">{{ $last_30 }}</span> IN LAST 30</p>
          </div>
        </div>
      </div>
    </div>
</div>


