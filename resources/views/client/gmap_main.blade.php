<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>DealerAddendums.com - Dealers Map</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- for ios 7 style, multi-resolution icon of 152x152 -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
	<!-- for Chrome on Android, multi-resolution icon of 196x196 -->
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- style -->

	<link rel="stylesheet" href="{{ asset('css/animate.css/animate.min.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/glyphicons/glyphicons.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('material-design-icons/material-design-icons.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
	
	<!-- build:css ../assets/styles/app.min.css -->
	<link rel="stylesheet" href="{{ asset('css/app_ori.css') }}" type="text/css" />
	<!-- endbuild -->
	<link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
	<style type="text/css">
		.list-group-item{
		  cursor:pointer;
		}
		#map{
		  color:#000;
		}
		li{
		list-style-type:none;
		}
		.box{
		margin-right: 0rem;
		margin-left: 0rem;
		}
		.text-md{
		font-size:1rem !important;
		}
  	</style>
  	<script src="{{ asset('libs/jquery/jquery/dist/jquery.js') }}"></script>
  	<script type="text/javascript">
		/* it is used to do azax function in laravel. */
		$.ajaxSetup({
		  headers: {
		      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});
		var base_url = '{{ $base_url }}'; //Site Base URL
		window.laravel = <?php echo json_encode([
			'csrf_token' => csrf_token(),
			'base_url' => $base_url,
		]); ?>;
	</script>
	
</head>
<body class="dark pace-done">
	@yield('content')
	@include('client.partials.gmap_footer')
</body>
</html>


