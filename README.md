<p align="center"><img src="https://d3manp75urydp4.cloudfront.net/Logo_300x36.png"></p>


## Getting Started

### Requirements

This project is based in Laravel 5.6 and Vue 2.0

You need composer and npm to install this project.

### Installing

```
	composer install
```

And then

```
	npm install
```

then compile dev version vue components

```
	npm run watch
```

After that, set database config from .env

Last, to login site
```
	http://your.domain.com/app/public/login
```

## Project Status

### Used Library

- Vue
- Vuetify for DataTable
- jQuery

### 2018-03-09

- Complete login portal
- Vehicle Overview page on Dashboard

### 2018-03-25
Addenumdum print part on Vehicle Overview Tab
- Crud Print options of the vehicle
- Implement download pdf and print view function of the vehicle information

### 2018-03-31
- Fixed Overview responsive
- Implement Phone SMS Print Modal Part
- Implement InfoSheet Print Modal Part
- Implement BuyerGuide Print Modal Part

### 2018-04-02
- Fixed Overview responsive
- Fixed Addendum print view responsive

### 2018-04-11
- Add Vehicle Page on Dashboard
- Import Vehicles Page on Dashboard
- Delte Vehicles Bulk Action on Vehicle Overview Page

### 2018-04-23
- Fixed 2018-04-12 bugs
- Complete edit dealer profile part.

### 2018-04-27
- Implement InfoSheet Settings functionality(Standard, Certified)
- Implement Print View(PDF download) function with Each Infosheet styles.

### 2018-05-11
- Fixed bugs
- Implement Addendum Settings functionality(New, Used)
- Implement Buyer Guide Settings functionality

### 2018-05-23
- Implement Addendum View(PDF downlaod) function with Each Addendum(New, Used) Styles.
- Complete User Tab.

### 2018-06-01
- Complete Addendum Options Tab
- Complete Group Options Tab
- Fixed Bugs

### 2018-06-09
- Complete Order Label Setting
- Complete Upgrade/Downgrade Membership

### 2018-06-15
- Implement Clear Label and Freshbook Recuerring Line function
- Checked and Fixed CRUD into Freshbook functions

### 2018-06-30
- Creating Template Builder

### 2018-07-13
- Fixed Template Builder middle preview
- Implement Template Builder right settings part

### 2018-07-30
- Complete Template Builder
- Implement Uploading Vehicle Photo

### 2018-09-03
- Complete Addendum Pritn Function
- Fixed google doc issues
- Fixed redirect main menu tabs
- Implement Chat function

### 2018-09-18

- Complete RootAdmin, ResellerAdmin, ResellerRoot, GroupAdmin Portal
- Add secondary VIN Decode button
- Complete User Privilege

### 2018-10-01

- Fixed Google Docs issues
- Update Template Builder UI
- Implement Bulk actions

### 2018-10-08

- Fixed Google Docs issues
- Implement Landing Page
- Implement Dealer Map by Vue
