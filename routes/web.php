<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Landing Page */
Route::get('/', function () {
    return view('welcome');
});

/* Login */
Route::get('/login','PagesController@login')->name('login');




/* Vechilce DashBoard After LogIn*/
Route::get('/home/{pages?}/{v_id?}', 'PagesController@home')->name('home')->middleware('auth');

/* Forget password submit*/
Route::post('/forget_password', 'Auth\AuthController@forget_password')->name('forget_password');

/* Login submit */
Route::post('/login_post', 'Auth\LoginController@postLogin')->name('login_post');

/* Get Privilege Info for Dashboard Tab Menu */
Route::post('/get_privilege', 'DashboardController@getPrivilege')->middleware('auth');

/* Get Data Info for Dashboard OverView */
Route::post('/get_data_for_overview', 'DashboardController@getDataForOverview')->middleware('auth');

/* Get Vehicle Data Info for Dashboard OverView */
Route::post('/get_vehciledata_for_overview', 'DashboardController@getVehicleDataForOverview')->middleware('auth');

/* Update User's print_unprint field */
Route::post('/update_print_unprint', 'DashboardController@updatePU')->middleware('auth');
/* Update User's new_used field */
Route::post('/update_new_used', 'DashboardController@updateNU')->middleware('auth');

/* Get Free Cap Status */
Route::post('/get_free_cap', 'DashboardController@getFreeCap')->middleware('auth');

/* Get Template Data */
Route::post('/get_template_data', 'DashboardController@getTemplateData')->middleware('auth');

/* Get Print Vehicle Data */
Route::post('/get_print_data', 'DashboardController@getPrintData')->middleware('auth');

/* Get Print options */
Route::post('/get_print_options', 'DashboardController@getPrintOptions')->middleware('auth');

/* Get Print options Table Data */
Route::post('/get_print_opt_tbl_data', 'DashboardController@getPrintOptTblData')->middleware('auth');

/* Add Option For Print Table */
Route::post('/add_option', 'DashboardController@addOption')->middleware('auth');

/* Add New Option */
Route::post('/add_new_option', 'DashboardController@addNewOption')->middleware('auth');

/* Get Sortable initial Data */
Route::post('/get_sortable', 'DashboardController@getSortable')->middleware('auth');

/* Save Sortable Options */
Route::post('/save_sortable', 'DashboardController@saveSortable')->middleware('auth');

/* Save Sortable Options */
Route::post('/delete_option', 'DashboardController@deleteOption')->middleware('auth');

/* Save Edited Option */
Route::post('/save_edited_option', 'DashboardController@saveEditedOption')->middleware('auth');

/* Save Adjustment Value */
Route::post('/save_adjustment', 'DashboardController@saveAdjustment')->middleware('auth');

/* Check print Addendum */
Route::post('/check_print_addendum', 'DashboardController@checkPrintAddendum')->middleware('auth');

/* Check print Addendum */
Route::post('/get_multiprint_data', 'PrintController@getMultiprintData')->middleware('auth');

/* Check print status */
Route::post('/print_status', 'PrintController@printStatus')->middleware('auth');

/* Get User Status */
Route::post('/get_user_status', 'PagesController@getUserStatus')->middleware('auth');


/* Generate Print Addendum PDF and Download */
Route::get('/multiple_download', 'PrintController@multipleDownload')->middleware('auth');

/* Get Vehicle Data from DealerInventory ID */
Route::post('/get_vehicle_info', 'DashboardController@getVehicleData')->middleware('auth');

/* Update Vehicle Data from DealerInventory ID */
Route::post('/update_vehicle_info', 'DashboardController@updateVehicleData')->middleware('auth');

/* Get data for print sms Modal */
Route::post('/get_data_for_printsms', 'PrintController@getDataForPrintsms')->middleware('auth');

/* set SMS Status in DealerInventory */
Route::post('/sms_status', 'PrintController@smsStatus')->middleware('auth');

/* Generate Phone SMS Print View to PDF and Download */
Route::get('/sms_download', 'PrintController@smsDownload')->middleware('auth');

/* Get Print Guide Data */
Route::post('/print_guide', 'PrintController@printGuide')->middleware('auth');

/* Save Buyer Guide Print Options */
Route::post('/save_bg_print', 'PrintController@saveBgPrint')->middleware('auth');

/* Save Buyer Guide BackPage Print Options */
Route::post('/save_bp_bg_print', 'PrintController@saveBpBgPrint')->middleware('auth');

/* Generate Buyer Print View to PDF and Download */
Route::get('/guide_download', 'PrintController@guideDownload')->middleware('auth');

/* Generate Spanish Buyer Print View to PDF and Download */
Route::get('/guide_download_spanish', 'PrintController@guideDownloadSpanish')->middleware('auth');

/* Generate BackPage Buyer Print View to PDF and Download */
Route::get('/backpage_download', 'PrintController@backpageDownload')->middleware('auth');

/* Generate Spanish BackPage Buyer Print View to PDF and Download */
Route::get('/backpage_download_spanish', 'PrintController@backpageDownloadSpanish')->middleware('auth');

/* Get data for infosheet modal */
Route::post('/get_data_for_infosheet', 'PrintController@getDataForInfosheet')->middleware('auth');

/* Set print infosheet status */
Route::post('/info_print_status', 'PrintController@infoPrintStatus')->middleware('auth');

/* Generate infosheet print View to PDF and Download */
Route::get('/info_print_download', 'PrintController@infoPrintDownload')->middleware('auth');

/* Decode VIN Number */
Route::post('/vin_decode', 'DashboardController@vinDecode')->middleware('auth');

/* Decode VIN Number using Only VINQuery*/
Route::post('/vinquery_decode', 'DashboardController@vinDecodeByVINQuery')->middleware('auth');

/* Save Dealer Inventory Information */
Route::post('/save_vehicle', 'DashboardController@saveVehicle')->middleware('auth');

/* Remove Imported Excel Vehicle Data from Inventory Table */
Route::post('/clear_imported_excel_vehicledata', 'DashboardController@clearImportedExcelVehicleData')->middleware('auth');

/* Import Excel Vehicle Data to Inventory Table */
Route::post('/import_vehicle_data', 'DashboardController@importVehicleData')->middleware('auth');

/* Vehicle Delete(DealerInventory row delete) */
Route::post('/vehicle_delete', 'DashboardController@vehicleDelete')->middleware('auth');

/* Retrieve Basic Data for dealer profile setting Modal */
Route::post('/get_data_for_dealer_profile', 'DashboardController@getDataForDealerProfile')->middleware('auth');

/* Update Dealer information */
Route::post('/update_dealer_info', 'DashboardController@updateDealerInfo')->middleware('auth');

/* Retrieve Basic Data for infosheet setting Modal */
Route::post('/get_data_for_infosheet_settings', 'DashboardController@getDataForInfoSheetSettings')->middleware('auth');

/* Update Infosheet Settings */
Route::post('/update_infosheet_settings', 'DashboardController@updateInfosheetSettings')->middleware('auth');

/* Retrieve Basic Data for Addendum setting Modal */
Route::post('/get_data_for_adm_settings', 'DashboardController@getDataForAdmSettings')->middleware('auth');

/* Update Addendum Settings */
Route::post('/update_adm_settings', 'DashboardController@updateAdmSettings')->middleware('auth');

/* Retrieve Basic Data for Users Tab */
Route::post('/get_data_for_users', 'DashboardController@getDataForUsers')->middleware('auth');

/* Save New User information */
Route::post('/save_user', 'DashboardController@saveUser')->middleware('auth');

/* Send User Password */
Route::post('/send_password','DashboardController@sendPassword')->middleware('auth');

/* Save Edit User information */
Route::post('/save_edit_user','DashboardController@saveEditUser')->middleware('auth');

/* Delete Selected User information */
Route::post('/delete_user', 'DashboardController@deleteUser')->middleware('auth');

/* Retrieve Basic Data for Addendum Options Tab */
Route::post('/get_data_for_options', 'DashboardController@getDataForOptions')->middleware('auth');

/* Add New Addendum Option */
Route::post('/add_new_ad_option', 'DashboardController@addNewAdOption')->middleware('auth');

/* Update Addendum Option */
Route::post('/update_ad_option', 'DashboardController@updateAdOption')->middleware('auth');

/* Delete Addendum Option */
Route::post('/delete_ad_option', 'DashboardController@deleteAdOption')->middleware('auth');

/* Save Addendum Option Re Order */
Route::post('/save_ad_options_reorder', 'DashboardController@saveAdOptionsReOrder')->middleware('auth');


/* Retrieve Basic Data for Addendum Options Tab */
Route::post('/get_data_for_groupoptions', 'DashboardController@getDataForGroupOptions')->middleware('auth');

/* Add New Addendum Option */
Route::post('/add_new_ad_groupoption', 'DashboardController@addNewAdGroupOption')->middleware('auth');

/* Delete Addendum Option */
Route::post('/delete_ad_option', 'DashboardController@deleteAdOption')->middleware('auth');

/* Save Addendum Option Re Order */
Route::post('/save_ad_options_reorder', 'DashboardController@saveAdOptionsReOrder')->middleware('auth');

/* Get All Addendum Options for Re Order */
Route::post('/get_all_ad_options', 'DashboardController@getAllAdOptions')->middleware('auth');

/* Retrieve Basic Data for Order Supplies Setting */
Route::post('/get_data_for_order_supplies', 'DashboardController@getDataForOrderSupplies')->middleware('auth');

/* Save Order Supplies Setting */
Route::post('/save_order_supplies', 'DashboardController@saveOrderSupplies')->middleware('auth');	

/* Clear Label and Freshbook Recuerring Line */
Route::post('/clear_label', 'DashboardController@clearLabel')->middleware('auth');

/* Go to Template Builder Page */
Route::get('/template/{id?}', 'TemplateController@index')->middleware('auth');

/* Get Exist Templates */
Route::post('/get_data_for_template', 'TemplateController@getDataForTemplate')->middleware('auth');

/* Get Template Data from _ID */
Route::post('/get_template', 'TemplateController@getTemplate')->middleware('auth');

/* Get Data for Base Template Style */
Route::post('/get_data_for_basetemplate', 'TemplateController@getDataForBasetemplate')->middleware('auth');

/* Save Template */
Route::post('/save_template', 'TemplateController@saveTemplate')->middleware('auth');

/* Update Template image */
Route::post('/update_template_images', 'TemplateController@updateTemplateImages')->middleware('auth');

/* Page to upload vehicle images */
Route::get('/upload_photo', 'PagesController@uploadPhotoPage')->middleware('auth');

/* upload vehicle images to */
Route::post('/upload_photo', 'PagesController@uploadPhoto')->middleware('auth');

/* Get Image Infos */
Route::post('/get_image_data', 'PagesController@getImageData')->middleware('auth');

/* Remove Image from S3 and DB */
Route::post('/remove_image_data', 'PagesController@removeImageData')->middleware('auth');

/* Update Image information */
Route::post('/save_image_data', 'PagesController@saveImageData')->middleware('auth');

/* Get Privilege Information */
Route::post('/get_privilageinfo', 'PagesController@getPrivilegeInfo')->middleware('auth');

/* Log Out */
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

/* Get User Account Information */
Route::post('/get_account_info', 'DashboardController@getAccountInfo')->middleware('auth');



////// Dealers Map API /////////
/* Map */
Route::get('/map','PagesController@gmap')->name('gmap');
Route::post('/get_map_info', 'PagesController@getMapInfo');
Route::post('/get_location', 'PagesController@getLocation');

////// END Dealers Map API /////////



///////////////// ADMIN Portal /////////////////////

/* Get Dealer Data Info for Admin Dashboard OverView */
Route::post('/get_dealerdata_for_dealerOverview', 'AdminDashboardController@getDealerDataForDealerOverview')->middleware('auth');

/* Pause Dealer Activity */
Route::post('/change_dealer_activity', 'AdminDashboardController@changeDealerActivity')->middleware('auth');

/* Delete Dealer */
Route::post('/delete_dealer', 'AdminDashboardController@deleteDealer')->middleware('auth');

/* Get Selected Dealer info */
Route::post('/get_dealer_info', 'AdminDashboardController@getDealerInfo')->middleware('auth');

/* Save edited Dealer Info */
Route::post('/edit_dealer', 'AdminDashboardController@updateDealerInfo')->middleware('auth');

/* Add new Dealer */
Route::post('/add_new_dealer', 'AdminDashboardController@addNewDealer')->middleware('auth');

/* Change admin information */
Route::post('/update_admin_information', 'AdminDashboardController@changeAdminInformation')->middleware('auth');

/* Get API Keys From DB */
Route::post('/get_api_keys', 'AdminDashboardController@getAPIKeys')->middleware('auth');

/* update API Keys */
Route::post('/update_api_keys', 'AdminDashboardController@updateAPIKeys')->middleware('auth');

/* Get Backgrounds */
Route::post('/get_backgrounds', 'AdminDashboardController@getBackgrounds')->middleware('auth');

/* Add new Background */
Route::post('/add_new_bg', 'AdminDashboardController@addNewBG')->middleware('auth');

/* Remove Background */
route::post('/remove_bg_img', 'AdminDashboardController@removeBgInfo')->middleware('auth');

/* Get Stock Images */
Route::post('/get_stock_images', 'AdminDashboardController@getStockImages')->middleware('auth');

/* Add new Stock Image */
Route::post('/add_new_stock', 'AdminDashboardController@addNewStock')->middleware('auth');

/* Remove Stock Image */
Route::post('/remove_stock_img', 'AdminDashboardController@removeStockImg')->middleware('auth');

/* Get user info for Admins */
Route::post('/get_user_info', 'AdminDashboardController@getUserInfo')->middleware('auth');

Route::post('/login_with_dealer', 'Auth\LoginController@logInDealer');

/* Import Merge Database */
Route::post('/get_all_tables', 'AdminDashboardController@getAllTables')->middleware('auth');
Route::post('/import_db', 'AdminDashboardController@importDB')->middleware('auth');

/* Remove Dealers */
Route::post('/dealer_delete', 'AdminDashboardController@removeDealers')->middleware('auth');

/** Set/Stop Impersonate User **/
Route::get('/users/{id}/impersonate', 'UserController@impersonate');
Route::get('/users/stop', 'UserController@stopImpersonate');
/** Set/Stop Impersonate User **/

Route::group(['middleware' => 'impersonate'], function()
{
    // ... 
});