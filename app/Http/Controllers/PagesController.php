<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
/* Load Model Start */
use App\User;
use App\Model\Dealer;
use App\Model\UserPrivilege;
use App\Model\DealerInventory;
use App\Model\VehiclePhoto;
use App\Model\Makes;
use App\Model\Models;
use App\Model\Colors;
use App\Model\CountTable;
use App\Model\AddendumData;
/* Load Model End */

class PagesController extends Controller
{
    //

	/**
	 * Rending login page 
	 */
    public function login(){
    	$this->data['isIPhone'] = $this->isIphone(); //Using IPhone?
    	return $this->_loadContent('client.pages.login');
    }
    /**
	 * Rending home dashboard page after login
     * @param $request Object
	 */
    public function home($pages = '', $v_id = '', Request $request){

        /* init variable */
        $user = Auth::user();
        
        $dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
        $result_status = $this->getUserStatus();
        $result_status = json_decode($result_status,true);
        $makeselect = '[';
        if(!empty($dealer_info->MAKE1)){
            $makeselect .= '"'.$dealer_info->MAKE1.'"';
        }
        if(!empty($dealer_info->MAKE2)){
            $makeselect .= ', "'.$dealer_info->MAKE2.'"';
        }
        if(!empty($dealer_info->MAKE3)){
            $makeselect .= ', "'.$dealer_info->MAKE3.'"';
        }
        if(!empty($dealer_info->MAKE4)){
            $makeselect .= ', "'.$dealer_info->MAKE4.'"';
        }
        if(!empty($dealer_info->MAKE5)){
            $makeselect .= ', "'.$dealer_info->MAKE5.'"';
        }
        $makeselect .=']';

        $this->data['cur_page'] = $pages;
        $this->data['user'] = $user;
        $this->data['dealer_info'] = $dealer_info;
        //$this->data['privilege_info'] = $privilege_info;
        $this->data['current_vehicle'] = $result_status['current_vehicle'];
        $this->data['plus_vehicle'] = $result_status['plus_vehicle'];
        $this->data['current_addendum'] = $result_status['current_addendum'];
        $this->data['need_addendum'] = $result_status['need_addendum'];
        $this->data['count_user'] = $result_status['count_user']; 
        $this->data['last_login'] = $result_status['last_login'];
        $this->data['printed_addendum'] = $result_status['printed_addendum'];
        $this->data['last_30'] = $result_status['last_30'];
        $this->data['makeselect'] = $makeselect;
        $this->data['current_vehicle_c'] = $result_status['current_vehicle_c'];
        $this->data['count_dealer'] = $result_status['count_dealer'];
        $this->data['vlast_30'] = $result_status['vlast_30'];

        return $this->_loadContent('client.pages.dashboard');
    }

    /**
     *
     * Get user's status information for header
     *
     * @return array
     */
    public function getUserStatus(){
        /* init variable */
        $current_vehicle = 0; // number of vehicle stock
        $current_vehicle_c = 0; // current number of vehicle stock 
        $plus_vehicle = 0; // number of vehicle stocked today
        $current_addendum = 0; // number of current addendum
        $need_addendum = 0; // number of need addendum
        $count_user = 0; // number of user
        $count_dealer = 0; // number of dealer
        $last_login = ''; // User Last login date
        $printed_addendum = 0; // number of printed addendum
        $last_30 = 0; // number of printed addendum last 30 days
        $vlast_30 = 0; // number of input addendum last 30 days
        $user = Auth::user();
        
        $dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
        $privilege_info = $this->_getPrivilege();
        
        $cur_date = date('Y-m-d');
        $date30=date('Y-m-d', strtotime("now -30 days") );
        if($user->USER_TYPE == 'RootAdmin'){
            $current_vehicle = (Dealer::sum('VEHICLES_NEW')) + (Dealer::sum('VEHICLES_USED')) + 309 + 460000; //???
            $current_vehicle_c = (Dealer::sum('CURRENT_NEW')) + (Dealer::sum('CURRENT_USED'));
            $need_addendum = (Dealer::sum('NEED_NEW')) + (Dealer::sum('NEED_USED'));
            $count_user = User::count();
            $count_dealer = Dealer::where('Active', 'Yes')->count();
            $last_log_user = User::where('USER_ID', $user->USER_ID)->orderBy('LAST_LOGIN', 'desc')->first();
            if($last_log_user){
                $last_login = date("d-m-Y", strtotime($last_log_user->LAST_LOGIN));
            }
            $printed_addendum = 135443 + 3454 + 460000; //???
            $last_30 = 18500; //???
            $vlast_30 = 129484; //???

        }elseif($user->USER_TYPE == 'ResellerAdmin' || $user->USER_TYPE == 'ResellerUser'){
            $admin_id = $user->USER_TYPE == 'ResellerUser'? $user->RESELLER_ADMIN: $user->USER_ID;
            $current_vehicle = DealerInventory::getCountInventoryResellerAdmin($admin_id);
            $plus_vehicle = DealerInventory::getCountInventoryResellerAdmin($admin_id, $cur_date);
            $current_addendum = DealerInventory::getCountInventoryResellerAdmin($admin_id,'',1);
            $need_addendum = DealerInventory::getCountInventoryResellerAdmin($admin_id,'',0);
            $printed_addendum = DealerInventory::getCountInventoryResellerAdmin($admin_id,'',1,'');
            $last_30 = DealerInventory::getCountInventoryByNUandPrintDate($admin_id, $date30, $cur_date);
            $vlast_30 = DealerInventory::getCountInventoryByNUandPrintDate($admin_id, $date30, $cur_date, 1);
            $count_user = User::leftJoin('dealer_dim', 'dealer_dim.DEALER_ID', '=', 'users.DEALER_ID')->where('dealer_dim.OWNER', $admin_id)->count();
            $count_dealer = Dealer::where('Active', 'Yes')->where('OWNER', $admin_id)->count();
        }elseif($user->USER_TYPE == 'GroupAdmin'){
            $dealer_group = $user->DEALER_GROUP;
            $current_vehicle = DealerInventory::getCountInventoryGroupAdmin($dealer_group);
            $plus_vehicle = DealerInventory::getCountInventoryGroupAdmin($dealer_group, $cur_date);
            $current_addendum = DealerInventory::getCountInventoryGroupAdmin($dealer_group,'',1);
            $need_addendum = DealerInventory::getCountInventoryGroupAdmin($dealer_group,'',0);
            $printed_addendum = DealerInventory::getCountInventoryGroupAdmin($dealer_group,'',1,'');

            $last_30 = DealerInventory::getCountInventorywithlast30GroupAdmin($dealer_group, $date30, $cur_date);
            $vlast_30 = DealerInventory::getCountInventorywithlast30GroupAdmin($dealer_group, $date30, $cur_date, 1);
            $count_user = User::leftJoin('dealer_dim', 'dealer_dim.DEALER_ID', '=', 'users.DEALER_ID')->where('dealer_dim.DEALER_GROUP', $dealer_group)->count();
            $count_dealer = Dealer::where('Active', 'Yes')->where('DEALER_GROUP', $dealer_group)->count();
        }else{
           if($privilege_info['APP_ENABLE'][$user->USER_TYPE] && $dealer_info){
                if($user->NEW_USED_BOTH == 'New'){
                    $current_vehicle = $dealer_info->VEHICLES_NEW;
                    $plus_vehicle = $dealer_info->PLUS_TODAY_N;
                    $current_addendum = $dealer_info->CURRENT_NEW;
                    $need_addendum = $dealer_info->NEED_NEW;
                    $printed_addendum = $dealer_info->PRINTED_NEW;
                    $last_30 = $dealer_info->LAST30_NEW;
                }elseif($user->NEW_USED_BOTH == 'Used'){
                    $current_vehicle = $dealer_info->VEHICLES_USED;
                    $plus_vehicle = $dealer_info->PLUS_TODAY_U;
                    $current_addendum = $dealer_info->CURRENT_USED;
                    $need_addendum = $dealer_info->NEED_USED;
                    $printed_addendum = $dealer_info->PRINTED_USED;
                    $last_30 = $dealer_info->LAST30_USED;
                }else{
                    $current_vehicle = (int)$dealer_info->VEHICLES_NEW + (int)$dealer_info->VEHICLES_USED;
                    $plus_vehicle = (int)$dealer_info->PLUS_TODAY_N + (int)$dealer_info->PLUS_TODAY_U;
                    $current_addendum = (int)$dealer_info->CURRENT_NEW + (int)$dealer_info->CURRENT_USED;
                    $need_addendum = (int)$dealer_info->NEED_NEW + (int)$dealer_info->NEED_USED;
                    $printed_addendum = (int)$dealer_info->PRINTED_NEW + (int)$dealer_info->PRINTED_USED;
                    $last_30 = (int)$dealer_info->LAST30_NEW + (int)$dealer_info->LAST30_USED;
                }
                $count_user = User::getTotalUserNum($user->DEALER_ID);
                $last_log_user = User::getLastLoginInfo($user->DEALER_ID);
                if($last_log_user){
                    $last_login = date("d-m-Y", strtotime($last_log_user->LAST_LOGIN));
                }
            }else{
                $current_vehicle = DealerInventory::getCountInventoryByNU($user->NEW_USED_BOTH);
                $plus_vehicle = DealerInventory::getCountInventoryByNU($user->NEW_USED_BOTH, '',$cur_date);
                $current_addendum = DealerInventory::getCountInventoryByNU($user->NEW_USED_BOTH, 1);
                $need_addendum = DealerInventory::getCountInventoryByNU($user->NEW_USED_BOTH, 0);
                $printed_addendum = DealerInventory::getCountInventoryByNU($user->NEW_USED_BOTH, 1,'','');
                $count_user = User::getTotalUserNum();
                $last_log_user = User::getLastLoginInfo();
                if($last_log_user){
                    $last_login = date("d-m-Y", strtotime($last_log_user->LAST_LOGIN));
                }
                $last_30 = DealerInventory::getCountInventoryByNUandPrintDate($user->NEW_USED_BOTH, $date30, $cur_date);
            } 
        }
        
        return json_encode(array(
            'current_vehicle' => $current_vehicle,
            'current_vehicle_c' => $current_vehicle_c,
            'plus_vehicle' => $plus_vehicle,
            'current_addendum' => $current_addendum,
            'need_addendum' => $need_addendum,
            'printed_addendum' => $printed_addendum,
            'count_user' => $count_user,
            'count_dealer' => $count_dealer,
            'last_login' => $last_login,
            'last_30' => $last_30,
            'vlast_30' => $vlast_30
        ));
    }

    /**
     * Get Privileage Information
     */
    public function _getPrivilege(){
        $p_info = UserPrivilege::get();
        $privilege_info = [];
        foreach($p_info as $info){
            $privilege_info[$info->PAGENAME] = $info;
        }
        return $privilege_info;
    }


    /**
     *
     * Return Privilege Information to view  
     *
     * @return Array Object
     */
    public function getPrivilegeInfo(){
        $privilege_info = $this->_getPrivilege();
        return json_encode(array('p_info' => $privilege_info));
    }

    /**
     *
     * Page to upload vehicle photos
     *
     */
    public function uploadPhotoPage(){
        $this->data['vimages'] = VehiclePhoto::getVImage();
        return $this->_loadContent('client.pages.upload_photo');
    }

    /**
     *
     * Get Image Data
     */
    public function getImageData(){
        $vimages = VehiclePhoto::getVImage();
        $makes = Makes::getAllMake();
        $models = Models::getAllModel();
        $colors = Colors::getAllColor();
        return json_encode(array('vimages' => $vimages, 'makes' => $makes, 'models' => $models, 'colors' => $colors));
    }

    /**
     *
     * Upload Vehicle photos
     */
    public function uploadPhoto(Request $request){
        if($request->hasFile('file')){
            $uuid = $request->input('uuid');
            /* Load AWS S3 Libary For Upload File */
            include_once(app_path().'/Libraries/s3/image_check.php');
            /* Load AWS S3 Libary For Upload File */
            $img_file = $request->file;
            $tmp_name = $_FILES['file']['tmp_name'];
            $name = $img_file->getClientOriginalName();
            $size = $img_file->getClientSize();
            $ext = $request->file->extension();
            $image_name = $uuid.'.'.$ext;
            /* Load AWS S3 Libary For Upload File */
            include_once(app_path().'/Libraries/s3/s3_config_2.php');
            /* Load AWS S3 Libary For Upload File */
            if(strlen($name) > 0){
                if (in_array($ext, $valid_formats)) {
                    $result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read', [], $_FILES['file']['type']);
                    if($result){
                        $id = VehiclePhoto::insertVImage(array('IMAGE' => $image_name));
                        if($id){
                            $info = VehiclePhoto::getVImageByID($id);
                            return json_encode(array('status' => 'success', 'info' => $info));
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     *
     * Remove Image from DB and S3
     */
    public function removeImageData(Request $request){
        $id = $request->input('id');
        $info = VehiclePhoto::getVImageByID($id);
        $image_name = $info->IMAGE;
        VehiclePhoto::removeVImageByID($id);
        /* Load AWS S3 Libary For Upload File */
        include_once(app_path().'/Libraries/s3/s3_config_2.php');
        /* Load AWS S3 Libary For Upload File */
        $result = $s3->deleteObject($bucket, $image_name);
        return json_encode(array('status' => 'success'));
    }

    /**
     *
     * Save Image information
     *
     */
    public function saveImageData(Request $request){
        $data = $request->input('data');
        $id = $data['_ID'];
        unset($data['_ID'], $data['MAKE_NAME'], $data['MODEL_NAME'], $data['COLOR_NAME'], $data['edit_on']);
        $status = VehiclePhoto::updateVImage($id, $data);
        if($status){
            $info = VehiclePhoto::getVImageByID($id);
            return json_encode(array('status' => 'success', 'info' => $info));
        }
    }


    /// Dealer Map Functions ////

    public function gmap(){
        return $this->_loadContent('client.pages.gmap');
    }

    public function getLocation(Request $request){
        $dtype = '';
        if($request->has('dtype')){
            $dtype = $request->input('dtype');
        }
        $query = Dealer::select('_ID', 'DEALER_ADDRESS', 'LAT1', 'LNG1', 'DEALER_CITY', 'DEALER_STATE', 'DEALER_ZIP', 'DEALER_NAME', 'ACCOUNT_TYPE')->where('ACTIVE', 'Yes');
        if($dtype == 'free'){
            $query =$query->where('ACCOUNT_TYPE', 'Free');
        }else if($dtype == 'paid'){
            $query =$query->where('ACCOUNT_TYPE', '<>','Free');
        }
        $location_info = $query->get();
        return json_encode(array('status' => 'success', 'location_info' => $location_info));
    }

    public function getMapInfo(){

        $count = CountTable::first();
        $cu_dealer = Dealer::where('ACTIVE','Yes')->count();
        $addendum_data = AddendumData::where('ITEM_PRICE', '>', 1)->sum('ITEM_PRICE');
        

        $count_data = array();
        $count_data['print_date']=date("Y-m-d");
        $count_data['count1']=$count->print_today;
        $count_data['atprints']=$count->printed;
        $count_data['oldprints']=$count->old_addendum;
        $count_data['last_30']=$count->last_30;
        $count_data['most_a_day']=$count->most_aday;

        $pa_dealer = Dealer::where('ACTIVE','Yes')->where('ACCOUNT_TYPE','<>','Free')->count();
        $fr_dealer = Dealer::where('ACTIVE','Yes')->where('ACCOUNT_TYPE','Free')->count();

        $vehicle_info = DealerInventory::where('PRINT_STATUS', '1')->where('STATUS', '1')->orderBy('PT', 'desc')->take(5)->get();

        $dealers = array();
        foreach($vehicle_info as $vinfo){
            $obj = array();
            $dealer_info = Dealer::where('DEALER_ID', $vinfo->DEALER_ID)->first();
            $obj['drow'] = $dealer_info;
            $obj['vrow'] = $vinfo;
            array_push($dealers, $obj);
        }

        return json_encode(array('status' => 'success', 'count_data' => $count_data, 'pa_dealer' => $pa_dealer, 'fr_dealer' => $fr_dealer, 'cu_dealer' => $cu_dealer, 'addendum_data' => $addendum_data, 'dealers' => $dealers));
    }
}
