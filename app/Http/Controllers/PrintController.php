<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
/* Load Model Start */
use App\User;
use App\Model\Dealer;
use App\Model\UserPrivilege;
use App\Model\DealerInventory;
use App\Model\TemplateData;
use App\Model\AddendumData;
use App\Model\AddendumDefault;
use App\Model\SMSTemplate;
use App\Model\VehicleBuyerGuide;
use App\Model\DefaultBuyerGuide;
use App\Model\InfoSheet;
use App\Model\TemplateBuilder;
use App\Model\Backgrounds;
/* Load Model End */
/*Load Dompdf Package */
use Dompdf\Dompdf;
use Dompdf\Options;
/*Load Dompdf Package */

/* Load PHP BardCode Generator*/
use Picqer\Barcode\BarcodeGenerator;
use Picqer\Barcode\BarcodeGeneratorPNG as BarcodeGeneratorPNG;
/* Load PHP BardCode Generator*/
/* Load SimpleHtmlParser Vendor for crawling */
use Sunra\PhpSimple\HtmlDomParser;
/* Load SimpleHtmlParser Vendor for crawling */

class PrintController extends Controller
{
	/**
	 *
	 * Return initial print data from vid
	 *
	 * @param vehicle id
	 * @return array
	 */
	public function getMultiprintData(Request $request){
		$vid_req = $request->input('vid');
		$vid_req = json_decode($vid_req);
		$vids = array();
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}

		$user = Auth::user();

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		$cdn_url="https://d1ja3ktoroz7zi.cloudfront.net/";
		$bcdn_url="https://d1ja3ktoroz7zi.cloudfront.net/";
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$print_infos = array();
		foreach($vids as $vid){
			$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
			$template_data = TemplateData::getTemplateData($user->DEALER_ID, $vehicle_info->NEW_USED);
			$template_builder = TemplateBuilder::getTemplateBuilderRow($user->DEALER_ID, $template_data->ADDENDUM_STYLE);
			//font style and font size for print addendum
			$bold3 = '';
			$bold4 = '';
			if($template_data->FONT_SIZE == 'Small'){
				$bold3 = "font-size:10px; line-height:10px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
				$bold4="font-size:8px;  line-height:8px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
			}elseif($template_data->FONT_SIZE == 'Medium'){
				$bold3="font-size:12px;  line-height:12px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
				$bold4="font-size:10px;  line-height:10px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
			}elseif($template_data->FONT_SIZE == "Large"){
				$bold3="font-size:14px;  line-height:14px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
				$bold4="font-size:12px;  line-height:12px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
			}
			$order_by = '_ID';
			if($vehicle_info->RE_ORDER == 1 || $dealer_info->RE_ORDER == 1){
				$order_by = 'ORDER_BY';
			}
			$ad_data = AddendumData::getOptionsForTable($user->DEALER_ID, $vehicle_info->VIN_NUMBER, $order_by);
			foreach($ad_data as $key => $arow){
				$oprice = '';
				if($arow->ITEM_PRICE<0){
					$oprice=floatVal(abs($arow->ITEM_PRICE));
					$oprice=number_format($oprice, 2, '.', ',');
					$oprice='-$'.$oprice;
				}else if($arow->ITEM_PRICE=="0"){
					$oprice='Free';
				}else if(strtolower($arow->ITEM_PRICE)=="np"){
					$oprice='';
				}else if(strtolower($arow->ITEM_PRICE)=="fr"){
					$oprice='Free';
				}else if(strtolower($arow->ITEM_PRICE)=="in"){
					$oprice='Included';
				}else if(strtolower($arow->ITEM_PRICE)=="inc"){
					$oprice='Included';
				}else if(strtolower($arow->ITEM_PRICE)=="nc"){
					$oprice='No Charge';
				}else{
					$oprice=floatval($arow->ITEM_PRICE);
					$oprice=number_format($oprice, 2, '.', ',');
					$oprice='$'.$oprice;
				}
				$ad_data[$key]->ITEM_PRICE = $oprice;
				if($arow->OR_OR_AD == 2){
					$optiontext="";
					$option_group = $arow->ITEM_DESCRIPTION;
					$option_group = explode(",", $option_group);
					foreach($option_group as $optiong){
						$optiontext.=$optiong."<br/>";
					}
					if($arow->ITEM_DESCRIPTION != '' && $arow->ITEM_DESCRIPTION != null){
						$ad_data[$key]->ITEM_DESCRIPTION = $optiontext;	
					}
				}else{
					$ad_data[$key]->ITEM_DESCRIPTION = html_entity_decode($arow->ITEM_DESCRIPTION);
				}
			}

			$print_style = array(
				'tb_standard_medium' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '#000',
						'height' => '1000px',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '400px',
						'margin' => '25px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '15px',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => '',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '40px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '565px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '735px'
					),
					'Stock_image_img' => array(
						'height' => '230px'
					),
					'Bar_float' => array(
						'height' => '160px'
					),
					'Bar_text' => array(
						'height' => '70px'
					),
					'Bar_code_img' => array(
						'height' => '90px'
					)
				),
				'tb_standard_small' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '#000',
						'height' => '1000px',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '540px',
						'margin' => '25px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '15px',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => '',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '40px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '665px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '855px'
					),
					'Stock_image_img' => array(
						'height' => '114px'
					)
				),
				'tb_standard_large' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '#000',
						'height' => '1000px',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '310px',
						'margin' => '5px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '15px',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => '',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '40px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '415px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '585px'
					),
					'Stock_image_img' => array(
						'height' => '380px'
					),
					'Bar_float' => array(
						'height' => '300px'
					),
					'Bar_text' => array(
						'height' => '170px'
					),
					'Bar_code_img' => array(
						'height' => '120px'
					),
					'Qr_text2' => array(
						'height' => '140px'
					),
				),
				'tb_standard_top' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => 'auto',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '10px',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '410px',
						'margin' => '430px 0 0 20px'
					),
					'Total' => array(
						'position' => 'absolute',
						'top' => '850px',
						'margin-top' => '',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => 'bold',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '50px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '820px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '10px'
					),
					'Stock_image_img' => array(
						'height' => '400px'
					),
					'Bar_float' => array(
						'height' => '320px'
					),
					'Bar_text' => array(
						'height' => '170px'
					),
					'Bar_code_img' => array(
						'height' => '120px'
					),
					'Qr_text2' => array(
						'height' => '150px'
					),
				),
				'tb_narrow_medium' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '252px 956px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => '#000',
						'height' => '986px',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '400px',
						'margin' => '25px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '15px',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => '',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '40px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '565px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '705px'
					),
					'Stock_image_img' => array(
						'height' => '253px'
					),
					'Bar_float' => array(
						'height' => '160px'
					),
					'Bar_text' => array(
						'height' => '70px'
					),
					'Bar_code_img' => array(
						'height' => '90px'
					)
				),
				'tb_narrow_small' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '252px 956px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => '#000',
						'height' => '986px',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '540px',
						'margin' => '5px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '15px',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => '',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '40px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '665px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '825px'
					),
					'Stock_image_img' => array(
						'height' => '122px'
					)
				),
				'tb_narrow_large' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '252px 956px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => '#000',
						'height' => '986px',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '300px',
						'margin' => '5px 0 0 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '15px',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => '',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '40px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '415px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '555px'
					),
					'Stock_image_img' => array(
						'height' => '400px'
					),
					'Bar_float' => array(
						'height' => '280px'
					),
					'Bar_text' => array(
						'height' => '160px'
					),
					'Bar_code_img' => array(
						'height' => '120px'
					),
					'Qr_text2' => array(
						'height' => '140px'
					),
				),
				'tb_narrow_top' => array(
					'Printit' => array(
						'background-image' => $template_builder?'url('.$bcdn_url.$template_builder->BACKGROUND_IMAGE.')':'',
						'background-size' => '252px 956px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => '',
						'height' => '986px',
						'position' => 'relative',
						'background-color' => $template_builder?$template_builder->BACKGROUND:'',
						'padding-bottom' => '10px',
						'float' => '',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '380px',
						'margin' => '430px 0 0 20px'
					),
					'Total' => array(
						'position' => 'absolute',
						'top' => '825px',
						'margin-top' => '',
						'margin-left' => '',
						'font-size' => '',
						'font-weight' => '',
						'font-family' => '',
						'width' => '100%'
					),
					'Address' => array(
						'padding-top' => '50px',
						'font-size' => '13px',
						'font-family' => ''
					),
					'Subtotal' => array(
						'top' => '820px',
						'font-size' => $template_builder?$this->_font_size('standard', $template_builder->SUBTOTAL_FONT):'',
						'display' => $template_builder?$this->_display($template_builder->SUBTOTAL_FONT):'',
					),
					'Stock_image' => array(
						'top' => '0px'
					),
					'Stock_image_img' => array(
						'height' => '400px'
					),
					'Bar_float' => array(
						'height' => '280px'
					),
					'Bar_text' => array(
						'height' => '160px'
					),
					'Bar_code_img' => array(
						'height' => '120px'
					),
					'Qr_text2' => array(
						'height' => '150px'
					),
				),
				'Standard' => array(
					'Printit' => array(
						'background-image' => '',
						'background-size' => '',
						'background-color' => 'white',
						'left' => '0',
						'width' => '341px',
						'color' => '',
						'height' => 'auto',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '',
						'float' => '',
						'font-family' => ''
					)
				),
				'EPA Template' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => 'auto',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '385px',
						'margin' => '420px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '48px',
						'margin-left' => '210px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '35px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Honda_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Honda_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Toyota_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Toyota_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Hyundai_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Hyundai_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Cadillac_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Cadillac_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Cadillac2015_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Cadillac2015-3_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Chevrolet_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Chevrolet_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Ford_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Ford_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Acura_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Acura_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Audi_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Audi_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Chrysler_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Chrysler_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Dodge_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Dodge_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Fiat_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Fiat_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Infiniti_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Infiniti_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Jaguar_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Jaguar_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Kia_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Kia_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Lexus_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Lexus_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Masarati_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Masarati_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Mercedes_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Mercedes_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Mitsubishi_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Mitsubishi_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Nissan_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Nissan_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Subaru_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Subaru_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Volvo_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Volvo_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Waldorf_EPA' => array(
					'Printit' => array(
						'background-image' => 'url(https://d1ja3ktoroz7zi.cloudfront.net/Waldorf_Background.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom1' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM1_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom1p' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM1_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Narrow Custom 1' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->NARROW_CUSTOM1_BG.')',
						'background-size' => '252px 959px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => 'black',
						'height' => '959px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '20px',
						'float' => '',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '360px',
						'margin' => '420px 0px 0px 10px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '23px',
						'margin-left' => '130px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					)
				),
				'Narrow Custom 2' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->NARROW_CUSTOM2_BG.')',
						'background-size' => '252px 959px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => 'black',
						'height' => '959px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '20px',
						'float' => '',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '280px',
						'margin' => '0 0 0 10px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '23px',
						'margin-left' => '140px',
						'font-size' => '20px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'LogoSection' => array(
						'height' => '230px',
						'padding-top' => '0px'
					),
					'LogoSection_img' => array(
						'width' => '100%',
						'height' => 'auto',
						'max-width' => '100%'
					)
				),
				'Narrow Custom 2p' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->NARROW_CUSTOM2_BG.')',
						'background-size' => '252px 959px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => 'black',
						'height' => '959px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '20px',
						'float' => '',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '280px',
						'margin' => '0 0 0 10px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '23px',
						'margin-left' => '140px',
						'font-size' => '20px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'LogoSection' => array(
						'height' => '230px',
						'padding-top' => '0px'
					),
					'LogoSection_img' => array(
						'width' => '100%',
						'height' => 'auto',
						'max-width' => '100%'
					)
				),
				'Custom1 SMS' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM1_SMS_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom1 QR' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM1_QR_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom1 VIN' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM1_VIN_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom1 VIN No MSRP' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM1_VIN_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '320px',
						'margin' => '107px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom2' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM2_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '440px',
						'margin' => '140px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom3' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM3_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '440px',
						'margin' => '140px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom6' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM6_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '870px',
						'margin' => '100px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '23px',
						'margin-left' => '160px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => '137px'
					)
				),
				'Custom7' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM7_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '870px',
						'margin' => '117px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '-3px',
						'margin-left' => '160px',
						'font-size' => '20px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => '137px'
					),
					'Msrp' => array(
						'margin' => '0 0 0 200px',
						'text-align' => 'center',
						'width' => '100px'
					),
					'Header' => array(
						'height' => '70px',
						'margin' => '0',
						'padding' => '17px 0 0 70px'
					),
					'Base' => array(
						'font-size' => '16px',
						'font-weight' => 'bold',
						'margin' => '398px 0 0 220px',
						'width' => '77px'
					),
					'Options' => array(
						'height' => '136px'
					),
					'Options_into' => array(
						'line-height' => '23px'
					)
				),
				'Custom7 Narrow' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM7N_BG.')',
						'background-size' => '252px 959px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => '',
						'height' => '959px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => 'Arial, Helvetica, sans-serif'
					),
					'Second_section' => array(
						'height' => '870px',
						'margin' => '117px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '3px',
						'margin-left' => '100px',
						'font-size' => '20px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => '118px'
					),
					'Msrp' => array(
						'margin' => '2px 0 0 118px',
						'text-align' => 'right',
						'width' => '100px'
					),
					'Header' => array(
						'height' => '53px',
						'margin' => '0',
						'padding' => '13px 0 0 50px'
					),
					'Base' => array(
						'font-size' => '14px',
						'font-weight' => 'bold',
						'margin' => '418px 0 0 130px',
						'width' => '86px'
					),
					'Options' => array(
						'height' => '95px'
					),
					'Options_into' => array(
						'line-height' => '23px'
					)
				),
				'Custom5' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM5_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '570px',
						'margin' => '110px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '51px',
						'font-size' => '13px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom4' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM4_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '400px',
						'margin' => '92px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '20px',
						'margin-left' => '195px',
						'font-size' => '24px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => '130px'
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom2 QR' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM2_QR_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '440px',
						'margin' => '140px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Custom2 No MSRP' => array(
					'Printit' => array(
						'background-image' => 'url('.$bcdn_url.$template_data->CUSTOM2_NM_BG.')',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => '',
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '440px',
						'margin' => '140px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'PrePrinted' => array(
					'Printit' => array(
						'background-image' => 'url('.$cdn_url.'twoup.jpg)',
						'background-size' => '336px 989px',
						'background-color' => '',
						'left' => '',
						'width' => '336px',
						'color' => '#0a1d69',
						'height' => '1000px',
						'position' => 'relative',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '580px',
						'margin' => '0'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '15px',
						'margin-left' => '255px',
						'font-size' => '12px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '30px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Standard Branded' => array(
					'Printit' => array(
						'background-image' => 'url('.$cdn_url.$template_data->STANDARD_BACKGROUND.'.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => $template_data->TEXT_COLOR,
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '440px',
						'margin' => '20px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Dealer Branded Custom QR' => array(
					'Printit' => array(
						'background-image' => 'url('.$cdn_url.$template_data->QR_BRANDED_COLOR.'.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => $template_data->TEXT_COLOR,
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '440px',
						'margin' => '20px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Dealer Branded with VIN Barcode' => array(
					'Printit' => array(
						'background-image' => 'url('.$cdn_url.$template_data->VIN_BRANDED_COLOR.'.jpg)',
						'background-size' => '346px 985px',
						'background-color' => '',
						'left' => '',
						'width' => '346px',
						'color' => $template_data->TEXT_COLOR,
						'height' => '984px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '440px',
						'margin' => '20px 0px 0px 20px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '25px',
						'margin-left' => '195px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'AAT-Narrow' => array(
					'Printit' => array(
						'background-image' => 'url('.$this->site_baseurl.'/img/aat-narrow.jpg)',
						'background-size' => '299px 1039px',
						'background-color' => '',
						'left' => '',
						'width' => '299px',
						'color' => '#000',
						'height' => '1039px',
						'position' => 'relative',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => '',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '120px',
						'margin' => '35px 0 0 10px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '35px',
						'margin-left' => '160px',
						'font-size' => '20px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => '118px'
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'AAT Narrow Series S' => array(
					'Printit' => array(
						'background-image' => 'url('.$this->site_baseurl.'/img/series-s.jpg)',
						'background-size' => '299px 1039px',
						'background-color' => '',
						'left' => '',
						'width' => '299px',
						'color' => '#000',
						'height' => '1039px',
						'position' => 'relative',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => '',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '120px',
						'margin' => '35px 0 0 10px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '35px',
						'margin-left' => '160px',
						'font-size' => '20px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => '118px'
					),
					'Address' => array(
						'padding-top' => '32px',
						'font-size' => '15px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
				'Reynolds Narrow Preprinted' => array(
					'Printit' => array(
						'background-image' => 'url('.$cdn_url.$template_data->Reynolds_Narrow_Pre_bg.')',
						'background-size' => '252px 959px',
						'background-color' => '',
						'left' => '',
						'width' => '252px',
						'color' => $template_data->TEXT_COLOR,
						'height' => '959px',
						'position' => '',
						'background-color' => '',
						'padding-bottom' => '10px',
						'float' => 'left',
						'font-family' => ''
					),
					'Second_section' => array(
						'height' => '580px',
						'margin' => '5px 0px 0px 10px'
					),
					'Total' => array(
						'position' => '',
						'top' => '',
						'margin-top' => '8px',
						'margin-left' => '100px',
						'font-size' => '22px',
						'font-weight' => 'bold',
						'font-family' => 'Arial, Helvetica, sans-serif',
						'width' => ''
					),
					'Address' => array(
						'padding-top' => '5px',
						'font-size' => '13px',
						'font-family' => 'Arial, Helvetica, sans-serif'
					)
				),
			);
			$barcode_url = '';
			if(!empty($vehicle_info->VIN_NUMBER)){
				$barcode_generator = new BarcodeGeneratorPNG;
				$barcode_url = 'data:image/png;base64,' . base64_encode($barcode_generator->getBarcode($vehicle_info->VIN_NUMBER, $barcode_generator::TYPE_CODE_39,2,200));
			}
			$base_price = '';
			if(!empty($template_data->BASE_PRICE)){
				$base_price = '$'.number_format(floatval($template_data->BASE_PRICE), 2, '.', ',');
			}
			$array_data = array(
				'vehicle_info' => $vehicle_info,
				'template_builder' => $template_builder,
				'template_data' => $template_data,
				'bold3' => $bold3,
				'bold4' => $bold4,
				'ad_data' => $ad_data,
				'print_style' => $print_style,
				'barcode_url' => $barcode_url,
				'base_price' => $base_price,
				'vid' => $vid
			);
			array_push($print_infos, $array_data);
		}
		
		$return_array = array(
			'dealer_info' => $dealer_info,
			'print_infos' => $print_infos
		);
		return json_encode($return_array);
	}

	/**
	 *
	 * Check Print status
	 *
	 * @param type, vehicle_id
	 * @return
	 */
	public function printStatus(Request $request){
		$vid_req = $request->input('vid');
		$vid_req = json_decode($vid_req);
		$vids = array();
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}
		$user = Auth::user();
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		foreach($vids as $vid){
			$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
			$initial_printinfo = $this->getPrintData($vid);
			$aprice = $initial_printinfo['aprice'];
			$vtotal = $initial_printinfo['total_price'];
			
			//number of AddendumData row
			$check_add = AddendumData::getNumberAddendumData($user->DEALER_ID, $vehicle_info->VIN_NUMBER); 
			//If number of AddendumData is zero, then insert Addendum Data base Addendum Default Data
			if($check_add != 0 && isset($vid) && $vehicle_info->EDIT_STATUS == 0){
				$ad_infos = AddendumDefault::getAdDefaultData($user->DEALER_ID, $vehicle_info->NEW_USED, $vehicle_model, $vehicle_style);
				foreach($ad_infos as $ad_info){
					$insert_array = array(
						'VEHICLE_ID' => $vid, 
						'ITEM_NAME' => $ad_info->ITEM_NAME, 
						'ITEM_DESCRIPTION' => $ad_info->ITEM_DESCRIPTION,
						'ITEM_PRICE' => $ad_info->ITEM_PRICE,
						'ACTIVE' => 'yes', 
						'DEALER_ID' => $user->DEALER_ID, 
						'CREATION_DATE' => date("Y-m-d"), 
						'SEPARATOR_BELOW' => $ad_info->SEPARATOR_BELOW, 
						'SEPARATOR_ABOVE' => $ad_info->SEPARATOR_ABOVE, 
						'OR_OR_AD' => $ad_info->OG_OR_AD, 
						'VIN_NUMBER' => $vehicle_info->VIN_NUMBER, 
						'ORDER_BY' => $ad_info->RE_ORDER
					);
					AddendumData::insertAddendumData($insert_array);
				}
			}
			DealerInventory::updateInventoryData($vid, array('PRINT_STATUS' => '1', 'PRINT_FLAG' => '1' , 'PRINT_DATE'=>date('Y-m-d'), 'UPDATE_DATE' => date('Y-m-d'), 'PT' => date('Y-m-d H:i:s')));
			if($vehicle_info->NEW_USED=="New"){
				$current_vehicle=$dealer_info->VEHICLES_NEW;
				$plus_vehicle=$dealer_info->PLUS_TODAY_N;
				$current_addendum=$dealer_info->CURRENT_NEW;
				$need_addendum=$dealer_info->NEED_NEW;
				$printed_addendum=$dealer_info->PRINTED_NEW;
				$last_30=$dealer_info->LAST30_NEW;
	
				if($vehicle_info->PRINT_STATUS == 0){
					$update_array = array(
						'CURRENT_NEW' => (int)$current_addendum +1,
						'LAST30_NEW' => (int)$last_30 +1,
						'PRINTED_NEW' => (int)$printed_addendum +1,
						'NEED_NEW' => (int)$need_addendum -1,
					);
					Dealer::updateDealerByID($user->DEALER_ID, $update_array);
				}
			}elseif($vehicle_info->NEW_USED=="Used"){
				$current_vehicle=$dealer_info->VEHICLES_USED;
				$plus_vehicle=$dealer_info->PLUS_TODAY_U;
				$current_addendum=$dealer_info->CURRENT_USED;
				$need_addendum=$dealer_info->NEED_USED;
				$printed_addendum=$dealer_info->PRINTED_USED;
				$last_30=$dealer_info->LAST30_USED;
	
				if($vehicle_info->PRINT_STATUS == 0){
					$update_array = array(
						'CURRENT_USED' => (int)$current_addendum +1,
						'LAST30_USED' => (int)$last_30 +1,
						'PRINTED_USED' => (int)$printed_addendum +1,
						'NEED_USED' => (int)$need_addendum -1,
					);
					Dealer::updateDealerByID($user->DEALER_ID, $update_array);
				}
			}	
		}
		return json_encode(array('status' => 'success'));
	}



	/**
	 *
	 * Generate PDF from print data
	 */
	public function multipleDownload(Request $request){
		$vid_req = $request->input('vid');
		$vid_req = json_decode($vid_req);
		$vids = array();
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}

		$user = Auth::user();
		$dtype = $request->has('dtype')?$request->input('dtype'):'';
		$bcdn_url = 'https://d1ja3ktoroz7zi.cloudfront.net/';
		$fcdn_url = 'https://d1xlji8qxtrdmo.cloudfront.net/';

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$js="";
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') === false && strpos($_SERVER['HTTP_USER_AGENT'], 'CriOS') === false){
			if($dtype == 'print'){
				$js='<script type="text/javascript">window.print();</script>';
			}else{
				$js="";	
			}
		}

		$default_style='
			#printit{
				background-repeat: no-repeat;
				background-position:top;
				top: 0;
				margin: 0;
			}
			#temp_printit{
				background-repeat: no-repeat;
				background-position:top;
				top: 0;
				margin: 0;
			}
			#logo_section {
				width:100%;
				height:100px;
				text-align:center;
				padding-top:20px;
			}
			#logo_section img, #temp_logo_section img {
				width:auto;
				height:100px;
				max-width:92%;
			}
			#second-section{
				overflow: hidden;
				width: 90%;
			}
			#second-section table{
				margin-top:20px;
			}
			#second-section_preprinted{
				overflow: hidden;
				width: 100%;
				height:580px;
			}
			#second-section_preprinted table{
				margin-top:48px;
			}
			.bold{
				font-weight: bold;
			}
			.temp_vin_section{
		    	text-align: left;
		  	}	
			.temp_vin_section > div{
			    width: 50%;
			    display: inline-block;
			    padding-top: 1px;
			    padding-bottom: 1px;
		  	}

			.bold1{
				font-family:Arial, Helvetica, sans-serif;
				font-size:12px;
				font-weight:bold;
				line-height:12px;
			}
			.bold2{
				font-family:Arial, Helvetica, sans-serif;
				font-size:10px;
				font-weight:bold;
				line-height:10px;
				
			}
			.totaltext {
				color: #ffffff;
				float: left;
				padding-right: 5px;
				text-align: right;
				width: 53%;
			}
			.totalnum {
				float: left;
				text-align: right;
				width: 42%;
			}
			#subtotal {
			    float: left;
			    width: 90px;
		  	}
		  	#subtotal_num {
			    float: right;
			    margin-right: 10px;
			    text-align: right;
		  	}
		  	#total {
			    color: #fff;
			    float: left;
			    text-align: right;
			    width: 105px;
		  	}
		  	#total_num {
			    float: left;
			    text-align: right;
			    
		  	}
			.subtotal {
				position: absolute;
				top: 565px;
				width: 100%;
			}
			.subtotaltext {
				float: left;
				text-align: right;
				width: 55%;
			}
			.subtotalnum {
				float: left;
				text-align: right;
				width: 41%;
			}
			.temp_address {
			    clear: both;
			    font-weight: bold;
			    text-align: left;
		  	}
			.address{
				padding-top:35px;
				padding-bottom:19.5px;
				margin-left:20px;
				font-size:15px;
				line-height:15px;
				font-weight:bold;
				font-family:Arial, Helvetica, sans-serif;
			}
			.address_custom5{
				padding-top:51px;
				padding-bottom:19.5px;
				margin-left:20px;
				font-size:13px;
				line-height:15px;
				width:200px;
				font-weight:bold;
				font-family:Arial, Helvetica, sans-serif;
				float: left;
			}
			.stock_image {
				padding-left: 13px;
				position: absolute;
			}
			.stock_image > img {
				height: 230px;
			}
			.medium_qr_code .qr_title {
				font-size: 20px;
				font-weight: bold;
				height: 25px;
				text-align: center;
				width: 100%;
			}
			.medium_qr_code .qr_float {
				clear: both;
				height: 140px;
				text-align: center;
				width: 100%;
			}
			.medium_qr_code .qr_text {
				float: left;
				font-size: 18px;
				height: 140px;
				text-align: left;
				width: 60%;
			}
			.medium_qr_code .qr_text2{
				float: left;
			    font-size: 18px;
			    text-align: left;
			    width: 100%;
				clear:both;
			}
			.medium_qr_code .qr_code {
				float: right;
				height: 140px;
				text-align: center;
				width: 40%;
			}
			.medium_qr_code .qr_code img {
				width:80% !important;
				height:auto !important;
			}
			.medium_qr_code .qr_footer {
				font-size: 12px;
				height: 40px;
				line-height: 13px;
				text-align: center;
				width: 100%;
			}
			.medium_bar_code .bar_title {
				font-size: 20px;
				font-weight: bold;
				height: 25px;
				text-align: center;
				width: 96%;
			}
			.medium_bar_code .bar_float {
				clear: both;
				text-align: center;
				width: 96%;
			}
			.medium_bar_code .bar_text {
				font-size: 14px;
				text-align: left;
				width: 100%;
			}
			.medium_bar_code .bar_code {
				text-align: center;
				width: 100%;
			}
			.medium_bar_code .bar_code img {
				width: 300px !important;
			}
			.medium_bar_code .bar_footer {
				font-size: 11px;
				height: 40px;
				line-height: 12px;
				text-align: center;
				width: 96%;
			}
			#qr{
				background-color:#000; 
				height:25px; 
				width:100%; 
				text-align:center; 
				color:#FFF; 
				font-family:Cambria; 
				font-size:16px;
				padding-top:5px;
			}
			.dae{
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
				font-weight:bold;
				line-height:14px;
		    }
			.msrp{
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				font-weight:bold;
				line-height:13px;
		    }
		    .msrp_custom6{
				font-size: 17px;
			    font-weight: bold;
			    height: 20px;
			    margin: 5px 0 0 195px;
		    }
		    .msrp_custom7{
				font-size: 17px;
			    font-weight: bold;
			    height: 20px;
			    margin: 0 0 0 200px;
			    text-align: right;
			    width: 100px;
		    }
		    .st_vin{
				font-family:Arial, Helvetica, sans-serif;
				font-size:16px;
				font-weight:bold;
				line-height:16px;
			}
			.last5{
				color: #000;
				font-size: 22px;
				margin-left: 173px;
				margin-top: 134px;
				width: 120px;
				font-weight:bold;
			}
			.barcode{
				margin-left: 50px;
		    	margin-top: 30px;
			}
			.barcode_dealerbrand{
				margin-left: 50px;
		    	margin-top: 45px;
			}
			.header{
				font-size: 17px;
			    font-weight: bold;
			    height: 70px;
			    line-height: 18px;
			    margin: 17px 0 0 100px;
			}
			.toptext{
			}
			.base {
			    font-size: 14px;
			    height: 16px;
			    margin: 423px 0 0 220px;
			    text-align: right;
			    width: 77px;
			}
			.options {
			    clear: both;
			    height: 84px;
			    margin-top: 10px;
			    width: 100%;
			}
			.options-into {
			    clear: both;
			    line-height: 20px;
			    width: 100%;
			}
			.left {
			    float: left;
			    padding-left: 17px;
			    width: 215px;
			}

			.right {
			    float: left !important;
			    text-align: right;
			    width: 65px;
			}
			.total-op {
			    clear: both;
			    font-size: 14px;
			    font-weight: bold;
			    margin-left: 230px;
			    text-align: right;
			    width: 67px;
				height:10px;
			}
			.total-op_custom4 {
				width:130px;
				margin-top:30px;
				margin-left:195px;
				font-size:18px;
				font-weight:bold;
				font-family:Arial, Helvetica, sans-serif;
				text-align:right;
			}
			.qr{
				padding-top:50px;
				padding-bottom:19.5px;
				margin-left:20px;
				font-size:13px;
				line-height:15px;
				font-weight:bold;
				font-family:Arial, Helvetica, sans-serif;
		    }
		    .subtotal_preprinted{
				margin-top:30px;
				margin-left:255px;
				font-size:12px;
				font-weight:bold;
				font-family:Arial, Helvetica, sans-serif;
			}
			.nameaddress{
				float:left;
				width:210px;
				font-weight:bold;
			}
			.model{
				float:left;
				width:120px;
				text-align:right;
				font-weight:bold;
			}
			.stock{
				width:80px;
				float:left;
				padding-top:30px;
				text-align:center;
				font-weight:bold;
			}
			.vin{
				width:140px;
				float:left;
				padding-top:30px;
				text-align:center;
				font-weight:bold;
			}
			.msrp_preprinted{
				padding-top:30px;
				font-weight:bold;
			}
			.extra{
				margin-left:10px;
			}
			.qrarea{
				clear: both;
			    margin-left: 42px;
			    margin-top: 34px;
			     width: 298px;
				color:#000;
			}
			.qrtitle{
				clear: both;
			    font-size: 14px;
			    font-weight: bold;
			    width: 100%;
			}
			.qrdesc{
				float: left;
			    font-size: 12px;
			    font-weight: bold;
			    height: 105px;
			    margin-right: 6px;
				margin-top:2px;
			    width: 48%;
				line-height:12px;
				word-wrap:break-word;
			}
			.mainqr{
				float: left;
			    margin-left: 23px;
			    margin-top: 6px;
			    width: 40%;
			}
			.vin_section{
				line-height: 17px;
				padding-left: 110px;
				padding-top: 485px;
				font-size: 14px;
			}
			.msrp_section{
				padding-left: 80px;
				padding-top: 32px;
				width: 165px;
			}
			.standard .font_MED {
			    font-size: 14px;
			    line-height: 14px;
			  }
			  .standard .font_SM{
			    font-size: 12px;
			    line-height: 12px;
			  }
			  .standard .font_LG{
			    font-size: 16px;
			    line-height: 16px;
			  }
			  .standard .font_XL{
			    font-size: 20px;
			    line-height: 20px;
			  }
			  .standard .font_XXL{
			    font-size: 24px;
			    line-height: 24px;
			  }

			  .narrow .font_MED {
			    font-size: 12px;
			    line-height: 12px;
			  }
			  .narrow .font_SM{
			    font-size: 10px;
			    line-height: 10px;
			  }
			  .narrow .font_LG{
			    font-size: 14px;
			    line-height: 14px;
			  }
			  .narrow .font_XL{
			    font-size: 18px;
			    line-height: 18px;
			  }
			  .narrow .font_XXL{
			    font-size: 20px;
			    line-height: 20px;
			  }
			  .narrow .mediumT .medium_qr_code .qr_title{
		    font-size: 20px;
		      font-weight: bold;
		      height: 40px;
		      text-align: center;
		      width: 100%;
		    padding-top:5px;
		  }
		  .narrow .mediumT .medium_qr_code .qr_float{
		    clear: both;
		      height: 120px;
		      text-align: center;
		      width: 100%;
		  }
		  .narrow .mediumT .medium_qr_code .qr_text{
		    float: left;
		      font-size: 16px;
		      height: 120px;
		      text-align: left;
		      width: 60%;
		  }
		  .narrow .mediumT .medium_qr_code .qr_code{
		    float: right;
		      height: 120px;
		      text-align: center;
		      width: 40%;
		  }
		  .narrow .mediumT .medium_qr_code .qr_code img{
		    width:80% !important;
		    height:auto !important;
		  }
		  .narrow .mediumT .medium_qr_code .qr_footer{
		    font-size: 10px;
		      height: 40px;
		      line-height: 12px;
		      text-align: center;
		      width: 100%;
		  }

		  .narrow .mediumT .medium_bar_code .bar_title{
		    font-size: 18px;
		      font-weight: bold;
		      height: 25px;
		      text-align: center;
		      width: 90%;
		  }
		  .narrow .mediumT .medium_bar_code .bar_float{
		    clear: both;
		      height: 150px;
		      padding-left: 10px;
		      text-align: center;
		      width: 90%;
		  }
		  .narrow .mediumT .medium_bar_code .bar_text{
		      font-size: 12px;
		      height: 70px;
		      text-align: left;
		      width: 100%;
		  }
		  .narrow .mediumT .medium_bar_code .bar_code{
		      text-align: center;
		      width: 90%;
		  }
		  .narrow .mediumT .medium_bar_code .bar_code img{
		    height: 70px !important;
		      width: 190px !important;
		  }
		  .narrow .mediumT .medium_bar_code .bar_footer{
		    font-size: 9px;
		      height: 40px;
		      line-height: 12px;
		      text-align: center;
		      width: 100%;
		  }

		  .standard .mediumT .medium_qr_code .qr_title{
		    font-size: 20px;
		      font-weight: bold;
		      height: 40px;
		      text-align: center;
		      width: 100%;
		    padding-top:5px;
		  }
		  .standard .mediumT .medium_qr_code .qr_float{
		    clear: both;
		      height: 140px;
		      text-align: center;
		      width: 100%;
		  }
		  .standard .mediumT .medium_qr_code .qr_text{
		    float: left;
		      font-size: 16px;
		      height: 140px;
		      text-align: left;
		      width: 60%;
		  }
		  .standard .mediumT .medium_qr_code .qr_code{
		    float: right;
		      height: 140px;
		      text-align: center;
		      width: 40%;
		  }
		  .standard .mediumT .medium_qr_code .qr_code img{
		    width:80% !important;
		    height:auto !important;
		  }
		  .standard .mediumT .medium_qr_code .qr_footer{
		    font-size: 10px;
		      height: 40px;
		      line-height: 12px;
		      text-align: center;
		      width: 100%;
		  }

		  .standard .mediumT .medium_bar_code .bar_title{
		    font-size: 20px;
		      font-weight: bold;
		      height: 25px;
		      text-align: center;
		      width: 90%;
		  }
		  .standard .mediumT .medium_bar_code .bar_float{
		    clear: both;
		      height: 160px;
		      padding-left: 10px;
		      text-align: center;
		      width: 90%;
		  }
		  .standard .mediumT .medium_bar_code .bar_text{
		      font-size: 14px;
		      height: 70px;
		      text-align: left;
		      width: 100%;
		  }
		  .standard .mediumT .medium_bar_code .bar_code{
		      text-align: center;
		      width: 90%;
		  }
		  .standard .mediumT .medium_bar_code .bar_code img{
		    height: 90px !important;
		      width: 250px !important;
		  }
		  .standard .mediumT .medium_bar_code .bar_footer{
		    font-size: 11px;
		      height: 40px;
		      line-height: 12px;
		      text-align: center;
		      width: 100%;
		  }
		  .topT .medium_qr_code .qr_title{
		    font-size: 20px;
		      font-weight: bold;
		      height: 25px;
		      text-align: center;
		      width: 100%;
		  }
		  .topT .medium_qr_code .qr_float{
		    clear: both;
		      height: 140px;
		      text-align: center;
		      width: 100%;
		  }
		  .topT .medium_qr_code .qr_text{
		    float: left;
		      font-size: 18px;
		      height: 140px;
		      text-align: left;
		      width: 60%;
		  }
		  .topT .medium_qr_code .qr_text2{
		    float: left;
		      font-size: 18px;
		      height: 150px;
		      text-align: left;
		      width: 100%;
		    clear:both;
		  }
		  .topT .medium_qr_code .qr_code{
		    float: right;
		      height: 140px;
		      text-align: center;
		      width: 40%;
		  }
		  .topT .medium_qr_code .qr_code img{
		    width:80% !important;
		    height:auto !important;
		  }
		  .topT .medium_qr_code .qr_footer{
		    font-size: 12px;
		      height: 40px;
		      line-height: 13px;
		      text-align: center;
		      width: 100%;
		  }

		  .topT .medium_bar_code .bar_title{
		    font-size: 20px;
		      font-weight: bold;
		      height: 25px;
		      text-align: center;
		      width: 96%;
		  }
		  .topT .medium_bar_code .bar_float{
		    clear: both;
		      height: 300px;
		      text-align: center;
		      width: 96%;
		  }
		  .topT .medium_bar_code .bar_text{
		      font-size: 14px;
		      height: 170px;
		      text-align: left;
		      width: 100%;
		  }
		  .topT .medium_bar_code .bar_code{
		      text-align: center;
		      width: 100%;
		  }
		  .topT .medium_bar_code .bar_code img{
		    height: 120px !important;
		      width: 300px !important;
		  }
		  .topT .medium_bar_code .bar_footer{
		    font-size: 11px;
		      height: 40px;
		      line-height: 12px;
		      text-align: center;
		      width: 96%;
		  }

		  .largeT .medium_qr_code .qr_title{
		    font-size: 20px;
		      font-weight: bold;
		      height: 25px;
		      text-align: center;
		      width: 100%;
		  }
		  .largeT .medium_qr_code .qr_float{
		    clear: both;
		      height: 140px;
		      text-align: center;
		      width: 100%;
		  }
		  .largeT .medium_qr_code .qr_text{
		    float: left;
		      font-size: 18px;
		      height: 140px;
		      text-align: left;
		      width: 60%;
		  }
		  .largeT .medium_qr_code .qr_text2{
		    float: left;
		      font-size: 18px;
		      height: 140px;
		      text-align: left;
		      width: 100%;
		    clear:both;
		  }
		  .largeT .medium_qr_code .qr_code{
		    float: right;
		      height: 140px;
		      text-align: center;
		      width: 40%;
		  }
		  .largeT .medium_qr_code .qr_code img{
		    width:80% !important;
		    height:auto !important;
		  }
		  .largeT .medium_qr_code .qr_footer{
		    font-size: 12px;
		      height: 40px;
		      line-height: 13px;
		      text-align: center;
		      width: 100%;
		  }

		  .largeT .medium_bar_code .bar_title{
		    font-size: 20px;
		      font-weight: bold;
		      height: 25px;
		      text-align: center;
		      width: 96%;
		  }
		  .largeT .medium_bar_code .bar_float{
		    clear: both;
		      height: 300px;
		      text-align: center;
		      width: 96%;
		  }
		  .largeT .medium_bar_code .bar_text{
		      font-size: 14px;
		      height: 170px;
		      text-align: left;
		      width: 100%;
		  }
		  .largeT .medium_bar_code .bar_code{
		      text-align: center;
		      width: 100%;
		  }
		  .largeT .medium_bar_code .bar_code img{
		    height: 120px !important;
		      width: 300px !important;
		  }
		  .largeT .medium_bar_code .bar_footer{
		    font-size: 11px;
		      height: 40px;
		      line-height: 12px;
		      text-align: center;
		      width: 96%;
		  }
		  .infographic {
		    clear: both;
		    height: auto;
		  }
		  .infographic .stock_image {
		    clear: both;
		  }
		';
		/*@page {margin:$top_margin+em $right_margin+em $bottom_margin+em $left_margin+em;}*/
		$html = '<html lang="en">
				<head>
					<style type="text/css">
						.page-break	{ display: block; page-break-before: always; }
						.page-break:last-child {
							page-break-before: avoid;
						}
						'.$default_style.'
			          	.bold1{
				            font-family:Arial, Helvetica, sans-serif;
				            font-size:14.015px;
				            font-weight:bold;
				            line-height:14.015px;
			          	}
						.bold2{
							font-family:Arial, Helvetica, sans-serif;
							font-size:12px;
							font-weight:bold;
							line-height:12px;
						}
					</style>
				</head>
			<body>';
		
		foreach($vids as $pg_index => $vid){
			$initial_printinfo = $this->getPrintData($vid);
			$aprice = $initial_printinfo['aprice'];
			$vtotal = $initial_printinfo['total_price'];
			$voptions = $initial_printinfo['options1'];
			$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
			$template_data = TemplateData::getTemplateData($user->DEALER_ID, $vehicle_info->NEW_USED);
			$template_builder = TemplateBuilder::getTemplateBuilderRow($user->DEALER_ID, $template_data->ADDENDUM_STYLE);
			//font style and font size for print addendum
			$bold3 = '';
			$bold4 = '';
			
			if($template_data->FONT_SIZE == 'Small'){
				$bold3 = "font-size:10px; line-height:10px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
				$bold4="font-size:8px;  line-height:8px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
			}elseif($template_data->FONT_SIZE == 'Medium'){
				$bold3="font-size:12px;  line-height:12px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
				$bold4="font-size:10px;  line-height:10px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
			}elseif($template_data->FONT_SIZE == "Large"){
				$bold3="font-size:14px;  line-height:14px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
				$bold4="font-size:12px;  line-height:12px;font-family:Arial, Helvetica, sans-serif; font-weight:bold;";
			}

			$qr_url = '';
			if($template_builder && $template_builder->QR_URL){
				$posvin = strpos($template_builder->QR_URL,'[VIN]');
	        	$posstock = strpos($template_builder->QR_URL,'[STOCK]');
	        	$qr_url = $template_builder->QR_URL;
	        	if($posvin){
	        		$qr_url = str_replace('[VIN]', $vehicle_info->VIN_NUMBER, $qr_url);
	        	}
	        	if($posstock){
	        		$qr_url = str_replace('[STOCK]', $vehicle_info->STOCK_NUMBER, $qr_url );
	        	}
			}else{
	        	$posvin = strpos($template_data->QR_URL,'[VIN]');
	        	$posstock = strpos($template_data->QR_URL,'[STOCK]');
	        	$qr_url = $template_data->QR_ADDRESS;
	        	if($posvin){
	        		$qr_url = str_replace('[VIN]', $vehicle_info->VIN_NUMBER, $qr_url);
	        	}
	        	if($posstock){
	        		$qr_url = str_replace('[STOCK]', $vehicle_info->STOCK_NUMBER, $qr_url );
	        	}
	        }

	        $left_margin=1.85+($template_data->LEFT_MARGIN*0.063);
			$right_margin=1.85+($template_data->RIGHT_MARGIN*0.063);
			$top_margin=3+($template_data->TOP_MARGIN*0.063);
			$bottom_margin=0+($template_data->BOTTOM_MARGIN*0.063);

			$barcode_url = '';
			if(!empty($vehicle_info->VIN_NUMBER)){
				$barcode_generator = new BarcodeGeneratorPNG;
				$barcode_url = 'data:image/png;base64,' . base64_encode($barcode_generator->getBarcode($vehicle_info->VIN_NUMBER, $barcode_generator::TYPE_CODE_39,2,200));
			}
			$pb=''; //page break html
			if($js == ''){
				$pb='<div class="page-break"></div>';
			}
			$class = 'page_'.$pg_index;
			$style = '';
			if(!$template_builder){
				switch ($template_data->ADDENDUM_STYLE) {
					case 'Standard':
						$style = '
							.'.$class.' #printit{
								background-color: white;
								background-size: 346px 985px;
								width: 341px;
								height: auto;
							}
						';
						break;
					case 'EPA Template':
						$style = '
							.'.$class.' #printit{
								background-color: white;
								background-size: 346px 985px;
								width: 346px;
								height: auto;
								padding-top: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 385px;
					            margin: 420px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:48px;
								margin-left:210px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:35px;
								font-size:15px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Honda_EPA':
					case 'Toyota_EPA':
					case 'Hyundai_EPA':
					case 'Cadillac_EPA':
					case 'Cadillac2015_EPA':
					case 'Chevrolet_EPA':
					case 'Ford_EPA':
					case 'Acura_EPA':
					case 'Audi_EPA':
					case 'Chrysler_EPA':
					case 'Dodge_EPA':
					case 'Fiat_EPA':
					case 'Infiniti_EPA':
					case 'Jaguar_EPA':
					case 'Kia_EPA':
					case 'Lexus_EPA':
					case 'Masarati_EPA':
					case 'Mercedes_EPA':
					case 'Mitsubishi_EPA':
					case 'Nissan_EPA':
					case 'Subaru_EPA':
					case 'Volvo_EPA':
					case 'Waldorf_EPA':
					case 'Custom1':
					case 'Custom1p':
					case 'Custom1 SMS':
					case 'Custom1 QR':
					case 'Custom1 VIN':
					case 'Custom1 VIN No MSRP':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 985px;
								width: 346px;
								height: 984px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 295px;
					            margin: 107px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
				          		margin-top: 25px;
								margin-left:195px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:32px;
								font-size:15px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Narrow Custom 1':
						$style = '
							.'.$class.' #printit{
								background-size: 252px 959px;
								color: black;
								width: 252px;
								height: 959px;
								padding-bottom: 20px;
							}
							.'.$class.' #second-section{
					            height: 360px;
					            margin: 420px 0px 0px 10px;
				          	}
				          	.'.$class.' .total{
								margin-top:23px;
								margin-left:130px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Narrow Custom 2':
					case 'Narrow Custom 2p':
						$style = '
							.'.$class.' #printit{
								background-size: 252px 959px;
								color: black;
								width: 252px;
								height: 959px;
								padding-bottom: 20px;
							}
							.'.$class.' #second-section{
					            height: 280px;
					            margin: 0 0 0 10px;
				          	}
				          	.'.$class.' .total{
								margin-top:23px;
								margin-left:140px;
								font-size:20px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' #logo_section{
								height: 230px;
								padding-top: 0px;
							}
							.'.$class.' #logo_section img{
								width: 100%;
								height: auto;
								max-width: 100%;
							}
						';
						break;
					case 'Custom2':
					case 'Custom3':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 984px;
								width: 346px;
								height: 984px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 410px;
					            margin: 140px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:25px;
								margin-left:195px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:32px;
								font-size:15px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Custom6':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 984px;
								width: 346px;
								height: 984px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 870px;
					            margin: 100px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:23px;
								margin-left:160px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
								width: 137px;
							}
							.'.$class.' .custom_msrp{
								font-size: 17px;
							    font-weight: bold;
							    height: 20px;
							    margin: 5px 0 0 195px;
							}
							.'.$class.' .header{
								font-size: 17px;
							    font-weight: bold;
							    height: 70px;
							    line-height: 18px;
							    margin: 17px 0 0 100px;
							}
							.'.$class.' .base {
							    font-size: 14px;
							    height: 16px;
							    margin: 423px 0 0 220px;
							    text-align: right;
							    width: 77px;
							}
							.'.$class.' .options {
							    clear: both;
							    height: 84px;
							    margin-top: 10px;
							    width: 100%;
							}
							.'.$class.' .options-into {
							    clear: both;
							    line-height: 20px;
							    width: 100%;
							}
						';
						break;
					case 'Custom7':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 984px;
								width: 346px;
								height: 984px;
								padding-bottom: 10px;
								float: left;
								font-family: Arial, Helvetica, sans-serif;
							}
							.'.$class.' #second-section{
					            height: 870px;
					            margin: 117px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:-3px;
								margin-left:160px;
								font-size:20px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
								width: 137px;
							}
							.'.$class.' .custom_msrp{
								font-size: 17px;
								font-weight: bold;
								height: 20px;
								margin: 0 0 0 200px;
								text-align: center;
								width: 100px;
							}
							.'.$class.' .header{
								font-size: 17px;
								font-weight: bold;
								height: 70px;
								line-height: 18px;
								margin: 0;
								padding: 17px 0 0 70px;
							}
							.'.$class.' .base {
							    font-size: 16px;
							    font-weight: bold;
							    height: 16px;
							    margin: 398px 0 0 220px;
							    text-align: right;
							    width: 77px;
							}
							.'.$class.' .options {
							    clear: both;
							    height: 136px;
							    margin-top: 10px;
							    width: 100%;
							}
							.'.$class.' .options-into {
							    clear: both;
							    line-height: 23px;
							    width: 100%;
							}
						';
						break;
					case 'Custom7 Narrow':
						$style = '
							.'.$class.' #printit{
								background-size: 252px 959px;
								width: 252px;
								height: 959px;
								padding-bottom: 10px;
								float: left;
								font-family: Arial, Helvetica, sans-serif;
							}
							.'.$class.' #second-section{
					            height: 870px;
					            margin: 117px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:3px;
								margin-left:100px;
								font-size:20px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
								width: 118px;
							}
							.'.$class.' .custom_msrp{
								font-size: 17px;
								font-weight: bold;
								height: 20px;
								margin: 2px 0 0 118px;
								text-align: right;
								width: 100px;
							}
							.'.$class.' .header{
								font-size: 17px;
								font-weight: bold;
								height: 53px;
								line-height: 18px;
								margin: 0;
								padding: 13px 0 0 50px;
							}
							.'.$class.' .base {
							    font-size: 14px;
							    font-weight: bold;
							    height: 16px;
							    margin: 418px 0 0 130px;
							    text-align: right;
							    width: 86px;
							}
							.'.$class.' .options {
							    clear: both;
							    height: 95px;
							    margin-top: 10px;
							    width: 100%;
							}
							.'.$class.' .options-into {
							    clear: both;
							    line-height: 23px;
							    width: 100%;
							}
						';
						break;
					case 'Custom5':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 984px;
								width: 346px;
								height: 984px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 570px;
					            margin: 110px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:25px;
								margin-left:195px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:51px;
								font-size:13px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Custom4':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 984px;
								width: 346px;
								height: 984px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 400px;
					            margin: 92px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:20px;
								margin-left:195px;
								font-size:24px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
								width: 130px;
							}
							.'.$class.' .address{
								padding-top:32px;
								font-size:13px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Custom2 QR':
					case 'Custom2 No MSRP':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 984px;
								width: 346px;
								height: 984px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 440px;
					            margin: 140px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:25px;
								margin-left:195px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:32px;
								font-size:15px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'PrePrinted':
						$style = '
							.'.$class.' #printit{
								background-size: 336px 989px;
								width: 336px;
								color: #0a1d69;
								height: 1000px;
								padding-bottom: 10px;
								position: relative;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 580px;
					            margin: 0;
				          	}
				          	.'.$class.' .total{
								margin-top:15px;
								margin-left:255px;
								font-size:12px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:30px;
								font-size:15px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Standard Branded':
					case 'Dealer Branded Custom QR':
					case 'Dealer Branded with VIN Barcode':
						$style = '
							.'.$class.' #printit{
								background-size: 346px 984px;
								width: 346px;
								color: '.$template_data->TEXT_COLOR.';
								height: 984px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 440px;
					            margin: 20px 0px 0px 20px;
				          	}
				          	.'.$class.' .total{
								margin-top:25px;
								margin-left:195px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:32px;
								font-size:15px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'AAT-Narrow':
					case 'AAT Narrow Series S':
						$style = '
							.'.$class.' #printit{
								background-size: 299px 989px;
								width: 299px;
								color: #000;
								height: 1039px;
								padding-bottom: 10px;
								position: relative;
							}
							.'.$class.' #second-section{
					            height: 120px;
					            margin: 35px 0 0 10px;
				          	}
				          	.'.$class.' .total{
								margin-top:35px;
								margin-left:160px;
								font-size:20px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
								width: 118px;
							}
							.'.$class.' .address{
								padding-top:32px;
								font-size:15px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					case 'Reynolds Narrow Preprinted':
						$style = '
							.'.$class.' #printit{
								background-size: 252px 959px;
								width: 252px;
								color: '.$template_data->TEXT_COLOR.';
								height: 959px;
								padding-bottom: 10px;
								float: left;
							}
							.'.$class.' #second-section{
					            height: 580px;
					            margin: 5px 0px 0px 10px;
				          	}
				          	.'.$class.' .total{
								margin-top:8px;
								margin-left:100px;
								font-size:22px;
								font-weight:bold;
								font-family:Arial, Helvetica, sans-serif;
							}
							.'.$class.' .address{
								padding-top:5px;
								font-size:13px;
								font-family:Arial, Helvetica, sans-serif;
							}
						';
						break;
					default:
						# code...
						break;
				}
			}else{
				$style='
					.'.$class.' #temp_printit{
						background-color: '.$template_builder->BACKGROUND.';
						width: '.($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252px':'346px').';
						color: #000;
						height: 985px;
						position: relative;
						font-family: Arial, Helvetica, sans-serif;
					}
					.'.$class.' #temp_logo_section{
						width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252':'346')-20).'px;
						height: 100px;
						padding: 0 10px;
						text-align: center;
						position: absolute;
						margin: 0 auto;
						top: '.$template_builder->LOGO_POSITION.'px;
					}
					.'.$class.' .temp_vin_section{
						padding: 0 10px;
						width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252':'346')-20).'px;
						position: absolute;
						top: '.$template_builder->VINFO_POSITION.'px;
					}
					.'.$class.' .temp_options_section{
						padding: 0 10px;
						width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252':'346')-20).'px;
						position: absolute;
						top: '.$template_builder->OPTIONS_POSITION.'px;
					}
					.'.$class.' #temp_subtotal_section{
						padding: 0 10px;
						width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252':'346')-20).'px;
						position: absolute;
						top: '.$template_builder->SUBTOTAL_POSITION.'px;
					}
					.'.$class.' #temp_total_section{
						padding: 0 10px;
						width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252':'346')-20).'px;
						position: absolute;
						top: '.$template_builder->TOTAL_POSITION.'px;
					}
					.'.$class.' #temp_address_section{
						padding: 0 10px;
						width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252':'346')-20).'px;
						position: absolute;
						top: '.$template_builder->ADDRESS_POSITION.'px;
					}
					.'.$class.' #temp_footer_section{
						padding: 0 10px;
						width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252':'346')-20).'px;
						position: absolute;
						top: '.$template_builder->FOOTER_POSITION.'px;
						height: '.$template_builder->FOOTER_HEIGHT.'px;
					}
				';
			}
			$order_by = '_ID';
			if($vehicle_info->RE_ORDER == 1 || $dealer_info->RE_ORDER == 1){
				$order_by = 'ORDER_BY';
			}
			$ad_datas = AddendumData::getOptionsForTable($user->DEALER_ID, $vehicle_info->VIN_NUMBER, $order_by);
			$bg_src = '';
			if(!$template_builder){
				switch ($template_data->ADDENDUM_STYLE) {
					case 'EPA Template':
						$bg_src = $bcdn_url.'background.jpg';
						break;
					case 'Honda_EPA':
						$bg_src = $bcdn_url.'Honda_Background.jpg';
						break;
					case 'Toyota_EPA':
						$bg_src = $bcdn_url.'Toyota_Background.jpg';
						break;
					case 'Hyundai_EPA':
						$bg_src = $bcdn_url.'Hyundai_Background.jpg';
						break;
					case 'Cadillac_EPA':
						$bg_src = $bcdn_url.'Cadillac_Background.jpg';
						break;
					case 'Cadillac2015_EPA':
						$bg_src = $bcdn_url.'Cadillac2015-3_Background.jpg';
						break;
					case 'Chevrolet_EPA':
						$bg_src = $bcdn_url.'Chevrolet_Background.jpg';
						break;
					case 'Ford_EPA':
						$bg_src = $bcdn_url.'Ford_Background.jpg';
						break;
					case 'Acura_EPA':
						$bg_src = $bcdn_url.'Acura_Background.jpg';
						break;
					case 'Audi_EPA':
						$bg_src = $bcdn_url.'Audi_Background.jpg';
						break;
					case 'Chrysler_EPA':
						$bg_src = $bcdn_url.'Chrysler_Background.jpg';
						break;
					case 'Dodge_EPA':
						$bg_src = $bcdn_url.'Dodge_Background.jpg';
						break;
					case 'Fiat_EPA':
						$bg_src = $bcdn_url.'Fiat_Background.jpg';
						break;
					case 'Infiniti_EPA':
						$bg_src = $bcdn_url.'Infiniti_Background.jpg';
						break;
					case 'Jaguar_EPA':
						$bg_src = $bcdn_url.'Jaguar_Background.jpg';
						break;
					case 'Kia_EPA':
						$bg_src = $bcdn_url.'Kia_Background.jpg';
						break;
					case 'Lexus_EPA':
						$bg_src = $bcdn_url.'Lexus_Background.jpg';
						break;
					case 'Masarati_EPA':
						$bg_src = $bcdn_url.'Masarati_Background.jpg';
						break;
					case 'Mercedes_EPA':
						$bg_src = $bcdn_url.'Mercedes_Background.jpg';
						break;
					case 'Mitsubishi_EPA':
						$bg_src = $bcdn_url.'Mitsubishi_Background.jpg';
						break;
					case 'Nissan_EPA':
						$bg_src = $bcdn_url.'Nissan_Background.jpg';
						break;
					case 'Subaru_EPA':
						$bg_src = $bcdn_url.'Subaru_Background.jpg';
						break;
					case 'Volvo_EPA':
						$bg_src = $bcdn_url.'Volvo_Background.jpg';
						break;
					case 'Waldorf_EPA':
						$bg_src = $bcdn_url.'Waldorf_Background.jpg';
						break;
					case 'Custom1':
					case 'Custom1p':
						$bg_src = $bcdn_url.$template_data->CUSTOM1_BG;
						break;
					case 'Narrow Custom 1':
						$bg_src = $bcdn_url.$template_data->NARROW_CUSTOM1_BG;
						break;
					case 'Narrow Custom 2':
					case 'Narrow Custom 2p':
						$bg_src = $bcdn_url.$template_data->NARROW_CUSTOM2_BG;
						break;
					case 'Custom1 SMS':
						$bg_src = $bcdn_url.$template_data->CUSTOM1_SMS_BG;
						break;
					case 'Custom1 QR':
						$bg_src = $bcdn_url.$template_data->CUSTOM1_QR_BG;
						break;
					case 'Custom1 VIN':
					case 'Custom1 VIN No MSRP':
						$bg_src = $bcdn_url.$template_data->CUSTOM1_VIN_BG;
						break;
					case 'Custom2':
						$bg_src = $bcdn_url.$template_data->CUSTOM2_BG;
						break;
					case 'Custom3':
						$bg_src = $bcdn_url.$template_data->CUSTOM3_BG;
						break;
					case 'Custom6':
						$bg_src = $bcdn_url.$template_data->CUSTOM6_BG;
						break;
					case 'Custom7':
						$bg_src = $bcdn_url.$template_data->CUSTOM7_BG;
						break;
					case 'Custom7 Narrow':
						$bg_src = $bcdn_url.$template_data->CUSTOM7N_BG;
						break;
					case 'Custom5':
						$bg_src = $bcdn_url.$template_data->CUSTOM5_BG;
						break;
					case 'Custom4':
						$bg_src = $bcdn_url.$template_data->CUSTOM4_BG;
						break;
					case 'Custom2 QR':
						$bg_src = $bcdn_url.$template_data->CUSTOM2_QR_BG;
						break;
					case 'Custom2 No MSRP':
						$bg_src = $bcdn_url.$template_data->CUSTOM2_NM_BG;
						break;
					case 'PrePrinted':
						$bg_src = $bcdn_url.'twoup.jpg';
						break;
					case 'Standard Branded':
						$bg_src = $bcdn_url.$template_data->STANDARD_BACKGROUND.'.jpg';
						break;
					case 'Dealer Branded Custom QR':
						$bg_src = $bcdn_url.$template_data->QR_BRANDED_COLOR.'.jpg';
						break;
					case 'Dealer Branded with VIN Barcode':
						$bg_src = $bcdn_url.$template_data->VIN_BRANDED_COLOR.'.jpg';
						break;
					case 'AAT-Narrow':
						$bg_src = $this->site_baseurl.'/img/aat-narrow.jpg';
						break;
					case 'AAT Narrow Series S':
						$bg_src = $this->site_baseurl.'/img/series-s.jpg';
						break;
					case 'Reynolds Narrow Preprinted':
						$bg_src = $bcdn_url.$template_data->Reynolds_Narrow_Pre_bg;
						break;
					default:
						break;
				}
				
				$background_html = '<div style="position: fixed; top:0; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$bg_src.'" style="width: 100%;">
						</div>';
				$adStyle = $template_data->ADDENDUM_STYLE;
				$html .= '<style type="text/css">@page {margin:'.$top_margin.'em '.$right_margin.'em '.$bottom_margin.'em '.$left_margin.'em;} '.$style.'</style>';
				$html .= '<div class="'.$class.'"><div id="printit">'.$background_html;
				if($template_data->ADDENDUM_STYLE == 'standard' || $template_data->ADDENDUM_STYLE == 'narrow' || $template_data->ADDENDUM_STYLE == 'Narrow Custom 2' || $template_data->ADDENDUM_STYLE == 'Narrow Custom 2p' || $template_data->ADDENDUM_STYLE == 'Standard Branded' || $template_data->ADDENDUM_STYLE == 'Dealer Branded Custom QR' || $template_data->ADDENDUM_STYLE == 'Dealer Branded with VIN Barcode'){
					$logo_section = true;
					$logo_img = $template_data->DEALER_LOGO;
				}else{
					$logo_section = false;
					$logo_img = '';
				}
				if($logo_section){
					$html .='<div id="logo_section">';
						if($logo_img != ''){
							$html .= '<img src="'.$fcdn_url.$logo_img.'" />';
						}
					$html .= '</div>';
				}
				if($template_data->ADDENDUM_STYLE == 'AAT-Narrow' || $template_data->ADDENDUM_STYLE == 'AAT Narrow Series S'){
					$html .= '<div class="vin_section">
								<div class="stock">'.$vehicle_info->STOCK_NUMBER.'</div>
					          	<div class="vin">'.$vehicle_info->VIN_NUMBER.'</div>
							    <div class="model">'.$vehicle_info->YEAR.', '.$vehicle_info->MAKE.', '.$vehicle_info->MODEL.'</div>
							</div>
							<div class="msrp_section">
						    	<div class="msrp">'.$aprice.'</div>
						  	</div>
					';
				}
				if($template_data->ADDENDUM_STYLE != 'Standard' && $template_data->ADDENDUM_STYLE != 'PrePrinted'){
					$html .= '<div id="second-section">';
					if($adStyle != 'Custom6' && $adStyle != 'Custom7' && $adStyle != 'Custom7 Narrow' && $adStyle != 'PrePrinted'){
						$html .= '<table width="100%" border="0">';
						if($adStyle != 'AAT-Narrow' && $adStyle != 'AAT Narrow Series S'){
							$html .= '<tr>';
							if($adStyle != 'Narrow Custom 1' && $adStyle != 'Narrow Custom 2' && $adStyle != 'Narrow Custom 2p' && $adStyle != 'Custom4'){
								$html .= '<td width="77%">';
								if($adStyle == 'standard' || $adStyle == 'narrow'){
									$html .= '<span>'.$vehicle_info->STOCK_NUMBER.'</span><br />
											<span>'.$vehicle_info->VIN_NUMBER.'</span><br />
									';
								}else{
									$html .= '<span class="bold1">'.$vehicle_info->STOCK_NUMBER.'</span><br />
											<span class="bold1">'.$vehicle_info->VIN_NUMBER.'</span><br />
									';
								}
								$html .= '</td>';
							}
							if($adStyle == 'Custom4'){
								$html .= '<td width="77%" style="padding-left:62px;">
					                  		<div class="bold1" style="padding-top:2px;text-align:center;">'.$vehicle_info->YEAR.', '.$vehicle_info->MAKE.', '.$vehicle_info->MODEL.'</div>
											<div class="bold1" style="padding-top:19px;text-align:center;">'.$vehicle_info->STOCK_NUMBER.'</div>
											<div class="bold1" style="padding-top:3px;text-align:center;">'.$vehicle_info->VIN_NUMBER.'</div>
					                  	</td>';
							}
							if($adStyle != 'Narrow Custom 1' && $adStyle != 'Narrow Custom 2' && $adStyle != 'Narrow Custom 2p'){
								$html .= '<td width="23%">&nbsp;</td>';
							}else{
								$html .= '<td colspan="2" width="100%"><span class="st_vin">Year: '.$vehicle_info->YEAR.' Make: '.$vehicle_info->MAKE.' Model: '.$vehicle_info->MODEL.'</span><br />
			        				<span class="st_vin">Stock: '.$vehicle_info->STOCK_NUMBER.'</span><br />
			            			<span class="st_vin">VIN: '.$vehicle_info->VIN_NUMBER.'</span></td>';
							}
							$html .= '</tr>';
						}
						$html .='<tr>
						        	<td></td>
						        	<td></td>
						      	</tr>';
						if($adStyle != 'Narrow Custom 2' && $adStyle != 'Narrow Custom 2p' && $adStyle != 'Custom4' && $adStyle != 'AAT-Narrow' && $adStyle != 'AAT Narrow Series S'){
							$html .= '<tr>
						        <td>
							        <span class="bold1">
							        		Year: '.$vehicle_info->YEAR;
						        		if($vehicle_info->MILEAGE != null && $vehicle_info->MILEAGE != '' && $adStyle == 'EPA Template' && $adStyle != 'Custom1 SMS' && $adStyle != 'Custom2' && $adStyle != 'Custom3' && $adStyle != 'Custom5' && $adStyle != 'Custom2 QR' && $adStyle != 'Custom2 No MSRP' && $adStyle != 'Standard Branded' && $adStyle != 'Dealer Branded Custom QR' && $adStyle != 'Dealer Branded with VIN Barcode'){
						        			$html .= '<span>
									        			&nbsp;&nbsp; Mileage : '.$vehicle_info->MILEAGE .'
									        		</span>';
						        		}
						        	$html .= '
						          	</span><br/>
			                      	<span class="bold1">Make: '.$vehicle_info->MAKE.'</span> <br />
			                      	<span class="bold1">Model: '.$vehicle_info->MODEL.'</span>';
			                      	if($adStyle == 'Standard Branded' || $adStyle == 'Dealer Branded Custom QR' || $adStyle == 'Dealer Branded with VIN Barcode'){
			                      		$html .= '<div style="width:50%; float:left">';
			                      		if($vehicle_info->EXT_COLOR != '' && $vehicle_info->EXT_COLOR != null){
			                      			$html .= '<span class="bold1">Color: '.$vehicle_info->EXT_COLOR.'</span><br />';
			                      		}
			                      		if($vehicle_info->TRIM != '' && $vehicle_info->TRIM != null){
			                      			$html .= '<span class="bold1">Trim: '.$vehicle_info->TRIM.'</span><br />';
			                      		}
								            $html .='<br>';
								        if($vehicle_info->MILEAGE != '' && $vehicle_info->MILEAGE != null){
								        	$html .= '<span class="bold1">Mileage: '.vehicle_info.MILEAGE.'</span>';
								        }
								        $html .= '</div>';
			                      	}
			                      	
			                $html .='</td>
						        <td></td>
					      	</tr>';
						}
						if($adStyle == 'Custom1 SMS' || $adStyle == 'Custom5'){
							$html .= '<tr>
							      		<td>&nbsp;</td>
					        			<td>&nbsp;</td>
							      	</tr>';
						}
						if($vehicle_info->MSRP != 0 && $vehicle_info->MSRP != null && $adStyle != 'Narrow Custom 1' && $adStyle != 'Narrow Custom 2' && $adStyle != 'Narrow Custom 2p' && $adStyle != 'Custom1 VIN No MSRP' && $adStyle != 'Custom2 No MSRP' && $adStyle != 'AAT-Narrow' && $adStyle != 'AAT Narrow Series S'){
							$html .='<tr>';
							if($adStyle != 'Custom1' && $adStyle != 'Custom1p' && $adStyle != 'Custom1 SMS' && $adStyle != 'Custom2' && $adStyle != 'Custom4'){
								$html .= '<td>
											<span class="bold1">Manufacturer Suggested Retail Price:</span>
										</td>';
								if($adStyle == 'Custom1' || $adStyle == 'Custom1p'){
									$html .='<td>';
									if($vehicle_info->NEW_USED == 'New'){
										$html .= '<span class="bold1">Manufacturer Suggested Retail Price:</span>';
									}elseif($vehicle_info->NEW_USED == 'Used'){
										$html .= '<span class="bold1">Market Suggested Retail Price:</span>';
									}
									$html .='</td>';
								}
							}
							if($adStyle == 'Custom1 SMS' && $vehicle_info->NEW_USED == 'New'){
								$html .= '<td>
						      		 		<span class="bold1">Manufacturer Suggested Retail Price:</span>
						      		 	</td>';
							}
							if($adStyle == 'Custom2' && $vehicle_info->NEW_USED == 'New'){
								$html .= '<td>
						      		 		<span class="bold1">Suggested Retail Price:</span>
						      		 	</td>';
							}
							if($adStyle == 'Custom1 SMS' && $vehicle_info->NEW_USED == 'Used'){
								$html .= '<td>
						      		 		<span class="bold1">Market Suggested Retail Price:</span>
						      		 	</td>';
							}
							if($adStyle == 'Custom4'){
								$html .= '<td><span class="bold1"></span></td>
										<td>
						      		 		<span style="font-size:20px; font-weight:bold">'.$aprice.'</span>
						      		 	</td>';
							}else{
								$html .= '<td>
						      		 		<span class="bold1">'.$aprice.'</span>
						      		 	</td>';
							}

							$html .='</tr>';
						}
						if($vehicle_info->MSRP != 0 && $vehicle_info->MSRP != null && ($adStyle == 'Narrow Custom 1' || $adStyle == 'Narrow Custom 2' || $adStyle == 'Narrow Custom 2p')){
							$html .= '<tr>
							      		<td colspan="2"><span class="msrp">Original MSRP</span> <span class="msrp">'.$aprice.'</span></td>
							      	</tr>';
						}
						if($adStyle == 'Narrow Custom 1' || $adStyle == 'Narrow Custom 2' || $adStyle == 'Narrow Custom 2p'){
							$html .= '<tr>
								        <td colspan="2" align="center"><span class="dae">DEALER-ADDED EQUIPMENT</span></td>
							      	</tr>';
						}
						$html .= '<tr>
				        			<td colspan="2"><div style="height:2px; width:100%;background-color:black;"></div></td>
				      			</tr>';
				      	$html .= '<tr><td>';

				      	if($adStyle != 'standard' && $adStyle != 'narrow' && $adStyle != 'Custom2 No MSRP'){
				      		$html .= '<span class="bold1">Dealer Installed Options</span>';
				      	}else{
				      		$html .= '<span>Recommended Options</span>';
				      	}

				      	$html .= '</td><td>&nbsp;</td></tr>';
				      	foreach($ad_datas as $ad_data){
				      		$html .= '<tr>';
				      		$html .= '<td>';
				      		if($ad_data->SEPARATOR_ABOVE == '1'){
				      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
				      				$html .= '<br />';
				      			}
				      		}
				      		$html .= '<span class="bold1">'.htmlspecialchars_decode($ad_data->ITEM_NAME).'</span>';
			      			if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
			      				$html .= '<br />';
			      			}
			      			if($ad_data->OR_OR_AD == 2){
								$optiontext="";
								$option_group = $ad_data->ITEM_DESCRIPTION;
								$option_group = explode(",", $option_group);
								foreach($option_group as $optiong){
									$optiontext.=$optiong."<br/>";
								}
								if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
									$html .="<div class='bold2' style='padding-left:10px;".$bold4."'>".$optiontext."</div>";
								}
							}else{
								if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
									$html .="<div class='bold2' style='padding-left:10px;".$bold4."'>".html_entity_decode($ad_data->ITEM_DESCRIPTION)."</div>";
								}
								
							}
			      			if($ad_data->SEPARATOR_BELOW == '1'){
				      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
				      				$html .= '<br />';
				      			}
			      			}
				      		$html .= '</td>';
				      		$oprice = '';
							if($ad_data->ITEM_PRICE<0){
								$oprice=floatVal(abs($ad_data->ITEM_PRICE));
								$oprice=number_format($oprice, 2, '.', ',');
								$oprice='-$'.$oprice;
							}else if($ad_data->ITEM_PRICE=="0"){
								$oprice='Free';
							}else if(strtolower($ad_data->ITEM_PRICE)=="np"){
								$oprice='';
							}else if(strtolower($ad_data->ITEM_PRICE)=="fr"){
								$oprice='Free';
							}else if(strtolower($ad_data->ITEM_PRICE)=="in"){
								$oprice='Included';
							}else if(strtolower($ad_data->ITEM_PRICE)=="inc"){
								$oprice='Included';
							}else if(strtolower($ad_data->ITEM_PRICE)=="nc"){
								$oprice='No Charge';
							}else{
								$oprice=floatval($ad_data->ITEM_PRICE);
								$oprice=number_format($oprice, 2, '.', ',');
								$oprice='$'.$oprice;
							}
				      		if($adStyle != 'Custom4'){
				      			$html .= '<td>';
				      			if($ad_data->SEPARATOR_ABOVE == '1'){
					      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
					      				$html .= '<br />';
					      			}
					      		}
					      		$html .= '<span class="bold1">'.$oprice.'</span>';
					      		if($ad_data->SEPARATOR_BELOW == '1'){
					      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
					      				$html .= '<br />';
					      			}
				      			}
				      			$html .= '</td>';
				      		}else{
				      			$html .= '<td valign="top" align="right">';
				      			if($ad_data->SEPARATOR_ABOVE == '1'){
					      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
					      				$html .= '<br />';
					      			}
					      		}
					      		$html .= '<span class="bold1">'.$oprice.'</span>';
					      		if($ad_data->SEPARATOR_BELOW == '1'){
					      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
					      				$html .= '<br />';
					      			}
				      			}
				      			$html .= '</td>';
				      		}
				      		$html .= '</tr>';
				      	}
				      	$html .= '</table>';
					}
					if($adStyle == 'Custom6' || $adStyle == 'Custom7' || $adStyle == 'Custom7 Narrow'){
						$html .= '<div class="header">';
						$html .= '<div class="toptext"> '.$vehicle_info->STOCK_NUMBER.' </div>
								<div class="toptext"> '.$vehicle_info->VIN_NUMBER.' </div>';
						if($adStyle == 'Custom7' || $adStyle == 'Custom7 Narrow'){
							$html .= '<div class="toptext" style="padding-top:2px;"> '.$vehicle_info->MODEL.' </div>';
						}
						if($adStyle == 'Custom6'){
							$html .= '<div class="toptext"> '.$vehicle_info->MODEL.' </div>';
						}
						$html .= '</div>';
						if($vehicle_info->MSRP != 0 && $vehicle_info->MSRP != ''){
							$html .= '<div class="msrp_custom">'.$aprice.'</div>';
						}
						$base_price = '';
						if(!empty($template_data->BASE_PRICE)){
							$base_price = '$'.number_format(floatval($template_data->BASE_PRICE), 2, '.', ',');
						}
						$html .= '<div class="base">'.$base_price.'</div>';
						$html .= '<div class="options">';
						foreach($ad_datas as $ad_data){
							$oprice = '';
							if($ad_data->ITEM_PRICE<0){
								$oprice=floatVal(abs($ad_data->ITEM_PRICE));
								$oprice=number_format($oprice, 2, '.', ',');
								$oprice='-$'.$oprice;
							}else if($ad_data->ITEM_PRICE=="0"){
								$oprice='Free';
							}else if(strtolower($ad_data->ITEM_PRICE)=="np"){
								$oprice='';
							}else if(strtolower($ad_data->ITEM_PRICE)=="fr"){
								$oprice='Free';
							}else if(strtolower($ad_data->ITEM_PRICE)=="in"){
								$oprice='Included';
							}else if(strtolower($ad_data->ITEM_PRICE)=="inc"){
								$oprice='Included';
							}else if(strtolower($ad_data->ITEM_PRICE)=="nc"){
								$oprice='No Charge';
							}else{
								$oprice=floatval($ad_data->ITEM_PRICE);
								$oprice=number_format($oprice, 2, '.', ',');
								$oprice='$'.$oprice;
							}
							$html .= '<div class="options-into">
										<div class="left"><span class="bold1">'.htmlspecialchars_decode($ad_data->ITEM_NAME).'</span></div>
								        <div class="right"><span class="bold1">'.$oprice.'</span></div>
							      	</div>';

						}
						$html .= '</div>';
						if($template_data->TOTAL_ADDS == '1'){
							$html .= '<div class="total-op">'.$voptions.'</div>';
						}
						$html .= '<div class="total">'.$vtotal.'</div>';
					}
					$html .= '</div>';
				}
				if($adStyle == 'PrePrinted'){
					$html .= '<div id="second-section_preprinted">
					<table width="100%" border="0">
						<tr>
							<td colspan="3">
								<div class="nameaddress">'.$dealer_info->DEALER_NAME.'<br>
									'.$dealer_info->DEALER_ADDRESS.'<br>
									'.$dealer_info->DEALER_CITY.' '.$dealer_info->DEALER_STATE.' '.$dealer_info->DEALER_ZIP.'<br>
		            				'.$dealer_info->PHONE.'
		        				</div>
		                      	<div class="model">'.$vehicle_info->YEAR.', '.$vehicle_info->MODEL.'</div>
		                  	</td>
						</tr>
						<tr>
					        <td colspan="2"></td>
					        <td width="24%"></td>
				      	</tr>
				      	<tr>
					        <td colspan="2"></td>
					        <td></td>
				      	</tr>
				      	<tr>
		        			<td colspan="3">
		        				<div class="stock">'.$vehicle_info->STOCK_NUMBER.'</div>
		                      	<div class="vin">'.$vehicle_info->VIN_NUMBER.'</div>
		                  	</td>
		      			</tr>';
		      		if($vehicle_info->MSRP != 0 && $vehicle_info->MSRP != null){
		      			$html .= '<tr>
				      				<td colspan="2">&nbsp;</td>
				      				<td width="24%"><div class="msrp_preprinted">'.$aprice.'</div></td>
				      			</tr>';
		      		}else{
		      			$html .= '<tr>
		      				<td colspan="2">&nbsp;</td>
		    				<td width="24%"></td>
		      			</tr>';	
		      		}
		      			$html .= '<tr>
				        	<td colspan="3">&nbsp;</td>
				      	</tr>
				      	<tr>
					        <td colspan="2">&nbsp;</td>
					        <td>&nbsp;</td>
				      	</tr>';
				    foreach($ad_datas as $ad_data){
				    	$html .= '<tr>
		      				<td colspan="2">
		      					<div class="extra">
		      						<span class="bold1">'.htmlspecialchars_decode($ad_data->ITEM_NAME).'</span>	
		      					</div>';
		      				if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
		      					$html .= '<br />';
		      				}
			      			if($ad_data->OR_OR_AD == 2){
								$optiontext="";
								$option_group = $ad_data->ITEM_DESCRIPTION;
								$option_group = explode(",", $option_group);
								foreach($option_group as $optiong){
									$optiontext.=$optiong."<br/>";
								}
								if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
									$html .="<div class='bold2' style='padding-left:10px;".$bold4."'>".$optiontext."</div>";
								}
							}else{
								if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
									$html .="<div class='bold2' style='padding-left:10px;".$bold4."'>".html_entity_decode($ad_data->ITEM_DESCRIPTION)."</div>";
								}
								
							}
							$oprice = '';
							if($ad_data->ITEM_PRICE<0){
								$oprice=floatVal(abs($ad_data->ITEM_PRICE));
								$oprice=number_format($oprice, 2, '.', ',');
								$oprice='-$'.$oprice;
							}else if($ad_data->ITEM_PRICE=="0"){
								$oprice='Free';
							}else if(strtolower($ad_data->ITEM_PRICE)=="np"){
								$oprice='';
							}else if(strtolower($ad_data->ITEM_PRICE)=="fr"){
								$oprice='Free';
							}else if(strtolower($ad_data->ITEM_PRICE)=="in"){
								$oprice='Included';
							}else if(strtolower($ad_data->ITEM_PRICE)=="inc"){
								$oprice='Included';
							}else if(strtolower($ad_data->ITEM_PRICE)=="nc"){
								$oprice='No Charge';
							}else{
								$oprice=floatval($ad_data->ITEM_PRICE);
								$oprice=number_format($oprice, 2, '.', ',');
								$oprice='$'.$oprice;
							}
			      		$html .= '</td>
			      			<td valign="top">
			      				<span class="bold1">'.$oprice.'</span>
			      			</td>
		      			</tr>';
				    }
				    $html .= '</table></div>';
				    $html .= '<div class="subtotal_preprinted">'.$voptions.'</div>';
				}
				if($adStyle == 'Custom4'){
					$html .='<div class="total-op_custom4">'.$voptions.'</div>';
				}
				if($adStyle == 'standard' || $adStyle == 'narrow'){
					$html .= '<div class="subtotal">
								<div class="subtotaltext">Subtotal:</div>
								<div class="subtotalnum">'.$voptions.'</div>
							</div>';
				}
				if($adStyle == 'AAT-Narrow'){
					$html .= '<div class="subtotal_preprinted">
								Total Options and Services: '.$voptions.'
							</div>';
				}
				if($adStyle != 'Standard' && $adStyle != 'Custom6' && $adStyle != 'Custom7' && $adStyle != 'Custom7 Narrow'){
					$html .= '<div class="total">'.$vtotal.'</div>';
				}
				if($adStyle != 'Standard' && $adStyle != 'Narrow Custom 1' && $adStyle != 'Narrow Custom 2' && $adStyle != 'Narrow Custom 2p' && $adStyle != 'Custom6' && $adStyle != 'Custom7' && $adStyle != 'Custom7 Narrow' && $adStyle != 'Custom5' && $adStyle != 'PrePrinted'){
					$html .= '<div class="address">
								'. $dealer_info->DEALER_NAME .'<br>
					    		'. $dealer_info->DEALER_ADDRESS .'<br>
					    		'. $dealer_info->DEALER_CITY .' '. $dealer_info->DEALER_STATE .' '. $dealer_info->DEALER_ZIP .'<br>
					    		'. $dealer_info->PHONE .'
							</div>';
				}
				if($adStyle == 'Custom5'){
					$html .= '<div class="address">
								'. $dealer_info->DEALER_NAME .'<br>
					    		'. $dealer_info->DEALER_ADDRESS .'<br>
					    		'. $dealer_info->DEALER_CITY .' '. $dealer_info->DEALER_STATE .' '. $dealer_info->DEALER_ZIP .'<br>
					    		'. $dealer_info->PHONE .'
							</div>';
				}
				if($adStyle == 'Custom1 SMS'){
					$html .= '<div class="last5">'.substr($vehicle_info->VIN_NUMBER, -5).'</div>';
				}
				if($adStyle == 'Dealer Branded Custom QR'){
					$html .= '<div class="qrarea">
								<div class="qrtitle">'.$template_data->QR_TITLE.'</div>
							    <div class="qrdesc">'.$template_data->QR_DESCRIPTION.'</div>
							    <div class="mainqr"><img src="https://chart.googleapis.com/chart?cht=qr&chs=100x100&chld=M|0&chl='.$qr_url.'"></div>
						  	</div>';
				}
				if($adStyle == 'Custom1 QR'){
					$html .= '<div style="margin-top:80px;margin-left:210px">
								<img src="https://chart.googleapis.com/chart?cht=qr&chs=100x100&chld=M|0&chl='.$qr_url.'" style="padding-top:10px;padding-bottom:10px;">
							</div>';
				}
				if($adStyle == 'Custom2 QR' || $adStyle == 'Custom2 No MSRP'){
					$html .= '<div style="margin-top:65px;margin-left:230px">
								<img src="https://chart.googleapis.com/chart?cht=qr&chs=75x75&chld=M|0&chl='.$qr_url.'" style="padding-top:10px;padding-bottom:10px;">
							</div>';
				}
				if($adStyle == 'Custom5'){
					$html .= '<div style="margin-top: -4px;margin-left: 233px;">
								<img src="https://chart.googleapis.com/chart?cht=qr&chs=90x90&chld=M|0&chl='.$qr_url.'" style="padding-top:10px;padding-bottom:10px;">
							</div>';
				}
				if($adStyle == 'Custom1 VIN' || $adStyle == 'Custom1 VIN No MSRP'){
					$html .= '<div style="margin-top:223px;margin-left:210px"> </div>';
				}
				if($adStyle == 'Custom1 VIN' || $adStyle == 'Custom1 VIN No MSRP' || $adStyle == 'Dealer Branded with VIN Barcode'){
					$html .= '<div class="barcode"><img width="250" src="'.$barcode_url.'"></div>';
				}
				if($adStyle == 'Standard'){
					$html .= `<table width="100%" border="0">
								<tr>
					              	<td width="70%"><span style="font-size:13px; font-family: 'Cambria Math'; font-stretch: 'narrower'">DEALER NAME AND ADDRESS<br>
						                <strong>`.$dealer_info->DEALER_NAME.`</strong><br>
						                `.$dealer_info->DEALER_ADDRESS.`<br>
						                `.$dealer_info->DEALER_CITY.` `.$dealer_info->DEALER_STATE.` `.$dealer_info->DEALER_ZIP.` </span>
						            </td>
						              <td width="30%" valign="baseline"><span style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower"> MODEL <br>
						                `.$vehicle_info->YEAR.` `.$vehicle_info->MAKE.`<br>
						                `.$vehicle_info->MODEL.`</span>
						            </td>
					            </tr>
							</table>`;
					$html .= '<br />';
					$html .= `<table width="100%" border="0">
					            <tr>
					              <td width="49%"><span style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower">STOCK NUMBER<br>
					                `.$vehicle_info->STOCK_NUMBER.`</span></td>
					              <td width="51%"><span style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower">VIN<br>
					                `.$vehicle_info->VIN_NUMBER.`</span></td>
					            </tr>
					      	</table>
							<br>
							<table width="99%" style="border:1px solid #000">`;
							if($vehicle_info->MSRP != 0 && $vehicle_info->MSRP != ''){
								$html .= `<tr style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower">
									<td width="80%">MANUFACTURERS SUGGESTED RETAIL PRICE</td>
					              	<td width="20%">`.$aprice.`</td>
								</tr>`;
							}
							$html .= `<tr>
									<td valign="baseline" colspan="2" style="height:350px; border-top:1px solid #000">
										<table width="100%" border="0">
						                  	<tr>
						                  		<td colspan="2"><span style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower">DEALER INSTALLED OPTIONS</span></td>
						                	</tr>`;
						    foreach($ad_datas as $ad_data){
						    	$html .= `<tr style="font-size:12px; font-family: 'Cambria Math'; font-stretch: narrower">`;
						    	$html .= '<td width="81%">'.htmlspecialchars_decode($ad_data->ITEM_NAME);
						    	if($ad_data->OR_OR_AD == 2){
									$optiontext="";
									$option_group = $ad_data->ITEM_DESCRIPTION;
									$option_group = explode(",", $option_group);
									foreach($option_group as $optiong){
										$optiontext.=$optiong."<br/>";
									}
									if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
										$html .="<div class='bold2' style='padding-left:10px;'>".$optiontext."</div>";
									}
								}else{
									if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
										$html .="<div class='bold2' style='padding-left:10px;'>".html_entity_decode($ad_data->ITEM_DESCRIPTION)."</div>";
									}
								}
								$html .= '</td>';
								$oprice = '';
								if($ad_data->ITEM_PRICE<0){
									$oprice=floatVal(abs($ad_data->ITEM_PRICE));
									$oprice=number_format($oprice, 2, '.', ',');
									$oprice='-$'.$oprice;
								}else if($ad_data->ITEM_PRICE=="0"){
									$oprice='Free';
								}else if(strtolower($ad_data->ITEM_PRICE)=="np"){
									$oprice='';
								}else if(strtolower($ad_data->ITEM_PRICE)=="fr"){
									$oprice='Free';
								}else if(strtolower($ad_data->ITEM_PRICE)=="in"){
									$oprice='Included';
								}else if(strtolower($ad_data->ITEM_PRICE)=="inc"){
									$oprice='Included';
								}else if(strtolower($ad_data->ITEM_PRICE)=="nc"){
									$oprice='No Charge';
								}else{
									$oprice=floatval($ad_data->ITEM_PRICE);
									$oprice=number_format($oprice, 2, '.', ',');
									$oprice='$'.$oprice;
								}
								$html .= '<td width="19%" valign="top">'.$oprice.'</td>';
								$html .= '</tr>';
						    }
							$html .= '</table></td></tr>';
							$html .=`<tr>
									<td colspan="2" style="border-bottom:1px solid #000"><p  style="font-size:12px; font-family: 'Cambria Math'; font-stretch: narrower"> `.$template_data->SLOGAN.`</p>
										<span style="font-size:15px; font-family: 'Cambria Math'; font-stretch: narrower">ADDED MARK-UP</span>
									</td>
								</tr>
								<tr style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower">
									<td>DEALERS INSTALLED OPTIONS SUBTOTAL </td>
									<td>`.$voptions.`</td>
								</tr>
								<tr>
									<td>
										<span style="font-size:15px; font-family: 'Cambria Math'; font-stretch: narrower">*SELLERS ASKING PRICE</span><br>
					                	<span  style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower">INCLUDES DEALERS INSTALLED OPTIONS</span>
					                </td>
					                <td><span  style="font-size:13px; font-family: 'Cambria Math'; font-stretch: narrower">`. $vtotal.`</span></td>
								</tr>
								<tr>
									<td colspan="2" align="center" style="font-size:11px; font-family: 'Cambria Math'; font-stretch: narrower;border-top:1px solid #000">Plus State and Local Taxes, License Fees, Documentary Fees or Finance Charges, if any</td>
								</tr>
								<tr>
					              	<td colspan="2"><div><img src="`.$this->site_baseurl.`/img/header.jpg" width="336" border="0" style="margin-bottom:-2px;" /></div>
					            		<div style="width:335px; border-left:20px solid #000;border-right:20px solid #000;border-bottom:20px solid #000; text-align:center"> 
					            			<img src="https://chart.googleapis.com/chart?cht=qr&chs=150x150&chld=M|0&chl=`.$qr_url.`" style="padding-top:10px;padding-bottom:10px;"> 
					            		</div>
					            	</td>
					            </tr>
					            <tr>
					            	<td colspan="2" align="center" style="font-size:12px; font-family: 'Cambria Math'; font-stretch: narrower">
					            		THIS ADDENDUM STICKER IS NOT AN OFFICIAL FACTORY OR GOVERNMENT STICKER<br>
					                	AFFIXED AS A SUPPLEMENTAL LABEL BY YOUR DEALER
					                </td>
					            </tr>
							</table>
							<h1 class="page-break"></h1>`;
				}
				$html .= '</div></div>';
				$html .= $pb;
			}else{
				if($template_builder->BORDER){
					$bg_src = $bcdn_url.$template_builder->BORDER_URL;
				}
				if($template_builder->BACKGROUND_IMAGE){
					$bg_src = $bcdn_url.$template_builder->BACKGROUND_IMAGE;
				}
				$html .= '<style type="text/css">@page {margin:'.$top_margin.'em '.$right_margin.'em '.$bottom_margin.'em '.$left_margin.'em;} '.$style.'</style>';
				$html .= '<div class="'.$class.'"><div id="temp_printit" class="'.($template_builder->TEMPLATE_WIDTH == 'Narrow'?'narrow':'standard').'">';
				$html .= '<div style="position: fixed; top:0; left:0;z-index: -1000;">
						  	<img src="'.$bg_src.'" style="height:985px; width: '.($template_builder->TEMPLATE_WIDTH == 'Narrow'?'252px':'346px').'">
						</div>';
				if($template_builder->SHOWLOGO){
					$html .= '<div id="temp_logo_section">';
					if($template_builder->DEALER_LOGO){
						$html .= '<img src="https://d1xlji8qxtrdmo.cloudfront.net/'.$template_builder->DEALER_LOGO.'" />';
					}
					$html .= '</div>';
				}
				$html .= '<div class="temp_vin_section font_'.$template_builder->VI_FONT.'">';
				if($template_builder->SHOWSTOCK){
					$html .= '<div class="temp_stock '.($template_builder->STOCKBOLD != '0'?'bold':'').'" style="width:100%;">'.$template_builder->STOCKDESC.' '.$vehicle_info->STOCK_NUMBER.'</div><br>';
				}
				if($template_builder->SHOWVIN){
					$html .= '<div class="temp_vin '.($template_builder->VINBOLD != '0'?'bold':'').'" style="width:100%;">'.$template_builder->VINDESC.' '.$vehicle_info->VIN_NUMBER.'</div><br>';
				}
				$i = 0;
				if($template_builder->SHOWYEAR){
					$html .= '<div class="'.($template_builder->YEARBOLD != '0'?'bold':'').'">'.$template_builder->YEARDESC.' '.$vehicle_info->YEAR.'</div>';
					if(($i % 2) == 1){
						$html .= '<br>';
					}
					$i++;
				}
				if($template_builder->SHOWCOLOR){
					$html .= '<div class="'.($template_builder->COLORBOLD != '0'?'bold':'').'">'.$template_builder->COLORDESC.' '.$vehicle_info->EXT_COLOR.'</div>';
					if(($i % 2) == 1){
						$html .= '<br>';
					}
					$i++;
				}
				if($template_builder->SHOWMAKE){
					$html .= '<div class="'.($template_builder->MAKEBOLD != '0'?'bold':'').'">'.$template_builder->MAKEDESC.' '.$vehicle_info->MAKE.'</div>';
					if(($i % 2) == 1){
						$html .= '<br>';
					}
					$i++;
				}
				if($template_builder->SHOWTRIM){
					$html .= '<div class="'.($template_builder->TRIMBOLD != '0'?'bold':'').'">'.$template_builder->TRIMDESC.' '.$vehicle_info->TRIM.'</div>';
					if(($i % 2) == 1){
						$html .= '<br>';
					}
					$i++;
				}
				if($template_builder->SHOWMODEL){
					$html .= '<div class="'.($template_builder->MODELBOLD != '0'?'bold':'').'">'.$template_builder->MODELDESC.' '.$vehicle_info->MODEL.'</div>';
					if(($i % 2) == 1){
						$html .= '<br>';
					}
					$i++;
				}
				if($template_builder->SHOWMILEAGE){
					$html .= '<div class="'.($template_builder->MILEAGEBOLD != '0'?'bold':'').'">'.$template_builder->MILEAGEDESC.' '.$vehicle_info->MILEAGE.'</div>';
					if(($i % 2) == 1){
						$html .= '<br>';
					}
					$i++;
				}
				$html .= '</div>';
				$html .= '<div class="temp_options_section">
			          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			          		<tbody>';
			    if($template_builder->SHOWMSRP){
			    	$html .= '<tr class="temp_msrp font_'.$template_builder->MSRP_FONT.' '.($template_builder->MSRPBOLD?'bold':'').'">
			          				<td><span id="temp_msrp">'.$template_builder->MSRPDESC.'</span></td>
			                        <td><span>'.$aprice.'</span></td>
			          			</tr>';
			    }
			    $html .= '<tr>
		                        <td colspan="2"><div style="height:2px; width:100%;background-color:black;"></div></td>
		                  	</tr>';
		        if($template_builder->SHOWSECTION){
		        	$html .= '<tr class="temp_section font_'.$template_builder->SECTION_FONT.' '.($template_builder->SECTIONBOLD?'bold': '').'">
		                        <td colspan="2"><span id="section">'.$template_builder->SECTIONDESC.'</span></td>
		                  	</tr>';	
		        }
		        foreach($ad_datas as $ad_data){
		        	$html .= '<tr>
		                  		<td style="padding:0px">';
		            if($ad_data->SEPARATOR_ABOVE == '1'){
		      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
		      				$html .= '<br />';
		      			}
		      		}
		      		$html .= '<span class="option font_'.$template_builder->OPTION_FONT.' '.($template_builder->OPTIONBOLD?'bold':'').'">'.htmlspecialchars_decode($ad_data->ITEM_NAME).'</span>';
		      		if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
	      				$html .= '<br />';
	      			}
	      			if($ad_data->OR_OR_AD == 2){
						$optiontext="";
						$option_group = $ad_data->ITEM_DESCRIPTION;
						$option_group = explode(",", $option_group);
						foreach($option_group as $optiong){
							$optiontext.=$optiong."<br/>";
						}
						if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
							$html .='<div class="opdesc font_'.$template_builder->OPDESC_FONT.' '.($template_builder->OPDESCBOLD?'bold':'').'">'.$optiontext.'</div>';
						}
					}else{
						if($ad_data->ITEM_DESCRIPTION != '' && $ad_data->ITEM_DESCRIPTION != null){
							$html .='<div class="opdesc font_'.$template_builder->OPDESC_FONT.' '.($template_builder->OPDESCBOLD?'bold':'').'">'.html_entity_decode($ad_data->ITEM_DESCRIPTION).'</div>';
						}
					}
					if($ad_data->SEPARATOR_BELOW == '1'){
		      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
		      				$html .= '<br />';
		      			}
	      			}
	      			$html .= '</td>';
	      			$html .= '<td valign="top" style="padding:0px">';
	      			$oprice = '';
					if($ad_data->ITEM_PRICE<0){
						$oprice=floatVal(abs($ad_data->ITEM_PRICE));
						$oprice=number_format($oprice, 2, '.', ',');
						$oprice='-$'.$oprice;
					}else if($ad_data->ITEM_PRICE=="0"){
						$oprice='Free';
					}else if(strtolower($ad_data->ITEM_PRICE)=="np"){
						$oprice='';
					}else if(strtolower($ad_data->ITEM_PRICE)=="fr"){
						$oprice='Free';
					}else if(strtolower($ad_data->ITEM_PRICE)=="in"){
						$oprice='Included';
					}else if(strtolower($ad_data->ITEM_PRICE)=="inc"){
						$oprice='Included';
					}else if(strtolower($ad_data->ITEM_PRICE)=="nc"){
						$oprice='No Charge';
					}else{
						$oprice=floatval($ad_data->ITEM_PRICE);
						$oprice=number_format($oprice, 2, '.', ',');
						$oprice='$'.$oprice;
					}
					if($ad_data->SEPARATOR_ABOVE == '1'){
		      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
		      				$html .= '<br />';
		      			}
		      		}
		      		$html .= '<span class="option font_'.$template_builder->OPTION_FONT.' '.($template_builder->OPTIONBOLD?'bold':'').'">'.$oprice.'</span>';
		      		if($ad_data->SEPARATOR_BELOW == '1'){
		      			for($i = 0; $i < intVal($ad_data->SEPARATOR_SPACES); $i++){
		      				$html .= '<br />';
		      			}
	      			}
	      			$html .= '</td>
			                  	</tr>';
			    }
			    $html .= '</tbody>
			          	</table>
					</div>';

				$html .= '<div id="temp_subtotal_section">';
				if($template_builder->SHOWSUBTOTAL){
					$html .= '<div class="temp_subtotal font_'.$template_builder->SUBTOTAL_FONT.' '.($template_builder->SUBTOTALBOLD?'bold':'').'">
							<div id="subtotal" style="text-align:right; width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)-($template_builder->TEMPLATE_WIDTH == 'Narrow'?20:30)-(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)*0.3+10)).'px">'.$template_builder->SUBTOTALDESC.'</div>
							<div id="subtotal_num" style="width:'.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)*0.3).'px">'.$voptions.'</div>
						</div>';
				}
				$html .= '</div>';
				$html .= '<div id="temp_total_section">';
				if($template_builder->SHOWTOTAL){
					$html .= '<div class="temp_total font_'.$template_builder->TOTAL_FONT.' '.($template_builder->TOTALBOLD?'bold':'').'">
			          		<span id="total" style="width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)-($template_builder->TEMPLATE_WIDTH == 'Narrow'?20:30)-(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)*0.41)).'px">'.$template_builder->TOTALDESC.'</span>
			              	<div id="total_num" style="width:'.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)*0.41).'px; margin-right: '.($template_builder->TEMPLATE_WIDTH == 'Standard'?'10px':'').'">'.$vtotal.'</div> 
			            </div>';
				}
				$html .= '</div>';
				$html .= '<div id="temp_address_section">
							<div class="temp_address font_MED bold">
				          		<span class="daddress">
				          			'.$dealer_info->DEALER_NAME.'<br>
						    		'.$dealer_info->DEALER_ADDRESS.'<br>
						    		'.$dealer_info->DEALER_CITY.' '.$dealer_info->DEALER_STATE.' '.$dealer_info->DEALER_ZIP.'<br>
						    		'.$dealer_info->PHONE.'
				          		</span>
				            </div>
						</div>';
		        $html .= '<div id="temp_footer_section">';
		        if($template_builder->STOCK_SELECT == 'default_image' || $template_builder->STOCK_SELECT == 'upload_image'){
		        	$html .= '<div class="infographic" style="width:'.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)-20).'px">
				              	<img class="temp_stock_image stockin" style="width:'. (($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)-20).'px; height: '.$template_builder->FOOTER_HEIGHT.'px" src="'.$fcdn_url.$template_builder->INFO_IMAGE.'">
				            </div>';
		        }
		        if($template_builder->STOCK_SELECT == 'qr_code'){
		        	$html .= '<div style="width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)-30).'px" class="infographic '.($template_builder->TEMPLATE_LAYOUT == 'Medium'?'mediumT ':'').($template_builder->TEMPLATE_LAYOUT == 'Top'?'topT ':'').($template_builder->TEMPLATE_LAYOUT == 'Large'?'largeT':'').'">';
		        	$html .= '<div class="medium_qr_code stockin">';
		        	if($template_builder->TEMPLATE_LAYOUT != 'Small'){
		        		$html .= '<div class="qr_title">'.$template_builder->QR_TITLE.'</div>';
		        	}
		        	$html .= '<div class="qr_float">';
		        	if($template_builder->TEMPLATE_LAYOUT != 'Small'){
		        		$html .= '<div class="qr_text">'.$template_builder->QR_TEXT.'</div>';
		        	}
		        	$html .= '<div class="qr_code">';
		        	if($template_builder->QR_URL){
		        		$html .= '<img src="https://chart.googleapis.com/chart?cht=qr&chs=100x100&chld=M|0&chl='.$qr_url.'" style="height: '.($template_builder->TEMPLATE_LAYOUT == 'Small'?($template_builder->FOOTER_HEIGHT-5).'px':'').'">';
		        	}else{
		        		$html .= '<img src="'.$this->site_baseurl.'/img/template/qr.png" style="height: '.($template_builder->TEMPLATE_LAYOUT == 'Small'?($template_builder->FOOTER_HEIGHT-5).'px':'').'">';
		        	}
		        	$html .= '</div>';
		        	$html .= '</div>';
		        	if($template_builder->TEMPLATE_LAYOUT == 'Top' || $template_builder->TEMPLATE_LAYOUT == 'Large'){
		        		$html .= '<div class="qr_text2">'.$template_builder->QR_TEXT2.'</div>';
		        	}
		        	if($template_builder->TEMPLATE_LAYOUT != 'Small'){
		        		$html .= '<div class="qr_footer">'.html_entity_decode($template_builder->QR_FOOTER).'</div>';
		        	}
		        	$html .= '</div>';
		        	$html .= '</div>';
		        }
		        if($template_builder->STOCK_SELECT == 'barcode'){
		        	$html .= '<div style="width: '.(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)-30).'px" class="infographic '.($template_builder->TEMPLATE_LAYOUT == 'Medium'?'mediumT ':'').($template_builder->TEMPLATE_LAYOUT == 'Top'?'topT ':'').($template_builder->TEMPLATE_LAYOUT == 'Large'?'largeT':'').'">';
		        	$html .= '<div class="medium_bar_code stockin">';
		        		if($template_builder->TEMPLATE_LAYOUT != 'Small'){
		        			$html .= '<div class="bar_title">'.$template_builder->BAR_TITLE.'</div>';
		        		}
		        		$html .= '<div class="bar_float">';
		        		if($template_builder->TEMPLATE_LAYOUT != 'Small'){
		        			$html .= '<div class="bar_text">'.$template_builder->BAR_TEXT.'</div>';
		        		}
		        		$html .= '<div class="bar_code" style="text-align:center; width:100%;">';
		        		$html .= '<img src="'.$barcode_url.'" style="width: '.($template_builder->TEMPLATE_LAYOUT == 'Small'?(($template_builder->TEMPLATE_WIDTH == 'Narrow'?252:346)-30).'px':'').'; height: '.($template_builder->TEMPLATE_LAYOUT == 'Small'?($template_builder->STOCK_SELECT-5).'px':'').';">';
		        		$html .= '</div>';
		        		$html .= '</div>';
		        		if($template_builder->TEMPLATE_LAYOUT != 'Small'){
		        			$html .= '<div class="bar_footer" style="white-space: wrap;">'.html_entity_decode($template_builder->BAR_FOOTER).'</div>';
		        		}
		        	$html .= '</div>';
		        	$html .= '</div>';
		        }
	          	$html .= '</div>
					</div></div>';
				$html .= $pb;
			}
		}

		$html .= $js.'</body>
			</html>';

		//In future, we will deal with several case of ADDENDUM_STYLE
		$addendum="Addendum".time().".pdf";
		$dompdf = new Dompdf();

		//background image access *important for external background image
		$dompdf->set_option('isRemoteEnabled', 'true');

		$margin_width=(($template_data->LEFT_MARGIN*0.063)*0.166044)+(($template_data->RIGHT_MARGIN*0.063)*0.166044);
		$margin_height=(($template_data->TOP_MARGIN*0.063)*0.166044)+(($template_data->BOTTOM_MARGIN*0.063)*0.166044);
		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper( array(0,0, (4.25+($margin_width)) * 72, (11+($margin_height)) * 72), "portrait" ); // 12" x 12"
		$dompdf->loadHtml($html);
		// Render the HTML as PDF
		$dompdf->render();
		// Output the generated PDF to Browser
		if($dtype=='download'){
			$dompdf->stream($addendum);
		}else{
			$dompdf->stream($addendum, array('Attachment' => 0));
		}
	}

	/**
	 *
	 * Get Data for Print SMS Modal View
	 *
	 * @param dealerinventory _ID
	 * @return array
	 *
	 */
	public function getDataForPrintsms(Request $request){
		$vid = $request->input('vid');
		$user = Auth::user();
		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		if(!$vehicle_info){
			return json_encode(array('status' => 'error', 'msg' => 'Sorry! This vehicle is not available.'));
		}
		$sms_temp_data = SMSTemplate::getSMSTemplateByUserID($user->USER_ID);
		if(!$sms_temp_data){
			return json_encode(array('status' => 'error', 'msg' => 'Sorry! There isn\'t your SMS template.'));	
		}
		return json_encode(array('status' => 'success', 'vehicle_info' => $vehicle_info, 'dealer_info' => $dealer_info, 'sms_temp_data' => $sms_temp_data));
	}

	/**
	 *
	 * set SMS Status for Downlaod PDF
	 *
	 * @param DealerInventory _ID
	 * @return Status
	 *
	 */
	public function smsStatus(Request $request){
		$vid = $request->input('vid');
		$t_date=date('Y-m-d');
		$starttime = date('Y-m-d H:i:s');
		if($request->has('type') && $request->input('type') == 'multiprint'){
			$print_ids = $request->has('vids')?$request->input('vids'):'';
			$print_ids = json_decode($print_ids, true);
			foreach($print_ids as $print_id){
				$result = DealerInventory::updateInventoryData($print_id, array('PRINT_SMS' => '1', 'PRINT_FLAG' => '1', 'PRINT_DATE' => $t_date, 'UPDATE_DATE' => $t_date, 'PT' => $starttime));
			}
		}else{
			$result = DealerInventory::updateInventoryData($vid, array('PRINT_SMS' => '1', 'PRINT_FLAG' => '1', 'PRINT_DATE' => $t_date, 'UPDATE_DATE' => $t_date, 'PT' => $starttime));
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 * 
	 * Generate Phone SMS print view to PDF and Download
	 *
	 * @param DealerInventory _ID
	 * 
	 */
	public function smsDownload(Request $request){
		$vid = $request->input('vid');
		$type = $request->has('type')?$request->input('type'):'';
		$user = Auth::user();
		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		// Get Dealer Information
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		// Get Dealer Inventory Information
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		// Get User's Template to format PDF Pages
		$template_data = TemplateData::getTemplateData($user->DEALER_ID, $vehicle_info->NEW_USED);
		// Get User's SMS Template
		$sms = SMSTemplate::getSMSTemplateByUserID($user->USER_ID);

		$cdn_url="http://addendum-backgrounds.s3.amazonaws.com";

		$js = '';
		if($type != 'download'){
			$js='<script type="text/javascript">window.print();</script>';
		}

		$trhtml = '';
		
		$left_margin=1.85+($sms->LEFT_MARGIN*0.063);
		$right_margin=1.85+($sms->RIGHT_MARGIN*0.063);
		$top_margin=3+($sms->TOP_MARGIN*0.063);
		$bottom_margin=0+($sms->BOTTOM_MARGIN*0.063);

		$colorrow = '';
		if(!empty($vehicle_info->EXT_COLOR)){
			$colorrow = '<span class="bold1">Color: '.$vehicle_info->EXT_COLOR.'</span><br />';		
		}

		$trimrow = '';
		if(!empty($vehicle_info->TRIM)){
			$trimrow = '<span class="bold1">Trim: '.$vehicle_info->TRIM.'</span>';
		}

		$mileagerow = '';
		if(!empty($vehicle_info->MILEAGE)){
			$mileagerow='<span class="bold1">Mileage: '.$vehicle_info->MILEAGE.'</span>';
		}

		$sms_code = $dealer_info->SMS_PREFIX.substr(str_replace(' ', '',$vehicle_info->VIN_NUMBER), -5);

		if($sms->TEMPLATE == 'SMS'){
			$html =<<<EOF
    <html lang="en">
      	<head>
	        <style type="text/css">
	          	@page { margin: 0px; }
	          	.page-break { display: block; page-break-before: always; }
	      		.page-break:last-child {
		            page-break-before: avoid;
	          	}
			  	body{
				  	font-family:Arial, Helvetica, sans-serif;
			  	}
	          	#printit{
		            background:url($cdn_url/$sms->COLOR.jpg) no-repeat;
		            height: 3068.75px;
		            width: 100%;
		            margin:$top_margin+em $right_margin+em $bottom_margin+em $left_margin+em;
	          	}
	          	#second-section{
		      		overflow: hidden;
		            height: auto;
		            width: 90%;
		            margin: 470px 0px 0px 62.5px;
		            height:1893px;
				 	font-family: Arial,Helvetica,sans-serif;
	          	}
	          	.bold1{
			      	font-family: Arial,Helvetica,sans-serif;
				    font-size: 50.50px;
				    line-height: 50.50px;
				    padding: 0 0 0 156.4px;
				    text-align: center;
				    width: 625.6px;
		    	}
			    .sms{
					font-size: 160px;
				    font-weight: bold;
				    line-height: 131px;
				    padding: 780px 0 0 185px;
				    text-align: center;
				    width: 656.88px;
				}
				.model{
					font-size: 50.5px;
				    font-weight: bold;
				    line-height: 50.5px;
				    overflow: hidden;
				    padding: 600px 0 0 156.4px;
				    text-align: center;
				    width: 625.6px;
				}
				.logo{
					padding: 250px 0 0 50px;
					text-align: center;
					width: 1000px;
				}
	        </style>
      	</head>
      	<body>
        	<div id="printit">
     	 		<div id="second-section">
            		<div class="sms">
    					$sms_code
    				</div>
    				<div class="model">
   						$vehcle_info->YEAR $vehicle_info->MODEL
    				</div>
    				<div class="bold1">
    					$vehicle_info->MAKE Stock # $vehicle_info->STOCK_NUMBER<br /> $vehicle_info->EXT_COLOR
    				</div>
          		</div>
          		<div class="logo">
    				<img src="http://d1xlji8qxtrdmo.cloudfront.net/$sms->LOGO" style="height:250px; width:auto;" />
    			</div>
        	</div>
      		$js
      	</body>
    </html>
EOF;
		}else{
			$scc = '';
			$ic = '';
			$lc = '';
			if($sms['SMS_CODE_CHECK']=="0"){
		  		$scc="visibility:hidden;";
		  	}
		  	if($sms['INFO_CHECK']=="0"){
				$ic="visibility:hidden;";
		  	}
		  	if($sms['LOGO_CHECK']=="0"){
				$lc="visibility:hidden;";
		  	}
		  	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
					  	<img src="http://d1xlji8qxtrdmo.cloudfront.net/'.$sms->CUSTOM1_BG.'" style="width: 90%;">
					</div>';
		  	$html =<<<EOF
    <html lang="en">
      	<head>
        	<style type="text/css">
          		@page { margin: 0px; }
          		.page-break { display: block; page-break-before: always; }
          		.page-break:last-child {
		            page-break-before: avoid;
	          	}
			  	body{
					font-family:Arial, Helvetica, sans-serif;
			  	}	
	          	#printit{
		            width: 346px;
		            margin:$top_margin+em $right_margin+em $bottom_margin+em $left_margin+em;
	          	}
    			#second-section{
					margin: 235px 0 0 30px;
    				overflow: hidden;
    				width: 90%;
    			}
    			.bold1{
      				font-family: Arial,Helvetica,sans-serif;
    				font-size: 16px;
					line-height: 16px;
    				padding: 0 0 0 50px;
    				text-align: center;
    				width: 200px;
				}
    			.sms{
					font-size: 46px;
    				font-weight: bold;
					line-height: 46px;
    				padding: 255px 0 0 38px;
    				text-align: center;
    				width: 226px;
					$scc
				}
				.model{
					font-size: 16px;
    				line-height: 16px;
    				overflow: hidden;
    				padding: 150px 0 0 35px;
    				width: 250px;
					$ic
				}
				.logo{
    				padding: 35px 0 0 10px;
    				text-align: center;
    				width: 325px;
					$lc
				}
        	</style>
      	</head>
      	<body>
        	<div id="printit">
        		$background_html
          		<div id="second-section">
    				<div class="sms">
    					$sms_code
    				</div>
				    <div class="model">
   						Make: $vehicle_info->YEAR $vehicle_info->MAKE<br />
    					Model: $vehicle_info->MODEL <br />Trim: $vehicle_info->TRIM <br />Stock: $vehicle_info->STOCK_NUMBER<br /> VIN: $vehicle_info->VIN_NUMBER
    				</div>
          		</div>
    			<div class="logo">
    				<img src="http://d1xlji8qxtrdmo.cloudfront.net/$sms->LOGO" style="height:80px; width:auto;" />
    			</div>
        	</div>
      		$js
  		</body>
    </html>
EOF;
		}

		$printsms = "SMS".$vid.".pdf";
		$dompdf = new DOMPDF();
		$margin_width=(($sms->LEFT_MARGIN*0.063)*0.166044)+(($template_data->RIGHT_MARGIN*0.063)*0.166044);
	  	$margin_height=(($sms->TOP_MARGIN*0.063)*0.166044)+(($template_data->BOTTOM_MARGIN*0.063)*0.166044);

	  	//background image access *important for external background image
		$dompdf->set_option('isRemoteEnabled', 'true');

	  	$dompdf->setPaper( array(0,0, (4.25+($margin_width)) * 72, (11+($margin_height)) * 72), "addendum" ); // 12" x 12"
	  	$dompdf->loadHtml($html);
	  	$dompdf->render();
	  	if($type == 'download')
	  	{
	  		if($dealer_info->SAVE_PDF == '1'){
	  			//put pdf to s3 bucket
	  		}
	  		$dompdf->stream($printsms);
	  	}else{
	  		if($dealer_info->SAVE_PDF == '1'){
	  			//put pdf to s3 bucket
	  		}
	  		$dompdf->stream($printsms,array('Attachment'=>0));
	  	}
	}

	/**
	 *
	 * Get Print Guide Data
	 *
	 * @param dealerinventory _ID
	 * @return array
	 *
	 */
	public function printGuide(Request $request){
		$user = Auth::user();
		if($request->has('vid')){
			$vid = $request->input('vid');
			//Get Vehicle Buyer Guide with dealerInventory _ID
			$vbg_info = VehicleBuyerGuide::getVehicleBuyerGuideByVehicleID($vid);
			if($vbg_info){
				return json_encode(array('vbg_info' => $vbg_info));
			}else{
				$dbg_info = DefaultBuyerGuide::getDefaultBuyerGuideByDealerID($user->DEALER_ID);
				return json_encode(array('vbg_info' => $dbg_info));
			}
		}else{
			$dbg_info = DefaultBuyerGuide::getDefaultBuyerGuideByDealerID($user->DEALER_ID);
			return json_encode(array('vbg_info' => $dbg_info));
		}
		
	}

	/**
	 *
	 * Save Buyer Guide Print Options
	 *
	 * @param print options
	 * @return status
	 *
	 */
	public function saveBgPrint(Request $request){
		$user = Auth::user();
		$type = '';
		$vids = array();
		if($request->has('vid')){
			$vid_req = $request->input('vid');
			$vid_req = json_decode($vid_req);
			if(is_array($vid_req)){
				$vids = $vid_req;
			}else{
				$vids[0] = $vid_req;
			}
		}
		

		
		$dg = 'asis';
		if($request->input('dg_implied') == 'Yes'){
			$dg = 'implied';
		}elseif($request->input('dg_warranty') == 'Yes'){
			$dg = 'warranty';
		}elseif($request->input('dg_warranty_s') == 'Yes'){
			$dg = 'warranty_spanish';
		}
		$sc_asis = $request->input('sc_asis') == true?'1':'0';
		$sc_implied = $request->input('sc_implied') == true?'1':'0';
		$sc_warranty = $request->input('sc_warranty') == true?'1':'0';
		$sc_warranty_s = $request->input('sc_warranty_s') == true?'1':'0';

		$wt_warranty = $request->input('wt_warranty') == true?'FULL':'Ltd.';
		$wt_warranty_s = $request->input('wt_warranty_s') == true?'FULL':'Ltd.';
		
		$m_warranty = $request->input('m_warranty') == true?'1':'0';
		$uv_warranty = $request->input('uv_warranty') == true?'1':'0';
		$ouv_warranty = $request->input('ouv_warranty') == true?'1':'0';
		
		$wt_parts = $request->input('wt_parts');
		$wt_labor = $request->input('wt_labor');
		$wt_sc = nl2br(htmlspecialchars($request->input('wt_sc'), ENT_QUOTES)); // to encode html entities
		$wt_duration = nl2br(htmlspecialchars($request->input('wt_duration'), ENT_QUOTES)); // to encode html entities

		$wts_parts = $request->input('wts_parts');
		$wts_labor = $request->input('wts_labor');
		$wts_sc = nl2br(htmlspecialchars($request->input('wts_sc'), ENT_QUOTES)); // to encode html entities
		$wts_duration = nl2br(htmlspecialchars($request->input('wts_duration'), ENT_QUOTES)); // to encode html entities
		
		$bp_dealer = $request->input('bp_dealer');
		$bp_address = $request->input('bp_address');
		$bp_phone = $request->input('bp_phone');
		$bp_email = $request->input('bp_email');
		$bp_complaints = $request->input('bp_complaints');

		$t_date=date('Y-m-d');
		$starttime = date('Y-m-d H:i:s');

		if(count($vids)){
			foreach($vids as $vid){
				//Get Vehicle Buyer Guide with dealerInventory _ID
				$vbg_info = VehicleBuyerGuide::getVehicleBuyerGuideByVehicleID($vid);
				$result = DealerInventory::updateInventoryData($vid, array('PRINT_GUIDE' => '1', 'PRINT_DATE' => $t_date, 'UPDATE_DATE' => $t_date, 'PT' => $starttime));
				$save_object = array(
					'M_WARRANTY' => $m_warranty,
					'UV_WARRANTY' => $uv_warranty,
					'OUV_WARRANTY' => $ouv_warranty,
					'DEFAULT_GUIDE' => $dg,
					'SC_ASIS' => $sc_asis,
					'SC_IMPLIED' => $sc_implied,
					'SC_WARRANTY' => $sc_warranty,
					'WARRANTY_TYPE' => $wt_warranty,
					'PARTS' => $wt_parts,
					'LABOR' => $wt_labor,
					'SYSTEMS_COVERED' => $wt_sc,
					'DURATION' => $wt_duration,
					'SC_WARRANTY_S' => $sc_warranty_s,
					'WARRANTY_TYPE_S' => $wt_warranty_s,
					'PARTS_S' => $wts_parts,
					'LABOR_S' => $wts_labor,
					'SYSTEMS_COVERED_S' => $wts_sc,
					'DURATION_S' => $wts_duration,
					'DEALER_ID' => $user->DEALER_ID,
					'VEHICLE_ID' => $vid,
					'LABEL_ASIS' => '',
					'LABEL_IMPLIED' => '',
					'LABEL_WARRANTY' => '',
					'LABEL_WARRANTY_S' => ''
				);
				if($vbg_info){
					$result = VehicleBuyerGuide::updateVehicleBuyerGuide($vbg_info->_ID, $save_object);
				}else{
					$result = VehicleBuyerGuide::insertVehicleBuyerGuide($save_object);
				}
			}
		}else{
			$save_object = array(
				'M_WARRANTY' => $m_warranty,
				'UV_WARRANTY' => $uv_warranty,
				'OUV_WARRANTY' => $ouv_warranty,
				'DEFAULT_GUIDE' => $dg,
				'SC_ASIS' => $sc_asis,
				'SC_IMPLIED' => $sc_implied,
				'SC_WARRANTY' => $sc_warranty,
				'WARRANTY_TYPE' => $wt_warranty,
				'PARTS' => $wt_parts,
				'LABOR' => $wt_labor,
				'SYSTEMS_COVERED' => $wt_sc,
				'DURATION' => $wt_duration,
				'SC_WARRANTY_S' => $sc_warranty_s,
				'WARRANTY_TYPE_S' => $wt_warranty_s,
				'PARTS_S' => $wts_parts,
				'LABOR_S' => $wts_labor,
				'SYSTEMS_COVERED_S' => $wts_sc,
				'DURATION_S' => $wts_duration,
				'DEALER_ID' => $user->DEALER_ID,
				'LABEL_ASIS' => '',
				'LABEL_IMPLIED' => '',
				'LABEL_WARRANTY' => '',
				'LABEL_WARRANTY_S' => ''
			);
			$dbg_info = DefaultBuyerGuide::getDefaultBuyerGuideByDealerID($user->DEALER_ID);

			if($dbg_info){
				$update_id = DefaultBuyerGuide::updateDefaultBuyerGuide($dbg_info->_ID, $save_object);
			}else{
				$insert_id = DefaultBuyerGuide::InsertDefaultBuyerGuide($save_object);
			}
		}
		
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Save Backpage Buyer Guide Print Options
	 *
	 * @param print options
	 * @return status
	 *
	 */
	public function saveBpBgPrint(Request $request){
		$bp_dealer = $request->input('bp_dealer');
		$bp_address = $request->input('bp_address');
		$bp_phone = $request->input('bp_phone');
		$bp_email = $request->input('bp_email');
		$bp_complaints = $request->input('bp_complaints');
		$request->session()->put('bp_dealer', $bp_dealer);
		$request->session()->put('bp_address', $bp_address);
		$request->session()->put('bp_phone', $bp_phone);
		$request->session()->put('bp_email', $bp_email);
		$request->session()->put('bp_complaints', $bp_complaints);
		return json_encode(array('status' => 'success'));
	}
	/**
	 *
	 * Generate Buyer Guide pirnt view
	 *
	 * @param dealerInventory _ID
	 *
	 */
	public function guideDownload(Request $request){
		$type = $request->input('type');
		$vids = array();
		$vid_req = $request->input('vid');
		$vid_req = json_decode($vid_req);
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}
		$user = Auth::user();
		$cdn_url="http://d1ja3ktoroz7zi.cloudfront.net";
		$lcdn_url= $this->site_baseurl.'/img';
		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		// Get Dealer Information
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$js='<script type="text/javascript">window.print();</script>';

		$html = '
		<html lang="en">
			<head>
				<style type="text/css">
					@page {margin:4em 4.72em 4em 4.75em;}
					.page-break { display: block; page-break-before: always; }
					.page-break:last-child {
						page-break-before: avoid;
					}
					body{
					}
					.make{
						position:absolute;
						top:130px;
						left:45px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
					}
					.model{
						position:absolute;
						top:130px;
						left:195px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
					}
					.year{
						position:absolute;
						top:130px;
						left:313px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
					}
					.vin{
						position:absolute;
						top:130px;
						left:470px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
					}
					.stock{
						position:absolute;
						top:130px;
						left:370px;
						width:60px;
						text-align:center;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
					}
					.stocktext{
						position:absolute;
						top:152px;
						left:380px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:8px;
					}
					.parts{
						position:absolute;
						top:336px;
						left:299px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
					}
					.labor{
						position:absolute;
						top:336px;
						left:412px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
					}

					.texttitle{
						position:absolute;
						top:150px;
						left:62px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:70px;
						font-weight:bold;
						color:#221e1f;
					}
					.text{
						position:absolute;
						top:196px;
						left:14px;
						font-family:Arial, Helvetica, sans-serif;
						font-size:20px;
						font-weight:bold;
						color:#221e1f;
						width:420px;
					}
				</style>
			</head>
			<body>';
		foreach($vids as $pg_index => $vid){
			// Get Dealer Inventory Information
			$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
			//Get Vehicle Buyer Guide with dealerInventory _ID
			$vbg_info = VehicleBuyerGuide::getVehicleBuyerGuideByVehicleID($vid);

			$stock_update = '';
			if($dealer_info->GUIDE_STOCK == '1'){
				$verow1=$vehicle_info[$dealer_info->GUIDE_FIELD];
				$stock_update=$dealer_info->GUIDE_PREFIX.substr($verow1, '-'.$dealer_info->GUIDE_CHARACTER);
			}
			$class = 'page_'.$pg_index;
			if($vbg_info->DEFAULT_GUIDE=="asis"){
				$label=$vbg_info->LABEL_ASIS;
				$bigtick=".".$class." .bigtick{
							position:absolute;
							top:213px;
							left:39px;
						}";
				$bigticktext='<div class="bigtick"><img src="'.$cdn_url.'/bigtick.jpg"  width="24" height="23"/></div>';
				$warrantycss="";
				$warrantytext='';
				$sccss="";
				$sctext="";
				if($vbg_info->SC_ASIS){
					$sccss=".".$class." .sc{
							position:absolute;
							top:674px;
							left:39px;
						}";
					$sctext='<div class="sc"><img width="10" height="9" src="'.$cdn_url.'/smalltick.png" /></div>';
				}
				$labor="";
				$duration="";
				$parts="";
				$systems="";
				$guidebg="/bg3.jpg";
			}elseif($vbg_info->DEFAULT_GUIDE=="implied"){
				$label=$vbg_info->LABEL_IMPLIED;
				$bigtick=".".$class." .bigtick{
						position:absolute;
						top:213px;
						left:40px;
					}";
				$bigticktext='<div class="bigtick"><img  width="24" height="23" src="'.$cdn_url.'/bigtick.jpg" /></div>';
				$warrantycss="";
				$warrantytext='';
				$sccss="";
				$sctext="";
				if($vbg_info->SC_IMPLIED){
					$sccss=".".$class." .sc{
							position:absolute;
							top:674px;
							left:39px;
						}";
					$sctext='<div class="sc"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
				}
				$text2="";
				$labor="";
				$duration="";
				$parts="";
				$systems="";
				$guidebg="/bg2.jpg";
			}elseif($vbg_info->DEFAULT_GUIDE=="warranty" || $vbg_info->DEFAULT_GUIDE=="warranty_spanish"){
				$label=$vbg_info->LABEL_WARRANTY;
				$bigtick=".".$class." .bigtick{
						position:absolute;
						top:277px;
						left:40px;
					}";
				$bigticktext='<div class="bigtick"><img src="'.$cdn_url.'/bigtick.jpg" width="24" height="23"/></div>';
				if($vbg_info->WARRANTY_TYPE == "FULL"){
					$warrantycss=".".$class." .warranty{
							position:absolute;
							top:318px;
							left:55px;
						}";
					$warrantytext='<div class="warranty"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
				}elseif($vbg_info->WARRANTY_TYPE == "Ltd."){
					$warrantycss=".".$class." .warranty{
							position:absolute;
							top:342px;
							left:55px;
						}";
					$warrantytext='<div class="warranty"><img src="'.$cdn_url.'/smalltick.png" width="10" height="9"/></div>';
				}

				$sccss="";
				$sctext="";
				if($vbg_info->SC_WARRANTY){
					$sccss=".".$class." .sc{
							position:absolute;
							top:674px;
							left:39px;
						}";
					$sctext='<div class="sc"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
				}
				$labor=$vbg_info->LABOR;
				$duration=html_entity_decode($vbg_info->DURATION);
				$parts=$vbg_info->PARTS;
				$mysystems = $vbg_info->SYSTEMS_COVERED;
				$systems_array = explode(",", $mysystems);
				$systems="";
				foreach($systems_array as $ssystems){
					$systems.=$ssystems."<br>";
				}
				$guidebg="/bg1.jpg";
			}

			$mwcss="";
			$mwtext="";
			if($vbg_info->M_WARRANTY){
				$mwcss=".".$class." .mw{
						position:absolute;
						top:557px;
						left:39px;
					}";
				$mwtext='<div class="mw"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
			}

			$uvwcss="";
			$uvwtext="";
			if($vbg_info->UV_WARRANTY){
				$uvwcss=".".$class." .uv{
						position:absolute;
						top:588px;
						left:39px;
					}";
				$uvwtext='<div class="uv"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
			}

			$ouvwcss="";
			$ouvwtext="";
			if($vbg_info->OUV_WARRANTY){
				$ouvwcss=".".$class." .ouvw{
						position:absolute;
						top:609px;
						left:39px;
					}";
				$ouvwtext='<div class="ouvw"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
			}

			if(strlen($duration)>380){
				$durcss=".".$class." .duration{
						position:absolute;
						top:426px;
						left:342px;
						width:310px;
						height:90px;
						overflow:hidden;
						font-family:Arial, Helvetica, sans-serif;
						font-size:10px;
						line-height:8px;
						word-wrap: break-word;
					}";
			}else{
				$durcss=".".$class." .duration{
						position:absolute;
						position:absolute;
						top:426px;
						left:342px;
						width:310px;
						height:90px;
						overflow:hidden;
						font-family:Arial, Helvetica, sans-serif;
						font-size:12px;
						line-height:10px;
						word-wrap: break-word;
					}";
			}

			if(strlen($systems)>380){
				$syscss=".".$class." .systems{
						position:absolute;
						top:430px;
						left:40px;
						width:295px;
						height:75px;
						overflow:hidden;
						font-family:Arial, Helvetica, sans-serif;
						font-size:12px;
						line-height:12px;
					}";
			}else{
				$syscss=".".$class." .systems{
						position:absolute;
						top:430px;
						left:40px;
						width:295px;
						height:75px;
						overflow:hidden;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
						line-height:14px;
					}";
			}
			$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
					  	<img src="'.$lcdn_url.'/'.$guidebg.'" style="width: 100%;">
					</div>';
			$html .= '
			<style type="text/css">'.$bigtick.' '.$warrantycss.' '.$sccss.' '.$mwcss.' '.$uvwcss.' '.$ouvwcss.' '.$durcss.' '.$syscss.'
			</style>'.$background_html.'
			<div class="'.$class.'">
			<div class="make">'.$vehicle_info->MAKE.'</div>
			<div class="year">'.$vehicle_info->YEAR.'</div>
			<div class="model">'.$vehicle_info->MODEL.'</div>
			<div class="stock">'.$vehicle_info->STOCK_NUMBER.$stock_update.'</div>
			<div class="stocktext">STOCK NO.</div>
			<div class="vin">'.$vehicle_info->VIN_NUMBER.'</div>
			'.$bigticktext.' '.$warrantytext.' '.$sctext.' '.$mwtext.' '.$uvwtext.' '.$ouvwtext.'
			<div class="parts">'.$parts.'</div>
			<div class="systems">'.$systems.'</div>
			<div class="duration">'.$duration.'</div>
			<div class="labor">'.$labor.'</div>
			</div>';
			if($pg_index < (count($vids)-1)){
				$html .= '<div class="page-break"></div>';
			}
		}
		$html .= $js.'</body></html>';

		$addendum="buyer_guides_".strtotime(date('Y-m-d')).".pdf";
		$dompdf = new DOMPDF();

		//background image access *important for external background image
		$dompdf->set_option('isRemoteEnabled', 'true');

		$dompdf->setPaper( array(0,0, 8.5 * 72, 11 * 72), "addendum" ); // 8.5" x 11"

		$dompdf->loadHtml($html);
		$dompdf->render();
		if($type == "print"){
			$dompdf->stream($addendum,array('Attachment'=>0));
		}else{
			$dompdf->stream($addendum);
		}
	}

	/**
	 *
	 * Generate Spanish Guide Print View
	 *
	 * @param print options
	 *
	 */
	public function guideDownloadSpanish(Request $request){
		$type = $request->input('type');
		$vids = array();
		$vid_req = $request->input('vid');
		$vid_req = json_decode($vid_req);
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}
		$user = Auth::user();

		$cdn_url="http://d1ja3ktoroz7zi.cloudfront.net";
		$lcdn_url= $this->site_baseurl.'/img';
		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		// Get Dealer Information
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);

		$js='<script type="text/javascript">window.print();</script>';
		$html ='<html lang="en">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<head>
			<style type="text/css">
				@page {margin:4em;}
				.page-break { display: block; page-break-before: always; }
				.page-break:last-child {
				page-break-before: avoid;
				}
				body{
				}
				.make{
					position:absolute;
					top:120px;
					left:15px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
				}
				.model{
					position:absolute;
					top:120px;
					left:165px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
				}
				.year{
					position:absolute;
					top:120px;
					left:295px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
				}
				.vin{
					position:absolute;
					top:120px;
					left:425px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
				}
				.stock{
					position:absolute;
					top:160px;
					left:15px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
				}
				.parts{
					position:absolute;
					top:475px;
					left:335px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
				}
				.labor{
					position:absolute;
					top:475px;
					left:515px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
				}

				.systems{
					position:absolute;
					top:570px;
					left:15px;
					width:295px;
					height:75px;
					overflow:hidden;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
					line-height:14px;
				}
				.duration{
					position:absolute;
					top:570px;
					left:342px;
					width:310px;
					height:150px;
					overflow:hidden;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
					line-height:14px;
					word-wrap: break-word;
				}
				.texttitle{
					position:absolute;
					top:750px;
					left:312px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:140px;
					font-weight:bold;
					color:#221e1f;
				}
				.texttitle1{
					position:absolute;
					top:750px;
					left:312px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:130px;
					font-weight:bold;
					color:#221e1f;
				}
				.texttitle2{
					position:absolute;
					top:310px;
					left:15px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:14px;
					font-weight:bold;
					color:#221e1f;
				}
				.text{
					position:absolute;
					top:315px;
					left:18px;
					font-family:Arial, Helvetica, sans-serif;
					font-size:12px;
					font-weight:bold;
					color:#221e1f;
					width:650px;
				}
				$bigtick
				$warrantycss
				$sccss
			</style>
		</head>
		<body>';
		foreach($vids as $pg_index => $vid){
			// Get Dealer Inventory Information
			$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
			//Get Vehicle Buyer Guide with dealerInventory _ID
			$vbg_info = VehicleBuyerGuide::getVehicleBuyerGuideByVehicleID($vid);
			$class = 'page_'.$pg_index;

			if($vbg_info->DEFAULT_GUIDE=="asis"){
				$label=$vbg_info->LABEL_ASIS;
				$bigtick=".".$class." .bigtick{
						position:absolute;
						top:268px;		
						left:35px;
					}";
				$bigticktext='<div class="bigtick"><img  width="36" height="35" src="'.$cdn_url.'/bigtick.jpg" /></div>';
				$warrantycss="";
				$warrantytext='';
				$sccss="";
				$sctext="";
				if($vbg_info->SC_ASIS){
					$sccss=".".$class." .sc{
							position:absolute;
							top:674px;
							left:39px;
						}";
					$sctext='<div class="sc"><img width="10" height="9" src="'.$cdn_url.'/smalltick.png" /></div>';
				}
				$texttitle="COMO ESTÁ—SIN GARANTÍA";
				$texttitlec='<div class="texttitle1">';
				$text1="USTED PAGARÁ TODOS LOS GASTOS DE CUALQUIER REPARACIÓN QUE SEA NECESARIA.";
				$text2=" El concesionario no asume ninguna responsabilidad por cualquier reparación, independientemente de las declaraciones verbales que haya hecho acerca del vehículo.";
				$labor="";
				$duration="";
				$parts="";
				$systems="";
			}elseif($vbg_info->DEFAULT_GUIDE=="implied"){
				$label=$vbg_info->LABEL_IMPLIED;
				$bigtick=".".$class." .bigtick{
						position:absolute;
						top:268px;
						left:35px;
					}";
				$bigticktext='<div class="bigtick"><img  width="36" height="35" src="'.$cdn_url.'/bigtick.jpg" /></div>';
				$warrantycss="";
				$warrantytext='';
				$sccss="";
				$sctext="";
				if($vbg_info->SC_IMPLIED){
					$sccss=".".$class." .sc{
							position:absolute;
							top:758px;
							left:16px;
						}";
					$sctext='<div class="sc"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
				}
				$texttitle="GARANTÍAS IMPLÍCITAS SOLAMENTE";
				$texttitlec='<div class="texttitle2">';
				$text1='Este término significa que el concesionario no hace promesas específicas de arreglar lo que requiera reparación cuando usted compra el vehículo o después del momento de la venta. Pero, las "garantías implícitas" de la ley estatal pueden darle a usted algunos derechos y hacer que el concesionario resuelva problemas graves que no fueron evidentes cuando usted compró el vehículo.';
				$text2="";
				$labor="";
				$duration="";
				$parts="";
				$systems="";
				
			}elseif($vbg_info->DEFAULT_GUIDE=="warranty" || $vbg_info->DEFAULT_GUIDE=="warranty_spanish"){
				$label=$vbg_info->LABEL_WARRANTY;
				$bigtick=".".$class." .bigtick{
						position:absolute;
						top:412px;
						left:35px;
					}";
				$bigticktext='<div class="bigtick"><img src="'.$cdn_url.'/bigtick.jpg" width="36" height="35"/></div>';
				if($vbg_info->WARRANTY_TYPE == "FULL"){
					$warrantycss=".".$class." .warranty{
							position:absolute;
							top:479px;
							left:18px;
						}";
					$warrantytext='<div class="warranty"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
				}elseif($vbg_info->WARRANTY_TYPE == "Ltd."){
					$warrantycss=".".$class." .warranty{
							position:absolute;
							top:479px;
							left:68px;
						}";
					$warrantytext='<div class="warranty"><img src="'.$cdn_url.'/smalltick.png" width="10" height="9"/></div>';
				}

				$sccss="";
				$sctext="";
				if($vbg_info->SC_WARRANTY){
					$sccss=".".$class." .sc{
							position:absolute;
							top:758px;
							left:16px;
						}";
					$sctext='<div class="sc"><img src="'.$cdn_url.'/smalltick.png"  width="10" height="9"/></div>';
				}
				$texttitle="COMO ESTÁ—SIN GARANTÍA";
				$texttitlec='<div class="texttitle1">';
				$text1="USTED PAGARÁ TODOS LOS GASTOS DE CUALQUIER REPARACIÓN QUE SEA NECESARIA.";
				$text2=" El concesionario no asume ninguna responsabilidad por cualquier reparación, independientemente	de las declaraciones verbales que haya hecho acerca del vehículo.";
				$labor=$vbg_info->LABOR;
				$duration=html_entity_decode($vbg_info->DURATION);
				$parts=$vbg_info->PARTS;
				$mysystems = $vbg_info->SYSTEMS_COVERED;
				$systems_array = explode(",", $mysystems);
				$systems="";
				foreach($systems_array as $ssystems){
					$systems.=$ssystems."<br>";
				}
			}
			$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
					  	<img src="'.$cdn_url.'/guide-bg-spanish.jpg" style="width: 100%;">
					</div>';
			$html .= '<style type="text/css">'.$bigtick.' '.$warrantycss.' '.$sccss.'
			</style>'.$background_html;
			$html .= '<div class="'.$class.'">
			<div class="make">'.$vehicle_info->MAKE.'</div>
			<div class="year">'.$vehicle_info->YEAR.'</div>
			<div class="model">'.$vehicle_info->MODEL.'</div>
			<div class="stock">'.$vehicle_info->STOCK_NUMBER.'</div>
			<div class="stocktext">STOCK NO.</div>
			<div class="vin">'.$vehicle_info->VIN_NUMBER.'</div>
			'.$bigticktext.' '.$warrantytext.' '.$sctext.'
			<div class="parts">'.$parts.'</div>
			<div class="systems">'.$systems.'</div>
			<div class="duration">'.$duration.'</div>
			<div class="labor">'.$labor.'</div>
			<div class="text">'.$text1.' '.$text2.'</div>
			</div>';
			if($pg_index < (count($vids)-1)){
				$html .= '<div class="page-break"></div>';
			}
		}
		$html .= $js.'</body></html>';
	
		/*$texttitlec $texttitle</div>*/
		$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
		$addendum="buyer_guide".$_GET['vid'].".pdf";
		$dompdf = new DOMPDF();

		//background image access *important for external background image
		$dompdf->set_option('isRemoteEnabled', 'true');

	  	$dompdf->setPaper( array(0,0, 8.5 * 72, 11 * 72), "addendum" ); // 8.5" x 11"
		$dompdf->loadHtml($html);
		$dompdf->render();
		if($type == 'print'){
			$dompdf->stream($addendum,array('Attachment'=>0));
		}else{
			$dompdf->stream($addendum);
		}
	}

	/**
	 *
	 * Generate BackPage Print View
	 *
	 */
	public function backpageDownload(Request $request){
		$user = Auth::user();
		$type = $request->input('type');
		$cdn_url="http://d1ja3ktoroz7zi.cloudfront.net";
		$lcdn_url= $this->site_baseurl.'/img';
		$bp_dealer = $request->session()->get('bp_dealer');
		$bp_address = $request->session()->get('bp_address');
		$bp_email = $request->session()->get('bp_email');
		$bp_phone = $request->session()->get('bp_phone');
		$bp_complaints = $request->session()->get('bp_complaints');

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));

		$js='<script type="text/javascript">window.print();</script>';

		$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
					  	<img src="'.$lcdn_url.'/backpage.jpg" style="width: 100%;">
					</div>';
		$html =<<<EOF
<html lang="en">
	<head>
		<style type="text/css">
			@page {margin:4em 4.72em 4em 4.75em;}
			.page-break { display: block; page-break-before: always; }
			.page-break:last-child {
			page-break-before: avoid;
			}
			body{
			}
			.dealer{
				position:absolute;
				top:705px;
				left:120px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
			.address{
				position:absolute;
				top:737px;
				left:110px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
			.complaints{
				position:absolute;
				top:800px;
				left:280px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
			.email{
				position:absolute;
				top:770px;
				left:380px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
			.phone{
				position:absolute;
				top:770px;
				left:115px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
		</style>
	</head>
	<body>
		$background_html
		<div class="address">$bp_address</div>
		<div class="dealer">$bp_dealer</div>
		<div class="phone">$bp_phone</div>
		<div class="email">$bp_email</div>
		<div class="complaints">$bp_complaints</div>
		$js
	</body>
</html>
EOF;
		$addendum="backpage.pdf";
		$dompdf = new DOMPDF();

		//background image access *important for external background image
		$dompdf->set_option('isRemoteEnabled', 'true');

		$dompdf->setPaper( array(0,0, 8.5 * 72, 11 * 72), "addendum" ); // 8.5" x 11"
		$dompdf->loadHtml($html);
		$dompdf->render();
		if($type == "print"){
			$dompdf->stream($addendum,array('Attachment'=>0));
		}else{
			$dompdf->stream($addendum);
		}

	}

	/**
	 *
	 * Generate Spanish Backpage Print View
	 *
	 */
	public function backpageDownloadSpanish(Request $request){
		$user = Auth::user();
		$type = $request->input('type');
		$cdn_url="http://d1ja3ktoroz7zi.cloudfront.net";
		$lcdn_url= $this->site_baseurl.'/img';
		$bp_dealer = $request->session()->get('bp_dealer');
		$bp_address = $request->session()->get('bp_address');
		$bp_email = $request->session()->get('bp_email');
		$bp_phone = $request->session()->get('bp_phone');
		$bp_complaints = $request->session()->get('bp_complaints');

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		$js='<script type="text/javascript">window.print();</script>';

		$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
					  	<img src="'.$cdn_url.'/buyers_guide_back_spanish.jpg" style="width: 100%;">
					</div>';
		$html =<<<EOF
<html lang="en">
	<head>
		<style type="text/css">
			@page { margin: 4.5em; }
			.page-break { display: block; page-break-before: always; }
			.page-break:last-child {
			page-break-before: avoid;
			}
			
			.dealer{
				position:absolute;
				top:640px;
				left:140px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
			.address{
				position:absolute;
				top:670px;
				left:130px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
			.complaints{
				position:absolute;
				top:745px;
				left:230px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
			}
		</style>
	</head>
	<body>
		$background_html
		<div class="address">$bp_address</div>
		<div class="dealer">$bp_dealer</div>
		<div class="complaints">$bp_complaints</div>
		$js
	</body>
</html>
EOF;
		$addendum="backpage.pdf";
		$dompdf = new DOMPDF();

		//background image access *important for external background image
		$dompdf->set_option('isRemoteEnabled', 'true');

		$dompdf->setPaper( array(0,0, 8.5 * 72, 11 * 72), "addendum" ); // 8.5" x 11"
		$dompdf->loadHtml($html);
		$dompdf->render();
		if($type == "print"){
			$dompdf->stream($addendum,array('Attachment'=>0));
		}else{
			$dompdf->stream($addendum);
		}
	}


	/**
	 *
	 * Get Data for Infosheet Print View
	 *
	 * @param vid, info_type
	 * @return object
	 *
	 */
	public function getDataForInfosheet(Request $request){
		$user = Auth::user();
		$vids = array();
		$vid_req = $request->input('vid');
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}
		$info_type = $request->input('info_type');

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		$cdn_url="http://d1ja3ktoroz7zi.cloudfront.net/";

		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$infosheet_data = InfoSheet::getInfosheetInfo($user->USER_ID, $info_type);
		$print_infos = array();
		if($infosheet_data){
			$infosheet_styles = array(
				'KBB' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/theme_2.jpg)',
						'background-size' => '765px 990px'
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '80px',
						'line-height' => '20px',
						'margin-left' => '340px',
						'width' => '400px',
						'height' => '',
						'float' => '',
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '50px',
						'line-height' => '26px',
						'margin-left' => '90px',
						'text-align' => 'left',
						'width' => ''
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '90px',
						'margin-top' => '10px',
						'width' => '270px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '270px'
					),
					'KBBDescription' => array(
						'padding-top' => '10px',
						'margin-left' => '90px',
						'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
						'height' => 'auto'
					),
					'KBBOptions' => array(
						'padding-top' => '',
						'margin-left' => '130px',
						'width' => '500px',
						'height' => '325px'
					),
					'KBBOptions_div' => array(
						'width' => '250px',
						'height' => '15px',
						'font-size' => '13px',
						'padding-top' => '',
						'padding-right' => ''
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '20px',
						'border-top' => '',
						'position' => '',
						'top' => ''
					)
				),
				'KBB NP' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/theme_2.jpg)',
						'background-size' => '765px 990px'
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '80px',
						'line-height' => '20px',
						'margin-left' => '340px',
						'width' => '400px',
						'height' => '',
						'float' => '',
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '50px',
						'line-height' => '26px',
						'margin-left' => '90px',
						'text-align' => 'left',
						'width' => ''
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '90px',
						'margin-top' => '10px',
						'width' => '270px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '270px'
					),
					'KBBDescription' => array(
						'padding-top' => '10px',
						'margin-left' => '90px',
						'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
						'height' => 'auto'
					),
					'KBBOptions' => array(
						'padding-top' => '',
						'margin-left' => '130px',
						'width' => '500px',
						'height' => '325px'
					),
					'KBBOptions_div' => array(
						'width' => '250px',
						'height' => '15px',
						'font-size' => '13px',
						'padding-top' => '',
						'padding-right' => ''
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '20px',
						'border-top' => '',
						'position' => '',
						'top' => ''
					)
				),
				'Dealer Custom 1' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC1_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBYear' => array(
						'font-size' => '32px',
						'padding-top' => '170px',
						'line-height' => '26px',
						'margin-left' => '',
						'width' => '',
						'text-align' => 'center'
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '60px',
						'margin-top' => '10px',
						'width' => '270px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '300px'
					),
					'KBBDescription' => array(
						'padding-top' => '10px',
						'margin-left' => '60px',
						'width' => '600px',
						'height' => '150px'
					),
					'KBBOptions' => array(
						'padding-top' => '',
						'margin-left' => '60px',
						'width' => '620px',
						'height' => '330px'
					),
					'KBBOptions_div' => array(
						'width' => '310px',
						'height' => '15px',
						'font-size' => '13px',
						'padding-top' => '',
						'padding-right' => ''
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '100px',
						'border-top' => '',
						'position' => '',
						'top' => '860px'
					)
				),
				'Dealer Custom 1 Certified' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC1_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBYear' => array(
						'font-size' => '32px',
						'padding-top' => '170px',
						'line-height' => '26px',
						'margin-left' => '',
						'width' => '',
						'text-align' => 'center'
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '60px',
						'margin-top' => '10px',
						'width' => '300px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '270px'
					),
					'KBBDescription' => array(
						'padding-top' => '10px',
						'margin-left' => '60px',
						'width' => '600px',
						'height' => '130px'
					),
					'KBBOptions' => array(
						'padding-top' => '',
						'margin-left' => '60px',
						'width' => '620px',
						'height' => '393px'
					),
					'KBBOptions_div' => array(
						'width' => '310px',
						'height' => '15px',
						'font-size' => '13px',
						'padding-top' => '',
						'padding-right' => ''
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '60px',
						'margin-left' => '90px',
						'padding-top' => '40px',
						'border-top' => '',
						'position' => '',
						'top' => '625px'
					)
				),
				'Dealer Custom 2 Certified' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '0 0 0 235px',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC2C_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBAddress' => array(
						'font-size' => '16px',
						'padding-left' => '100px',
						'padding-top' => '165px',
						'line-height' => '16px',
						'margin-left' => '',
						'width' => '500px',
						'height' => '275px'
					),
					'KBBYear' => array(
						'font-size' => '32px',
						'padding-top' => '170px',
						'line-height' => '26px',
						'margin-left' => '',
						'width' => ''
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '60px',
						'margin-top' => '202px',
						'width' => '300px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '270px'
					),
					'KBBDescription' => array(
						'padding-top' => '25px',
						'margin-left' => '65px',
						'width' => '440px',
						'height' => '146px'
					),
					'KBBOptions' => array(
						'padding-top' => '10px',
						'margin-left' => '65px',
						'width' => '440px',
						'height' => '220px'
					),
					'KBBOptions_div' => array(
						'width' => '220px',
						'height' => '15px',
						'font-size' => '13px',
						'padding-top' => '',
						'padding-right' => ''
					),
					'KBBPrice' => array(
						'width' => '550px',
						'font-size' => '60px',
						'margin-left' => '',
						'padding-top' => '40px',
						'border-top' => '',
						'position' => '',
						'top' => '860px'
					)
				),
				'Dealer Custom 2' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC2_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBYear' => array(
						'font-size' => '32px',
						'padding-top' => '230px',
						'line-height' => '26px',
						'margin-left' => '',
						'width' => '',
						'text-align' => 'center'
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '60px',
						'margin-top' => '10px',
						'width' => '300px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '270px'
					),
					'KBBDescription' => array(
						'padding-top' => '40px',
						'margin-left' => '60px',
						'width' => '600px',
						'height' => '150px'
					),
					'KBBOptions' => array(
						'padding-top' => '20px',
						'margin-left' => '60px',
						'width' => '620px',
						'height' => '278px'
					),
					'KBBOptions_div' => array(
						'width' => '310px',
						'height' => '15px',
						'font-size' => '13px',
						'padding-top' => '',
						'padding-right' => ''
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '36px',
						'border-top' => '',
						'position' => '',
						'top' => '790px'
					)
				),
				'Dealer Custom 3' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC3_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBYear' => array(
						'font-size' => '30px',
						'padding-top' => '230px',
						'line-height' => '26px',
						'margin-left' => '280px',
						'width' => '450px',
						'text-align' => 'center'
					),
					'KBBCol1' => array(
						'font-size' => '14px',
						'margin-left' => '285px',
						'margin-top' => '10px',
						'width' => '220px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '14px',
						'width' => '220px'
					),
					'KBBOptions' => array(
						'padding-top' => '25px',
						'margin-left' => '285px',
						'width' => '440px',
						'height' => '220px',
						'line-height' => '12px'
					),
					'KBBOptions_div' => array(
						'width' => '210px',
						'height' => '15px',
						'font-size' => '13px',
						'padding-top' => '5px',
						'padding-right' => '5px'
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '30px',
						'border-top' => '',
						'position' => 'absolute',
						'top' => '790px'
					)
				),
				'Dealer Custom 4' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC4_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBYear' => array(
						'font-size' => '30px',
						'padding-top' => '160px',
						'line-height' => '26px',
						'margin-left' => '200px',
						'text-align' => 'center',
						'width' => '500px'
					),
					'KBBCol1' => array(
						'font-size' => '14px',
						'margin-left' => '285px',
						'margin-top' => '17px',
						'width' => '220px',
						'height' => '90px'
					),
					'KBBCol2' => array(
						'font-size' => '14px',
						'width' => '220px'
					),
					'KBBDescription' => array(
						'padding-top' => '45px',
						'margin-left' => '68px',
						'width' => '680px',
						'height' => '163px'
					),
					'KBBOptions' => array(
						'padding-top' => '35px',
						'margin-left' => '70px',
						'width' => '640px',
						'height' => '310px'
					),
					'KBBOptions_div' => array(
						'width' => '280px',
						'height' => '21px',
						'font-size' => '13px',
						'padding-top' => '5px',
						'padding-right' => '5px'
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '60px',
						'margin-left' => '140px',
						'padding-top' => '59px',
						'border-top' => '',
						'position' => '',
						'top' => '830px'
					)
				),
				'Dealer Custom 4 NP' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC4_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBYear' => array(
						'font-size' => '30px',
						'padding-top' => '160px',
						'line-height' => '26px',
						'margin-left' => '200px',
						'text-align' => 'center',
						'width' => '500px'
					),
					'KBBCol1' => array(
						'font-size' => '14px',
						'margin-left' => '285px',
						'margin-top' => '17px',
						'width' => '220px',
						'height' => '90px'
					),
					'KBBCol2' => array(
						'font-size' => '14px',
						'width' => '220px'
					),
					'KBBDescription' => array(
						'padding-top' => '45px',
						'margin-left' => '68px',
						'width' => '680px',
						'height' => '163px'
					),
					'KBBOptions' => array(
						'padding-top' => '35px',
						'margin-left' => '70px',
						'width' => '640px',
						'height' => '310px'
					),
					'KBBOptions_div' => array(
						'width' => '280px',
						'height' => '21px',
						'font-size' => '13px',
						'padding-top' => '5px',
						'padding-right' => '5px'
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '60px',
						'margin-left' => '140px',
						'padding-top' => '59px',
						'border-top' => '',
						'position' => '',
						'top' => '830px'
					)
				),
				'Pure Cars 1' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/purecars2.jpg)',
						'background-size' => '765px 990px'
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '80px',
						'line-height' => '20px',
						'margin-left' => '340px',
						'width' => '400px',
						'height' => ''
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '40px',
						'line-height' => '26px',
						'margin-left' => '90px',
						'width' => ''
					),
					'KBBCol1' => array(
						'font-size' => '14px',
						'margin-left' => '90px',
						'margin-top' => '202px',
						'width' => '200px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '14px',
						'width' => '230px'
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '30px',
						'border-top' => '',
						'position' => 'absolute',
						'top' => '880px'
					)
				),
				'Pure Cars 4' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_PURE4_BG.')',
						'background-size' => '765px 990px'
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '80px',
						'line-height' => '20px',
						'margin-left' => '340px',
						'width' => '400px',
						'height' => ''
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '40px',
						'line-height' => '26px',
						'margin-left' => '90px',
						'width' => ''
					),
					'KBBCol1' => array(
						'font-size' => '14px',
						'margin-left' => '90px',
						'margin-top' => '202px',
						'width' => '200px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '14px',
						'width' => '230px'
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '30px',
						'border-top' => '',
						'position' => 'absolute',
						'top' => '880px'
					)
				),
				'Pure Cars 2' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/purecars2.jpg)',
						'background-size' => '765px 990px'
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '80px',
						'line-height' => '20px',
						'margin-left' => '340px',
						'width' => '400px',
						'height' => ''
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '40px',
						'line-height' => '26px',
						'margin-left' => '90px',
						'width' => ''
					),
					'KBBCol1' => array(
						'font-size' => '14px',
						'margin-left' => '90px',
						'margin-top' => '202px',
						'width' => '200px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '14px',
						'width' => '230px'
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '30px',
						'border-top' => '',
						'position' => 'absolute',
						'top' => '880px'
					)
				),
				'Pure Cars 3' => array(
					'KBBPrintit' => array(
						'height' => '990px',
						'width' => '765px',
						'padding' => '',
						'float' => '',
						'position' => '',
						'background-color' => 'white',
						'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/purecars3.jpg)',
						'background-size' => '765px 990px'
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '80px',
						'line-height' => '20px',
						'margin-left' => '340px',
						'width' => '400px',
						'height' => ''
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '40px',
						'line-height' => '26px',
						'margin-left' => '90px',
						'width' => ''
					),
					'KBBCol1' => array(
						'font-size' => '14px',
						'margin-left' => '90px',
						'margin-top' => '202px',
						'width' => '200px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '14px',
						'width' => '230px'
					),
					'KBBPrice' => array(
						'width' => '600px',
						'font-size' => '42px',
						'margin-left' => '90px',
						'padding-top' => '30px',
						'border-top' => '',
						'position' => 'absolute',
						'top' => '880px'
					)
				),
				'Dealer Custom' => array(
					'KBBPrintit' => array(
						'height' => '880px',
						'width' => '655px',
						'padding' => '55px',
						'float' => 'left',
						'position' => '',
						'background-color' => $infosheet_data->INFO_BACKGROUND,
						'background-image' => '',
						'background-size' => ''
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '20px',
						'line-height' => '20px',
						'margin-left' => '40px',
						'width' => '300px',
						'height' => '145px',
						'float' => 'left'
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '10px',
						'line-height' => '26px',
						'margin-left' => '24px',
						'width' => '600px'
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '24px',
						'margin-top' => '10px',
						'width' => '270px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '270px'
					),
					'KBBDescription' => array(
						'padding-top' => '10px',
						'margin-left' => '24px',
						'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
						'height' => '150px'
					),
					'KBBOptions' => array(
						'padding-top' => '10px',
						'margin-left' => '24px',
						'width' => '600px',
						'height' => '352px'
					),
					'KBBOptions_div' => array(
						'width' => '300px',
						'height' => '15px',
						'font-size' => '12px',
						'padding-top' => '',
						'padding-right' => '',
					),
					'KBBPrice' => array(
						'width' => '626px',
						'font-size' => '42px',
						'margin-left' => '',
						'padding-top' => '15px',
						'border-top' => '7px '.$infosheet_data->INFO_BORDER.' solid',
						'position' => '',
						'top' => '890px'
					)
				),
				'Dealer Custom NP' => array(
					'KBBPrintit' => array(
						'height' => '880px',
						'width' => '655px',
						'padding' => '55px',
						'float' => 'left',
						'position' => '',
						'background-color' => $infosheet_data->INFO_BACKGROUND,
						'background-image' => '',
						'background-size' => ''
					),
					'KBBAddress' => array(
						'font-size' => '20px',
						'padding-left' => '',
						'padding-top' => '20px',
						'line-height' => '20px',
						'margin-left' => '40px',
						'width' => '300px',
						'height' => '145px',
						'float' => 'left'
					),
					'KBBYear' => array(
						'font-size' => '26px',
						'padding-top' => '10px',
						'line-height' => '26px',
						'margin-left' => '24px',
						'width' => '600px'
					),
					'KBBCol1' => array(
						'font-size' => '16px',
						'margin-left' => '24px',
						'margin-top' => '10px',
						'width' => '270px',
						'height' => ''
					),
					'KBBCol2' => array(
						'font-size' => '16px',
						'width' => '270px'
					),
					'KBBDescription' => array(
						'padding-top' => '10px',
						'margin-left' => '24px',
						'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
						'height' => '150px'
					),
					'KBBOptions' => array(
						'padding-top' => '10px',
						'margin-left' => '24px',
						'width' => '600px',
						'height' => '352px'
					),
					'KBBOptions_div' => array(
						'width' => '300px',
						'height' => '15px',
						'font-size' => '12px',
						'padding-top' => '',
						'padding-right' => '',
					),
					'KBBPrice' => array(
						'width' => '626px',
						'font-size' => '42px',
						'margin-left' => '',
						'padding-top' => '15px',
						'border-top' => '7px '.$infosheet_data->INFO_BORDER.' solid',
						'position' => '',
						'top' => '890px'
					)
				)
			);
			foreach($vids as $pg_index => $vid){
				$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
				$pure_bigimg = '';
				$pure_smallimg = '';
				if($infosheet_data->INFO_STYLE == 'Pure Cars 1' || $infosheet_data->INFO_STYLE == 'Pure Cars 2' || $infosheet_data->INFO_STYLE == 'Pure Cars 3' || $infosheet_data->INFO_STYLE == 'Pure Cars 4'){
					$purecars_url = str_ireplace("[VIN]", $vehicle_info->VIN_NUMBER, $infosheet_data->PURECARS_URL);
					$html = HtmlDomParser::file_get_html( $purecars_url );
					foreach($html->find('div.size1of3') as $k){
						foreach($k->find('div.borderGray') as $e){
						   foreach($e->find('img.iconCircleLarge') as $f){
						    	$pure_bigimg.='<div class="pc2optionbig"><img class="pc2bigimage" src="'.$f->srcraster.'" />';
							}
							foreach($e->find('p.fontBold') as $g){
							    $pure_bigimg.='<br><span class="pc2bigtext">'.$g->innertext . '</span></div>';
							}
						}
					}
					$i=0;
					$j=0;
					foreach($html->find('.valueAddedHighlights') as $p){
						foreach($p->find('div.size1of2') as $o){
							foreach($o->find('div.size1of2') as $e){
								$i++;
								if($i>=13){
									break;
								}
							   	foreach($e->find('img.paddingVerticalMedium') as $f){
							    	$pure_smallimg.='<div class="pc2optionsmall"><img class="pc2smallimage" src="'.$f->srcraster.'" />';
								}
								foreach($e->find('p.textMediumSmall') as $g){
								    $pure_smallimg.='<br><span class="pc2smalltext">'.$g->innertext . '</span></div>';
								}
							}

						}
					}
				}
				$array_data = array(
					'vehicle_info' => $vehicle_info, 
					'pure_bigimg' => $pure_bigimg, 
					'pure_smallimg' => $pure_smallimg, 
				);
				array_push($print_infos, $array_data);
			}
			return json_encode(array('infosheet_data' => $infosheet_data, 'dealer_info' => $dealer_info, 'infosheet_styles' => $infosheet_styles, 'print_infos' => $print_infos));
		}		
	}

	/**
	 *
	 * Set print infosheet status
	 *
	 * @param vid
	 * @return status
	 *
	 */
	public function infoPrintStatus(Request $request){
		$vids = array();
		$vid_req = $request->input('vid');
		$vid_req = json_decode($vid_req);
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}

		$t_date=date('Y-m-d');
		$starttime = date('Y-m-d H:i:s');
		foreach($vids as $vid){
			$result = DealerInventory::updateInventoryData($vid, array('PRINT_INFO' => '1', 'PRINT_DATE' => $t_date, 'UPDATE_DATE' => $t_date, 'PT' => $starttime));	
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Generate infosheet print view pdf and download
	 *
	 * @param vid, info_type, type
	 *
	 */
	public function infoPrintDownload(Request $request){
		$type = $request->input('type');
		$vids = array();
		$vid_req = $request->input('vid');
		$vid_req = json_decode($vid_req);
		if(is_array($vid_req)){
			$vids = $vid_req;
		}else{
			$vids[0] = $vid_req;
		}
		$info_type = $request->input('info_type');
		$user = Auth::user();

		$cdn_url = "http://d1ja3ktoroz7zi.cloudfront.net";

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$infosheet_data = InfoSheet::getInfosheetInfo($user->USER_ID, $info_type);

		$js = "";
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') === false && strpos($_SERVER['HTTP_USER_AGENT'], 'CriOS') === false) {
		    if ($type == 'print') {
		        $js = '<script type="text/javascript"> try{ this.print();
		  }</script>';
		    }
		}
		$infosheet_styles = array(
			'KBB' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '0',
					'float' => '',
					'position' => '',
					'background-color' => '',
					'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/theme_2.jpg)',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '120px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => '',
					'float' => 'none'
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '50px',
					'line-height' => '26px',
					'margin-left' => '90px',
					'text-align' => 'left',
					'width' => 'auto'
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '90px',
					'margin-top' => '10px',
					'width' => '270px',
					'height' => 'auto'
				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '10px',
					'margin-left' => '90px',
					'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
					'height' => '145px'
				),
				'KBBOptions' => array(
					'padding-top' => '0',
					'margin-left' => '130px',
					'width' => '500px',
					'height' => '330px'
				),
				'KBBOptions_div' => array(
					'width' => '250px',
					'height' => '15px',
					'font-size' => '13px',
					'padding-top' => '',
					'padding-right' => ''
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '20px',
					'border-top' => '',
					'position' => '',
					'top' => ''
				)
			),
			'KBB NP' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '0',
					'float' => '',
					'position' => '',
					'background-color' => '',
					'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/theme_2.jpg)',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '120px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => '',
					'float' => 'none'
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '50px',
					'line-height' => '26px',
					'margin-left' => '90px',
					'text-align' => 'left',
					'width' => 'auto'
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '90px',
					'margin-top' => '10px',
					'width' => '270px',
					'height' => 'auto'
				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '10px',
					'margin-left' => '90px',
					'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
					'height' => '145px'
				),
				'KBBOptions' => array(
					'padding-top' => '0',
					'margin-left' => '130px',
					'width' => '500px',
					'height' => '330px'
				),
				'KBBOptions_div' => array(
					'width' => '250px',
					'height' => '15px',
					'font-size' => '13px',
					'padding-top' => '',
					'padding-right' => ''
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '20px',
					'border-top' => '',
					'position' => '',
					'top' => ''
				)
			),
			'Dealer Custom 1' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '0',
					'float' => '',
					'position' => '',
					'background-color' => '',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC1_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '120px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => '',
					'float' => 'none'
				),
				'KBBYear' => array(
					'font-size' => '32px',
					'padding-top' => '195px',
					'line-height' => '26px',
					'margin-left' => '0',
					'width' => '100%',
					'text-align' => 'center'
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '60px',
					'margin-top' => '10px',
					'width' => '300px',
					'height' => '65px',
				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '10px',
					'margin-left' => '60px',
					'width' => '600px',
					'height' => '150px'
				),
				'KBBOptions' => array(
					'padding-top' => '0',
					'margin-left' => '60px',
					'width' => '620px',
					'height' => '330px'
				),
				'KBBOptions_div' => array(
					'width' => '310px',
					'height' => '15px',
					'font-size' => '13px',
					'padding-top' => '',
					'padding-right' => ''
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '430px',
					'border-top' => '',
					'position' => '',
					'top' => '860px'
				)
			),
			'Dealer Custom 1 Certified' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => 'white',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC1_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '120px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => '',
					'float' => ''
				),
				'KBBYear' => array(
					'font-size' => '32px',
					'padding-top' => '170px',
					'line-height' => '26px',
					'margin-left' => '',
					'width' => '',
					'text-align' => 'center'
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '60px',
					'margin-top' => '10px',
					'width' => '300px',
					'height' => ''
				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '10px',
					'margin-left' => '60px',
					'width' => '600px',
					'height' => '130px'
				),
				'KBBOptions' => array(
					'padding-top' => '',
					'margin-left' => '60px',
					'width' => '620px',
					'height' => '393px'
				),
				'KBBOptions_div' => array(
					'width' => '310px',
					'height' => '15px',
					'font-size' => '13px',
					'padding-top' => '',
					'padding-right' => ''
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '60px',
					'margin-left' => '90px',
					'padding-top' => '40px',
					'border-top' => '',
					'position' => '',
					'top' => '625px'
				)
			),
			'Dealer Custom 2 Certified' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '0 0 0 250px',
					'float' => '',
					'position' => '',
					'background-color' => '',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC2C_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '16px',
					'padding-left' => '100px',
					'padding-top' => '200px',
					'line-height' => '16px',
					'margin-left' => '65px',
					'width' => '500px',
					'height' => 'auto',
					'float' => ''
				),
				'KBBYear' => array(
					'font-size' => '32px',
					'padding-top' => '170px',
					'line-height' => '26px',
					'margin-left' => '65px',
					'width' => '',
					'text-align' => 'center',
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '60px',
					'margin-top' => '202px',
					'width' => '300px',
					'height' => '',

				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '45px',
					'margin-left' => '65px',
					'width' => '440px',
					'height' => '160px'
				),
				'KBBOptions' => array(
					'padding-top' => '10px',
					'margin-left' => '65px',
					'width' => '440px',
					'height' => '200px'
				),
				'KBBOptions_div' => array(
					'width' => '220px',
					'height' => '15px',
					'font-size' => '13px',
					'padding-top' => '',
					'padding-right' => ''
				),
				'KBBPrice' => array(
					'width' => '550px',
					'font-size' => '48px',
					'margin-left' => '30px',
					'padding-top' => '59px',
					'border-top' => '',
					'position' => '',
					'top' => ''
				)
			),
			'Dealer Custom 2' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => 'white',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC2_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '120px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => '',
					'float' => ''
				),
				'KBBYear' => array(
					'font-size' => '32px',
					'padding-top' => '230px',
					'line-height' => '26px',
					'margin-left' => '',
					'width' => '',
					'text-align' => 'center'
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '60px',
					'margin-top' => '10px',
					'width' => '300px',
					'height' => ''
				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '40px',
					'margin-left' => '60px',
					'width' => '600px',
					'height' => '150px'
				),
				'KBBOptions' => array(
					'padding-top' => '20px',
					'margin-left' => '60px',
					'width' => '620px',
					'height' => '278px'
				),
				'KBBOptions_div' => array(
					'width' => '310px',
					'height' => '15px',
					'font-size' => '13px',
					'padding-top' => '',
					'padding-right' => ''
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '36px',
					'border-top' => '',
					'position' => '',
					'top' => '790px'
				)
			),
			'Dealer Custom 3' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => 'white',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC3_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '120px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => '',
					'float' => ''
				),
				'KBBYear' => array(
					'font-size' => '30px',
					'padding-top' => '230px',
					'line-height' => '26px',
					'margin-left' => '280px',
					'width' => '450px',
					'text-align' => 'center'
				),
				'KBBCol1' => array(
					'font-size' => '14px',
					'margin-left' => '285px',
					'margin-top' => '10px',
					'width' => '220px',
					'height' => ''
				),
				'KBBCol2' => array(
					'font-size' => '14px',
					'width' => '220px'
				),
				'KBBOptions' => array(
					'padding-top' => '25px',
					'margin-left' => '285px',
					'width' => '440px',
					'height' => '220px',
					'line-height' => '12px'
				),
				'KBBOptions_div' => array(
					'width' => '210px',
					'height' => '15px',
					'font-size' => '13px',
					'padding-top' => '5px',
					'padding-right' => '5px'
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '30px',
					'border-top' => '',
					'position' => 'absolute',
					'top' => '790px'
				)
			),
			'Dealer Custom 4' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '0',
					'float' => '',
					'position' => '',
					'background-color' => '',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC4_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '20px',
					'line-height' => '20px',
					'margin-left' => '40px',
					'width' => '300px',
					'height' => '145px',
					'float' => 'left'
				),
				'KBBYear' => array(
					'font-size' => '30px',
					'padding-top' => '180px',
					'line-height' => '26px',
					'margin-left' => '200px',
					'text-align' => 'center',
					'width' => '500px'
				),
				'KBBCol1' => array(
					'font-size' => '14px',
					'margin-left' => '300px',
					'margin-top' => '25px',
					'width' => '220px',
					'height' => '90px'
				),
				'KBBCol2' => array(
					'font-size' => '14px',
					'width' => '220px'
				),
				'KBBDescription' => array(
					'padding-top' => '50px',
					'margin-left' => '68px',
					'width' => '680px',
					'height' => '163px'
				),
				'KBBOptions' => array(
					'padding-top' => '0',
					'margin-left' => '70px',
					'width' => '640px',
					'height' => '296px'
				),
				'KBBOptions_div' => array(
					'width' => '280px',
					'height' => '21px',
					'font-size' => '13px',
					'padding-top' => '5px',
					'padding-right' => '5px'
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '60px',
					'margin-left' => '140px',
					'padding-top' => '120px',
					'border-top' => '',
					'position' => '',
					'top' => '830px'
				)
			),
			'Dealer Custom 4 NP' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => '',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_DC4_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '20px',
					'line-height' => '20px',
					'margin-left' => '40px',
					'width' => '300px',
					'height' => '145px',
					'float' => 'left'
				),
				'KBBYear' => array(
					'font-size' => '30px',
					'padding-top' => '160px',
					'line-height' => '26px',
					'margin-left' => '200px',
					'text-align' => 'center',
					'width' => '500px'
				),
				'KBBCol1' => array(
					'font-size' => '14px',
					'margin-left' => '300px',
					'margin-top' => '30px',
					'width' => '220px',
					'height' => '90px'
				),
				'KBBCol2' => array(
					'font-size' => '14px',
					'width' => '220px'
				),
				'KBBDescription' => array(
					'padding-top' => '45px',
					'margin-left' => '68px',
					'width' => '680px',
					'height' => '163px'
				),
				'KBBOptions' => array(
					'padding-top' => '35px',
					'margin-left' => '70px',
					'width' => '640px',
					'height' => '310px'
				),
				'KBBOptions_div' => array(
					'width' => '280px',
					'height' => '21px',
					'font-size' => '13px',
					'padding-top' => '5px',
					'padding-right' => '5px'
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '60px',
					'margin-left' => '140px',
					'padding-top' => '59px',
					'border-top' => '',
					'position' => '',
					'top' => '830px'
				)
			),
			'Pure Cars 1' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => 'white',
					'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/purecars2.jpg)',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '80px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => ''
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '40px',
					'line-height' => '26px',
					'margin-left' => '90px',
					'width' => ''
				),
				'KBBCol1' => array(
					'font-size' => '14px',
					'margin-left' => '90px',
					'margin-top' => '202px',
					'width' => '200px',
					'height' => ''
				),
				'KBBCol2' => array(
					'font-size' => '14px',
					'width' => '230px'
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '30px',
					'border-top' => '',
					'position' => 'absolute',
					'top' => '880px'
				)
			),
			'Pure Cars 4' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => 'white',
					'background-image' => 'url('.$cdn_url.$infosheet_data->INFO_PURE4_BG.')',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '80px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => ''
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '40px',
					'line-height' => '26px',
					'margin-left' => '90px',
					'width' => ''
				),
				'KBBCol1' => array(
					'font-size' => '14px',
					'margin-left' => '90px',
					'margin-top' => '202px',
					'width' => '200px',
					'height' => ''
				),
				'KBBCol2' => array(
					'font-size' => '14px',
					'width' => '230px'
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '30px',
					'border-top' => '',
					'position' => 'absolute',
					'top' => '880px'
				)
			),
			'Pure Cars 2' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => 'white',
					'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/purecars2.jpg)',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '80px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => ''
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '40px',
					'line-height' => '26px',
					'margin-left' => '90px',
					'width' => ''
				),
				'KBBCol1' => array(
					'font-size' => '14px',
					'margin-left' => '90px',
					'margin-top' => '202px',
					'width' => '200px',
					'height' => ''
				),
				'KBBCol2' => array(
					'font-size' => '14px',
					'width' => '230px'
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '30px',
					'border-top' => '',
					'position' => 'absolute',
					'top' => '880px'
				)
			),
			'Pure Cars 3' => array(
				'KBBPrintit' => array(
					'height' => '990px',
					'width' => '765px',
					'padding' => '',
					'float' => '',
					'position' => '',
					'background-color' => 'white',
					'background-image' => 'url(http://d1ja3ktoroz7zi.cloudfront.net/purecars3.jpg)',
					'background-size' => '765px 990px'
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '80px',
					'line-height' => '20px',
					'margin-left' => '340px',
					'width' => '400px',
					'height' => ''
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '40px',
					'line-height' => '26px',
					'margin-left' => '90px',
					'width' => ''
				),
				'KBBCol1' => array(
					'font-size' => '14px',
					'margin-left' => '90px',
					'margin-top' => '202px',
					'width' => '200px',
					'height' => ''
				),
				'KBBCol2' => array(
					'font-size' => '14px',
					'width' => '230px'
				),
				'KBBPrice' => array(
					'width' => '600px',
					'font-size' => '42px',
					'margin-left' => '90px',
					'padding-top' => '30px',
					'border-top' => '',
					'position' => 'absolute',
					'top' => '880px'
				)
			),
			'Dealer Custom' => array(
				'KBBPrintit' => array(
					'height' => '880px',
					'width' => '655px',
					'padding' => '55px',
					'float' => 'left',
					'position' => '',
					'background-color' => $infosheet_data->INFO_BACKGROUND,
					'background-image' => '',
					'background-size' => ''
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '20px',
					'line-height' => '20px',
					'margin-left' => '10px',
					'width' => '350px',
					'height' => '145px',
					'float' => 'left'
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '10px',
					'line-height' => '26px',
					'margin-left' => '24px',
					'width' => '600px',
					'text-align' => 'left',
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '24px',
					'margin-top' => '10px',
					'width' => '270px',
					'height' => 'auto'
				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '10px',
					'margin-left' => '24px',
					'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
					'height' => '135px'
				),
				'KBBOptions' => array(
					'padding-top' => '10px',
					'margin-left' => '24px',
					'width' => '600px',
					'height' => '300px'
				),
				'KBBOptions_div' => array(
					'width' => '300px',
					'height' => '15px',
					'font-size' => '12px',
					'padding-top' => '',
					'padding-right' => '',
				),
				'KBBPrice' => array(
					'width' => '630px',
					'font-size' => '42px',
					'margin-left' => '0',
					'padding-top' => '10px',
					'border-top' => '7px '.$infosheet_data->INFO_BORDER.' solid',
					'position' => '',
					'top' => '890px'
				)
			),
			'Dealer Custom NP' => array(
				'KBBPrintit' => array(
					'height' => '880px',
					'width' => '655px',
					'padding' => '55px',
					'float' => 'left',
					'position' => '',
					'background-color' => $infosheet_data->INFO_BACKGROUND,
					'background-image' => '',
					'background-size' => ''
				),
				'KBBAddress' => array(
					'font-size' => '20px',
					'padding-left' => '',
					'padding-top' => '20px',
					'line-height' => '20px',
					'margin-left' => '10px',
					'width' => '350px',
					'height' => '145px',
					'float' => 'left'
				),
				'KBBYear' => array(
					'font-size' => '26px',
					'padding-top' => '10px',
					'line-height' => '26px',
					'margin-left' => '24px',
					'width' => '600px',
					'text-align' => 'left',
				),
				'KBBCol1' => array(
					'font-size' => '16px',
					'margin-left' => '24px',
					'margin-top' => '10px',
					'width' => '270px',
					'height' => 'auto'
				),
				'KBBCol2' => array(
					'font-size' => '16px',
					'width' => '270px'
				),
				'KBBDescription' => array(
					'padding-top' => '10px',
					'margin-left' => '24px',
					'width' => empty($infosheet_data->QR_INFO_ADDRESS)?'600px':'423px',
					'height' => '185px'
				),
				'KBBOptions' => array(
					'padding-top' => '10px',
					'margin-left' => '24px',
					'width' => '600px',
					'height' => '255px'
				),
				'KBBOptions_div' => array(
					'width' => '300px',
					'height' => '15px',
					'font-size' => '12px',
					'padding-top' => '',
					'padding-right' => '',
				),
				'KBBPrice' => array(
					'width' => '630px',
					'font-size' => '42px',
					'margin-left' => '0',
					'padding-top' => '10px',
					'border-top' => '7px '.$infosheet_data->INFO_BORDER.' solid',
					'position' => '',
					'top' => '890px'
				)
			)
		);
		$html = '';
		if($infosheet_data->INFO_STYLE == 'Info Book'){
			$html .= '<!DOCTYPE html>
					    <html lang="en">
					    	<head>
						     	<style>
							 	@page { margin: 500px 180px; font-family:Arial, Helvetica, sans-serif; font-size:14px;line-height:14px;}
							          .page-break { display: block; page-break-before: always; }
							          .page-break:last-child {
							            page-break-before: avoid;
										
							          }
							    #header { position: fixed; left: 0px; top: -380px; right: 0px; height: 300px;  text-align: center; font-size:48px; line-height:48px; }
							    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; }
								.price{
									font-size:26px;
									font-weight:bold;
									width:100%;
									clear:both;
									line-height:26px;
								}
								.year{
									font-size:26px;
									line-height:26px;
									font-weight:bold;
									width:100%;
								}
								.clear{
									clear:both;
									width:100%;
									height:auto;
								}
								.options{
									width:780px;
									padding-top:10px;
									height:500px;
									clear:both;
									margin-left:40px;
								}
								.spank{
									font-weight:bold;
									font-size:16px;
								}
								li{
									font-size:14px;
									lnk-height:15px;
									word-break: break-word;
									width:200px;
								}
						  	</style>
						</head>
						<body>';
		}elseif($infosheet_data->INFO_STYLE == 'Narrow Standard' || $infosheet_data->INFO_STYLE == 'Narrow Standard NP'){
			$html .= '<html lang="en">
				      	<head>
					        <style type="text/css">
					          @page { margin:3em 1.85em 0em 1.85em }
					          .page-break { display: block; page-break-before: always; }
					          .page-break:last-child {
					            page-break-before: avoid;
				          	}
					          #printit{
					            height: 800px;
					            width: 100%;
								font-family:Arial, Helvetica, sans-serif;
					          }
					          #second-section{
					      		overflow: hidden;
					            width: 90%;
					            margin: 170px 0px 0px 20px;
					            height:460px;
					          }
					          .bold1{
					            font-family:Arial, Helvetica, sans-serif;
					            font-size:14px;
					            font-weight:bold;
					            line-height:14px;
					          }
					          .bold2{
					            font-family:Arial, Helvetica, sans-serif;
					            font-size:14px;
					            font-weight:bold;
					            line-height:14px;
					          }
					          .total{
					            margin-left:210px;
					            margin-top:20px;
					            font-size:22px;
					            font-weight:bold;
					            font-family:Arial, Helvetica, sans-serif;
					          }
					          .address{
					            padding-top:93.75px;
					            margin-left:62.5px;
					            font-size:40.625px;
					            line-height:40.625px;
					            font-weight:bold;
					            font-family:Arial, Helvetica, sans-serif;
					          }
					        </style>
					      </head>
					      <body>';
		}else{
			$html .= '<html lang="en">
	    					<head>
	      						<style type="text/css">
	       							@page {
	        							margin:0px;
	      							}
			  						body{
			  						}
		  							.page-break { display: block; page-break-before: always; }
	          						.page-break:last-child {
	          							page-break-before: avoid;
						          	}
								    #printit {
						          		background-color: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrintit']['background-color'].';
								      	height:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrintit']['height'].';
								      	padding: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrintit']['padding'].';
								      	width: 100%;
									  	font-family:Arial, Helvetica, sans-serif;
								    }
									.address{
										text-align: center;
										font-size:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBAddress']['font-size'].';
										margin-left:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBAddress']['margin-left'].';
										padding-top:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBAddress']['padding-top'].';
										line-height:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBAddress']['line-height'].';
										width:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBAddress']['width'].';
										float: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBAddress']['float'].';
										font-weight:bold;
									}
									.name{
										font-weight:bold;
										font-size:22px;
									}
									.year{
										font-size: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBYear']['font-size'].';
								        padding-top: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBYear']['padding-top'].';
								        line-height: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBYear']['line-height'].';
								        font-weight: bold;
								        text-align: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBYear']['text-align'].';
								        margin-left: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBYear']['margin-left'].';
								        width: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBYear']['width'].';
									}
									.col1{
										font-size: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBCol1']['font-size'].';
										font-weight: bold;
										line-height: 22px;
										margin-top: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBCol1']['margin-top'].';
										margin-left: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBCol1']['margin-left'].';
										float: left;
										width: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBCol1']['width'].';
										height: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBCol1']['height'].';
									}
									.col2{
										font-size:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBCol2']['font-size'].';
										font-weight:bold;
										line-height:22px;
										float:left;
										margin-top:0px;
										width:'.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBCol2']['width'].';
										height:100px;
									}
									
									.descdiv{
										width:100%;
										height:auto;
										clear:both;
									}
									.qr{
										float:left;
										width:150px;
										font-size:16px;
										font-weight:bold;
										text-align:center;
										padding-top:5px;
										padding-left:5px;
									}
									.options{
										padding-top: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBOptions']['padding-top'].';
										float: left;
						              	width: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBOptions']['width'].';
										margin-left: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBOptions']['margin-left'].';
										height: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBOptions']['height'].';
										overflow: hidden;
										margin-top: 10px;
									}
									.wed{
										left:35%;
										font-size:70px;
										font-weight:bold;
										width:50%;
										position:absolute;
										top:2350px;
									}
									li{
										font-weight:bold;
										font-size:14px;
										list-style-type:none;
									}
									.spank{
										font-weight:bold;
										font-size:18px;
									}
									.KBBprice{
										position: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrice']['position'].';
										text-align: center;
										font-size: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrice']['font-size'].';
										font-weight: bold;
										margin-left: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrice']['margin-left'].';
										padding-top: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrice']['padding-top'].';
										border-top: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrice']['border-top'].';
										width: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBPrice']['width'].';
									}
									.certified{
										font-size: 42px;
									    font-weight: bold;
									    left: 1840px;
									    padding-top: 120px;
									    position: absolute;
									    text-align: center;
									    top: 2658px;
									}
									.vbigimage{
										text-align:center;
										padding:20px;
										border:1px solid #CCC;
										width:600px;
									}
									.vsmallimage{
										text-align:center;
										width:300px;
									}
									.row1{
										width:2216px;
										margin-top:180px;
										margin-left:288px;
									}
									.optionbig{
										border:3px solid #CCC;
										width:540px;
										height:510px;
										margin-bottom:60px;
										text-align:center;
									}
									.optionsmall{
										width:300px;
										height:180px;
										text-align:center;
										overflow:hidden;
									}
									.bigimage{
										padding-top:30px;
										padding-bottom:30px;
										width:360px;
									}
									.bigtext{
										font-weight:bold;
									}
									.smallimage{
										width:120px;
										height:auto;
									}
									.smalltext{
										font-size:30px;
									}
									#main{
										border-radius:10px;
										border:7px '.$infosheet_data->INFO_BORDER.' solid;
										background:#FFF;
										width:630px;
										height:880px;
									}
									#section1{
										height:167px;
										width:630px;
										border-bottom:7px '.$infosheet_data->INFO_BORDER.' solid;
										clear:both;
									}
									#logo{
										height:135px;
										width:265px;
										padding-top:20px;
										padding-left:32px;
										text-align:center;
										float:left;
									}
									.clear{
										clear:both;
										width:100%;
										height:auto;
									}
						  		</style>
							</head>
							<body>';
		}
		foreach($vids as $pg_index => $vid){
			$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
			$odometer = "";
			$trans = "";
			$drivetrain = "";
			$engine = "";
			$hmpg = "";
			$cmpg = "";
		 	$mileagerow = "";
		 	$trimrow = "";
		 	if (!empty($vehicle_info->MILEAGE)) {
	            $odometer = $vehicle_info->MILEAGE;
	            $mileagerow = 'Mileage: ' . $vehicle_info->MILEAGE . '<br />';
	        }

	        if (!empty($vehicle_info->TRANSMISSION)) {
	            $trans = "Transmission: " . $vehicle_info->TRANSMISSION . "<br>";
	        } 
	        if (!empty($vehicle_info->EXT_COLOR)) {
	            $drivetrain = "Color: " . $vehicle_info->EXT_COLOR . "<br>";
	        }
	        if (!empty($vehicle_info->ENGINE)) {
	            $engine = "Engine: " . $vehicle_info->ENGINE;
	        }
	        if (!empty($vehicle_info->TRIM)) {
	            $trimrow = 'Trim: ' . $vehicle_info->TRIM . '';
	        }
	        if (!empty($vehicle_info->HMPG)) {
	            $hmpg = $vehicle_info->HMPG;
	        }
	        if (!empty($vehicle_info->CMPG)) {
	            $cmpg = $vehicle_info->CMPG;
	        }
	        if (!empty($vehicle_info->OPTIONS)) {
	            $myoptions = $vehicle_info->OPTIONS;
	            $options_array = explode(",", $myoptions);
	            $i = 0;
	            $s = 0;
	            $z = 0;
	            $left = "";
	            $right = "";
	            $thtml = "";
	            $soptions = "";
	            if($infosheet_data->INFO_STYLE == 'Dealer Custom 1 Certified' ){
	            	foreach ($options_array as $soptions) {
		                $i = $i + 1;
		                if (($i % 2) == 0) {
		                    $right .= "<li>" . $soptions . "</li>";
		                } else {
		                    $left .= "<li>" . $soptions . "</li>";
		                }
		                if ($i == 40) {
		                    break;
		                }
		            }
	            }elseif($infosheet_data->INFO_STYLE == 'Dealer Custom 2 Certified'){
	            	foreach ($options_array as $soptions) {
		                $i = $i + 1;
		                if (($i % 2) == 0) {
		                    $right .= "<li>" . $soptions . "</li>";
		                } else {
		                    $left .= "<li>" . $soptions . "</li>";
		                }
		                if ($i == 11) {
		                    break;
		                }
		            }
	            }elseif($infosheet_data->INFO_STYLE == 'Dealer Custom' ||$infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
	            	foreach ($options_array as $soptions) {
		                $i = $i + 1;
		                if (($i % 2) == 0) {
		                    $right .= "<li>" . $soptions . "</li>";
		                } else {
		                    $left .= "<li>" . $soptions . "</li>";
		                }
		                if ($i == 17) {
		                    break;
		                }
		            }
	            }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 3'){
	            	foreach ($options_array as $soptions) {
		                $i = $i + 1;
		                if ($i <= 8) {
		                    $right .= "<li>" . $soptions . "</li>";
		                } elseif ($i > 8 and $i <= 16) {
		                    $left .= "<li>" . $soptions . "</li>";
		                }
		                if ($i == 16) {
		                    break;
		                }
		            }
	            }elseif($infosheet_data->INFO_STYLE == 'Info Book'){
	            	foreach ($options_array as $soptions) {
		                $i = $i + 1;
		                if ($i == 1) {
		                    $thtml .= "<div style='clear:both;'>";
		                    $thtml .= "<div style='width:800px;float:left;'><li>" . $soptions . "</li></div>";
		                } elseif ($i == 2) {
		                    $thtml .= "<div style='width:800px;float:left;'><li>" . $soptions . "</li></div>";
		                } elseif ($i == 3) {
		                    $thtml .= "<div style='width:800px;float:left;'><li>" . $soptions . "</li></div>";
		                    $thtml .= "</div>";
		                    $i = 0;
		                }
		            }
	            }elseif($infosheet_data->INFO_STYLE == 'Narrow Standard' || $infosheet_data->INFO_STYLE == 'Narrow Standard NP' ){
	            	foreach ($options_array as $soptions) {
		                $i = $i + 1;
		                $thtml .= "<br />" . $soptions;
		                if ($i == 20) {
		                    break;
		                }
		            }
	            }else{
	            	foreach ($options_array as $soptions) {
		                $i = $i + 1;
		                if (($i % 2) == 0) {
		                    if (strlen($soptions) > 90) {
		                        $s = $s + 3;
		                    } elseif (strlen($soptions) > 45) {
		                        $s = $s + 2;
		                    } else {
		                        $s = $s + 1;
		                    }
		                    if ($s < 20) {
		                        $right .= "<li>" . $soptions . "</li>";
		                    }
		                } else {
		                    if (strlen($soptions) > 90) {
		                        $z = $z + 3;
		                    } elseif (strlen($soptions) > 45) {
		                        $z = $z + 2;
		                    } else {
		                        $z = $z + 1;
		                    }
		                    if ($z < 20) {
		                        $left .= "<li>" . $soptions . "</li>";
		                    }
		                }
		                if ($i == 40) {
		                    break;
		                }
		            }
	            }
	        } else {
	            $i = 0;
	            $left = "";
	            $right = "";
	            $thtml = "";
	            $soptions = "";
	        }
	        $first = '';
	        if (!empty($vehicle_info->PHOTOS)) {
	            $myimages = $vehicle_info->PHOTOS;
	            $images_array = explode(", ", $myimages);
	            if(count($images_array) == 1){
	            	$images_array = explode(";", $images_array[0]);
	            }
	            $first = $images_array[0];
	            $i = 0;
	            $coll2 = "";
	            $coll3 = "";
	            $coll1 = "";
	            $f = 0;
	            $simages = "";
	            foreach ($images_array as $simages) {
	                $i = $i + 1;
	                $f = $f + 1;
	                if ($i == 1) {
	                    $coll1 .= "<img src='" . $simages . "' /><br><br>";
	                } elseif ($i == 2) {
	                    $coll2 .= "<img src='" . $simages . "' /><br><br>";
	                } elseif ($i == 3) {
	                    $coll3 .= "<img src='" . $simages . "' /><br><br>";
	                    $i = 0;
	                }
	                if ($f == 12) {
	                    break;
	                }
	            }
	        } else {
	            $i = 0;
	            $coll2 = "";
	            $coll3 = "";
	            $coll1 = "";
	            $f = 0;
	            $simages = "";
	            $first = "";
	        }
	        $price = floatval($vehicle_info->MSRP);
	        $price = number_format($price, 2, '.', ',');
	        $desccount = str_word_count($vehicle_info->DESCRIPTION);
	        if ($desccount < 100) {
	            $descriptionfont = "font-size:14px;line-height:14px;font-weight:bold;";
	        } else if ($desccount < 200) {
	            $descriptionfont = "font-size:12px;line-height:12px;font-weight:bold;";
	        } else if ($desccount > 200) {
	            $descriptionfont = "font-size:9px;line-height:9px;font-weight:bold;";
	        }

	        if (!empty($infosheet_data->QR_INFO_ADDRESS)) {
	            $posvin = strpos($infosheet_data->QR_INFO_ADDRESS, '[VIN]');
	            $posstock = strpos($infosheet_data->QR_INFO_ADDRESS, '[STOCK]');
	            if ($posvin == true) {
	                $url = str_ireplace("[VIN]", $vehicle_info->VIN_NUMBER, $infosheet_data->QR_INFO_ADDRESS);
	            } elseif ($posstock == true) {
	                $url = str_ireplace("[STOCK]", $vehicle_info->STOCK_NUMBER, $infosheet_data->QR_INFO_ADDRESS);
	            } else {
	                $url = $infosheet_data->QR_INFO_ADDRESS;
	            }
	            $image = 'http://chart.googleapis.com/chart?cht=qr&chs=448x448&chld=M|0&chl=' . urlencode($url);
	            $file = file_get_contents($image);
	            $handle = fopen(__DIR__.'/../../../public/img/qr.png', "w");
	            fwrite($handle, $file);
	            fclose($handle);

	            $qr = '<div class="qr">Scan for more info<br><img src="'.$this->site_baseurl.'/img/qr.png" style="width: 120px;" ></div>';
	        } else {
	            $qr = "";
	        }
	        $background_html = '';
	        $wed = ''; //Dealer Custom 1 Certified Variable

	        /*Pure Cars*/

	        $purecars_url = str_ireplace("[VIN]", $vehicle_info->VIN_NUMBER, $infosheet_data->PURECARS_URL);
			$bigimg = "";
	        $z = 0;
	        $x = 0;
	        $leftbigimg = "";
	        $rightbigimg = "";
	        $smallimg = "";
	        $i = 0;
	        $j = 0;
	        $leftsmallimg = "";
	        $rightsmallimg = "";
	        $smalltable = "";
	        if($infosheet_data->INFO_STYLE == 'Pure Cars 1' || $infosheet_data->INFO_STYLE == 'Pure Cars 2' || $infosheet_data->INFO_STYLE == 'Pure Cars 3' || $infosheet_data->INFO_STYLE == 'Pure Cars 4'){
	        	$html = HtmlDomParser::file_get_html( $purecars_url );
	        	if($infosheet_data->INFO_STYLE == 'Pure Cars 1' || $infosheet_data->INFO_STYLE == 'Pure Cars 2' || $infosheet_data->INFO_STYLE == 'Pure Cars 3'){
		        	foreach ($html->find('div.size1of3') as $k) {
			            foreach ($k->find('div.borderGray') as $e) {
			                foreach ($e->find('img.iconCircleLarge') as $f) {
			                    $z++;
			                    if ($z <= 3) {
			                        $leftbigimg .= '<td class="vbigimage"><img class="bigimage" src="' . $f->srcraster . '" />';
			                    } else {
			                        $rightbigimg .= '<td class="vbigimage"><img class="bigimage" src="' . $f->srcraster . '" />';
			                    }
			                }
			                foreach ($e->find('p.fontBold') as $g) {
			                    $x++;
			                    if ($x <= 3) {
			                        $leftbigimg .= '<br><span class="bigtext">' . $g->innertext . '</span></td>';
			                    } else {
			                        $rightbigimg .= '<br><span class="bigtext">' . $g->innertext . '</span></td>';
			                    }
			                }
			            }
			        }
		        }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 4'){
		        	foreach ($html->find('div.size1of3') as $k) {
			            foreach ($k->find('div.borderGray') as $e) {
			                foreach ($e->find('img.iconCircleLarge') as $f) {
			                    $z++;
			                    if ($z <= 3) {
			                        if ($f->srcraster == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/bd86e5f204ee4c8b8cfc80023c3f4514.png") {
			                            $bimgstring = "blind_big.png";
			                        } else if ($f->srcraster == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/2929de1e778545649f717293a55abf9c.png") {
			                            $bimgstring = "automatic_big.png";
			                        } else {
			                            $bimgstring = $f->srcraster;
			                        }
			                        $leftbigimg .= '<td class="vbigimage"><img class="bigimage" src="' . $bimgstring . '" />';
			                    } else {
			                        if ($f->srcraster == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/bd86e5f204ee4c8b8cfc80023c3f4514.png") {
			                            $bimgstring = "blind_big.png";
			                        } else if ($f->srcraster == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/2929de1e778545649f717293a55abf9c.png") {
			                            $bimgstring = "automatic_big.png";
			                        } else {
			                            $bimgstring = $f->srcraster;
			                        }
			                        $rightbigimg .= '<td class="vbigimage"><img class="bigimage" src="' . $bimgstring . '" />';
			                    }
			                }
			                foreach ($e->find('p.fontBold') as $g) {
			                    $x++;
			                    if ($x <= 3) {
			                        $leftbigimg .= '<br><span class="bigtext">' . $g->innertext . '</span></td>';
			                    } else {
			                        $rightbigimg .= '<br><span class="bigtext">' . $g->innertext . '</span></td>';
			                    }
			                }
			            }
			        }
		        }
		        
		        $bigtable = '<table cellspacing="35"><tr>' . $leftbigimg . '</tr><tr>' . $rightbigimg . '</tr></table>';
		        
		        if($infosheet_data->INFO_STYLE == 'Pure Cars 1'){
		        	foreach ($html->find('.valueAddedHighlights') as $p) {
			            foreach ($p->find('div.size1of2') as $o) {

			                foreach ($o->find('div.size1of2') as $e) {
			                    foreach ($e->find('img.paddingVerticalMedium') as $f) {
			                        $j++;
			                        if ($j >= 13) {
			                            break;
			                        }
			                        if ($j <= 6) {
			                            if ($imgg == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/2929de1e778545649f717293a55abf9c.png") {
			                                $leftsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="2929de1e778545649f717293a55abf9c.png" />';
			                            } else {
			                                $leftsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="' . $f->srcraster . '" />';
			                            }

			                        } else {
			                            if (strpos($f->srcraster, 'Automatic') !== false) {
			                                $imgstring = str_replace("Automatic", "png", $f->srcraster);
			                            } else {
			                                $imgstring = $f->srcraster;
			                            }

			                            $rightsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="' . $imgstring . '" />';

			                        }
			                    }
			                    foreach ($e->find('p.textMediumSmall') as $g) {
			                        $i++;
			                        if ($i >= 13) {
			                            break;
			                        }
			                        if ($j <= 6) {
			                            $leftsmallimg .= '<br><span class="smalltext">' . $g->innertext . '</span></td>';
			                        } else {
			                            $rightsmallimg .= '<br><span class="smalltext">' . $g->innertext . '</span></td>';
			                        }
			                    }
			                }
			            }
			        }
		        }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 4'){
		        	foreach ($html->find('.valueAddedHighlights') as $p) {
			            foreach ($p->find('div.size1of2') as $o) {

			                foreach ($o->find('div.size1of2') as $e) {
			                    foreach ($e->find('img.paddingVerticalMedium') as $f) {
			                        $j++;
			                        if ($j >= 13) {
			                            break;
			                        }
			                        if ($j <= 6) {
			                            $imgg = $f->srcraster;
			                            if ($imgg == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/2929de1e778545649f717293a55abf9c.png") {
			                                $leftsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="2929de1e778545649f717293a55abf9c.png" />';
			                            } else if ($imgg == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/bd86e5f204ee4c8b8cfc80023c3f4514.png") {
			                                $leftsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="blind_big.png" />';
			                            } else {
			                                $leftsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="' . $f->srcraster . '" />';
			                            }


			                        } else {
			                            $imgg = $f->srcraster;
			                            if ($imgg == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/2929de1e778545649f717293a55abf9c.png") {
			                                $rightsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="2929de1e778545649f717293a55abf9c.png" />';
			                            } else if ($imgg == "https://purecarshub.blob.core.windows.net/valuehighlightbadges/bd86e5f204ee4c8b8cfc80023c3f4514.png") {

			                                $rightsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="blind_big.png" />';
			                            } else {
			                                $rightsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="' . $f->srcraster . '" />';
			                            }

			                        }
			                    }
			                    foreach ($e->find('p.textMediumSmall') as $g) {
			                        $i++;
			                        if ($i >= 13) {
			                            break;
			                        }
			                        if ($j <= 6) {

			                            $leftsmallimg .= '<br><span class="smalltext">' . $g->innertext . '</span></td>';
			                        } else {
			                            $rightsmallimg .= '<br><span class="smalltext">' . $g->innertext . '</span></td>';
			                        }
			                    }
			                }
			            }
			        }
		        }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 2'){
		        	foreach ($html->find('.valueAddedHighlights') as $p) {
			            foreach ($p->find('div.size1of2') as $o) {

			                foreach ($o->find('div.size1of2') as $e) {
			                    foreach ($e->find('img.paddingVerticalMedium') as $f) {
			                        $j++;
			                        if ($j >= 13) {
			                            break;
			                        }
			                        if ($j <= 6) {
			                            $leftsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="' . $f->srcraster . '" />';
			                        } else {
			                            $rightsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="' . $f->srcraster . '" />';
			                        }
			                    }
			                    foreach ($e->find('p.textMediumSmall') as $g) {
			                        $i++;
			                        if ($i >= 13) {
			                            break;
			                        }
			                        if ($j <= 6) {
			                            $leftsmallimg .= '<br><span class="smalltext">' . $g->innertext . '</span></td>';
			                        } else {
			                            $rightsmallimg .= '<br><span class="smalltext">' . $g->innertext . '</span></td>';
			                        }
			                    }
			                }
			            }
			        }
		        }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 3'){
		        	foreach ($html->find('.valueAddedHighlights') as $p) {
			            foreach ($p->find('div.size1of2') as $o) {
			                foreach ($o->find('div.size1of2') as $e) {
			                    foreach ($e->find('img.paddingVerticalMedium') as $f) {
			                        $j++;
			                        if ($j >= 7) {
			                            break;
			                        }
			                        if ($j <= 6) {
			                            $leftsmallimg .= '<td class="vsmallimage"><img class="smallimage" src="' . $f->srcraster . '" />';
			                        }
			                    }
			                    foreach ($e->find('p.textMediumSmall') as $g) {
			                        $i++;
			                        if ($i >= 7) {
			                            break;
			                        }
			                        if ($j <= 6) {
			                            $leftsmallimg .= '<br><span class="smalltext">' . $g->innertext . '</span></td>';
			                        }
			                    }
			                }
			            }
			        }
		        }
		        
		        if ($leftsmallimg == "" and $rightsmallimg == "") {
		            $smalltable = "";
		        } else {
		            $smalltable = '<table cellspacing="35"><tr>' . $leftsmallimg . '</tr><tr>' . $rightsmallimg . '</tr></table>';
		        }
	        }
	         /*Pure Cars*/
        

	        if($infosheet_data->INFO_STYLE == 'Dealer Custom 4' || $infosheet_data->INFO_STYLE == 'Dealer Custom 4 NP'){
				$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_DC4_BG.'" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'KBB' || $infosheet_data->INFO_STYLE == 'KBB NP'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/theme_2.jpg" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Dealer Custom 1'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_DC1_BG.'" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Dealer Custom 2'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_DC2_BG.'" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Dealer Custom 3'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_DC3_BG.'" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Dealer Custom 1 Certified'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_DC1C_BG.'" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Dealer Custom 2 Certified'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_DC2C_BG.'" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 1' || $infosheet_data->INFO_STYLE == 'Pure Cars 2'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="http://d1ja3ktoroz7zi.cloudfront.net/purecars2.jpg" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 4'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_PURE4_BG.'" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Pure Cars 3'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="http://d1ja3ktoroz7zi.cloudfront.net/purecars3.jpg" style="width: 100%;">
						</div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Narrow Standard' || $infosheet_data->INFO_STYLE == 'Narrow Standard NP'){
	        	$background_html = '<div style="position: fixed; top:20px; left:0; text-align: center;z-index: -1000;">
						  	<img src="'.$cdn_url.'/'.$infosheet_data->INFO_NARROW_BG.'" style="width: 100%;">
						</div>';
	        }
	        if($infosheet_data->INFO_STYLE == 'Info Book'){
	        	$html .= '<div id="header">
						    <table width="100%" border="0">
						  		<tr>
						    		<td width="40%" valign="top"><strong>'.$dealer_info->DEALER_NAME.'</strong><br />
										'.$dealer_info->DEALER_ADDRESS.'<br />
										'.$dealer_info->DEALER_CITY.', '.$dealer_info->DEALER_STATE.' '.$dealer_info->DEALER_ZIP.'<br />
										'.$dealer_info->PHONE.'<br />
										'.$dealer_info->QR_ADDRESS.'
									</td>
						    		<td width="60%" valign="top" align="right"><div id="logo"><img src="http://d1xlji8qxtrdmo.cloudfront.net/'.$infosheet_data->INFO_LOGO.'" style="width:auto;height:100%;max-width:1000px;" /></div>
						    		</td>
						  		</tr>
							</table>
							<hr />
					  	</div>
					  	<div id="footer">
					  		<hr />
						    <p class="page" align="center">Data on this page may have come in part, or entirely from one or more of the following providers: ALG, NADA Guides, Chrome, Vencentric, Vinquery. Please refer to our visiter Agreement for further information on vehicle data. Copyright 2015 DealerAddendums Inc.</p>
					  	</div>
					  	<div id="content">
					  		<div>
						   		<div class="year">'.$vehicle_info->YEAR.' '.$vehicle_info->MAKE.' '.$vehicle_info->MODEL.'</div><div class="price">$'.$price.'
							</div>
							<table width="100%" border="0">
						  		<tr>
								    <td width="40%" valign="top"><img  style="height:auto;width:850px;" src="'.$first.'" /></td>
								    <td width="30%" valign="top">
										Mileage:'.$vehicle_info->MILEAGE.'<br /><br />
										Exterior color:'.$vehicle_info->EXT_COLOR.'<br /><br />
										Interior color:'.$vehicle_info->INT_COLOR.'<br /><br />
										Bodystyle:'.$vehicle_info->BODYSTYLE.'<br /><br />
										Doors:'.$vehicle_info->DOORS.'<br /><br />
										Engine:'.$vehicle_info->ENGINE.'<br /><br />
									</td>
									<td width="30%" valign="top">
										Transmission:'.$vehicle_info->TRANSMISSION.'<br /><br />
										Fuel Type:'.$vehicle_info->FUEL.'<br /><br />
										Drivetrain:'.$vehicle_info->DRIVETRAIN.'<br /><br />
										Stock Number:'.$vehicle_info->STOCK_NUMBER.'<br /><br />
										VIN:'.$vehicle_info->VIN_NUMBER.'
									</td>
						  		</tr>
							</table>
							<div class="clear">
								<br />
								<span class="spank">Vehicle Options:</span><hr />
								<div class="options">
									'.$thtml.'
								</div>
							</div>
						</div>
					    <div style="page-break-before: always;">
							<br />
							<span class="spank">Vehicle Photos:</span><hr />
							<table width="100%">
								<tr>
									<td width="32%" style="word-wrap:break-word;" valign="top" align="left">
										'.$coll1.'
									</td>
									<td width="32%" style="word-wrap:break-word;" valign="top" align="center">
										'.$coll2.'
									</td>
									<td width="32%" style="word-wrap:break-word;" valign="top" align="right">
										'.$coll3.'
									</td>
								</tr>
							</table>
						</div>
				  	</div><div class="page-break"></div>';
	        }elseif($infosheet_data->INFO_STYLE == 'Narrow Standard' || $infosheet_data->INFO_STYLE == 'Narrow Standard NP'){
	        	$html .= $background_html.'<div id="printit">
			          	<div id="second-section">
				            <table width="100%" border="0">
				              	<tr>
				                	<td colspan="3"><span class="bold1">Stock: '.$vehicle_info->STOCK_NUMBER.'</span><br /><span class="bold1">VIN: '.$vehicle_info->VIN_NUMBER.'</span><br /></td>
				              	</tr>
				               	<tr>
				                	<td width="50%" class="bold1" valign="top">Year: '.$vehicle_info->YEAR.'<br />Make: '.$vehicle_info->MAKE.'<br />Model: '.$vehicle_info->MODEL.'</td>
				                	<td valign="top" colspan="2"  class="bold1">'.$mileagerow.' '.$drivetrain.' '.$trimrow.'</td>
				              	</tr>
				               	<tr>
				                	<td colspan="3"><div style="height:2px; width:100%;background-color:black; margin-top:15px;  margin-bottom:15px;"></div></td>
				                </tr>
							  	<tr style="font-size:14px;"><td  colspan="3"><strong>Options:</strong><br />'.$thtml.'</td></tr>
				            </table>
			          	</div>';
			          	if($infosheet_data->INFO_STYLE == 'Narrow Standard'){
				          	$html .= '<div class="total">$'.$price.'</div>';
			          	}
		        $html .= '</div><div class="page-break"></div>';
	        }else{
	        	$html .= '<style type="text/css">';
	        	$html .= '.KBBdescription{
							'.$descriptionfont.'
							padding-top: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBDescription']['padding-top'].';
							margin-left: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBDescription']['margin-left'].';
							width: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBDescription']['width'].';
							height: '.$infosheet_styles[$infosheet_data->INFO_STYLE]['KBBDescription']['height'].';
							float: left;
							overflow: hidden;
		          		}';
		        $html .= '</style>';
	        	$html .= $background_html;
		  		$html .='<div id="printit">';
		  		if($infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
		  			$html .= '<div id="main">
								  <div id="section1">
								  	<div id="logo"><img src="http://d1xlji8qxtrdmo.cloudfront.net/'.$infosheet_data->INFO_LOGO.'" style="width:auto;height:100%;max-width:1000px;" /></div>';
		  		}
		  			if($infosheet_data->INFO_STYLE == 'KBB' || $infosheet_data->INFO_STYLE == 'KBB NP' || $infosheet_data->INFO_STYLE == 'Pure Cars 1' || $infosheet_data->INFO_STYLE == 'Pure Cars 4' || $infosheet_data->INFO_STYLE == 'Pure Cars 2' || $infosheet_data->INFO_STYLE == 'Pure Cars 3' || $infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
		  				$html .= '<div class="address"><span class="name">'.$dealer_info->DEALER_NAME.'</span><br>'.$dealer_info->DEALER_ADDRESS.'<br>'.$dealer_info->DEALER_CITY.'<br>'.$dealer_info->DEALER_STATE.' '.$dealer_info->DEALER_ZIP.'<br><br>'.$dealer_info->DEALER_PHONE.'</div>';	
		  			}
		  			if($infosheet_data->INFO_STYLE == 'Dealer Custom 2 Certified'){
		  				$html .= '<div class="address">'.$dealer_info->DEALER_NAME.'<br>'.$dealer_info->DEALER_ADDRESS.'<br>'.$dealer_info->DEALER_CITY.'<br>'.$dealer_info->DEALER_STATE.' '.$dealer_info->DEALER_ZIP.'<br><br>'.$dealer_info->DEALER_PHONE.'</div>';
		  			}
		  			if($infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
		  				$html .= '</div>';
		  			}
		  			if($infosheet_data->INFO_STYLE != 'Dealer Custom 2 Certified' && $infosheet_data->INFO_STYLE != 'Pure Cars 1' && $infosheet_data->INFO_STYLE != 'Pure Cars 4' && $infosheet_data->INFO_STYLE != 'Pure Cars 2' && $infosheet_data->INFO_STYLE != 'Pure Cars 3'){
		  				$html .= '<div class="year">'.$vehicle_info->YEAR.' '.$vehicle_info->MAKE.' '.$vehicle_info->MODEL.'</div>';
		  			}
		  			if($infosheet_data->INFO_STYLE != 'Dealer Custom 2 Certified'){
		  				if($infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
	  						$html .= '<div class="clear">';
	  					}else{
	  						$html .= '<div class="descdiv">';		
	  					}
		  				
		  				if($infosheet_data->INFO_STYLE == 'Pure Cars 1' || $infosheet_data->INFO_STYLE == 'Pure Cars 4' || $infosheet_data->INFO_STYLE == 'Pure Cars 2' || $infosheet_data->INFO_STYLE == 'Pure Cars 3'){
		  					$html .= '<div class="row1" style="width:1290px;">';
			  					$html .= '<table>
			  								<tr>
												<td class="col1">
													<span class="year">'.$vehicle_info->YEAR.' '.$vehicle_info->MAKE.' '.$vehicle_info->MODEL.'</span><br>
													STOCK: '.$vehicle_info->STOCK_NUMBER.'<br>
													VIN: '.$vehicle_info->VIN_NUMBER.'<br>'.
													$odometer.'
												</td>
												<td class="col2">'.
													$trans.' '.
													$drivetrain.' '.
													$engine.'
												</td>
												<td class="qr">'.$qr.'
												</td>
											</tr>
										</table>';
							$html .= '</div>';														
		  				}else{
		  					$html .= '<div class="col1">';
								if($infosheet_data->INFO_STYLE == 'Dealer Custom 4' || $infosheet_data->INFO_STYLE == 'Dealer Custom 4 NP'){
									$html .= $odometer.' &nbsp;<br>'.
											$vehicle_info->STOCK_NUMBER.'<br>'.
											$vehicle_info->VIN_NUMBER.'<br>';
								}else{
									$html .= 'STOCK: '.$vehicle_info->STOCK_NUMBER.'<br>'.
											'VIN: '.$vehicle_info->VIN_NUMBER.'<br>'.
											$odometer;
								}
								
							$html .= '</div>';
							if($infosheet_data->INFO_STYLE != 'Dealer Custom 4' && $infosheet_data->INFO_STYLE != 'Dealer Custom 4 NP'){
								$html .= '<div class="col2">
											'.$trans.' '.
											$drivetrain.' '.
											$engine.'
											</div>';
							}
		  				}
						$html .= '</div>';
		  			}

					if($infosheet_data->INFO_STYLE == 'Pure Cars 1' || $infosheet_data->INFO_STYLE == 'Pure Cars 4' || $infosheet_data->INFO_STYLE == 'Pure Cars 2' || $infosheet_data->INFO_STYLE == 'Pure Cars 3'){
						$html .= '<div class="descdiv">'.$smalltable.'</div>';
						$html .= '<div class="descdiv">'.$bigtable.'</div>';
					}else{
						if($infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
	  						$html .= '<div class="clear">';
	  					}else{
	  						$html .= '<div class="descdiv">';		
	  					}
							$html .= '<div class="KBBdescription">';
								if($infosheet_data->INFO_STYLE == 'KBB' || $infosheet_data->INFO_STYLE == 'KBB NP' || $infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
									$html .= '<span class="spank">Description:</span><br>';
								}
								$html .= $vehicle_info->DESCRIPTION;
							$html .= '</div>';
							if($infosheet_data->INFO_STYLE == 'KBB' || $infosheet_data->INFO_STYLE == 'KBB NP' || $infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
								$html .= $qr;
							}
						$html .= '</div>';

						if($infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
	  						$html .= '<div class="clear">';
	  					}else{
	  						$html .= '<div class="descdiv">';		
	  					}

						$html .= '<div class="options">';
								if($infosheet_data->INFO_STYLE == 'KBB' || $infosheet_data->INFO_STYLE == 'KBB NP' || $infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
									$html .= '<span class="spank">Options:</span><br>';
								}
								$html .= '<table width="100%">
									<tr>
										<td width="50%">
											'.$left.'
										</td>
										<td width="50%">
											'.$right.'
										</td>
									</tr>
								</table>
							</div>
						</div>';
					}

					if($infosheet_data->INFO_STYLE == 'Pure Cars 3'){
						$html = '<div class="descdiv">
							<div class="options">
								Options:<br>
								<table width="100%">
									<tr>
										<td width="50%">
											'.$left.'
										</td>
										<td width="50%">
											'.$right.'
										</td>
									</tr>
								</table>
							</div>
						</div>';
					}
					
					if($infosheet_data->INFO_STYLE != 'KBB NP' && $infosheet_data->INFO_STYLE != 'Dealer Custom 3' && $infosheet_data->INFO_STYLE != 'Dealer Custom 4 NP' && $infosheet_data->INFO_STYLE != 'Pure Cars 1' && $infosheet_data->INFO_STYLE != 'Pure Cars 4' &&  $infosheet_data->INFO_STYLE != 'Pure Cars 3' && $infosheet_data->INFO_STYLE != 'Dealer Custom NP' && $infosheet_data->INFO_NP_CHECK == '0'){
						if($infosheet_data->INFO_STYLE == 'Dealer Custom'){

							$html .= '<div class="clear"><div class="KBBprice">Sale Price $'.$price.'</div></div>';	
						}else{
							$html .= '<div class="KBBprice">$'.$price.'</div>';	
						}
					}
					if($infosheet_data->INFO_STYLE == 'Dealer Custom 3'){
						$html .= '<div class="mpg_city">'.$cmpg.'</div>
									<div class="mpg_highway">'.$hmpg.'</div>';
					}
					if($infosheet_data->INFO_STYLE == 'Dealer Custom 1 Certified' || $infosheet_data->INFO_STYLE == 'Dealer Custom 2 Certified'){
						$html .= '<div class="wed">'.$wed.'</div>';
					}
					if($infosheet_data->INFO_STYLE == 'Dealer Custom 2 Certified'){
						$html .= '<div class="certified">'.$vehicle_info->INSP_NUMB.'</div>';	
					}
					if($infosheet_data->INFO_STYLE == 'Dealer Custom' || $infosheet_data->INFO_STYLE == 'Dealer Custom NP'){
						$html .= '</div>';
					}
				$html.='</div><div class="page-break"></div>';
	        }
		}
        
        $html .= $js;
		$html .= '</body></html>';
		$infosheet = "infosheet_" . strtotime(date('Y-m-d')) . ".pdf";
		$dompdf = new DOMPDF();
		//background image access *important for external background image
		$dompdf->set_option('isRemoteEnabled', 'true');
		if ($infosheet_data->INFO_STYLE == "Narrow Standard" or $infosheet_data->INFO_STYLE == "Narrow Standard NP") {
	        $dompdf->setPaper(array(0, 0, 4.25 * 72, 11 * 72), "infosheet");
	    }else{
	        $dompdf->setPaper(array(0, 0, 8.5 * 72, 11 * 72), "infosheet"); // 12" x 12"
	    }
	    $dompdf->loadHtml($html);
	    $dompdf->render();
	    if ($type == 'download') {
	        $dompdf->stream($infosheet);
	    } else {
	        $dompdf->stream($infosheet, array('Attachment' => 0));
	    }
	}

	/**
	 *
	 * Get Font_size
	 *
	 */
	public function _font_size($style, $font){
		if($style == 'standard'){
			if($font=="SM"){
				return "12px";
			}elseif($font=="MED"){
				return "14px";
			}elseif($font=="LG"){
				return "16px";
			}elseif($font=="XL"){
				return "20px";
			}elseif($font=="XXL"){
				return "24px";
			}	
		}else{
			if($font=="SM"){
				return "10px";
			}elseif($font=="MED"){
				return "12px";
			}elseif($font=="LG"){
				return "14px";
			}elseif($font=="XL"){
				return "18px";
			}elseif($font=="XXL"){
				return "20px";
			}
		}
		
	}
	public function _font_bold($bold){
		if($bold=="1"){
			return "bold";
		}else{
			return "";
		}
	}
	public function _display($display){
		if($display!="1"){
		  	return "none";
		}else{
		  	return "";
		}
  	}

  	public function getPrintData($vid){
		$user = Auth::user(); // Logged In User Information
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$vehicle_model=htmlspecialchars($vehicle_info->MODEL, ENT_QUOTES);
		$vehicle_style=htmlspecialchars($vehicle_info->BODYSTYLE, ENT_QUOTES);
		$base_price = 0; //template base_price
		$addendum_row = TemplateData::getTemplateData($user->DEALER_ID, $vehicle_info->NEW_USED);
		$base_price= (float)$addendum_row->BASE_PRICE;

		//number of AddendumData row
		$check_add = AddendumData::getNumberAddendumData($user->DEALER_ID, $vehicle_info->VIN_NUMBER); 
		//If number of AddendumData is zero, then insert Addendum Data base Addendum Default Data
		if($check_add != 0 && isset($vid) && $vehicle_info->EDIT_STATUS == 0){
			$ad_infos = AddendumDefault::getAdDefaultData($user->DEALER_ID, $vehicle_info->NEW_USED, $vehicle_model, $vehicle_style);
			foreach($ad_infos as $ad_info){
				$insert_array = array(
					'VEHICLE_ID' => $vid, 
					'ITEM_NAME' => $ad_info->ITEM_NAME, 
					'ITEM_DESCRIPTION' => $ad_info->ITEM_DESCRIPTION,
					'ITEM_PRICE' => $ad_info->ITEM_PRICE,
					'ACTIVE' => 'yes', 
					'DEALER_ID' => $user->DEALER_ID, 
					'CREATION_DATE' => date("Y-m-d"), 
					'SEPARATOR_BELOW' => $ad_info->SEPARATOR_BELOW, 
					'SEPARATOR_ABOVE' => $ad_info->SEPARATOR_ABOVE, 
					'OR_OR_AD' => $ad_info->OG_OR_AD, 
					'VIN_NUMBER' => $vehicle_info->VIN_NUMBER, 
					'ORDER_BY' => $ad_info->RE_ORDER
				);
				AddendumData::insertAddendumData($insert_array);
			}
			DealerInventory::updateInventoryData($vid, array('EDIT_STATUS' => '1', 'EDIT_DATE' => date("Y-m-d")));
		}

		$addendum_total_price = AddendumData::getSumItemPrice($user->DEALER_ID, $vehicle_info->VIN_NUMBER);
		$addendum_total_price = (float)$addendum_total_price->options;
		$toption = '';

		//Casting options1
		if(($addendum_total_price+$base_price)<0){
			$toption=abs($addendum_total_price)+$base_price;
			$toption=number_format($toption, 2, '.', ',');
			$toption='-$'.$toption;
		}else{
			$toption=$addendum_total_price+$base_price;
			$toption=number_format($toption, 2, '.', ',');
			$toption='$'.$toption;
		}
		$amsrp = '';
		$ams = '';
		// Casting amsrp
		if($vehicle_info->MSRP_ADJUSTMENT != "" && $vehicle_info->MSRP_ADJUSTMENT != 'None'){
			$amsrp=$vehicle_info->MSRP_ADJUSTMENT;
			$ams=$vehicle_info->MSRP_ADJUSTMENT;
		}else{
			$amsrp=$addendum_row->ADJUSTMENT;
			$ams=$addendum_row->ADJUSTMENT;
		}

		//Casting for aprice
		if(strpos($amsrp, '%') !== false){
			if (strpos($amsrp, '-') !== false) {
				$str=str_replace("%","",$amsrp);
				$amsrp=(-(floatval($vehicle_info->MSRP)/100)*(str_replace("-","",$str)));
			}else{
				$str=str_replace("%","",$amsrp);
				$amsrp=(+(floatval($vehicle_info->MSRP)/100)*($str));
			}
		}

		//Casting Total Price
		$intotal = floatval($vehicle_info->MSRP)+$addendum_total_price+floatval($amsrp)+$base_price;
		if($intotal<0){
			$tintotal=abs($intotal);
			$tintotal=number_format($tintotal, 2, '.', ',');
			$tintotal='-$'.$tintotal;
		}else{
			$tintotal=$intotal;
			$tintotal=number_format($tintotal, 2, '.', ',');
			$tintotal='$'.$tintotal;
		}

		//Casting price
		if($vehicle_info->MSRP<0){
			$tmsrp=abs($vehicle_info->MSRP);
			$tmsrp=number_format($tmsrp, 2, '.', ',');
			$tmsrp='-$'.$tmsrp;
		}else if(empty($vehicle_info->MSRP)){
			$tmsrp=0.00;
			$tmsrp='-$'.$tmsrp;
		}else{
			$tmsrp=$vehicle_info->MSRP;
			$tmsrp=number_format($tmsrp, 2, '.', ',');
			$tmsrp='$'.$tmsrp;
			
		}

		$aprice=(float)$vehicle_info->MSRP+floatval($amsrp);

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));

		$return_arry = array(
			'id' => $vid,
			'stock' => $vehicle_info->STOCK_NUMBER,
			'vin' => $vehicle_info->VIN_NUMBER,
			'price' => $tmsrp,
			'options1' => $toption,
			'total_price' => $tintotal,
			'year' => $vehicle_info->YEAR,
			'make' => htmlspecialchars($vehicle_info->MAKE, ENT_QUOTES),
			'model' => $vehicle_model,
			'amsrp' => $ams,
			'aprice' => "$".number_format($aprice, 2, '.', ',')
		);
		return $return_arry;
  	}

}