<?php

namespace App\Http\Controllers\Auth;

/**/
use App\User;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/**/
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
      Auth::logout();
      return redirect('/login');
    }

    /**
     * Login the user.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(Request $request) {

        // Validate email and password.

        $this->validate($request, [
            'username'    => 'required',
            'password' => 'required|min:4'
        ]);
        
        // login in user if successful
        if ($this->signIn($request)) {
            return json_encode(array('status' => 'success'));
            //return redirect()->route('home');
        }
       $err_msg = 'Could not sign you in with those credentials.';
       return redirect()->route('login');
    }

    public function logInDealer(Request $request){
        $dealer_id = $request->input('dealer_id');
        $user = User::where('DEALER_ID', $dealer_id)->where('USER_TYPE', 'DealerAdmin')->first();
        if($user){
            $credentials =  [
                'username' => $user->username,
                'password' => $user->password,
                //'status' => 'Y'
            ];
            if(Auth::attempt($credentials)){
                return json_encode(array('status' => 'success'));
            }
        }
    }

    /**
     * Check user status
     *
     * @param Request $request
     * @return Boolean
     */
    protected function signIn(Request $request) {
        return Auth::attempt($this->getCredentials($request));
    }


    /**
     * Get the user credentials to login.
     *
     * @param Request $request
     * @return array
     */
    protected function getCredentials(Request $request) {
        //For validating from existing password, customized Illuminate\Auth\EloquentUserProvider:validateCredentials()
        /*
            //--------------- Change validateCredentials as below -----------------------

            $plain = $credentials['password'];

            //return $this->hasher->check($plain, $user->getAuthPassword());

            // Avoid hash check 
            if($plain == $user->getAuthPassword()){
                return true;
            }else{
                return false;
            }
        */
        
        return [
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            //'status' => 'Y'
        ];
        
    }
}
