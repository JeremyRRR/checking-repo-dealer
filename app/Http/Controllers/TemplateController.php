<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
/* Load Model Start */
use App\User;
use App\Model\Backgrounds;
use App\Model\Stockimage;
use App\Model\TemplateBuilder;
/* Load Model End */

class TemplateController extends Controller
{
	/* Go to Template Builder Page */
	public function index($id = ''){
		$this->data['id'] = $id?$id:0;
		return $this->_loadContent('client.pages.template');
	}

	/**
	 * Get Exist Templates
	 *
	 * @return array
	 */
	public function getDataForTemplate(){
		$user = Auth::user();
		$temp_data = TemplateBuilder::getTemplateBuilderByDID($user->DEALER_ID);
		return json_encode(array('temp_data' => $temp_data));
	}

	/**
	 * Get Template Data from _ID
	 *
	 * @param _ID
	 * @return Object
	 */

	public function getTemplate(Request $request){
		$id = $request->input('id');
		$temp_data = TemplateBuilder::getTemplateDataByID($id);
		return json_encode(array('temp_data' => $temp_data));
	}

	/**
	 *
	 * Get Data for Template Base Style
	 */
	public function getDataForBasetemplate(Request $request){
		$base_width = $request->input('width');
		$base_layout = $request->input('layout');
		$bg_infos = Backgrounds::getBGInfoByBaseStyle($base_width, $base_layout);
		$si_infos = Stockimage::getSIInfoByBaseStyle($base_width, $base_layout);
		return json_encode(array('bg_info' => $bg_infos, 'si_info' => $si_infos));
	}

	/**
	 *
	 * Save Template Builder Information
	 *
	 */
	public function saveTemplate(Request $request){
		$user = Auth::user();
		$dealer_id = $user->DEALER_ID;
		$data = $request->input('data');
		$data = json_decode($data, true);
		$data['DEALER_ID'] = $dealer_id;
		$id = $request->input('id');
		if($id == 0){
			$data['TEMPLATE_CREATE'] = date('Y-m-d');
			$id = TemplateBuilder::insertTemplateData($data);
		}else{
			TemplateBuilder::updateTemplate($id, $data);
		}
		return json_encode(array('id'=>$id));
	}

	/**
	 *
	 * updateTemplateImages
	 */
	public function updateTemplateImages(Request $request){
		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/image_check.php');
		/* Load AWS S3 Libary For Upload File */
		$id = $request->input('id');
		if(intVal($id) != 0){
			$template_info = TemplateBuilder::getTemplateDataByID($id);
			if($request->hasFile('bg_image') && ($template_info->BACKGROUND_IMAGE == '' || $template_info->BACKGROUND_IMAGE == null)){
				$img_file = $request->bg_image;
				$tmp_name = $_FILES['bg_image']['tmp_name'];
				$name = $img_file->getClientOriginalName();
				$size = $img_file->getClientSize();
				$ext = $request->bg_image->extension();
				$image_name = time().'.'.$ext;
				/* Load AWS S3 Libary For Upload File */
				include_once(app_path().'/Libraries/s3/s3_config_1.php');
				/* Load AWS S3 Libary For Upload File */
				if(strlen($name) > 0){
					if (in_array($ext, $valid_formats)) {
						$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
						if($result){
							TemplateBuilder::updateTemplate($id, array('BACKGROUND_IMAGE' => $image_name));
						}
					}
				}
			}
			if($request->hasFile('logo_image') && ($template_info->DEALER_LOGO == '' || $template_info->DEALER_LOGO == null)){
				$img_file = $request->logo_image;
				$tmp_name = $_FILES['logo_image']['tmp_name'];
				$name = $img_file->getClientOriginalName();
				$size = $img_file->getClientSize();
				$ext = $request->logo_image->extension();
				$image_name = time().'.'.$ext;
				/* Load AWS S3 Libary For Upload File */
				include_once(app_path().'/Libraries/s3/s3_config.php');
				/* Load AWS S3 Libary For Upload File */
				if(strlen($name) > 0){
					if (in_array($ext, $valid_formats)) {
						$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
						if($result){
							TemplateBuilder::updateTemplate($id, array('DEALER_LOGO' => $image_name));
						}
					}
				}
			}
			if($request->hasFile('footer_image') && ($template_info->INFO_IMAGE == '' || $template_info->INFO_IMAGE == null)){
				$img_file = $request->footer_image;
				$tmp_name = $_FILES['footer_image']['tmp_name'];
				$name = $img_file->getClientOriginalName();
				$size = $img_file->getClientSize();
				$ext = $request->footer_image->extension();
				$image_name = time().'.'.$ext;
				/* Load AWS S3 Libary For Upload File */
				include_once(app_path().'/Libraries/s3/s3_config.php');
				/* Load AWS S3 Libary For Upload File */
				if(strlen($name) > 0){
					if (in_array($ext, $valid_formats)) {
						$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
						if($result){
							TemplateBuilder::updateTemplate($id, array('INFO_IMAGE' => $image_name));
						}
					}
				}
			}
			return json_encode(array('status' => 'success'));
		}		
	}
}