<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
/* Load Model Start */
use App\User;
use App\Model\Dealer;
use App\Model\UserPrivilege;
use App\Model\DealerInventory;
use App\Model\TemplateData;
use App\Model\AddendumData;
use App\Model\AddendumDefault;
use App\Model\Constants;
use App\Model\OptionsModelList;
use App\Model\InfoSheet;
use App\Model\TemplateBuilder;
use App\Model\BodyStyle;
use App\Model\LabelPrice;
use App\Model\PendingLabel;
/* Load Model End */

/* Load FreshBook Library */
require_once(app_path() . '/Libraries/freshbooks/freshbooks.php');
use easyFreshBooksAPI;
/* Load FreshBook Library */

/* Load SwitfMailer Library For Send Mail */
require_once(app_path().'/../vendor/swiftmailer/swiftmailer/lib/swift_required.php');
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;
/* Load SwitfMailer Library For Send Mail */



class DashboardController extends Controller
{
	/**
	 * Get privilege information from user
	 *
	 * @return Object
	 */
	public function getPrivilege(){
		$user = Auth::user();
		$privilege_info = UserPrivilege::getPrivilegeByUserType($user->USER_TYPE);
		return json_encode(array('privilege_info' => $privilege_info));
	}

	/**
	 * Get user, information for Dashboard Overview
	 *
	 * @return Object
	 */
	public function getDataForOverview(){
		$user = Auth::user();
		//$privilege_info = UserPrivilege::getPrivilegeByUserType($user->USER_TYPE);
		//return json_encode(array('user' => $user,'privilege_info' => $privilege_info));
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		return json_encode(array('user' => $user, 'dealer_info' => $dealer_info));
	}

	/**
	 * Get user, information for Dashboard Overview
	 *
	 * @param page, sortby, decending, rowsperpage, search
	 * @return Object
	 */
	public function getVehicleDataForOverview(Request $request){
		$user = Auth::user();
		$page = $request->input('page');
		$sortby = $request->input('sortBy');
		$descending = $request->input('descending');
		$rowsperpage = $request->input('rowsPerPage');
		$filter = $request->input('filter');
		$order = 'asc';
		$start = ($page-1)*$rowsperpage;


		if($descending == 0){
			$order = 'desc';
		}
		
		$items = DealerInventory::getVehicleData($user->DEALER_ID, $user->PRINT_UNPRINT, $user->ACTIVE_INACTIVE, $user->NEW_USED_BOTH, $filter, $start, $rowsperpage,$sortby, $order);

		$total = DealerInventory::getNumVehicleData($user->DEALER_ID, $user->PRINT_UNPRINT, $user->ACTIVE_INACTIVE, $user->NEW_USED_BOTH, $filter);
		return json_encode(array('items' => $items, 'total' => $total));
	}

	/**
	 * Update User's print_unprint field
	 *
	 * @param print_unprint flag
	 * @return string
	 */
	public function updatePU(Request $request){
		$flag = $request->input('p_flag');
		$user = Auth::user();
		if(User::updateUser($user->USER_ID, array('PRINT_UNPRINT' => $flag))){
			return json_encode(array('status' => 'success'));
		}
		return json_encode(array('status' => 'fail'));
	}

	/**
	 *
	 * Get Vehicle Infromation from Vehicle ID
	 *
	 * @param dealerinventory id
	 * @return object
	 *
	 */
	public function getVehicleData(Request $request){
		$vid = $request->input('vid');
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		return json_encode(array('vehicle_info' => $vehicle_info));
	}

	/**
	 *
	 * Update DealerInventory from vid
	 *
	 * @param object
	 * @return status
	 *
	 */
	public function updateVehicleData(Request $request){
		$data = $request->input('data');
		$data = json_decode($data, true);
		$result = DealerInventory::updateInventoryData($data['_ID'], $data);
		return json_encode(array('status' => 'success'));
	}

	/**
	 * Update User's new_used field
	 *
	 * @param new_used flag
	 * @return array
	 */
	public function updateNU(Request $request){
		$flag = $request->input('nu_flag');
		$user = Auth::user();
		$nub = '';
		if($flag == 0){
			$nub = "New";
		}elseif($flag == 1){
			$nub = "Used";
		}else{
			$nub = "Both";
		}
		if(User::updateUser($user->USER_ID, array('NEW_USED_BOTH' => $nub))){
			/* Get header card information from updated new_used_both value */			
			/* init variable */
	        $current_vehicle = 0; // number of vehicle stock
	        $plus_vehicle = 0; // number of vehicle stocked today
	        $current_addendum = 0; // number of current addendum
	        $need_addendum = 0; // number of need addendum
	        $count_user = 0; // number of user
	        $last_login = ''; // User Last login date
	        $printed_addendum = 0; // number of printed addendum
	        $last_30 = 0; // number of printed addendum last 30 days
	        
	        $dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
	        $cur_date = date('Y-m-d');
        	if($user->USER_TYPE=="DealerUser" || $user->USER_TYPE=="DealerAdmin" || $user->USER_TYPE=="UserRestricted"){
        		if($nub == 'New'){
                    $current_vehicle = $dealer_info->VEHICLES_NEW;
                    $plus_vehicle = $dealer_info->PLUS_TODAY_N;
                    $current_addendum = $dealer_info->CURRENT_NEW;
                    $need_addendum = $dealer_info->NEED_NEW;
                    $printed_addendum = $dealer_info->PRINTED_NEW;
                    $last_30 = $dealer_info->LAST30_NEW;
                }elseif($nub == 'Used'){
                    $current_vehicle = $dealer_info->VEHICLES_USED;
                    $plus_vehicle = $dealer_info->PLUS_TODAY_U;
                    $current_addendum = $dealer_info->CURRENT_USED;
                    $need_addendum = $dealer_info->NEED_USED;
                    $printed_addendum = $dealer_info->PRINTED_USED;
                    $last_30 = $dealer_info->LAST30_USED;
                }else{
                    $current_vehicle = (int)$dealer_info->VEHICLES_NEW + (int)$dealer_info->VEHICLES_USED;
                    $plus_vehicle = (int)$dealer_info->PLUS_TODAY_N + (int)$dealer_info->PLUS_TODAY_U;
                    $current_addendum = (int)$dealer_info->CURRENT_NEW + (int)$dealer_info->CURRENT_USED;
                    $need_addendum = (int)$dealer_info->NEED_NEW + (int)$dealer_info->NEED_USED;
                    $printed_addendum = (int)$dealer_info->PRINTED_NEW + (int)$dealer_info->PRINTED_USED;
                    $last_30 = (int)$dealer_info->LAST30_NEW + (int)$dealer_info->LAST30_USED;
                }
                $count_user = User::getTotalUserNum($user->DEALER_ID);
                $last_log_user = User::getLastLoginInfo($user->DEALER_ID);
                if($last_log_user){
                    $last_login = date("d-m-Y", strtotime($last_log_user->LAST_LOGIN));
                }
        	}else{
        		$current_vehicle = DealerInventory::getCountInventoryByNU($nub);
                $plus_vehicle = DealerInventory::getCountInventoryByNU($nub, '',$cur_date);
                $current_addendum = DealerInventory::getCountInventoryByNU($nub, 1);
                $need_addendum = DealerInventory::getCountInventoryByNU($nub, 0);
                $printed_addendum = DealerInventory::getCountInventoryByNU($nub, 1,'','');
                $count_user = User::getTotalUserNum();
                $last_log_user = User::getLastLoginInfo();
                if($last_log_user){
                    $last_login = date("d-m-Y", strtotime($last_log_user->LAST_LOGIN));
                }
                $date30=date('Y-m-d', strtotime("now -30 days") );
                $last_30 = DealerInventory::getCountInventoryByNUandPrintDate($nub, $date30, $cur_date);
        	}
        	$header_card_array = array(
        		'current_vehicle' => $current_vehicle,
        		'plus_vehicle' => $plus_vehicle,
        		'current_addendum' => $current_addendum,
        		'need_addendum' => $need_addendum,
        		'count_user' => $count_user,
        		'last_login' => $last_login,
        		'printed_addendum' => $printed_addendum,
        		'last_30' => $last_30
        	);

			return json_encode(array('status' => 'success','header_card_data' => $header_card_array));
		}
		return json_encode(array('status' => 'fail'));
	}

	/**
	 *
	 * Get Free Print Cap Status Info
	 *
	 * @param type string
	 * @return status string
	 *
	 */
	public function getFreeCap(Request $request){
		$type = $request->input('type');
		$user = Auth::user();
		$multiple_count = 0;
		if($request->has('print')){
			$multiple_count = count($request->input('print'));
		}
		$status = 'no';
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		
		if($dealer_info->ACCOUNT_TYPE == 'Free'){
			if($type == 'single'){
				$total_print = DealerInventory::getTotalPrintCount($user->DEALER_ID);
			}else if($type == 'multiple'){
				$total_print = DealerInventory::getTotalPrintCount($user->DEALER_ID) + $multiple_count;
			}
			if($this->isTimestamp($dealer_info->DEALER_ID)==1){
				$timestamp=(((time()-$dealer_info->DEALER_ID)/60)/60)/24;
			}else{
				$timestamp=29;
			}
			if($total_print>=25 or $timestamp>30){
				$status = "no";
			}else{
				$status = "ok";
			}
		}else{
			$status = "ok";
		}
		return json_encode(array('status' => $status));
	}

	/**
	 *
	 * Get Template Data
	 *
	 * @return object
	 */
	public function getTemplateData(){
		$user = Auth::user();
		$n_template_data = TemplateData::getTemplateData($user->DEALER_ID, 'New');
		if(!$n_template_data){
			$insert_array = array(
				'DEALER_ID' => $user->DEALER_ID, 
				'TEMPLATE_TYPE' => 'New', 
				'USER_ID' => '0', 
				'RIGHT_MARGIN' => '0', 
				'LEFT_MARGIN' => '0', 
				'TOP_MARGIN' => '0', 
				'BOTTOM_MARGIN' => '0', 
				'ADDENDUM_STYLE' => 'EPA Template', 
				'DEALER_LOGO' => '', 
				'QR_INFO_ADDRESS' => '', 
				'ADDENDUM_BACKGROUND' => '000000', 
				'INFO_STYLE' => '', 
				'INFO_BACKGROUND' => '#FFFFFF', 
				'INFO_LOGO' => '', 
				'INFO_BORDER' => '#000000', 
				'TEXT_COLOR' => '#000000', 
				'PURECARS_URL' => '', 
				'STANDARD_BACKGROUND' => 'branded_black', 
				'SLOGAN' => '', 
				'QR_ADDRESS' => 'http://fueleconomy.gov/m/', 
				'QR_TITLE' => '', 
				'QR_DESCRIPTION' => '', 
				'QR_BRANDED_COLOR' => 'branded_QR_black', 
				'VIN_BRANDED_COLOR' => 'branded_VIN_black',
				'MKT_PRC_TXT' => '',
				'CDJR_PRC_TXT' => '',
				'TOTAL_S_TXT' => ''
			);
			TemplateData::insertTemplateData($insert_array);
			$n_template_data = TemplateData::getTemplateData($user->DEALER_ID, 'New');
		}
		$u_template_data = TemplateData::getTemplateData($user->DEALER_ID, 'Used');
		if(!$u_template_data){
			$insert_array = array(
				'DEALER_ID' => $user->DEALER_ID, 
				'TEMPLATE_TYPE' => 'Used', 
				'USER_ID' => '0', 
				'RIGHT_MARGIN' => '0', 
				'LEFT_MARGIN' => '0', 
				'TOP_MARGIN' => '0', 
				'BOTTOM_MARGIN' => '0', 
				'ADDENDUM_STYLE' => 'EPA Template', 
				'DEALER_LOGO' => '', 
				'QR_INFO_ADDRESS' => '', 
				'ADDENDUM_BACKGROUND' => '000000', 
				'INFO_STYLE' => '', 
				'INFO_BACKGROUND' => '#FFFFFF', 
				'INFO_LOGO' => '', 
				'INFO_BORDER' => '#000000', 
				'TEXT_COLOR' => '#000000', 
				'PURECARS_URL' => '', 
				'STANDARD_BACKGROUND' => 'branded_black', 
				'SLOGAN' => '', 
				'QR_ADDRESS' => 'http://fueleconomy.gov/m/', 
				'QR_TITLE' => '', 
				'QR_DESCRIPTION' => '', 
				'QR_BRANDED_COLOR' => 'branded_QR_black', 
				'VIN_BRANDED_COLOR' => 'branded_VIN_black',
				'MKT_PRC_TXT' => '',
				'CDJR_PRC_TXT' => '',
				'TOTAL_S_TXT' => ''
			);
			TemplateData::insertTemplateData($insert_array);
			$u_template_data = TemplateData::getTemplateData($user->DEALER_ID, 'Used');
		}
		return json_encode(array('n_addendum_row' => $n_template_data, 'u_addendum_row' => $u_template_data));
	}

	/**
	 * Get Print Vehicle Data
	 *
	 * @param vehicle id
	 * @return array
	 */
	public function getPrintData(Request $request){
		$vid = $request->input('id'); // Vehicle Id
		$user = Auth::user(); // Logged In User Information
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$vehicle_model=htmlspecialchars($vehicle_info->MODEL, ENT_QUOTES);
		$vehicle_style=htmlspecialchars($vehicle_info->BODYSTYLE, ENT_QUOTES);
		$base_price = 0; //template base_price
		$addendum_row = TemplateData::getTemplateData($user->DEALER_ID, $vehicle_info->NEW_USED);
		$base_price= (float)$addendum_row->BASE_PRICE;

		//number of AddendumData row
		$check_add = AddendumData::getNumberAddendumData($user->DEALER_ID, $vehicle_info->VIN_NUMBER); 
		//If number of AddendumData is zero, then insert Addendum Data base Addendum Default Data
		if($check_add != 0 && isset($vid) && $vehicle_info->EDIT_STATUS == 0){
			$ad_infos = AddendumDefault::getAdDefaultData($user->DEALER_ID, $vehicle_info->NEW_USED, $vehicle_model, $vehicle_style);
			foreach($ad_infos as $ad_info){
				$insert_array = array(
					'VEHICLE_ID' => $vid, 
					'ITEM_NAME' => $ad_info->ITEM_NAME, 
					'ITEM_DESCRIPTION' => $ad_info->ITEM_DESCRIPTION,
					'ITEM_PRICE' => $ad_info->ITEM_PRICE,
					'ACTIVE' => 'yes', 
					'DEALER_ID' => $user->DEALER_ID, 
					'CREATION_DATE' => date("Y-m-d"), 
					'SEPARATOR_BELOW' => $ad_info->SEPARATOR_BELOW, 
					'SEPARATOR_ABOVE' => $ad_info->SEPARATOR_ABOVE, 
					'OR_OR_AD' => $ad_info->OG_OR_AD, 
					'VIN_NUMBER' => $vehicle_info->VIN_NUMBER, 
					'ORDER_BY' => $ad_info->RE_ORDER
				);
				AddendumData::insertAddendumData($insert_array);
			}
			DealerInventory::updateInventoryData($vid, array('EDIT_STATUS' => '1', 'EDIT_DATE' => date("Y-m-d")));
		}

		$addendum_total_price = AddendumData::getSumItemPrice($user->DEALER_ID, $vehicle_info->VIN_NUMBER);
		$addendum_total_price = (float)$addendum_total_price->options;
		$toption = '';

		//Casting options1
		if(($addendum_total_price+$base_price)<0){
			$toption=abs($addendum_total_price)+$base_price;
			$toption=number_format($toption, 2, '.', ',');
			$toption='-$'.$toption;
		}else{
			$toption=$addendum_total_price+$base_price;
			$toption=number_format($toption, 2, '.', ',');
			$toption='$'.$toption;
		}
		$amsrp = '';
		$ams = '';
		// Casting amsrp
		if($vehicle_info->MSRP_ADJUSTMENT != "" && $vehicle_info->MSRP_ADJUSTMENT != 'None'){
			$amsrp=$vehicle_info->MSRP_ADJUSTMENT;
			$ams=$vehicle_info->MSRP_ADJUSTMENT;
		}else{
			$amsrp=$addendum_row->ADJUSTMENT;
			$ams=$addendum_row->ADJUSTMENT;
		}

		//Casting for aprice
		if(strpos($amsrp, '%') !== false){
			if (strpos($amsrp, '-') !== false) {
				$str=str_replace("%","",$amsrp);
				$amsrp=(-(floatval($vehicle_info->MSRP)/100)*(str_replace("-","",$str)));
			}else{
				$str=str_replace("%","",$amsrp);
				$amsrp=(+(floatval($vehicle_info->MSRP)/100)*($str));
			}
		}

		//Casting Total Price
		$intotal = floatval($vehicle_info->MSRP)+$addendum_total_price+floatval($amsrp)+$base_price;
		if($intotal<0){
			$tintotal=abs($intotal);
			$tintotal=number_format($tintotal, 2, '.', ',');
			$tintotal='-$'.$tintotal;
		}else{
			$tintotal=$intotal;
			$tintotal=number_format($tintotal, 2, '.', ',');
			$tintotal='$'.$tintotal;
		}

		//Casting price
		if($vehicle_info->MSRP<0){
			$tmsrp=abs($vehicle_info->MSRP);
			$tmsrp=number_format($tmsrp, 2, '.', ',');
			$tmsrp='-$'.$tmsrp;
		}else if(empty($vehicle_info->MSRP)){
			$tmsrp=0.00;
			$tmsrp='-$'.$tmsrp;
		}else{
			$tmsrp=$vehicle_info->MSRP;
			$tmsrp=number_format($tmsrp, 2, '.', ',');
			$tmsrp='$'.$tmsrp;
			
		}

		$aprice=(float)$vehicle_info->MSRP+floatval($amsrp);

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));

		$return_arry = array(
			'id' => $vid,
			'stock' => $vehicle_info->STOCK_NUMBER,
			'vin' => $vehicle_info->VIN_NUMBER,
			'price' => $tmsrp,
			'options1' => $toption,
			'total_price' => $tintotal,
			'year' => $vehicle_info->YEAR,
			'make' => htmlspecialchars($vehicle_info->MAKE, ENT_QUOTES),
			'model' => $vehicle_model,
			'amsrp' => $ams,
			'aprice' => "$".number_format($aprice, 2, '.', ',')
		);
		return json_encode($return_arry);
	}

	/**
	 *
	 * Get Print Options Information from vehicle id
	 *
	 * @param vehicle_id Int
	 * @return object
	 */
	public function getPrintOptions(Request $request){
		$user = Auth::user();
		$vid = $request->input('id');
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$ad_infos = AddendumDefault::getAdDefaultDataForOption($user->DEALER_ID, $vehicle_info->NEW_USED, $dealer_info->RE_ORDER);
		$options_array = array();
		foreach($ad_infos as $key => $ad_info){
			$num_row = AddendumData::getNumberAddendumDataByItemName($user->DEALER_ID, $vehicle_info->VIN_NUMBER, $ad_info->ITEM_NAME);
			if($num_row == 0){
				if($ad_info->OG_OR_AD == '1'){
					$options_array[$key]['text'] = $ad_info->ITEM_NAME;
					$options_array[$key]['value'] = $ad_info->_ID;
				}else{
					$options_array[$key]['text'] = 'Group: '.$ad_info->ITEM_NAME;
					$options_array[$key]['value'] = $ad_info->_ID;
				}
			}
		}
		return json_encode(array('options_array' => $options_array));
	}

	/**
	 *
	 * Get Print Options Information for Table from vehicle id
	 *
	 * @param vehicle_id Int
	 * @return object
	 */
	public function getPrintOptTblData(Request $request){
		$user = Auth::user();
		$vid = $request->input('id');
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);

		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));

		$order_by = '_ID';
		if($vehicle_info->RE_ORDER == 1 || $dealer_info->RE_ORDER == 1){
			$order_by = 'ORDER_BY';
		}
		$information = AddendumData::getOptionsForTable($user->DEALER_ID, $vehicle_info->VIN_NUMBER, $order_by);
		return json_encode(array('options_data' => $information));
	}

	/**
	 *
	 * Add option to addendum table from dealer_id, vehilce_id, ad_id
	 * 
	 * @param vid, option_id
	 * @return status, object
	 */

	public function addOption(Request $request){
		$user = Auth::user();
		$vid = $request->input('vid');
		$option_id = $request->input('option_id');
		$ad_info = AddendumDefault::getAdDefaultDataByID($option_id);
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$insert_array = array(
			'VEHICLE_ID' => $vid,
			'ITEM_NAME' => $ad_info->ITEM_NAME,
			'ITEM_DESCRIPTION' => $ad_info->ITEM_DESCRIPTION,
			'ITEM_PRICE' => $ad_info->ITEM_PRICE,
			'ACTIVE' => 'yes',
			'DEALER_ID' => $user->DEALER_ID,
			'CREATION_DATE' => date('Y-m-d'),
			'SEPARATOR_BELOW' => $ad_info->SEPARATOR_BELOW,
			'SEPARATOR_ABOVE' => $ad_info->SEPARATOR_ABOVE,
			'OR_OR_AD' => $ad_info->OG_OR_AD,
			'VIN_NUMBER' => $vehicle_info->VIN_NUMBER
		);
		$id = AddendumData::insertAddendumData($insert_array);
		$insert_array['_ID'] = $id;

		//update Dealer Inventory Edit_status and edit_date
		DealerInventory::updateInventoryData($vid, array('EDIT_STATUS' => '1', 'EDIT_DATE' => date("Y-m-d")));
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Add new option
	 *
	 * @param item name, price, description, seprate above, below, spaces
	 * @return status
	 */
	public function addNewOption(Request $request){
		$user = Auth::user();
		$item_name = htmlspecialchars($request->input('item_name'), ENT_QUOTES);
		$item_desc = nl2br(htmlspecialchars($request->input('item_desc'), ENT_QUOTES));
		$item_price = $request->input('item_price');
		$item_price = str_replace("$", "", $item_price);
		$item_price = str_replace(",", "", $item_price);
		$sep_below = $request->input('sep_below') == true? "1": "0";
		$sep_above = $request->input('sep_above') == true? "1": "0";
		$sep_spaces = $request->input('sep_spaces') != ''? $request->input('sep_spaces'): '2';
		$save_future = $request->input('save_future');

		$vid = $request->input('vid');

		/* If save_future_use checkbox value true, then insert data to addendum_defaults Table */
		if($save_future){
			$insert_default_array = array(
				'DEALER_ID' => $user->DEALER_ID,
				'ITEM_NAME' => $item_name,
				'ITEM_PRICE' => $item_price,
				'ITEM_DESCRIPTION' => $item_desc,
				'AD_TYPE' => 'Both',
				'MODELS' => '-NONE',
				'BODY_STYLES' => '-NONE',
				'SEPARATOR_BELOW' => $sep_below,
				'SEPARATOR_ABOVE' => $sep_above,
				'OG_OR_AD' => '1',
				'SEPARATOR_SPACES' => $sep_spaces
			);

			AddendumDefault::insertAddendumDefault($insert_default_array);
		}

		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$insert_array = array(
			'VEHICLE_ID' => $vid,
			'ITEM_NAME' => $item_name,
			'ITEM_DESCRIPTION' => $item_desc,
			'ITEM_PRICE' => $item_price,
			'ACTIVE' => 'yes',
			'DEALER_ID' => $user->DEALER_ID,
			'CREATION_DATE' => date('Y-m-d'),
			'SEPARATOR_BELOW' => $sep_below,
			'SEPARATOR_ABOVE' => $sep_above,
			'OR_OR_AD' => "1",
			'VIN_NUMBER' => $vehicle_info->VIN_NUMBER,
			'SEPARATOR_SPACES' => $sep_spaces
		);
		AddendumData::insertAddendumData($insert_array);
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Get sortable initial Data for re-order
	 *
	 * @param vehicle id
	 * @return array
	 */
	public function getSortable(Request $request){
		$user = Auth::user();
		$vid = $request->input('vid');
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$order_by = '_ID';
		if($vehicle_info->RE_ORDER == 1 || $dealer_info->RE_ORDER == 1){
			$order_by = 'ORDER_BY';
		}
		$addendum_data = AddendumData::getOptionsForTable($user->DEALER_ID, $vehicle_info->VIN_NUMBER, $order_by);
		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		return json_encode(array('addendum_data' => $addendum_data));
	}

	/**
	 *
	 * Save re-order option
	 * @param vehicle_id, ordered IDs
	 * @return status,
	 */
	public function saveSortable(Request $request){
		$user = Auth::user();
		$vid = $request->input('vid');
		$order_ids = $request->input('order');
		$order_ids = json_decode($order_ids);
		$i = 0;
		foreach($order_ids as $id){
			AddendumData::updateAddendumData($id, array('ORDER_BY' => $i));
			$i++;
		}

		//update Dealer Inventory re-order field to 1
		DealerInventory::updateInventoryData($vid, array('RE_ORDER' => 1 ));
		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Delete Option
	 *
	 * @param addendum _ID
	 * @return status
	 */
	public function deleteOption(Request $request){
		$id = $request->input('id');
		$add_info = AddendumData::getAddendumDataByID($id);
		$vid = $add_info->VEHICLE_ID;
		//Delete option from AddendumData table
		AddendumData::deleteAddendumData($id);
		//update dealer_inventory Table
		DealerInventory::updateInventoryData($vid, array('EDIT_STATUS' => '1', 'EDIT_DATE' => date("Y-m-d")));

		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Save edited option
	 *
	 * @param item name, price, description, seprate above, below, spaces
	 * @return status
	 */
	public function saveEditedOption(Request $request){
		$user = Auth::user();
		$vid = $request->input('vid');
		$item_name = htmlspecialchars($request->input('item_name'), ENT_QUOTES);
		$item_desc = nl2br(htmlspecialchars($request->input('item_desc'), ENT_QUOTES));
		$item_price = $request->input('item_price');
		$item_price = str_replace("$", "", $item_price);
		$item_price = str_replace(",", "", $item_price);
		$sep_below = $request->input('sep_below') == true? "1": "0";
		$sep_above = $request->input('sep_above') == true? "1": "0";
		$sep_spaces = $request->input('sep_spaces') != ''? $request->input('sep_spaces'): '2';
		$id = $request->input('id');
		$or_or_ad = $request->input('or_or_ad'); //Currently, deal with or_or_ad = 1
		$update_array = array(
			'ITEM_NAME' => $item_name,
			'ITEM_DESCRIPTION' => $item_desc,
			'ITEM_PRICE' => $item_price,
			'SEPARATOR_SPACES' => $sep_spaces,
			'SEPARATOR_BELOW' => $sep_below,
			'SEPARATOR_ABOVE' => $sep_above
		);
		AddendumData::updateAddendumData($id, $update_array);
		//update dealer_inventory Table
		DealerInventory::updateInventoryData($vid, array('EDIT_STATUS' => '1', 'EDIT_DATE' => date("Y-m-d")));
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Save Adjustment Value
	 *
	 * @param vehicle id, adjustment value
	 * @return status string
	 */
	public function saveAdjustment(Request $request){
		$vid = $request->input('vid');
		$adjustment_value = $request->input('adjustment_value');
		DealerInventory::updateInventoryData($vid, array('MSRP_ADJUSTMENT' => $adjustment_value));
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Check Print Addendum
	 *
	 * @param vehicle id
	 * @return addendum_style
	 */
	public function checkPrintAddendum(Request $request){
		$user = Auth::user();
		$vid = $request->input('vid');
		$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
		$template_data = TemplateData::getTemplateData($user->DEALER_ID, $vehicle_info->NEW_USED);
		return json_encode(array('add_style' => $template_data->ADDENDUM_STYLE));
	}

	/**
	 *
	 * Decode VIN Number
	 *
	 * @param stock_number, vin_number, msrp
	 * @return object
	 *
	 */
	public function vinDecode(Request $request){
		$stock_number = $request->input('stock_number');
		$vin_number = $request->input('vin_number');
		$msrp = $request->input('msrp');
		$vin_data = array();
		if(strlen($vin_number) == 17){
			$vin_audit_key = 'PUXYZK7BV2XBQSE';
			$vin_decode_url = 'http://specifications.vinaudit.com/getspecifications.php?key='.$vin_audit_key.'&vin='.strtoupper(trim($vin_number,' ')).'&format=json';
			$vin_content = file_get_contents($vin_decode_url);
			$vin_content = json_decode($vin_content,true);
			if($vin_content['success'] == false){
				$vinquery_key = 'ea71c182-1288-485d-9ea0-2cebb96689d7';
				$vinquery_url = 'http://ws.vinquery.com/restxml.aspx?accessCode='.$vinquery_key.'&vin='.strtoupper(trim($vin_number,' ')).'&reportType=3';
				$vin_content = file_get_contents($vinquery_url);
				$fileContents = trim(str_replace('"', "'", $vin_content));
				$vin_content = json_encode(simplexml_load_string($fileContents));
				$vin_content = json_decode($vin_content, true);
				if($vin_content['VIN']['@attributes']['Status'] == 'SUCCESS'){
					if(isset($vin_content['VIN']['Vehicle']['@attributes'])){
						$vin_data = array(
							'Year' => $vin_content['VIN']['Vehicle']['@attributes']['Model_Year'],
							'Make' => $vin_content['VIN']['Vehicle']['@attributes']['Make'],
							'Model' => $vin_content['VIN']['Vehicle']['@attributes']['Model'],
							'Trim' => $vin_content['VIN']['Vehicle']['@attributes']['Trim_Level'],
						);	
						foreach($vin_content['VIN']['Vehicle']['Item'] as $items){
							switch ($items['@attributes']['Key']) {
								case 'Body Style':
									$vin_data = array_merge($vin_data, array('Style' => $items['@attributes']['Value']));
									break;
								case 'Engine Type':
									$vin_data = array_merge($vin_data, array('Engine' => $items['@attributes']['Value']));
									break;
								default:
									# code...
									break;
							}
						}
					}else{
						$vin_data = array(
							'Year' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Model_Year'],
							'Make' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Make'],
							'Model' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Model'],
							'Trim' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Trim_Level'],
						);	
						foreach($vin_content['VIN']['Vehicle'][0]['Item'] as $items){
							switch ($items['@attributes']['Key']) {
								case 'Body Style':
									$vin_data = array_merge($vin_data, array('Style' => $items['@attributes']['Value']));
									break;
								case 'Engine Type':
									$vin_data = array_merge($vin_data, array('Engine' => $items['@attributes']['Value']));
									break;
								
								default:
									# code...
									break;
							}
						}
					}
				}
			}else{
				$vin_data = array(
					'Year' => $vin_content['attributes']['Year'],
					'Make' => $vin_content['attributes']['Make'],
					'Model' => $vin_content['attributes']['Model'],
					'Trim' => $vin_content['attributes']['Trim Variations'],
					'Style' => $vin_content['attributes']['Vehicle Style'],
					'Doors' => $vin_content['attributes']['Doors'],
					'Transmission' => $vin_content['attributes']['Transmission Type'],
					'Engine Size' => $vin_content['attributes']['Engine Size'],
					'Engine' => $vin_content['attributes']['Engine'],
					'Drivetrain' => $vin_content['attributes']['Driven Wheels'],
					'Highway Mileage' => $vin_content['attributes']['Highway Mileage'],
					'City Mileage' => $vin_content['attributes']['City Mileage']
				);
			}
			if(count($vin_data) > 0){
				return json_encode(array('status' => 'success', 'vehicle_data' => $vin_data));	
			}else{
				return json_encode(array('status' => 'fail'));
			}
			
		}
	}

	/**
	 *
	 * Decode VIN Number Using Only VINQuery
	 *
	 * @param stock_number, vin_number, msrp
	 * @return object
	 *
	 */
	public function vinDecodeByVINQuery(Request $request){
		$stock_number = $request->input('stock_number');
		$vin_number = $request->input('vin_number');
		$msrp = $request->input('msrp');
		$vin_data = array();
		if(strlen($vin_number) == 17){
			$vinquery_key = 'ea71c182-1288-485d-9ea0-2cebb96689d7';
			$vinquery_url = 'http://ws.vinquery.com/restxml.aspx?accessCode='.$vinquery_key.'&vin='.strtoupper(trim($vin_number,' ')).'&reportType=3';
			$vin_content = file_get_contents($vinquery_url);
			$fileContents = trim(str_replace('"', "'", $vin_content));
			$vin_content = json_encode(simplexml_load_string($fileContents));
			$vin_content = json_decode($vin_content, true);
			if($vin_content['VIN']['@attributes']['Status'] == 'SUCCESS'){
				if(isset($vin_content['VIN']['Vehicle']['@attributes'])){
					$vin_data = array(
						'Year' => $vin_content['VIN']['Vehicle']['@attributes']['Model_Year'],
						'Make' => $vin_content['VIN']['Vehicle']['@attributes']['Make'],
						'Model' => $vin_content['VIN']['Vehicle']['@attributes']['Model'],
						'Trim' => $vin_content['VIN']['Vehicle']['@attributes']['Trim_Level'],
					);	
					foreach($vin_content['VIN']['Vehicle']['Item'] as $items){
						switch ($items['@attributes']['Key']) {
							case 'Body Style':
								$vin_data = array_merge($vin_data, array('Style' => $items['@attributes']['Value']));
								break;
							case 'Engine Type':
								$vin_data = array_merge($vin_data, array('Engine' => $items['@attributes']['Value']));
								break;
							default:
								# code...
								break;
						}
					}
				}else{
					$vin_data = array(
						'Year' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Model_Year'],
						'Make' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Make'],
						'Model' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Model'],
						'Trim' => $vin_content['VIN']['Vehicle'][0]['@attributes']['Trim_Level'],
					);	
					foreach($vin_content['VIN']['Vehicle'][0]['Item'] as $items){
						switch ($items['@attributes']['Key']) {
							case 'Body Style':
								$vin_data = array_merge($vin_data, array('Style' => $items['@attributes']['Value']));
								break;
							case 'Engine Type':
								$vin_data = array_merge($vin_data, array('Engine' => $items['@attributes']['Value']));
								break;
							
							default:
								# code...
								break;
						}
					}
				}
			}
		}
		if(count($vin_data) > 0){
			return json_encode(array('status' => 'success', 'vehicle_data' => $vin_data));	
		}else{
			return json_encode(array('status' => 'fail'));
		}
			
	}

	/**
	 *
	 * Save Vehicle Information(DealerInventory)
	 *
	 * @param dealer inventory information
	 * @return status
	 *
	 */
	public function saveVehicle(Request $request){
		$user = Auth::user();
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);

		$data = array(
			'DEALER_ID' => $user->DEALER_ID,
			'VIN_NUMBER' => $request->input('vin_number'),
			'STOCK_NUMBER' => $request->input('stock_number'),
			'YEAR' => $request->input('year'),
			'MAKE' => $request->input('make'),
			'MODEL' => $request->input('model'),
			'BODYSTYLE' => $request->input('style'),
			'DOORS' => $request->input('doors'),
			'TRIM' => $request->input('trim'),
			'EXT_COLOR' => $request->input('ext_color'),
			'INT_COLOR' => $request->input('int_color'),
			'ENGINE' => $request->input('engine'),
			'FUEL' => $request->input('engine_type'),
			'DRIVETRAIN' => $request->input('drivetrain'),
			'TRANSMISSION' => $request->input('transmission'),
			'MILEAGE' => $request->input('mileage'),
			'DATE_IN_STOCK' => $request->input('date_in'),
			'STATUS' => '1',
			'PRINT_STATUS' => '0',
			'MSRP' => preg_replace("/[^0-9.]/", "", trim($request->input('msrp'))),
			'INPUT_DATE' => date('Y-m-d'),
			'NEW_USED' => $request->input('new_used'),
			'DESCRIPTION' => nl2br(htmlspecialchars($request->input('description'), ENT_QUOTES)),
			'OPTIONS' => nl2br(htmlspecialchars($request->input('options'), ENT_QUOTES)),
			'HMPG' => preg_replace("/[^0-9.]/", "", trim($request->input('mpg_highway',' '))), 
			'CMPG' => preg_replace("/[^0-9.]/", "", trim($request->input('mpg_city',' '))), 
			'CREATED_BY' => 'VIN API',
			'UPDATE_DATE' => date('Y-m-d')
		);

		$inserted_id = DealerInventory::insertInventoryData($data);
		$new_used = $request->input('new_used');
		if($new_used == 'new'){
			$update_array = array(
				'VEHICLES_NEW' => (int)$dealer->VEHICLES_NEW +1,
				'NEED_NEW' => (int)$dealer->NEED_NEW +1,
				'PLUS_TODAY_N' => (int)$dealer->PLUS_TODAY_N +1
			);
			Dealer::updateDealerByID($user->DEALER_ID, $update_array);
		}elseif($new_used == 'used'){
			$update_array = array(
				'VEHICLES_USED' => (int)$dealer->VEHICLES_USED +1,
				'NEED_USED' => (int)$dealer->NEED_USED +1,
				'PLUS_TODAY_U' => (int)$dealer->PLUS_TODAY_U +1
			);
			Dealer::updateDealerByID($user->DEALER_ID, $update_array);
		}

		return json_encode(array('status' => 'success', 'vid' => $inserted_id));
	}

	/**
	 *
	 * Remove Imported Excel Vehicle Data from Inventory Table
	 *
	 * @return status
	 *
	 */
	public function clearImportedExcelVehicleData(){
		$user = Auth::user();
		DealerInventory::deleteInventoryExcelData($user->DEALER_ID);
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Import Vehicle Data to Inventory Table
	 *
	 * @param fields_arr, fields
	 * @return status
	 *
	 */
	public function importVehicleData(Request $request){
		$user = Auth::user();
		$fields_arr = $request->input('fields_arr');
		$fields = $request->input('fields');
		$insert_array = array();
		$insert_array['NEW_USED'] = 'NEW';
		foreach($fields_arr as $field_name => $field_index){
			if($fields[$field_index] == '\\N'){
				$insert_array[$field_name] = '';	
			}else{
				if($field_name == 'STOCK_NUMBER'){
					if(isset($fields[$field_index]) && $fields[$field_index] != null && strlen($fields[$field_index])){
						$insert_array[$field_name] = nl2br(htmlspecialchars($fields[$field_index], ENT_QUOTES));
					}else{
						$insert_array[$field_name] = '';
					}
				}elseif($field_name =='MSRP'){
					//remove commas, doller from MSRP field
					if(isset($fields[$field_index]) && $fields[$field_index] != null && strlen($fields[$field_index])){
						$insert_array[$field_name] = str_replace(",","",$fields[$field_index]);
						$insert_array[$field_name] = str_replace("$","",$insert_array[$field_name]);
					}else{
						$insert_array[$field_name] = '';
					}
				}elseif($field_name == 'NEW_USED'){
					if(strlen($fields[$field_index]) == 0){
						$insert_array[$field_name] = 'NEW';
					}else{
						if(strtolower($fields[$field_index][0]) == 'u'){
							$insert_array[$field_name] = 'USED';
						}elseif(strtolower($fields[$field_index][0]) == 'n'){
							$insert_array[$field_name] = 'NEW';
						}
					}
				}else{
					if(isset($fields[$field_index]) && $fields[$field_index] != null && strlen($fields[$field_index])){
						$insert_array[$field_name] = $fields[$field_index];	
					}
				}
			}
		}
		$insert_array['DEALER_ID'] = $user->DEALER_ID;
		$insert_array['DATE_IN_STOCK'] = date('Y-m-d');
		$insert_array['CREATED_BY'] = 'EXCEL';
		$vehicle_data = DealerInventory::getVehicleDataByDealerIDandStock($user->DEALER_ID, $insert_array['STOCK_NUMBER']);
		if($vehicle_data){
			if($vehicle_data->CREATED_BY == 'EXCEL'){
				DealerInventory::updateInventoryData($vehicle_data->_ID, $insert_array);
			}
		}else{
			DealerInventory::insertInventoryData($insert_array);
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Vehicle Delete(DealerInventory row delete)
	 *
	 * @param Vehicle ids, type(singledelete/multidelete)
	 * @return status
	 *
	 */
	public function vehicleDelete(Request $request){
		$type = $request->input('type');
		$ids = $request->input('ids');
		$ids = json_decode($ids);
		$user = Auth::user();
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$count = 0;
		foreach($ids as $vid){
			$count++;
			$vehicle_info = DealerInventory::getVehicleInfoByID($vid);
			if($vehicle_info->PRINT_STATUS == 0){
				if($vehicle_info->NEW_USED == 'New'){
					$update_array = array(
						'VEHICLES_NEW' => intVal($dealer_info->VEHICLES_NEW)-$count,
						'NEED_NEW' => intVal($dealer_info->NEED_NEW)-$count,
					);
					Dealer::updateDealerByID($user->DEALER_ID, $update_array);
				}elseif($vehicle_info->NEW_USED == 'Used'){
					$update_array = array(
						'VEHICLES_USED' => intVal($dealer_info->VEHICLES_USED)-$count,
						'NEED_USED' => intVal($dealer_info->NEED_USED)-$count,
					);
					Dealer::updateDealerByID($user->DEALER_ID, $update_array);
				}
			}else{
				if($vehicle_info->NEW_USED == 'New'){
					$update_array = array(
						'VEHICLES_NEW' => intVal($dealer_info->VEHICLES_NEW)-$count,
						'CURRENT_NEW' => intVal($dealer_info->CURRENT_NEW)-$count,
					);
					Dealer::updateDealerByID($user->DEALER_ID, $update_array);
				}elseif($vehicle_info->NEW_USED == 'Used'){
					$update_array = array(
						'VEHICLES_USED' => intVal($dealer_info->VEHICLES_USED)-$count,
						'CURRENT_USED' => intVal($dealer_info->CURRENT_USED)-$count,
					);
					Dealer::updateDealerByID($user->DEALER_ID, $update_array);
				}
			}
			DealerInventory::updateInventoryData($vid, array('STATUS' => '0'));
		}
		return json_encode(array('status'=>'success'));
	}

	/**
	 *
	 * Retrieve Basic Data for dealer profile setting Modal
	 *
	 *@return Object
	 */
	public function getDataForDealerProfile(){
		$mfee_manual = Constants::getConstantInfoByKey('MFEE_Manual');
		$mfee_automatic = Constants::getConstantInfoByKey('MFEE_Automatic Web');
		$mfee_automatic_dms = Constants::getConstantInfoByKey('MFEE_Automatic DMS');
		$mfee_info = array(
			'Manual' => $mfee_manual,
			'Automatic' => $mfee_automatic,
			'Automatic_DMS' => $mfee_automatic_dms
		);
		$makes = OptionsModelList::getAllMake();
		return json_encode(array('mfee_info' => $mfee_info, 'makes' => $makes));
	}


	/**
	 *
	 * Update Dealer Info
	 *
	 * @param dealer info array
	 * @return status
	 */
	public function updateDealerInfo(Request $request){
		$new_dealer_info = $request->input('dealer_info');
		$new_dealer_info = json_decode($new_dealer_info, true);
		$account_type = $request->input('account_type'); // Membership Account Type
		$user = Auth::user();
		$ori_dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, $new_dealer_info);
		$freshbooks = new easyFreshBooksAPI();
		$freshbooks->fburl = Constants::getConstantInfoByKey('FRESHBOOKS_API_URL');
		$freshbooks->fbtoken = Constants::getConstantInfoByKey('FRESHBOOKS_AUTH_TOKEN');
		$ori_dealer_name_arr = explode(' ', trim($ori_dealer_info->PRIMARY_CONTACT));
		$ori_dealer_first_name = $ori_dealer_name_arr[0];

		if($account_type != 'Same'){
			if($ori_dealer_info->ACCOUNT_TYPE != $account_type){
				if($account_type == 'Free'){
					$freshbooks->recurringDelete($ori_dealer_info->RECURE_ID);
					$update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('ACCOUNT_TYPE' => $account_type, 'RECURE_ID' => ''));

					/* for sending mail to dealer */
					$subject = ' '.config('app.DOMAIN').' Platform Downgrade Confirmation';
					$from = array(config('app.SYSTEM_EMAIL') => config('app.DOMAIN'));
					$to = array($ori_dealer_info->PRIMARY_CONTACT_EMAIL => $ori_dealer_info->PRIMARY_CONTACT);
					$text = "Hello " . $ori_dealer_first_name . ", Your account on " . config('app.DOMAIN') . " has been successfully downgraded to Free account. This account has a lifetime cap of 50 unique addendum prints. - " . config('app.SITE_SUPPORT') . ", " . config('app.COMPANY_NAME') . " Support Team";
					$html = "Hello " . $ori_dealer_first_name . ", <br><br>Your account on " . config('app.DOMAIN') . " has been successfully downgraded to Free account. This account has a lifetime cap of 50 unique addendum prints. <br><br><strong>" . config('app.SITE_SUPPORT') . ",  <br>" . config('app.COMPANY_NAME') . " <br>Support Team</strong>";
					$msend_result = $this->_SendMail($subject, $from, $to, $html, $text);

					/* for sending mail to addendum */
					$subject1 = ' '.config('app.DOMAIN').' Platform Downgrade Confirmation';
					$from1 = array($ori_dealer_info->PRIMARY_CONTACT_EMAIL => $ori_dealer_info->PRIMARY_CONTACT);
					$to1 = array(config('app.CRM_EMAIL') => config('app.SITE_AUTHOR'));
					$text1 = "Hello " . config('app.SITE_AUTHOR') . ", " . $ori_dealer_info->DEALER_NAME . " just downgraded their account to free account. Dealers details: Name:" . $ori_dealer_info->DEALER_NAME . ", ID:" . $ori_dealer_info->DEALER_ID . ",  - " . config('app.SITE_SUPPORT') . ", " . config('app.COMPANY_NAME') . " Support Team";
					$html1 = "Hello " . config('app.SITE_AUTHOR')  . ", <br><br>" . $ori_dealer_info->DEALER_NAME . " just downgraded their account to free account. <br><br>Dealers details: <br>Name:" . $ori_dealer_info->DEALER_NAME . "<br>ID:" . $ori_dealer_info->DEALER_ID . "<br><br>" . config('app.SITE_SUPPORT') . ", <br>" . config('app.COMPANY_NAME') . " <br>Support Team";
					$msend_result = $this->_SendMail($subject1, $from1, $to1, $html1, $text1);
					
				}else if($account_type == 'Manual' && $ori_dealer_info->ACCOUNT_TYPE == 'Free'){
					if($ori_dealer_info->CLIENT_ID == "") {
						$freshbooks->client->email = $ori_dealer_info->PRIMARY_CONTACT_EMAIL;
                        $freshbooks->client->first_name = $ori_dealer_info->PRIMARY_CONTACT;
                        $freshbooks->client->organization = $ori_dealer_info->DEALER_NAME;
                        $freshbooks->client->p_street1 = $ori_dealer_info->DEALER_ADDRESS;
                        $freshbooks->client->p_state = $ori_dealer_info->DEALER_STATE;
                        $freshbooks->client->p_country = 'USA';
                        $freshbooks->client->p_city = $ori_dealer_info->DEALER_CITY;
                        $freshbooks->client->p_code = $ori_dealer_info->DEALER_ZIP;
                        $client_id = $freshbooks->clientCreate();
                        //read error
                        $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('CLIENT_ID' => $client_id));
					}else{
						$client_id = $ori_dealer_info->CLIENT_ID;
					}

					$freshbooks->recurring->client_id = $client_id;
					$freshbooks->recurring->date = ''; //mysql format
					$freshbooks->recurring->frequency = 'monthly';

					//all other required properties should be populated
				 	$freshbooks->line->name = 'Membership Fee'; // (Optional)
					$freshbooks->line->description = 'Dealer Addendums - Manual Load'; // (Optional)
					$freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Manual'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $recurring_id = $freshbooks->recurringCreate();
                    $recurring = $freshbooks->recurringGet($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('LINE_ID' => $recurring->recurring->lines->line->line_id, 'ACCOUNT_TYPE' => $account_type, 'RECURE_ID' => $recurring_id));

                    /* for sending mail to dealer */
                    $subject = ' ' . config('app.DOMAIN') . ' service has been upgraded';
                    $from = array(config('app.SYSTEM_EMAIL') => config('app.DOMAIN'));
					$to = array($ori_dealer_info->PRIMARY_CONTACT_EMAIL => $ori_dealer_info->PRIMARY_CONTACT);
					$text = "Hello " . $ori_dealer_first_name . ",\n Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. \n \n Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from Free plan to our Manual Load subscription plan. \n You can now create and print unlimited addendums.\n\n" . config('app.SITE_SUPPORT') . "\n" . config('app.COMPANY_NAME') . "\n Support Team";
                    $html = "Hello " . $ori_dealer_first_name . ", <br><br>Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. <br><br> Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from Free plan to our <strong>Manual Load</strong> subscription plan. <br> You can now create and print unlimited addendums.<br><br><strong>" . config('app.SITE_SUPPORT') . ",</strong>  <br>" . config('app.COMPANY_NAME') . "<br> Support Team";
                    $msend_result = $this->_SendMail($subject, $from, $to, $html, $text);

                    /* for sending mail to addendum */
                    $subject1 = ' ' . config('app.DOMAIN') . ' Upgrade Notification';
                    $to1 = array(config('app.CRM_EMAIL') => config('app.SITE_AUTHOR'));
                    $text1 = "Hello Admin, A dealer just upgraded his account from Free to Manual Load. Dealers details: Name:" . $ori_dealer_info->DEALER_NAME . ", ID:" . $ori_dealer_info->DEALER_ID . ",  - " . config('app.SITE_SUPPORT') . ", " . config('app.COMPANY_NAME') . " Support Team";
                    $html1 = "Hello Admin, <br><br>A dealer just upgraded his account from Free to Manual Load. <br>Dealers details: Name:" . $ori_dealer_info->DEALER_NAME . ", ID:" . $ori_dealer_info->DEALER_ID . "<br>" . config('app.SITE_SUPPORT') . ", <br>" . config('app.COMPANY_NAME') . " Support Team";
                    $msend_result = $this->_SendMail($subject1, $from, $to1, $html1, $text1);

				}else if($account_type == 'Automatic Web' && $ori_dealer_info->ACCOUNT_TYPE == 'Manual'){
					$recurring_id = $ori_dealer_info->RECURE_ID;
                    $freshbooks->line->line_id = $ori_dealer_info->LINE_ID;
                    $freshbooks->line->name = 'Membership Fee'; // (Optional)
                    $freshbooks->line->description = 'Dealer Addendums - Automatic Web'; // (Optional)
                    $freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Automatic Web'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $result = $freshbooks->recurringLinesUpdate($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('ACCOUNT_TYPE' => $account_type));

                    $this->_sendUpgradeNoti($ori_dealer_info->PRIMARY_CONTACT_EMAIL, $ori_dealer_info->PRIMARY_CONTACT, $account_type, $ori_dealer_info->ACCOUNT_TYPE.' Load', $ori_dealer_first_name, $ori_dealer_info->DEALER_NAME, $ori_dealer_info->DEALER_ID);

				}else if($account_type == 'Automatic DMS' && $ori_dealer_info->ACCOUNT_TYPE == 'Manual'){
					$recurring_id = $ori_dealer_info->RECURE_ID;
                    $freshbooks->line->line_id = $ori_dealer_info->LINE_ID;
                    $freshbooks->line->name = 'Membership Fee'; // (Optional)
                    $freshbooks->line->description = 'Dealer Addendums - Automatic DMS'; // (Optional)
                    $freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Automatic DMS'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $result = $freshbooks->recurringLinesUpdate($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('ACCOUNT_TYPE' => $account_type));

                    $this->_sendUpgradeNoti($ori_dealer_info->PRIMARY_CONTACT_EMAIL, $ori_dealer_info->PRIMARY_CONTACT, $account_type, $ori_dealer_info->ACCOUNT_TYPE.' Load', $ori_dealer_first_name, $ori_dealer_info->DEALER_NAME, $ori_dealer_info->DEALER_ID);

				}else if($account_type == 'Automatic DMS' && $ori_dealer_info->ACCOUNT_TYPE == 'Automatic Web'){
					$recurring_id = $ori_dealer_info->RECURE_ID;
                    $freshbooks->line->line_id = $ori_dealer_info->LINE_ID;
                    $freshbooks->line->name = 'Membership Fee'; // (Optional)
                    $freshbooks->line->description = 'Dealer Addendums - Automatic DMS'; // (Optional)
                    $freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Automatic DMS'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $result = $freshbooks->recurringLinesUpdate($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('ACCOUNT_TYPE' => $account_type));

                    $this->_sendUpgradeNoti($ori_dealer_info->PRIMARY_CONTACT_EMAIL, $ori_dealer_info->PRIMARY_CONTACT, $account_type, $ori_dealer_info->ACCOUNT_TYPE, $ori_dealer_first_name, $ori_dealer_info->DEALER_NAME, $ori_dealer_info->DEALER_ID);

				}else if($account_type == 'Manual' && $ori_dealer_info->ACCOUNT_TYPE == 'Automatic Web'){
					$recurring_id = $ori_dealer_info->RECURE_ID;
                    $freshbooks->line->line_id = $ori_dealer_info->LINE_ID;
                    $freshbooks->line->name = 'Membership Fee'; // (Optional)
                    $freshbooks->line->description = 'Dealer Addendums - Manual Load'; // (Optional)
                    $freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Manual'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $result = $freshbooks->recurringLinesUpdate($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('ACCOUNT_TYPE' => $account_type));

                    $this->_sendDowngradeNoti($ori_dealer_info->PRIMARY_CONTACT_EMAIL, $ori_dealer_info->PRIMARY_CONTACT, $account_type.' Load', $ori_dealer_info->ACCOUNT_TYPE, $ori_dealer_first_name, $ori_dealer_info->DEALER_NAME, $ori_dealer_info->DEALER_ID);

				}else if($account_type == 'Manual' && $ori_dealer_info->ACCOUNT_TYPE == 'Automatic DMS'){
					$recurring_id = $ori_dealer_info->RECURE_ID;
                    $freshbooks->line->line_id = $ori_dealer_info->LINE_ID;
                    $freshbooks->line->name = 'Membership Fee'; // (Optional)
                    $freshbooks->line->description = 'Dealer Addendums - Manual Load'; // (Optional)
                    $freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Manual'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $result = $freshbooks->recurringLinesUpdate($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('ACCOUNT_TYPE' => $account_type));

                    $this->_sendDowngradeNoti($ori_dealer_info->PRIMARY_CONTACT_EMAIL, $ori_dealer_info->PRIMARY_CONTACT, $account_type.' Load', $ori_dealer_info->ACCOUNT_TYPE, $ori_dealer_first_name, $ori_dealer_info->DEALER_NAME, $ori_dealer_info->DEALER_ID);

				}else if($account_type == 'Automatic Web' && $ori_dealer_info->ACCOUNT_TYPE == 'Automatic DMS'){
					$recurring_id = $ori_dealer_info->RECURE_ID;
                    $freshbooks->line->line_id = $ori_dealer_info->LINE_ID;
                    $freshbooks->line->name = 'Membership Fee'; // (Optional)
                    $freshbooks->line->description = 'Dealer Addendums - Automatic Web'; // (Optional)
                    $freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Automatic Web'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $result = $freshbooks->recurringLinesUpdate($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('ACCOUNT_TYPE' => $account_type));

                    $this->_sendDowngradeNoti($ori_dealer_info->PRIMARY_CONTACT_EMAIL, $ori_dealer_info->PRIMARY_CONTACT, $account_type, $ori_dealer_info->ACCOUNT_TYPE, $ori_dealer_first_name, $ori_dealer_info->DEALER_NAME, $ori_dealer_info->DEALER_ID);
				}else if($account_type == 'Automatic Web' && $ori_dealer_info->ACCOUNT_TYPE == 'Free'){
					if($ori_dealer_info->CLIENT_ID == "") {
						$freshbooks->client->email = $ori_dealer_info->PRIMARY_CONTACT_EMAIL;
                        $freshbooks->client->first_name = $ori_dealer_info->PRIMARY_CONTACT;
                        $freshbooks->client->organization = $ori_dealer_info->DEALER_NAME;
                        $freshbooks->client->p_street1 = $ori_dealer_info->DEALER_ADDRESS;
                        $freshbooks->client->p_state = $ori_dealer_info->DEALER_STATE;
                        $freshbooks->client->p_country = 'USA';
                        $freshbooks->client->p_city = $ori_dealer_info->DEALER_CITY;
                        $freshbooks->client->p_code = $ori_dealer_info->DEALER_ZIP;
                        $client_id = $freshbooks->clientCreate();
                        //read error
                        $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('CLIENT_ID' => $client_id));
					}else{
						$client_id = $ori_dealer_info->CLIENT_ID;
					}

					$freshbooks->recurring->client_id = $client_id;
					$freshbooks->recurring->date = ''; //mysql format
					$freshbooks->recurring->frequency = 'monthly';

					//all other required properties should be populated
				 	$freshbooks->line->name = 'Membership Fee'; // (Optional)
					$freshbooks->line->description = 'Dealer Addendums - Automatic Web'; // (Optional)
					$freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Automatic Web'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $recurring_id = $freshbooks->recurringCreate();
                    $recurring = $freshbooks->recurringGet($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('LINE_ID' => $recurring->recurring->lines->line->line_id, 'ACCOUNT_TYPE' => $account_type, 'RECURE_ID' => $recurring_id));

                    /* for sending mail to dealer */
                    $subject = ' ' . config('app.DOMAIN') . ' service has been upgraded';
                    $from = array(config('app.SYSTEM_EMAIL') => config('app.DOMAIN'));
					$to = array($ori_dealer_info->PRIMARY_CONTACT_EMAIL => $ori_dealer_info->PRIMARY_CONTACT);
					$text = "Hello " . $ori_dealer_first_name . ",\n \n Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. \n\n Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from Free plan to our Automatic Web plan. Now we can automatically import your dealer inventory every night, making printing the next day AUTOMATIC. \n\n A " . config('app.COMPANY_NAME') . " customer onboarding representative will contact you shortly to arrange your automatic inventory feed and answer any questions you have. Please let us know if you have any questions.  Thanks and happy printing! \n\n " . config('app.SITE_SUPPORT') . "\n " . config('app.COMPANY_NAME') . "\n Support Team";
                    $html = "Hello " . $ori_dealer_first_name . ", <br><br>Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. <br><br> Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from Free to our <strong>Automatic Web</strong> option. Now we can automatically import your dealer inventory every night, making printing the next day AUTOMATIC. <br><br> A " . config('app.COMPANY_NAME') . " customer onboarding representative will contact you shortly to arrange your automatic inventory feed and answer any questions you have. Please let us know if you have any questions.  Thanks and happy printing!<br><br><strong>" . config('app.SITE_SUPPORT') . ",</strong>  <br>" . config('app.COMPANY_NAME') . " <br>Support Team";
                    $msend_result = $this->_SendMail($subject, $from, $to, $html, $text);

                    /* for sending mail to addendum */
                    $subject1 = ' ' . config('app.DOMAIN') . ' Upgrade Notification';
                    $to1 = array(config('app.CRM_EMAIL') => config('app.SITE_AUTHOR'));
                    $text1 = "Hello Admin, A dealer just upgraded his account from Free to Automatic Web. Dealers details: Name:" . $ori_dealer_info->DEALER_NAME . ", ID:" . $ori_dealer_info->DEALER_ID . ",  - " . SITE_SUPPORT . ", " . config('app.COMPANY_NAME') . " Support Team";
                    $html1 = "Hello Admin, <br>A dealer just upgraded his account from Free to Automatic Web. <br>Dealers details: Name:" . $ori_dealer_info->DEALER_NAME . ", ID:" . $ori_dealer_info->DEALER_ID . "<br>" . SITE_SUPPORT . ", <br>" . config('app.COMPANY_NAME') . " Support Team";
                    $msend_result = $this->_SendMail($subject1, $from, $to1, $html1, $text1);
				
				}else if($account_type == 'Automatic DMS' && $ori_dealer_info->ACCOUNT_TYPE == 'Free'){
					if($ori_dealer_info->CLIENT_ID == "") {
						$freshbooks->client->email = $ori_dealer_info->PRIMARY_CONTACT_EMAIL;
                        $freshbooks->client->first_name = $ori_dealer_info->PRIMARY_CONTACT;
                        $freshbooks->client->organization = $ori_dealer_info->DEALER_NAME;
                        $freshbooks->client->p_street1 = $ori_dealer_info->DEALER_ADDRESS;
                        $freshbooks->client->p_state = $ori_dealer_info->DEALER_STATE;
                        $freshbooks->client->p_country = 'USA';
                        $freshbooks->client->p_city = $ori_dealer_info->DEALER_CITY;
                        $freshbooks->client->p_code = $ori_dealer_info->DEALER_ZIP;
                        $client_id = $freshbooks->clientCreate();
                        //read error
                        $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('CLIENT_ID' => $client_id));
					}else{
						$client_id = $ori_dealer_info->CLIENT_ID;
					}

					$freshbooks->recurring->client_id = $client_id;
					$freshbooks->recurring->date = ''; //mysql format
					$freshbooks->recurring->frequency = 'monthly';

					//all other required properties should be populated
				 	$freshbooks->line->name = 'Membership Fee'; // (Optional)
					$freshbooks->line->description = 'Dealer Addendums - Automatic DMS'; // (Optional)
					$freshbooks->line->unit_cost = Constants::getConstantInfoByKey('MFEE_Automatic DMS'); // Default is 0
                    $freshbooks->line->quantity = '1'; // Default is 0
                    $freshbooks->addLine();

                    //try to create new invoice with provided data on FB server
                    $recurring_id = $freshbooks->recurringCreate();
                    $recurring = $freshbooks->recurringGet($recurring_id);
                    $update_id = Dealer::updateDealerByID($ori_dealer_info->DEALER_ID, array('LINE_ID' => $recurring->recurring->lines->line->line_id, 'ACCOUNT_TYPE' => $account_type, 'RECURE_ID' => $recurring_id));

                    /* for sending mail to dealer */
                    $subject = ' ' . config('app.DOMAIN') . ' service has been upgraded';
                    $from = array(config('app.SYSTEM_EMAIL') => config('app.DOMAIN'));
					$to = array($ori_dealer_info->PRIMARY_CONTACT_EMAIL => $ori_dealer_info->PRIMARY_CONTACT);
					$text = "Hello " . $ori_dealer_first_name . ",\n \n Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. \n\n Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from Free plan to our Automatic DMS plan. Now we can automatically import your dealer inventory every night, making printing the next day AUTOMATIC. \n\n A " . config('app.COMPANY_NAME') . " customer onboarding representative will contact you shortly to arrange your automatic inventory feed and answer any questions you have. Please let us know if you have any questions.  Thanks and happy printing! \n\n " . config('app.SITE_SUPPORT') . "\n " . config('app.COMPANY_NAME') . "\n Support Team";
                    $html = "Hello " . $ori_dealer_first_name . ", <br><br>Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. <br><br> Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from Free to our <strong>Automatic DMS</strong> option. Now we can automatically import your dealer inventory every night, making printing the next day AUTOMATIC. <br><br> A " . config('app.COMPANY_NAME') . " customer onboarding representative will contact you shortly to arrange your automatic inventory feed and answer any questions you have. Please let us know if you have any questions.  Thanks and happy printing!<br><br><strong>" . config('app.SITE_SUPPORT') . ",</strong>  <br>" . config('app.COMPANY_NAME') . " <br>Support Team";
                    $msend_result = $this->_SendMail($subject, $from, $to, $html, $text);

                    /* for sending mail to addendum */
                    $subject1 = ' ' . config('app.DOMAIN') . ' Upgrade Notification';
                    $to1 = array(config('app.CRM_EMAIL') => config('app.SITE_AUTHOR'));
                    $text1 = "Hello Admin, A dealer just upgraded his account from Free to Automatic DMS. Dealers details: Name:" . $ori_dealer_info->DEALER_NAME . ", ID:" . $ori_dealer_info->DEALER_ID . ",  - " . config('app.SITE_SUPPORT') . ", " . config('app.COMPANY_NAME') . " Support Team";
                    $html1 = "Hello Admin, <br>A dealer just upgraded his account from Free to Automatic DMS. <br>Dealers details: Name:" . $ori_dealer_info->DEALER_NAME . ", ID:" . $ori_dealer_info->DEALER_ID . "<br>" . config('app.SITE_SUPPORT') . ", <br>" . config('app.COMPANY_NAME') . " Support Team";
				}

				$data_value = '';
				if($account_type == 'Free'){
					$data_value = 'Trial';
				}else if($account_type == 'Manual'){
					$data_value = 'Manual';
				}else if($account_type == 'Automatic Web'){
					$data_value = 'Auto-Web';
				}else if($account_type == 'Automatic DMS'){
					$data_value = 'Auto-DMS';
				}
				/* ??? */
				$data_string = '{
					"party": {
						"fields": [
							{
								"value": "'.$data_value.'",
								"definition": { "id": 207029 }
							},
							{
								"definition": { "id": 361029 } ,
								"value": "Manual"
							}
						]
					}
				}';

				$ch = curl_init('https://api.capsulecrm.com/api/v2/parties/' . $ori_dealer_info->CRM_ID);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Authorization: Bearer '.$this->capsule_key.'',
                        'Content-Type: application/json',
                        'Accept: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                $result = curl_exec($ch);
                /* ??? */
			}
		}
	 	return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Retrieve Basic Data for infosheet settings Modal
	 *
	 * @return object
	 *
	 */
	public function getDataForInfoSheetSettings(Request $request){
		$user = Auth::user();
		$type = $request->input('type');
		$infosheet_info = InfoSheet::getInfosheetInfo($user->USER_ID, $type);
		return json_encode(array('infosheet_info' => $infosheet_info));
	}

	/**
	 *
	 *	Update InfoSheet settings
	 * 
	 * @param type, infosheet_settings_array, info_style
	 * @return status
	 *
	 */
	public function updateInfosheetSettings(Request $request){
		$user = Auth::user();
		$type = $request->input('type');
		$infosheet_info = $request->input('infosheet_info');
		$infosheet_info = json_decode($infosheet_info);
		$info_style = $request->input('info_style');
		switch ($info_style) {
			case 'KBB' || 'KBB NP' || 'Info Book' || 'Dealer Custom 2 Certified' || 'Dealer Custom 3' || 'Dealer Custom 4' || 'Dealer Custom 4 NP':
				# code...
				$update_array = array(
					'INFO_STYLE' => $info_style,
					'QR_INFO_ADDRESS' => $infosheet_info->QR_INFO_ADDRESS,
					'INFO_NP_CHECK' => $infosheet_info->INFO_NP_CHECK
				);
				$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, $update_array);
				break;
			case 'Dealer Custom' || 'Dealer Custom NP':
				# code...
				$update_array = array(
					'INFO_STYLE' => $info_style,
					'INFO_BACKGROUND' => $infosheet_info->INFO_BACKGROUND,
					'INFO_BORDER' => $infosheet_info->INFO_BORDER,
					'INFO_NP_CHECK' => $infosheet_info->INFO_NP_CHECK
				);
				$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, $update_array);
				break;
			case 'Narrow Standard' || 'Narrow Standard NP' || 'Dealer Custom 1' || 'Dealer Custom 2' || 'Dealer Custom 1 Certified':
				# code...
				$update_array = array(
					'INFO_STYLE' => $info_style,
					'INFO_NP_CHECK' => $infosheet_info->INFO_NP_CHECK
				);
				$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, $update_array);
				break;
			case 'Pure Cars 1' || 'Pure Cars 2' || 'Pure Cars 3' || 'Pure Cars 4':
				# code...
				$update_array = array(
					'INFO_STYLE' => $info_style,
					'QR_INFO_ADDRESS' => $infosheet_info->QR_INFO_ADDRESS,
					'INFO_NP_CHECK' => $infosheet_info->INFO_NP_CHECK,
					'PURECARS_URL' => $infosheet_info->PURECARS_URL
				);
				$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, $update_array);
				break;
			default:
				# code...
				break;
		}

		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/image_check.php');
		/* Load AWS S3 Libary For Upload File */

		if($request->hasFile('bg_img') && ($info_style == 'Narrow Standard' || $info_style == 'Narrow Standard NP' || $info_style == 'Dealer Custom 1' || $info_style == 'Dealer Custom 2' || $info_style == 'Dealer Custom 3' || $info_style == 'Dealer Custom 4' || $info_style == 'Dealer Custom 4 NP' || $info_style == 'Dealer Custom 1 Certified' || $info_style == 'Dealer Custom 2 Certified' || $info_style == 'Pure Cars 4')){
			$img_file = $request->bg_img;
			$tmp_name = $_FILES['bg_img']['tmp_name'];
			$name = $img_file->getClientOriginalName();
			$size = $img_file->getClientSize();
			$ext = $request->bg_img->extension();
			$image_name = time().'.'.$ext;
			/* Load AWS S3 Libary For Upload File */
			include_once(app_path().'/Libraries/s3/s3_config_1.php');
			/* Load AWS S3 Libary For Upload File */
			if(strlen($name) > 0){
				if (in_array($ext, $valid_formats)) {
					$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
					if ($result) {
						switch ($info_style) {
							case 'Narrow Standard':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_NARROW_BG' => $image_name));
								break;
							case 'Narrow Standard NP':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_NARROW_NP_BG' => $image_name));
								break;
							case 'Dealer Custom 1':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_DC1_BG' => $image_name));
								break;
							case 'Dealer Custom 2':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_DC2_BG' => $image_name));
								break;
							case 'Dealer Custom 3':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_DC3_BG' => $image_name));
								break;
							case 'Dealer Custom 4' || 'Dealer Custom 4 NP':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_DC4_BG' => $image_name));
								break;
							case 'Dealer Custom 1 Certified':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_DC1C_BG' => $image_name));
								break;
							case 'Dealer Custom 2 Certified':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_DC2C_BG' => $image_name));
								break;
							case 'Pure Cars 4':
								# code...
								$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_PURE4_BG' => $image_name));
								break;
							
							default:
								# code...
								break;
						}
					}
				}
			}
		}
		
		if($request->hasFile('logo_img') && ($info_style == 'Dealer Custom' || $info_style == 'Dealer Custom NP' || $info_style == 'Standard 3' || $info_style == 'Info Book')){
			$img_file = $request->logo_img;
			$tmp_name = $_FILES['logo_img']['tmp_name'];
			$name = $img_file->getClientOriginalName();
			$size = $img_file->getClientSize();
			$ext = $request->logo_img->extension();
			$image_name = time().'.'.$ext;
			/* Load AWS S3 Libary For Upload File */
			include_once(app_path().'/Libraries/s3/s3_config.php');
			/* Load AWS S3 Libary For Upload File */
			if(strlen($name) > 0){
				if (in_array($ext, $valid_formats)) {
					$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
					if($result){
						//Update InfoSheet Logo
						$update_id = InfoSheet::updateInfosheetInfo($user->USER_ID, $type, array('INFO_LOGO' => $image_name));
					}
				}
			}
		}

		return json_encode(array('status' => 'success'));
	}

	/**
	 * Retrieve Basic Data for Addendum setting Modal
	 *
	 * @param type?New:Used
	 * @return json array
	 *
	 */

	public function getDataForAdmSettings(Request $request){
		$type = $request->input('type');
		$user = Auth::user();
		$template_data = TemplateData::getTemplateData($user->DEALER_ID, $type);
		if(!$template_data){
			$insert_array = array(
				'DEALER_ID' => $user->DEALER_ID, 
				'TEMPLATE_TYPE' => $type, 
				'USER_ID' => '0', 
				'RIGHT_MARGIN' => '0', 
				'LEFT_MARGIN' => '0', 
				'TOP_MARGIN' => '0', 
				'BOTTOM_MARGIN' => '0', 
				'ADDENDUM_STYLE' => 'EPA Template', 
				'DEALER_LOGO' => '', 
				'QR_INFO_ADDRESS' => '', 
				'ADDENDUM_BACKGROUND' => '000000', 
				'INFO_STYLE' => '', 
				'INFO_BACKGROUND' => '#FFFFFF', 
				'INFO_LOGO' => '', 
				'INFO_BORDER' => '#000000', 
				'TEXT_COLOR' => '#000000', 
				'PURECARS_URL' => '', 
				'STANDARD_BACKGROUND' => 'branded_black', 
				'SLOGAN' => '', 
				'QR_ADDRESS' => 'http://fueleconomy.gov/m/', 
				'QR_TITLE' => '', 
				'QR_DESCRIPTION' => '', 
				'QR_BRANDED_COLOR' => 'branded_QR_black', 
				'VIN_BRANDED_COLOR' => 'branded_VIN_black',
				'MKT_PRC_TXT' => '',
				'CDJR_PRC_TXT' => '',
				'TOTAL_S_TXT' => ''
			);
			TemplateData::insertTemplateData($insert_array);
			$template_data = TemplateData::getTemplateData($user->DEALER_ID, $type);
		}
		$template_builder = TemplateBuilder::getTemplateBuilder($user->DEALER_ID, $type);
		return json_encode(array('template_data' => $template_data, 'template_builder' => $template_builder));
	}

	/**
	 *
	 * Update Addendum Settings
	 *
	 * @param type, adm_style, adm_info
	 * @return status
	 *
	 */
	public function updateAdmSettings(Request $request){
		$user = Auth::user();
		$type = $request->input('type');
		$adm_style = $request->input('adm_style');
		$adm_info = json_decode($request->input('adm_info'), true);
		$template_data = TemplateData::getTemplateData($user->DEALER_ID, $type);
		$adm_info['ADDENDUM_STYLE'] = $adm_style;
		unset($adm_info['_ID']);
		unset($adm_info['TEMPLATE_TYPE']);
		$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, $adm_info);

		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/image_check.php');
		/* Load AWS S3 Libary For Upload File */

		// Upload background image to S3 and update db
		if($request->hasFile('bg_img') && ($adm_style == 'Custom1' || $adm_style == 'Custom1p' || $adm_style == 'Custom1 QR' || $adm_style == 'Custom1 SMS' || $adm_style == 'Custom1 VIN' || $adm_style == 'Custom1 VIN No MSRP' || $adm_style == 'Custom2' || $adm_style == 'Custom3' || $adm_style == 'Custom5' || $adm_style == 'Custom6' || $adm_style == 'Custom7' || $adm_style == 'Custom7 Narrow' || $adm_style == 'Custom2 QR' || $adm_style == 'Custom2 No MSRP' || $adm_style == 'Wholesale' || $adm_style == 'Custom4' || $adm_style == 'Narrow Custom 1' || $adm_style == 'Narrow Custom 2' || $adm_style == 'Narrow Custom 2p')){
			$img_file = $request->bg_img;
			$tmp_name = $_FILES['bg_img']['tmp_name'];
			$name = $img_file->getClientOriginalName();
			$size = $img_file->getClientSize();
			$ext = $request->bg_img->extension();
			$image_name = time().'.'.$ext;
			/* Load AWS S3 Libary For Upload File */
			include_once(app_path().'/Libraries/s3/s3_config_1.php');
			/* Load AWS S3 Libary For Upload File */
			if(strlen($name) > 0){
				if (in_array($ext, $valid_formats)) {
					$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
					if ($result) {
						switch ($adm_style) {
							case 'Custom1' || 'Custom1p':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM1_BG' => $image_name));
								break;
							case 'Custom1 QR':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM1_QR_BG' => $image_name));
								break;
							case 'Custom1 SMS':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM1_SMS_BG' => $image_name));
								break;
							case 'Custom1 VIN' || 'Custom1 VIN No MSRP':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM1_VIN_BG' => $image_name));
								break;
							case 'Custom2':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM2_BG' => $image_name));
								break;
							case 'Custom3':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM3_BG' => $image_name));
								break;
							case 'Custom5':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM5_BG' => $image_name));
								break;
							case 'Custom6':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM6_BG' => $image_name));
								break;
							case 'Custom7':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM7_BG' => $image_name));
								break;
							case 'Custom7 Narrow':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM7N_BG' => $image_name));
								break;
							case 'Custom2 QR':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM2_QR_BG' => $image_name));
								break;
							case 'Custom2 No MSRP':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM2_NM_BG' => $image_name));
								break;
							case 'Wholesale':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('WHOLESALE_BG' => $image_name));
								break;
							case 'Custom4':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('CUSTOM4_BG' => $image_name));
								break;
							case 'Narrow Custom 1':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('NARROW_CUSTOM1_BG' => $image_name));
								break;
							case 'Narrow Custom 2' || 'Narrow Custom 2p':
								# code...
								$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('NARROW_CUSTOM2_BG' => $image_name));
								break;
							default:
								# code...
								break;
						}
					}
				}
			}
		}

		// Upload logo image to S3 and update db
		if($request->hasFile('logo_img') && ($adm_style == 'Narrow Branded' || $adm_style == 'Standard Branded' || $adm_style == 'Dealer Branded Simple' || $adm_style == 'Dealer Branded Savings' || $adm_style == 'Dealer Branded Custom QR' || $adm_style == 'Narrow Branded Custom QR' || $adm_style == 'Dealer Branded with VIN Barcode' || $adm_style == 'Narrow Custom 2' || $adm_style == 'Narrow Custom 2p')){
			$img_file = $request->logo_img;
			$tmp_name = $_FILES['logo_img']['tmp_name'];
			$name = $img_file->getClientOriginalName();
			$size = $img_file->getClientSize();
			$ext = $request->logo_img->extension();
			$image_name = time().'.'.$ext;
			/* Load AWS S3 Libary For Upload File */
			include_once(app_path().'/Libraries/s3/s3_config.php');
			/* Load AWS S3 Libary For Upload File */
			if(strlen($name) > 0){
				if (in_array($ext, $valid_formats)) {
					$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
					if($result){
						//Update Addendum Logo
						$update_id = TemplateData::updateTemplateData($user->DEALER_ID, $type, array('DEALER_LOGO' => $image_name));
					}
				}
			}
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Get Account Information
	 *
	 */
	public function getAccountInfo(){
		$user = Auth::user();
		$user_info = User::where('USER_ID', $user->USER_ID)->first();
		return json_encode(array('user_info' => $user_info));
	}

	/**
	 *
	 * Retrive Basic Data for Users
	 *
	 */
	public function getDataForUsers(){
		$user = Auth::user();
		$dealer_id = $user->DEALER_ID;
		$reseller_id = $user->USER_ID;
		$users = [];
		if($user->USER_TYPE == 'ResellerAdmin'){
			$users = User::where('RESELLER_ADMIN', $reseller_id)->get();
		}else{
			$users = User::where('DEALER_ID', $dealer_id)->get();
		}
		return json_encode(array('users' => $users));
	}

	/**
	 *
	 * Save New User inforamtion
	 *
	 * @param name, user_type, username, password, email, notify
	 * @return status
	 */
	public function saveUser(Request $request){
		$notify = $request->input('notify');
		$name = $request->input('name');
		$user_type = $request->input('user_type');
		$username = $request->input('username');
		$password = $request->input('password');
		$email = $request->input('email');
		$user_id = '';
		$uid = time();
        $date = date("Y-m-d");

        $cur_user = Auth::user();
		$dealer_info = Dealer::getDealerByDeaderID($cur_user->DEALER_ID);

		$reseller_id = '';
		if ($user_type == 'ResellerAdmin') {
            $dealer_id = $cur_user->USER_ID;
            $reseller_id = $cur_user->USER_ID;
        } else {
            $dealer_id = $cur_user->DEALER_ID;
        }

        $insert_array = array(
        	'USER_ID' => $uid,
        	'USER_TYPE' => $user_type,
        	'NAME' => $name,
        	'DEALER_ID' => $dealer_id,
        	'EMAIL' => $email,
        	'DEALER_GROUP' => '',
        	'USER_PHONE' => '',
        	'username' => $username,
        	'password' => $password,
        	'USER_IMAGE' => '',
        	'RESELLER_ADMIN' => $reseller_id != ''? $reseller_id : null,
        	'CREATOR_ID' => $cur_user->USER_ID,
        	'CREATE_DATE' => $date
        );
		$inserted_id = User::insertUser($insert_array);
		$name_array = explode(' ', trim($name));
		$first_name = $name_array[0];
		$last_name = '';
		if(count($name_array) > 1){
			$last_name = $name_array[1];	
		}
        $data_string1='{
		    "party": {
		    "type": "person",
		    "firstName": "'.$first_name.'",
		    "lastName": "'.$last_name.'",
		    "organisation": '.$dealer_info->CRM_ID.',
		    "emailAddresses": [
		      {
		        "type": "Work",
		        "address": "'.$email.'"
		      }
		    ],
		    "fields": [
		      {
		        "definition": { "id": 207030 } ,
		        "value": "'.$username.'"
		      },
		      {
		        "definition": { "id": 207031 } ,
		        "value": "'.$password.'"
		      }
		    ]
		  }
		}';
		$ch = curl_init('https://api.capsulecrm.com/api/v2/parties');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer '.$this->capsule_key.'',
                'Content-Type: application/json',
                'Accept: application/json',
                'Content-Length: ' . strlen($data_string1))
        );

        $result = curl_exec($ch);
        $infosheet_s_insert_array = array(
        	'_ID' => NULL,
        	'DEALER_ID' => $uid,
        	'USER_ID' => $uid,
        	'INFO_TYPE' => 'standard',
        	'INFO_STYLE' => 'KBB'
        );
        $infosheet_c_insert_array = array(
        	'_ID' => NULL,
        	'DEALER_ID' => $uid,
        	'USER_ID' => $uid,
        	'INFO_TYPE' => 'certified',
        	'INFO_STYLE' => 'KBB',
        );

        $infosheet_s_id = InfoSheet::insertInfosheetInfo($infosheet_s_insert_array);
        $infosheet_c_id = InfoSheet::insertInfosheetInfo($infosheet_c_insert_array);

        if($user_type == 'Dealer'){
        	$dealer_name = htmlspecialchars($name, ENT_QUOTES);
        	$dealer_insert_array = array(
        		'_ID' => $uid,
				'ACTIVE' => 'Yes' ,
				'DEALER_ID' => $dealer_id ,
				'DEALER_NAME' => $dealer_name ,
				'PRIMARY_CONTACT' => $dealer_name,
				'PRIMARY_CONTACT_EMAIL' => $email ,
				'DEALER_LOGO' => 'http://cars.liqueo.com/Logos/Honda_Black.png' ,
				'DEALER_ADDRESS' => '' ,
				'DEALER_CITY' => '' ,
				'DEALER_STATE' => '' ,
				'DEALER_ZIP' => '' ,
				'PHONE' => '' ,
				'BILLING_DATE' => '' ,
				'BILLING_STREET' => '' ,
				'BILLING_CITY' => '' ,
				'BILLING_STATE' => '' ,
				'BILLING_ZIP' => '' ,
				'ACCOUNT_TYPE' => 'Automatic Web' ,
				'ADDENDUM_STYLE' => 'Standard'
        	);
        	$dealer_insert_id = Dealer::insertDealerInfo($dealer_insert_array);
        }

        if($notify == 1){
        	$dealer_name = htmlspecialchars($name, ENT_QUOTES);
        	$subject = 'A new user account for ' . $dealer_name . ' has been created on DealerAddendums.com';
        	$from = array('no-reply@dealeraddendums.com' => 'DealerAddendums.com');
        	$to = array($email => $dealer_name);
        	$text = "Hello " . $name . ", Your " . $this->site_baseurl . " credentials are: Username: " . $username . " Password: " . $password . " - Allan Tone, DealerAddendums Inc Support Team";
        	$html = "Hello " . $name . ", <br><br> Your " . $this->site_baseurl . " credentials are:<br>Username: " . $username . "<br>Password: " . $password . "<br><br><strong>Allan Tone,</strong><br>DealerAddendums Inc<br>Support Team";
        	$this->_SendMail($subject, $from, $to, $html, $text);
        }

        return json_encode(array('status' => 'success', 'user_id'=>$uid));
	}

	/**
	 * Send Password to user
	 *
	 * @param user_id
	 * @return status
	 */
	public function sendPassword(Request $request){
		$user_id = $request->input('user_id');
		$cur_user = Auth::user();
		$user = User::getUser($user_id);
		$name = $user->NAME;
		$name_array = explode(' ',trim($name));
		$first_name = $name_array[0];

		$subject = 'Password Reminder from DealerAddendums.com';
		$from = array('appadmin@dealeraddendums.com' => 'Allan Tone');
		$to = array($user->EMAIL  => $user->NAME);
		$text = "Hello " . $first_name . ", Your " . $this->site_baseurl . " credentials are: Username: " . $user->username . " Password: " . $user->password . " - Allan Tone, DealerAddendums Inc Support Team";
    	$html = "Hello " . $first_name . ", <br><br> Your " . $this->site_baseurl . " credentials are:<br>Username: " . $user->username . "<br>Password: " . $user->password . "<br><br><strong>Allan Tone,</strong><br>DealerAddendums Inc<br>Support Team";
    	$this->_SendMail($subject, $from, $to, $html, $text);
    	return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Save Edit User information
	 *
	 * @param user ID, Name, Username, Password, Email, active_inactive, profile_img
	 * @return status
	 */
	public function saveEditUser(Request $request){
		$user_id = $request->input('user_id');
		$name = $request->input('name');
		$password = $request->input('password');
		$email = $request->input('email');
		$active_inactive = $request->input('active_inactive');
		$array = array(
			'NAME' => $name,
			'PASSWORD' => $password,
			'EMAIL' => $email,
			'ACTIVE_INACTIVE' => $active_inactive
		);
		$update_id = User::updateUser($user_id, $array);
		if($request->hasFile('profile_img')){
			$img_file = $request->profile_img;
			$tmp_name = $_FILES['profile_img']['tmp_name'];
			$name = $img_file->getClientOriginalName();
			$size = $img_file->getClientSize();
			$ext = $request->profile_img->extension();
			$image_name = time().'.'.$ext;
			/* Load AWS S3 Libary For Upload File */
			include_once(app_path().'/Libraries/s3/s3_config.php');
			/* Load AWS S3 Libary For Upload File */
			if(strlen($name) > 0){
				if (in_array($ext, $valid_formats)) {
					$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
					if($result){
						//Update Addendum Logo
						$update_id = User::updateUser($user_id, array('USER_IMAGE' => $image_name));
					}
				}
			}
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Delete Selected User information
	 *
	 * @param user ID
	 * @return status
	 */
	public function deleteUser(Request $request){
		$user_id = $request->input('user_id');
		User::deleteUser($user_id);
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Retrieve Basic Data for Addendum Options Tab
	 *
	 */
	public function getDataForOptions(){
		$user = Auth::user();
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$order_by = 'ITEM_NAME';
		if($dealer_info->RE_ORDER == 1){
			$order_by = 'RE_ORDER';
		}
		$information = AddendumDefault::getOptionsForTable($user->DEALER_ID, '1',$order_by);
		$all_options = AddendumDefault::getOptionsForTable($user->DEALER_ID,'',$order_by);
		$body_styles = BodyStyle::getBodyStyles($user->DEALER_ID);
		if(!$body_styles){
			$body_styles = BodyStyle::getAllBodyStyle();
		}
		$make1 = $dealer_info->MAKE1;
		$make2 = $dealer_info->MAKE2;
		$make3 = $dealer_info->MAKE3;
		$make4 = $dealer_info->MAKE4;
		$make5 = $dealer_info->MAKE5;
		$models = OptionsModelList::getModels($make1, $make2, $make3, $make4, $make5);
		
		return json_encode(array('options' => $information, 'full_options' => $all_options, 'body_styles' => $body_styles, 'models' => $models));
	}

	/**
	 *
	 * Add new Addendum Option
	 *
	 * @param option cols
	 * @return status
	 */
	public function addNewAdOption(Request $request){
		$user = Auth::user();
		$ad_name = $request->input('ad_name');
		$ad_price = $request->input('ad_price');
		$ad_desc = $request->input('ad_desc');
		$ad_type = $request->input('ad_type');
		$ad_styles = $request->input('ad_styles');
		$ad_models = $request->input('ad_models');
		$ad_ada = $request->input('ad_ada');
		$ad_adb = $request->input('ad_adb');
		if($ad_styles == ''){
			$adm_style = 'NONE';
		}
		if($ad_models == ''){
			$ad_models = 'NONE';
		}
		$ad_name = nl2br(htmlspecialchars($ad_name, ENT_QUOTES));
		$ad_desc = nl2br(htmlspecialchars($ad_desc, ENT_QUOTES));
		$ad_price = str_replace("$", "", $ad_price);
        $ad_price = str_replace(",", "", $ad_price);
        if($ad_ada == true){
        	$ad_ada = '1';
        }else{
        	$ad_ada = '0';
        }
        if($ad_adb == true){
        	$ad_adb = '1';
        }else{
        	$ad_adb = '0';
        }

        $insert_array = array(
        	'DEALER_ID' => $user->DEALER_ID,
        	'ITEM_NAME' => $ad_name,
        	'ITEM_PRICE' => $ad_price,
        	'ITEM_DESCRIPTION' => $ad_desc,
        	'MODELS' => $ad_models,
        	'BODY_STYLES' => $ad_styles,
        	'AD_TYPE' => $ad_type,
        	'SEPARATOR_BELOW' => $ad_adb,
        	'SEPARATOR_ABOVE' => $ad_ada,
        	'OG_OR_AD' => '1',
        );

        $inserted_id = AddendumDefault::insertAddendumDefault($insert_array);
        return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Update Addendum Option
	 *
	 */

	public function updateAdOption(Request $request){
		$user = Auth::user();
		$id = $request->input('ad_id');
		$items = json_decode($request->input('items'),true);
		unset($items['_ID']);
		$updated_id = AddendumDefault::updateAddendumDefault($id, $items);
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Delete Addendum Option
	 *
	 */

	public function deleteAdOption(Request $request){
		$id = $request->input('ad_id');
		AddendumDefault::deleteAddendumDefault($id);
		return json_encode(array('status' => 'success'));
	}


	/**
	 * Save Addendum Options Re Order
	 *
	 */

	public function saveAdOptionsReOrder(Request $request){
		$user = Auth::user();
		$order_ids = $request->input('order');
		$order_ids = json_decode($order_ids);
		$i = 0;
		foreach($order_ids as $id){
			AddendumDefault::updateAddendumDefault($id, array('RE_ORDER' => $i));
			$i++;
		}

		//update Dealer Inventory re-order field to 1
		Dealer::updateDealerByID($user->DEALER_ID, array('RE_ORDER' => 1 ));
		//update user last activity time
		User::updateUser($user->USER_ID, array('LAST_ACTIVITY' => date("Y-m-d H:i:s")));
		return json_encode(array('status' => 'success'));
	}


	/**
	 *
	 * Retrieve Basic Data for Addendum Group Options Tab
	 *
	 */
	public function getDataForGroupOptions(){
		$user = Auth::user();
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		$order_by = 'ITEM_NAME';
		if($dealer_info->RE_ORDER == 1){
			$order_by = 'RE_ORDER';
		}
		$information = AddendumDefault::getOptionsForTable($user->DEALER_ID, '2', $order_by);
		$options_information = AddendumDefault::getOptionsForTable($user->DEALER_ID, '1', $order_by);

		$all_options = AddendumDefault::getOptionsForTable($user->DEALER_ID,'',$order_by);

		$body_styles = BodyStyle::getBodyStyles($user->DEALER_ID);
		if(!$body_styles){
			$body_styles = BodyStyle::getAllBodyStyle();
		}
		$make1 = $dealer_info->MAKE1;
		$make2 = $dealer_info->MAKE2;
		$make3 = $dealer_info->MAKE3;
		$make4 = $dealer_info->MAKE4;
		$make5 = $dealer_info->MAKE5;
		$models = OptionsModelList::getModels($make1, $make2, $make3, $make4, $make5);
		
		return json_encode(array('groupOptions' => $information, 'options' => $options_information, 'full_options' => $all_options, 'body_styles' => $body_styles, 'models' => $models));
	}

	/**
	 *
	 * Add new Addendum Option
	 *
	 * @param option cols
	 * @return status
	 */
	public function addNewAdGroupOption(Request $request){
		$user = Auth::user();
		$ad_name = $request->input('ad_name');
		$ad_price = $request->input('ad_price');
		$ad_desc = $request->input('ad_desc');
		$ad_type = $request->input('ad_type');
		$ad_styles = $request->input('ad_styles');
		$ad_models = $request->input('ad_models');
		$ad_ada = $request->input('ad_ada');
		$ad_adb = $request->input('ad_adb');
		if($ad_styles == ''){
			$adm_style = 'NONE';
		}
		if($ad_models == ''){
			$ad_models = 'NONE';
		}
		$ad_name = nl2br(htmlspecialchars($ad_name, ENT_QUOTES));
		$ad_price = str_replace("$", "", $ad_price);
        $ad_price = str_replace(",", "", $ad_price);
        if($ad_ada == true){
        	$ad_ada = '1';
        }else{
        	$ad_ada = '0';
        }
        if($ad_adb == true){
        	$ad_adb = '1';
        }else{
        	$ad_adb = '0';
        }

        $insert_array = array(
        	'DEALER_ID' => $user->DEALER_ID,
        	'ITEM_NAME' => $ad_name,
        	'ITEM_PRICE' => $ad_price,
        	'ITEM_DESCRIPTION' => $ad_desc,
        	'MODELS' => $ad_models,
        	'BODY_STYLES' => $ad_styles,
        	'AD_TYPE' => $ad_type,
        	'SEPARATOR_BELOW' => $ad_adb,
        	'SEPARATOR_ABOVE' => $ad_ada,
        	'OG_OR_AD' => '2',
        );

        $inserted_id = AddendumDefault::insertAddendumDefault($insert_array);
        return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Get All Addendum Options for Re Order
	 */
	public function getAllAdOptions(){
		$user = Auth::user();
		$order_by = 'RE_ORDER';
		$all_options = AddendumDefault::getOptionsForTable($user->DEALER_ID,'',$order_by);
		return json_encode(array('full_options' => $all_options));
	}

	/**
	 *
	 * Get Basic Data for Order Supplies Setting
	 *
	 */
	public function getDataForOrderSupplies(){
		$user = Auth::user();
		$lprice_info = LabelPrice::getAllLabelPrice();
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		return json_encode(array('lprice_info' => $lprice_info, 'dealer_info' => $dealer_info));
	}

	/**
	 *
	 * Save Order Supplies Setting
	 *
	 * @param label_price, changeAddress, newAddress
	 * @return status
	 */
	public function saveOrderSupplies(Request $request){
		$user = Auth::user();
		$lprice = $request->input('lprice');
		$changeAddress = $request->input('changeAddress');
		$newAddress = $request->input('newAddress');
		$dealer_info = Dealer::getDealerByDeaderID($user->DEALER_ID);
		
		//informal first name generation
		$name = $dealer_info->PRIMARY_CONTACT;
        $name_array = explode(' ', trim($name));
        $first_name = $name_array[0];
        if($dealer_info->ACCOUNT_TYPE == 'Free'){
        	return json_encode(array('status' => 'free'));
        }else{
        	$lprice_info = LabelPrice::getLabelPriceByQuantity($lprice);
        	$freshbooks = new easyFreshBooksAPI();
        	
        	$freshbooks->fburl = Constants::getConstantInfoByKey('FRESHBOOKS_API_URL');
			$freshbooks->fbtoken = Constants::getConstantInfoByKey('FRESHBOOKS_AUTH_TOKEN');
        	
        	$recurring_id = $dealer_info->RECURE_ID;
        	$freshbooks->line->name = 'Addendum Supplies'; // (Optional)
            $freshbooks->line->description = $lprice .' - '.$dealer_info->DEALER_NAME; // (Optional)
            $freshbooks->line->unit_cost = $lprice_info->PRICE; // Default is 0
            $freshbooks->line->quantity = '1'; // Default is 0
            $freshbooks->addLine();

            //try to create new invoice with provided data on FB server
            $result = $freshbooks->recurringLinesAdd($recurring_id);
            $pendingLabel_id = PendingLabel::insertPendingLabel(array('RECURE_ID' => $recurring_id, 'LINE_ID' => $result->lines->line_id));

            //email user the order confirmation
            $subject = 'Addendum Labels Order Confirmation';
            $from = array("no-reply@dealeraddendums.com" => "DealerAddendums.com");
            $to = array($user->EMAIL => $user->NAME);
            $text = "Hello " . $user->NAME . ", \n We just received your order for " . $lprice . ". \n\n Your label order will be shipped to you via USPS Priority Mail within one business day. Your next invoice will include a charge for your label order in the amount of $" . $lprice_info->PRICE . " \n\nAllan Tone\n DealerAddendums Inc\n - Support Team";
            $html = "Hello " . $user->NAME . ", <br><br>We just received your order for " . $lprice . ".<br><br>Your order will be shipped to you via USPS Priority Mail within one business day. Your next invoice will include a charge for your order in the amount of $" . $lprice_info->PRICE . ". <br><br><strong>Allan Tone,</strong><br>DealerAddendums Inc<br>Support Team";

            $this->_SendMail($subject, $from, $to, $html, $text);

            //email the admin the order information
            $subject = '!! ADDENDUM ORDER !!';
            $from = array("no-reply@dealeraddendums.com" => "DealerAddendums.com");
            $to = array("appadmin@dealeraddendums.com" => "Allan Tone");
            if ($changeAddress == '0') {
                $newaddress = "<h2>" . $dealer_info->DEALER_NAME . "<br><Care Of: " . $dealer_info->PRIMARY_CONTACT . "<br>" . $dealer_info->DEALER_ADDRESS . "<br>" . $dealer_info->DEALER_CITY . ", " . $dealer_info->DEALER_STATE . ". " . $dealer_info->DEALER_ZIP . "</h2>";
            } else if ($_POST['addresschange'] == '2') {
                $newaddress = nl2br($newAddress);

            }
            $text = "" . $user->NAME . " at " . $dealer_info->DEALER_NAME . " just placed an order for " . $lprice . ". Dealers details: Name: " . $dealer_info->DEALER_NAME . ", Street: " . $dealer_info->DEALER_ADDRESS . ", CITY: " . $dealer_info->DEALER_CITY . ", State: " . $dealer_info->DEALER_STATE . ", Zip: " . $dealer_info->DEALER_ZIP . ", Phone: " . $dealer_info->PHONE . ",  - Allan Tone, DealerAddendums Inc Support Team";
            $html = "" . $user->NAME . " at " . $dealer_info->DEALER_NAME . " just placed an order for " . $lprice . ". <br><br><strong>Dealers details:</strong><br><br>" . $newaddress . "<h2>Dealer Phone: " . $dealer_info->PHONE . "<br>Dealer Email: " . $dealer_info->PRIMARY_CONTACT_EMAIL . "</h2><br><br>  - Allan Tone,<br> DealerAddendums Inc <br>Support Team";
            $this->_SendMail($subject, $from, $to, $html, $text);
            $data_string3='{
			  "task" : {
			    "description" : "Send labels to dealership - '.$dealer_info->DEALER_NAME.'",
				"category": {
			                "id": 595606
			            },
				"owner": {
			               "username":"Carol"
			            },
			    "detail" : null,
			    "dueOn" : "'.date("Y-m-d").'",
			    "dueTime" : "00:00:00"
			  }
			}';
            $ch3 = curl_init('https://api.capsulecrm.com/api/v2/tasks');
            curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch3, CURLOPT_POSTFIELDS, $data_string3);
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch3, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$this->capsule_key.'',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Content-Length: ' . strlen($data_string3))
            );

            $result3 = curl_exec($ch3);
            return json_encode(array('status' => 'success'));
        }
	}


	/**
	 *
	 * Clear Label and Freshbook Recurring Lines
	 *
	 * @param Invoice ID
	 *
	 */
	public function clearLabel(Request $request){
		$invoice_id = $request->input('object_id');
		$freshbooks = new easyFreshBooksAPI();

		$freshbooks->fburl = Constants::getConstantInfoByKey('FRESHBOOKS_API_URL');
		$freshbooks->fbtoken = Constants::getConstantInfoByKey('FRESHBOOKS_AUTH_TOKEN');

		$invoice=$freshbooks->invoiceGet($invoice_id);
		$recurring_id=$invoice->invoice->recurring_id;
		if(isset($recurring_id)){
			$pending_labels = PendingLabel::getPendingLabels($recurring_id);
			foreach($pending_labels as $pending_label){
				$result=$freshbooks->recurringLinesDelete($pending_label->RECURE_ID, $pending_label->LINE_ID);
				PendingLabel::deletePendingLabelByLineID($pending_label->LINE_ID);
			}
		}
	}

	/**
	 *
	 * Return Timestamp?
	 *
	 * @param timestamp String
	 * @return status int
	 *
	 */
	public function isTimestamp($timestamp) {
	    if(ctype_digit($timestamp) && strtotime(date('Y-m-d H:i:s',$timestamp)) === (int)$timestamp) {
	        return 1;
	    } else {
	        return 0;
		}
	}

	/**
	 *
	 *
	 * Sending Mail Funtion
	 *
	 */
	public function _SendMail($subject, $from, $to, $html, $text){
		// Create the Transport
		$transport = (new Swift_SmtpTransport('smtp.mandrillapp.com', 587))
	  		->setUsername(Constants::getConstantInfoByKey('MANDRILL_USERNAME'))
	  		->setPassword(Constants::getConstantInfoByKey('MANDRILL_PASSWORD'));

  		// Create the Mailer using your created Transport
	  	$mailer = new Swift_Mailer($transport);
	  	// Create a message
		$message = (new Swift_Message($subject))
			->setFrom($from)
			->setTo($to)
			->setBody($html,'text/html')
			->addPart($text, 'text/plain');
		return $mailer->send($message);
	}

	/**
	 *
	 * Template Send Upgrade Membership Notification
	 *
	 */
	public function _sendUpgradeNoti($email, $name, $new_account_type, $old_account_type, $first_name, $dealer_name, $dealer_id){
		/* for sending mail to dealer */
        $subject = ' ' . config('app.DOMAIN') . ' Upgrade Confirmation';
        $from = array(config('app.SYSTEM_EMAIL') => config('app.DOMAIN'));
		$to = array($email => $name);

		$text = "Hello " . $first_name . ",\n \n Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. \n\n Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from ".$old_account_type." plan to our ".$new_account_type." plan. Now we can automatically import your dealer inventory every night, making printing the next day AUTOMATIC. \n\n A " . config('app.COMPANY_NAME') . " customer onboarding representative will contact you shortly to arrange your automatic inventory feed and answer any questions you have. Please let us know if you have any questions.  Thanks and happy printing! \n\n " . config('app.SITE_SUPPORT') . "\n " . config('app.COMPANY_NAME') . "\n Support Team";
        $html = "Hello " . $first_name . ", <br><br>Congratulations!  With your subscription to " . config('app.COMPANY_NAME') . " - creating, printing, and managing your addendums has never been easier. <br><br> Your " . config('app.COMPANY_NAME') . " account has been successfully upgraded from ".$old_account_type." to our <strong>".$new_account_type."</strong> option. Now we can automatically import your dealer inventory every night, making printing the next day AUTOMATIC. <br><br> A " . config('app.COMPANY_NAME') . " customer onboarding representative will contact you shortly to arrange your automatic inventory feed and answer any questions you have. Please let us know if you have any questions.  Thanks and happy printing!<br><br><strong>" . config('app.SITE_SUPPORT') . ",</strong>  <br>" . config('app.COMPANY_NAME') . " <br>Support Team";
        $msend_result = $this->_SendMail($subject, $from, $to, $html, $text);


        /* for sending mail to addendum */
        $subject1 = ' ' . config('app.DOMAIN') . ' Upgrade Notification';
        $to1 = array(config('app.CRM_EMAIL') => config('app.SITE_AUTHOR'));
        $text1 = "Hello Admin, " . $dealer_name . " just upgraded their account from ".$old_account_type." to ".$new_account_type.". Dealers details: Name:" . $dealer_name . ", ID:" . $dealer_id . ",  - " . config('app.SITE_SUPPORT') . ", " . config('app.COMPANY_NAME') . " Support Team";
        $html1 = "Hello Admin, <br><br>" . $dealer_name . " just upgraded their account from ".$old_account_type." to ".$new_account_type.". <br>Dealers details: <br>Name:" . $dealer_name . ", <br>ID:" . $dealer_id . "<br><br>" . config('app.SITE_SUPPORT') . ", <br>" . config('app.COMPANY_NAME') . " <br>Support Team";
        $msend_result = $this->_SendMail($subject1, $from, $to1, $html1, $text1);
	}

	/**
	 *
	 * Template Send Upgrade Membership Notification
	 *
	 */
	public function _sendDowngradeNoti($email, $name, $new_account_type, $old_account_type, $first_name, $dealer_name, $dealer_id){
		/* for sending mail to dealer */
        $subject = ' ' . config('app.DOMAIN') . ' Downgrade Confirmation';
        $from = array(config('app.SYSTEM_EMAIL') => config('app.DOMAIN'));
		$to = array($email => $name);

		$text = "Hello " . $first_name . ", Your account platform successfully downgraded from ".$old_account." to ".$new_account_type.". You can still add and print unlimited vehicle addendums but your vehicles will no longer be added automatically. - " . config('app.SITE_SUPPORT') . ", " . config('app.COMPANY_NAME') . " Support Team";
        $html = "Hello " . $first_name . ", <br><br>Your account platform successfully downgraded from ".$old_account." to ".$new_account_type.". You can still add and print unlimited vehicle addendums but your vehicles will no longer be added automatically. <br><br>" . config('app.SITE_SUPPORT') . ",  <br>" . config('app.COMPANY_NAME') . " <br>Support Team";
		$msend_result = $this->_SendMail($subject, $from, $to, $html, $text);

		/* for sending mail to addendum */

		$subject1 = ' ' . config('app.DOMAIN') . ' Downgrade Notification';
		$to1 = array(config('app.CRM_EMAIL') => config('app.SITE_AUTHOR'));
		$text1 = "Hello Admin, " . $dealer_name . " just downgraded their account from ".$old_account." Web to ".$new_account_type.". Dealers details: Name:" . $dealer_name . ", ID:" . $dealer_id . ",  - " . config('app.SITE_SUPPORT') . ", " . config('app.COMPANY_NAME') . " Support Team";
        $html1 = "Hello Admin, <br><br>" . $dealer_name . " just degraded his account from ".$old_account." Web to ".$new_account_type.". <br>Dealers details: Name:" . $dealer_name . ", ID:" . $dealer_id . "<br>" . config('app.SITE_SUPPORT') . ", <br>" . config('app.COMPANY_NAME') . " Support Team";
        $msend_result = $this->_SendMail($subject1, $from, $to1, $html1, $text1);
	}



}