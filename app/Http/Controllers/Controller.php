<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    //protected $user;
    protected $site_baseurl;
    protected $data;
    protected $capsule_key;

    /**
     * Make a constructor to initialize Auth check.
     */
    public function __construct() {
        //$this->user = Auth::user();
        $this->site_baseurl = config('app.site_url');
        $this->data = array();
        $this->capsule_key = 'v6v0NhSXosze3Isonanslc68/eAlH3YUIvd1/3Po+xP+CR0AVBNRuONPsIjcMKuc';
        //view()->share('signedIn', Auth::check()); 
        //view()->share('user', $this->user); //Logged USER Information
        view()->share('base_url', $this->site_baseurl); //Site Base URL
        view()->share('latest_version_news', 'Fixed user rights to be dynamic');
        view()->share('release_version', 'v.3.70.76 - Updated 03/06/18 at 03:30pm MST');
    }

    /**
     * Rending View with Data
     * @param $url
     */
    public function _loadContent($url){
      return view($url)->with($this->data);
    }

    /**
     * Checking Iphone device
     * @param user_agent
     * @return Boolean
     */
    public function isIphone($user_agent=NULL) {
        if(!isset($user_agent)) {
            $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        }
        return (strpos($user_agent, 'iPhone') !== FALSE);
    }
}
