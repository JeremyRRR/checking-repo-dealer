<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
/* Load Model Start */
use App\User;
use App\Model\Dealer;
use App\Model\Backgrounds;
use App\Model\Stockimage;
use App\Model\UserPrivilege;
use App\Model\DealerInventory;
use App\Model\TemplateData;
use App\Model\AddendumData;
use App\Model\AddendumDefault;
use App\Model\Constants;
use App\Model\OptionsModelList;
use App\Model\InfoSheet;
use App\Model\TemplateBuilder;
use App\Model\BodyStyle;
use App\Model\LabelPrice;
use App\Model\PendingLabel;
use App\Model\VehicleBuyerGuide;
use App\Model\DefaultBuyerGuide;
/* Load Model End */

/* Load FreshBook Library */
require_once(app_path() . '/Libraries/freshbooks/freshbooks.php');
use easyFreshBooksAPI;
/* Load FreshBook Library */

/* Load SwitfMailer Library For Send Mail */
require_once(app_path().'/../vendor/swiftmailer/swiftmailer/lib/swift_required.php');
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;
/* Load SwitfMailer Library For Send Mail */



class AdminDashboardController extends Controller
{

	/**
	 *
	 */
	public function getDealerDataForDealerOverview(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$page = $request->input('page');
		$sortby = $request->input('sortBy');
		$descending = $request->input('descending');
		$rowsperpage = $request->input('rowsPerPage');
		$filter = $request->input('filter');
		$order = 'asc';
		$start = ($page-1)*$rowsperpage;
		if($descending == 0){
			$order = 'desc';
		}
		$owner_id = '';
		$group_id = '';
		if($user->USER_TYPE == 'ResellerAdmin'){
			$owner_id = $user->USER_ID;
		}elseif($user->USER_TYPE == 'ResellerUser'){
			$owner_id = $user->RESELLER_ADMIN;
		}elseif($user->USER_TYPE == 'GroupAdmin'){
			$group_id = $user->DEALER_GROUP;
		}
		$items = Dealer::getDealerData($filter, $start, $rowsperpage, $sortby, $order, $owner_id, $group_id);
		$total = Dealer::getNumDealerData($filter, $owner_id, $group_id);
		return json_encode(array('status' => 'success', 'items' => $items, 'total' => $total));
	}

	/**
	 *
	 * change Dealer Activity
	 * @param dealer_id, status
	 */
	public function changeDealerActivity(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$dealer_id = $request->input('dealer_id');
		$type = $request->input('type');
		$status = 'Yes';
		if($type == 'pause'){
			$status = 'No';
		}
		$result = Dealer::updateDealerByID($dealer_id, array('ACTIVE' => $status));
		if($result){
			return json_encode(array('status' => 'success'));
		}else{
			return json_encode(array('status' => 'fail'));
		}
	}

	/**
	 * Delete Dealer
	 * @param dealer_id
	 */
	public function deleteDealer(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$dealer_id = $request->input('dealer_id');
		$dealer_info = Dealer::getDealerByDeaderID($dealer_id);
		$dealer_users = User::getAllDealers($dealer_id);
		foreach($dealer_users as $d_user){
			if(!empty($d_user->CRM_ID) and $d_user->CRM_ID != "" and $d_user->CRM_ID != "NULL") {
				$url='https://api.capsulecrm.com/api/v2/parties/'.$d_user->CRM_ID;
				$ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Authorization: Bearer 9tsU1eNJx5V+xaGGw1/EAUokNwE/8RYdrSCjcakcmDYuW7IhWonBAlCIz3HjPVrV')
                );

                curl_exec($ch);
			}
		}
		if(!empty($dealer_info->CRM_ID) and $dealer_info->CRM_ID != "" and $dealer_info->CRM_ID != "NULL"){
			$url='https://api.capsulecrm.com/api/v2/parties/'.$dealer_info->CRM_ID;
	        $ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Authorization: Bearer 9tsU1eNJx5V+xaGGw1/EAUokNwE/8RYdrSCjcakcmDYuW7IhWonBAlCIz3HjPVrV')
	        );

	        $result = curl_exec($ch);
		}
		if(!empty($dealer_info->RECURE_ID) and $dealer_info->RECURE_ID != "" and $dealer_info->RECURE_ID != "NULL"){
			$freshbooks = new easyFreshBooksAPI();
            $recurring_id = $dealer_info['RECURE_ID'];
            $freshbooks->recurringDelete($recurring_id);
		}
		// Delete Dealer information with Related Table
		TemplateData::where('DEALER_ID', $dealer_id)->delete();
		AddendumDefault::where('DEALER_ID', $dealer_id)->delete();
		User::where('DEALER_ID', $dealer_id)->delete();
		Dealer::where('DEALER_ID', $dealer_id)->delete();
		TemplateBuilder::where('DEALER_ID', $dealer_id)->delete();
		VehicleBuyerGuide::where('DEALER_ID', $dealer_id)->delete();
		AddendumData::where('DEALER_ID', $dealer_id)->delete();
		InfoSheet::where('DEALER_ID', $dealer_id)->delete();
		DealerInventory::where('DEALER_ID', $dealer_id)->delete();
		DefaultBuyerGuide::where('DEALER_ID', $dealer_id)->delete();
		BodyStyle::where('DEALER_ID', $dealer_id)->delete();
		return json_encode(array('status' => 'success'));
	}

	/**
	 * Get selected Dealer info
	 *
	 * @param dealer_id
	 * @return Object
	 */
	public function getDealerInfo(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$dealer_id = $request->input('dealer_id');
		$dealer_info = Dealer::getDealerByDeaderID($dealer_id);
		if($dealer_info){
			return json_encode(array('status' => 'success', 'dealer_info' => $dealer_info));
		}else{
			return json_encode(array('status' => 'fail'));
		}
	}

	/** 
	 * Update Dealer Info
	 *
	 * @param dealer_id
	 * @return status
	 */
	public function updateDealerInfo(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$new_dealer_info = json_decode($request->input('dealer_info'), true);
		$id = $new_dealer_info['_ID'];
		$ori_dealer_info = Dealer::getDealerByID($id);
		unset($new_dealer_info['_ID']);
		Dealer::where('_ID', $id)->update($new_dealer_info);
		if($new_dealer_info['DEALER_ID'] != $ori_dealer_info->DEALER_ID){
			User::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			AddendumData::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			AddendumDefault::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			DealerInventory::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			TemplateData::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			DefaultBuyerGuide::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			BodyStyle::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			Dealer::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			TemplateBuilder::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			InfoSheet::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
			VehicleBuyerGuide::where('DEALER_ID', $ori_dealer_info->DEALER_ID)->update(array('DEALER_ID' => $new_dealer_info['DEALER_ID']));
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Add New Dealer
	 *
	 * @param dealer_info, type(email notify)
	 * @return status
	 */

	public function addNewDealer(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$dealer_info = json_decode($request->input('dealer_info'), true);
		$type = $request->input('type');
		$uid = time();
		if($user->USER_TYPE == 'RootAdmin'){
			$uid = User::insertGetId(array(
				'USER_ID' => $uid,
				'USER_TYPE' => 'DealerAdmin',
				'NAME' => $dealer_info['DEALER_NAME'],
				'DEALER_ID' => $dealer_info['DEALER_ID'],
				'EMAIL' => $dealer_info['PRIMARY_CONTACT_EMAIL'],
				'username' => $dealer_info['username'],
				'password' => $dealer_info['password'],
				'CREATOR_ID' => '0',
				'CREATE_DATE' => date("Y-m-d"),
				'NEW_USED_BOTH' => 'Both'
			));	
		}elseif($user->USER_TYPE == 'GroupAdmin'){
			$uid = User::insertGetId(array(
				'USER_ID' => $uid,
				'USER_TYPE' => 'DealerAdmin',
				'NAME' => $dealer_info['DEALER_NAME'],
				'DEALER_ID' => $dealer_info['DEALER_ID'],
				'EMAIL' => $dealer_info['PRIMARY_CONTACT_EMAIL'],
				'username' => $dealer_info['username'],
				'password' => $dealer_info['password'],
				'CREATOR_ID' => '0',
				'CREATE_DATE' => date("Y-m-d"),
				'LAST_LOGIN' => date("Y-m-d")
			));	
		}else{
			$uid = User::insertGetId(array(
				'USER_ID' => $uid,
				'USER_TYPE' => 'DealerAdmin',
				'NAME' => $dealer_info['DEALER_NAME'],
				'DEALER_ID' => $dealer_info['DEALER_ID'],
				'EMAIL' => $dealer_info['PRIMARY_CONTACT_EMAIL'],
				'username' => $dealer_info['username'],
				'password' => $dealer_info['password'],
				'CREATOR_ID' => '0',
				'CREATE_DATE' => date("Y-m-d"),
				'NEW_USED_BOTH' => 'Both',
				'LAST_LOGIN' => date("Y-m-d")
			));	
		}
		$admin_id = '';
		if($user->USER_TYPE == 'GroupAdmin' || $user->USER_TYPE == 'ResellerAdmin'){
			$admin_id = $user->USER_ID;
		}elseif($user->USER_TYPE == 'ResellerUser'){
			$admin_id = $user->RESELLER_ADMIN;
		}
		$d_id = Dealer::insertGetId(array(
			'ACTIVE' => 'Yes',
			'DEALER_ID' => $dealer_info['DEALER_ID'],
			'OWNER' => $admin_id,
			'DEALER_GROUP' => $user->USER_TYPE == 'GroupAdmin'?$user->DEALER_GROUP: '', 
			'DEALER_NAME' => htmlspecialchars($dealer_info['DEALER_NAME'], ENT_QUOTES),
			'PRIMARY_CONTACT' => $dealer_info['DEALER_NAME'],
			'PRIMARY_CONTACT_EMAIL' => $dealer_info['PRIMARY_CONTACT_EMAIL'],
			'DEALER_ADDRESS' => $dealer_info['DEALER_ADDRESS'],
			'DEALER_CITY' => $dealer_info['DEALER_CITY'],
			'DEALER_STATE' => $dealer_info['DEALER_STATE'],
			'DEALER_ZIP' => $dealer_info['DEALER_ZIP'],
			'DEALER_LOGO' => 'http://cars.liqueo.com/Logos/Honda_Black.png',
			'DEALER_PHONE' => $dealer_info['PRIMARY_CONTACT'],
			'BILLING_DATE' => '',
			'BILLING_STREET' => '',
			'BILLING_CITY' => '',
			'BILLING_STATE' => '',
			'BILLING_ZIP' => '',
			'ACCOUNT_TYPE' => $dealer_info['ACCOUNT_TYPE']
		));
		if($user->USER_TYPE != 'GroupAdmin'){
			$dealer_inventory_id = DealerInventory::insertGetId(array(
				'DEALER_ID' => $dealer_info['DEALER_ID'],
				'VIN_NUMBER' => '2HGFC3B96HH362096',
				'STOCK_NUMBER' => 'STOCK_TEST1',
				'YEAR' => '2017',
				'MAKE' => 'Honda',
				'MODEL' => 'Civic',
				'BODYSTYLE' => 'Coupe',
				'DOORS' => '2 door',
				'TRIM' => 'Touring',
				'EXT_COLOR' => 'White',
				'INT_COLOR' => 'BLACK&IVORY',
				'ENGINE' => 'I-4 cyl',
				'FUEL' => 'Regular Unleaded',
				'DRIVETRAIN' => 'Front-wheel Drive',
				'TRANSMISSION' => 'continuously variable automatic',
				'MILEAGE' => '10',
				'DATE_IN_STOCK' => date("Y-m-d"),
				'STATUS' => '1',
				'PRINT_STATUS' => '0',
				'MSRP' => '27100',
				'INPUT_DATE' => date("Y-m-d"),
				'NEW_USED' => 'New',
				'DESCRIPTION' => 'Description',
				'OPTIONS' => 'Air Filtration,Airbag Occupancy Sensor,Back-Up Camera,Body-Colored Front Bumper,Body-Colored Power Heated Side Mirrors',
				'HMPG' => '100',
				'CMPG' => '200',
				'CREATED_BY' => 'VIN API',
				'UPDATE_DATE' => '2HGFC3B96HH362096'
			));
			$dealer_inventory_id = DealerInventory::insertGetId(array(
				'DEALER_ID' => $dealer_info['DEALER_ID'],
				'VIN_NUMBER' => '2HGFC2F50GH562035',
				'STOCK_NUMBER' => 'STOCK_TEST2',
				'YEAR' => '2016',
				'MAKE' => 'Honda',
				'MODEL' => 'Civic',
				'BODYSTYLE' => 'Sedan',
				'DOORS' => '2 door',
				'TRIM' => 'Touring',
				'EXT_COLOR' => 'White',
				'INT_COLOR' => 'BLACK&IVORY',
				'ENGINE' => 'I-4 cyl',
				'FUEL' => 'Regular Unleaded',
				'DRIVETRAIN' => 'Front-wheel Drive',
				'TRANSMISSION' => 'continuously variable automatic',
				'MILEAGE' => '10',
				'DATE_IN_STOCK' => date("Y-m-d"),
				'STATUS' => '1',
				'PRINT_STATUS' => '0',
				'MSRP' => '20275',
				'INPUT_DATE' => date("Y-m-d"),
				'NEW_USED' => 'Used',
				'DESCRIPTION' => 'Description',
				'OPTIONS' => 'All pricing includes $790.00 for Honda cars and $830 for Honda Trucks destination/delivery charge. Pricing does not include any add on accessories',
				'HMPG' => '10000',
				'CMPG' => '20000',
				'CREATED_BY' => 'VIN API',
				'UPDATE_DATE' => '2HGFC3B96HH362096'
			));
			$dealer_addendum_defaults_id = AddendumDefault::insertGetId(array(
				'DEALER_ID' => $dealer_info['DEALER_ID'],
				'ITEM_NAME' => 'Performance Package',
				'ITEM_DESCRIPTION' => 'Nitrogen Tire Fill<br />Clear Bra<br />Wheel Locks',
				'ITEM_PRICE' => '499',
				'MODELS' => 'ALL',
				'BODY_STYLES' => 'ALL',
				'AD_TYPE' => 'Both',
				'SEPARATOR_BELOW' => '0',
				'SEPARATOR_ABOVE' => '0',
				'OG_OR_AD' => '1'
			));
			$dealer_addendum_defaults_id = AddendumDefault::insertGetId(array(
				'DEALER_ID' => $dealer_info['DEALER_ID'],
				'ITEM_NAME' => 'Window Tint',
				'ITEM_DESCRIPTION' => '3M Scotchguard window tint with lifetime scratch/peel/fade warranty',
				'ITEM_PRICE' => '299',
				'MODELS' => 'ALL',
				'BODY_STYLES' => 'ALL',
				'AD_TYPE' => 'Both',
				'SEPARATOR_BELOW' => '0',
				'SEPARATOR_ABOVE' => '0',
				'OG_OR_AD' => '1'
			));
			Dealer::where('DEALER_ID', $dealer_info['DEALER_ID'])->update(array(
				'VEHICLES_NEW' => 1,
				'NEED_NEW' => 1,
				'PLUS_TODAY_N' => 1,
				'VEHICLES_USED' => 1,
				'NEED_USED' => 1,
				'PLUS_TODAY_U' => 1,
			));
		}
		
		if($type == 'notify'){
			$subject = "New Dealer Account Created by Administrator";
			$from = array(config('app.SYSTEM_EMAIL') => config('app.DOMAIN'));
			$to = array($dealer_info['PRIMARY_CONTACT_EMAIL'] => $dealer_info['DEALER_NAME']);
			$text = "Hello " . $dealer_info['DEALER_NAME'] . ",\r\n";
			$text .= "Administrator created an account with your email. Please login to ".config('app.DOMAIN')."\r\n";
			$text .= "Username:".$dealer_info['username']."\r\n";
			$text .= "Password:".$dealer_info['password']."\r\n";
			$text .= "Thanks\r\n";
			$text .= config('app.SITE_AUTHOR')."\r\n";
			$text .= config('app.DOMAIN')."\r\n";
			$html = "Hello " . $dealer_info['DEALER_NAME'] . ",<br/>";
			$html .= "Administrator created an account with your email. Please login to ".config('app.DOMAIN')."<br/>";
			$html .= "Username:".$dealer_info['username']."<br/>";
			$html .= "Password:".$dealer_info['password']."<br/>";
			$html .= "Thanks<br/>";
			$html .= config('app.SITE_AUTHOR')."<br/>";
			$html .= config('app.DOMAIN')."<br/>";
			$msend_result = $this->_SendMail($subject, $from, $to, $html, $text);
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Change Admin Information
	 *
	 * @param Name, username, password, email
	 * @return status
	 */
	public function changeAdminInformation(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$admin_info = json_decode($request->input('admin_info'), true);
		User::where('USER_ID', $user->USER_ID)->update($admin_info);
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Get API Keys
	 *
	 * @return apiKey Array
	 */
	public function getAPIKeys(){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$api_keys = Constants::get();
		return json_encode(array('api_keys' => $api_keys));
	}

	/**
	 * Get Backgrounds
	 */
	public function getBackgrounds(){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$bg_infos = Backgrounds::get();
		return json_encode(array('bg_infos' => $bg_infos));
	}

	/**
	 * Update API Keys
	 *
	 * @param ApiKeys Array
	 * @return status
	 */
	public function updateAPIKeys(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$api_keys = json_decode($request->input('api_keys'), true);
		foreach($api_keys as $api_key){
			$id = $api_key['_ID'];
			unset($api_key['_ID']);
			Constants::where("_ID", $id)->update($api_key);
		}
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Add new Background
	 * @param Form Data
	 * @return status, _ID
	 */
	public function addNewBG(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$width_type = $request->input('bg_width_type');
		$layout_type = $request->input('bg_layout_type');
		$color = $request->input('bg_color');

		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/image_check.php');
		/* Load AWS S3 Libary For Upload File */
		if($request->hasFile('bg_url_file')){
			$img_file = $request->bg_url_file;
			$tmp_name = $_FILES['bg_url_file']['tmp_name'];
			$name = $img_file->getClientOriginalName();
			$size = $img_file->getClientSize();
			$ext = $request->bg_url_file->extension();
			$image_name = $width_type.'_'.$layout_type.'_'.$color.'.'.$ext;
			/* Load AWS S3 Libary For Upload File */
			include_once(app_path().'/Libraries/s3/s3_config_1.php');
			/* Load AWS S3 Libary For Upload File */
			if(strlen($name) > 0){
				if (in_array($ext, $valid_formats)) {
					$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
					if ($result) {
						$id = Backgrounds::insertGetId(array(
							'BG_BASE' => $width_type,
							'BG_LAYOUT' => $layout_type,
							'BG_COLOR' => $color,
							'BG_URL' => $image_name
						));
						if($id){
							$info = Backgrounds::where('_ID', $id)->first();
							return json_encode(array('status' => 'success', 'bg_info' => $info));	
						}
					}
				}
			}
			return json_encode(array('status' => 'fail'));
		}
	}


	/**
	 *
	 * Remove Background Information
	 *
	 * @param ID
	 * @return status
	 */
	public function removeBgInfo(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$id = $request->input('id');
		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/image_check.php');
		/* Load AWS S3 Libary For Upload File */

		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/s3_config_1.php');
		/* Load AWS S3 Libary For Upload File */
		$info = Backgrounds::where('_ID', $id)->first();
		Backgrounds::where('_ID', $id)->delete();
		$s3->deleteObject($bucket, $info->BG_URL);
		return json_encode(array('status' => 'success'));
	}

	/**
	 *
	 * Get Stock Images
	 *
	 */
	public function getStockImages(){
		$stock_infos = Stockimage::get();
		return json_encode(array('stock_infos' => $stock_infos));
	}

	/**
	 *
	 * Add new stock image
	 */
	public function addNewStock(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$width_type = $request->input('si_width_type');
		$layout_type = $request->input('si_layout_type');
		$si_name = $request->input('si_name');

		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/image_check.php');
		/* Load AWS S3 Libary For Upload File */
		if($request->hasFile('si_url_file')){
			$img_file = $request->si_url_file;
			$tmp_name = $_FILES['si_url_file']['tmp_name'];
			$name = $img_file->getClientOriginalName();
			$size = $img_file->getClientSize();
			$ext = $request->si_url_file->extension();
			$image_name = time().'.'.$ext;
			/* Load AWS S3 Libary For Upload File */
			include_once(app_path().'/Libraries/s3/s3_config.php');
			/* Load AWS S3 Libary For Upload File */
			if(strlen($name) > 0){
				if (in_array($ext, $valid_formats)) {
					$result = $s3->putObjectFile($tmp_name, $bucket, $image_name, 'public-read');
					if ($result) {
						$id = Stockimage::insertGetId(array(
							'SI_BASE' => $width_type,
							'SI_LAYOUT' => $layout_type,
							'SI_NAME' => $si_name,
							'SI_IMAGE' => $image_name
						));
						if($id){
							$info = Stockimage::where('_ID', $id)->first();
							return json_encode(array('status' => 'success', 'stock_info' => $info));	
						}
					}
				}
			}
			return json_encode(array('status' => 'fail'));
		}
	}

	/**
	 * Remove Stock Image
	 */

	public function removeStockImg(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));
		}
		$id = $request->input('id');
		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/image_check.php');
		/* Load AWS S3 Libary For Upload File */

		/* Load AWS S3 Libary For Upload File */
		include_once(app_path().'/Libraries/s3/s3_config.php');
		/* Load AWS S3 Libary For Upload File */
		$info = Stockimage::where('_ID', $id)->first();
		Stockimage::where('_ID', $id)->delete();
		$s3->deleteObject($bucket, $info->SI_IMAGE);
		return json_encode(array('status' => 'success'));
	}

	public function getUserInfo(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));	
		}
		User::where('USER_ID', $user->USER_ID)->first();
		return json_encode(array('status' => 'success', 'user' => $user));
	}

	public function removeDealers(Request $request){
		$user = Auth::user();
		if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin'){
			return json_encode(array('status' => 'not_admin'));	
		}
		$ids = $request->input('ids');
		$dealer_ids = json_decode($ids);
		foreach($dealer_ids as $dealer_id){
			$dealer_info = Dealer::where('_ID', $dealer_id)->first();
			$dealer_users = Dealer::where('DEALER_ID', $dealer_id)->get();
			foreach ($dealer_users as $key => $urow) {
				if(!empty($urow->CRM_ID) and $urow->CRM_ID!="" and $urow->CRM_ID!="NULL") {
					$url = 'https://api.capsulecrm.com/api/v2/parties/' . $urow->CRM_ID;
	                $ch = curl_init($url);
	                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                        'Authorization: Bearer 9tsU1eNJx5V+xaGGw1/EAUokNwE/8RYdrSCjcakcmDYuW7IhWonBAlCIz3HjPVrV')
	                );

	                curl_exec($ch);
				}
			}
			if(!empty($dealer_info->CRM_ID) and $dealer_info->CRM_ID!="" and $dealer_info->CRM_ID!="NULL"){
	            $url = 'https://api.capsulecrm.com/api/v2/parties/' . $dealer_info->CRM_ID;
	            $ch = curl_init($url);
	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                    'Authorization: Bearer 9tsU1eNJx5V+xaGGw1/EAUokNwE/8RYdrSCjcakcmDYuW7IhWonBAlCIz3HjPVrV')
	            );

	            $result = curl_exec($ch);
	            //$data=json_decode($result);
	        }
	        if (!empty($dealer_info->RECURE_ID) and $dealer_info->RECURE_ID!="" and $dealer_info->RECURE_ID!="NULL") {
				$freshbooks = new easyFreshBooksAPI();

		        $recurring_id = $dealer_info->RECURE_ID;
		        $freshbooks->recurringDelete($recurring_id);
		    }
		    TemplateData::where('DEALER_ID', $dealer_id)->delete();
		    AddendumDefault::where('DEALER_ID', $dealer_id)->delete();
		    User::where('DEALER_ID', $dealer_id)->delete();
		    Dealer::where('DEALER_ID', $dealer_id)->delete();
		    TemplateBuilder::where('DEALER_ID', $dealer_id)->delete();
		    VehicleBuyerGuide::where('DEALER_ID', $dealer_id)->delete();
		    AddendumData::where('DEALER_ID', $dealer_id)->delete();
		    InfoSheet::where('DEALER_ID', $dealer_id)->delete();
		    DealerInventory::where('DEALER_ID', $dealer_id)->delete();
		    DefaultBuyerGuide::where('DEALER_ID', $dealer_id)->delete();
		}
		return json_encode(array('status' => 'success'));
	}




	public function impersonate($id)
	{
	    $user = User::find($id);

	    // Guard against administrator impersonate
	    if($user->USER_TYPE != 'RootAdmin' && $user->USER_TYPE != 'ResellerAdmin' && $user->USER_TYPE == 'ResellerUser' && $user->USER_TYPE != 'GroupAdmin')
	    {
	    	Auth::user()->setImpersonating($user->USER_ID);
	    }
	    else
	    {
	    	flash()->error('Impersonate disabled for this user.');
	    }

	    return redirect()->back();
	}

	public function stopImpersonate()
	{
	    Auth::user()->stopImpersonating();

	    flash()->success('Welcome back!');

	    return redirect()->back();
	}

	/**
	 * Import DB and Merge
	 *
	 */
	public function getAllTables(){
		$tables = DB::select('SHOW TABLES');
		$tables_arr = array();
		foreach ($tables as $table){
			foreach($table as $key => $value){
				array_push($tables_arr, $value);
			}
		}
		return json_encode(array('status' => 'success', 'tables' => $tables_arr));
	}

	/**
	 *
	 * Import DB
	 */
	public function importDB(Request $request){
		$table_name = $request->input('table');
		if($table_name == 'options_model_list'){
			$table_name = 'model_data';
		}
		if(DB::connection('mysql2')->getSchemaBuilder()->hasTable($table_name)){
			$count = DB::connection('mysql2')->table($table_name)->count(); 
			$loop = $count / 1000 + 1;
			for($i = 0; $i < $loop; $i++){
				$rows = DB::connection('mysql2')->table($table_name)->skip($i*1000)->take($i*1000+1000)->get();
				foreach($rows as $index => $row){
					switch ($table_name) {
						case 'addendum_data':
							$exist_row = DB::table('addendum_data')->
								where('VEHICLE_ID', $row->vehicle_id)->
								where('ITEM_NAME', $row->item_name)->
								where('ITEM_PRICE', $row->item_price)->
								where('DEALER_ID', $row->DEALER_ID)->
								where('CREATION_DATE', $row->CREATION_DATE)->
								where('OR_OR_AD', $row->OG_OR_AD)->get();
							if(!count($exist_row)){
								$insert_data = array(
									'VEHICLE_ID' => $row->vehicle_id,
									'ITEM_NAME' => $row->item_name,
									'ITEM_DESCRIPTION' => $row->description,
									'ITEM_PRICE' => $row->item_price,
									'ACTIVE' => $row->active,
									'DEALER_ID' => $row->DEALER_ID,
									'CREATION_DATE' => $row->CREATION_DATE,
									'SEPARATOR_BELOW' => $row->SEPARATOR_BELOW,
									'SEPARATOR_ABOVE' => $row->SEPARATOR_ABOVE,
									'OR_OR_AD' => $row->OG_OR_AD,
									'VIN_NUMBER' => $row->VIN_NUMBER,
									'ORDER_BY' => $row->ORDER_BY,
									'SEPARATOR_SPACES' => $row->SEPARATOR_SPACES
								);
								DB::table('addendum_data')->insertGetId($insert_data);
							}
							
							break;
						case 'addendum_defaults':
							$exist_row = DB::table('addendum_defaults')->
								where('DEALER_ID', $row->DEALER_ID)->
								where('ITEM_NAME', $row->ITEM_NAME)->
								where('ITEM_PRICE', $row->PRICE)->
								where('AD_TYPE', $row->AD_TYPE)->
								where('OG_OR_AD', $row->OG_OR_AD)->get();
							if(!count($exist_row)){
								$model = '';
								if($row->MODEL_1 == 'ALL' || $row->MODEL_2 == 'ALL' || $row->MODEL_3 == 'ALL' || $row->MODEL_4 == 'ALL' || $row->MODEL_5 == 'ALL' || $row->MODEL_6 == 'ALL' || $row->MODEL_7 == 'ALL' || $row->MODEL_8 == 'ALL' || $row->MODEL_9 == 'ALL' || $row->MODEL_10 == 'ALL' || $row->MODEL_11 == 'ALL' || $row->MODEL_12 == 'ALL' || $row->MODEL_13== 'ALL' || $row->MODEL_14 == 'ALL' || $row->MODEL_15 == 'ALL' || $row->MODEL_16 == 'ALL' || $row->MODEL_17 == 'ALL' || $row->MODEL_18 == 'ALL' || $row->MODEL_19 == 'ALL' || $row->MODEL_20 == 'ALL'){
									$model = 'ALL';
								}else if(strpos($row->MODEL_1, 'NONE') !== false || strpos($row->MODEL_2, 'NONE') !== false || strpos($row->MODEL_3, 'NONE') !== false || strpos($row->MODEL_4, 'NONE') !== false || strpos($row->MODEL_5, 'NONE') !== false || strpos($row->MODEL_6, 'NONE') !== false || strpos($row->MODEL_7, 'NONE') !== false || strpos($row->MODEL_8, 'NONE') !== false || strpos($row->MODEL_9, 'NONE') !== false || strpos($row->MODEL_10, 'NONE') !== false || strpos($row->MODEL_11, 'NONE') !== false || strpos($row->MODEL_12, 'NONE') !== false || strpos($row->MODEL_13, 'NONE') !== false || strpos($row->MODEL_14, 'NONE') !== false || strpos($row->MODEL_15, 'NONE') !== false || strpos($row->MODEL_16, 'NONE') !== false || strpos($row->MODEL_17, 'NONE') !== false || strpos($row->MODEL_18, 'NONE') !== false || strpos($row->MODEL_19, 'NONE') !== false || strpos($row->MODEL_20, 'NONE') !== false ){
									$model = 'NONE';
								}else{
									$models = array();
									if($row->MODEL_1){
										array_push($models, $row->MODEL_1);
									}
									if($row->MODEL_2){
										array_push($models, $row->MODEL_2);
									}
									if($row->MODEL_3){
										array_push($models, $row->MODEL_3);
									}
									if($row->MODEL_4){
										array_push($models, $row->MODEL_4);
									}
									if($row->MODEL_5){
										array_push($models, $row->MODEL_5);
									}
									if($row->MODEL_6){
										array_push($models, $row->MODEL_6);
									}
									if($row->MODEL_7){
										array_push($models, $row->MODEL_7);
									}
									if($row->MODEL_8){
										array_push($models, $row->MODEL_8);
									}
									if($row->MODEL_9){
										array_push($models, $row->MODEL_9);
									}
									if($row->MODEL_10){
										array_push($models, $row->MODEL_10);
									}
									if($row->MODEL_11){
										array_push($models, $row->MODEL_11);
									}
									if($row->MODEL_12){
										array_push($models, $row->MODEL_12);
									}
									if($row->MODEL_13){
										array_push($models, $row->MODEL_13);
									}
									if($row->MODEL_14){
										array_push($models, $row->MODEL_14);
									}
									if($row->MODEL_15){
										array_push($models, $row->MODEL_15);
									}
									if($row->MODEL_16){
										array_push($models, $row->MODEL_16);
									}
									if($row->MODEL_17){
										array_push($models, $row->MODEL_17);
									}
									if($row->MODEL_18){
										array_push($models, $row->MODEL_18);
									}
									if($row->MODEL_19){
										array_push($models, $row->MODEL_19);
									}
									if($row->MODEL_20){
										array_push($models, $row->MODEL_20);
									}

									$model = implode(',', $models);
								}
								$body_style = '';
								if($row->BODY_STYLE_1 == 'ALL' || $row->BODY_STYLE_2 == 'ALL' || $row->BODY_STYLE_3 == 'ALL' || $row->BODY_STYLE_4 == 'ALL' || $row->BODY_STYLE_5 == 'ALL' || $row->BODY_STYLE_6 == 'ALL' || $row->BODY_STYLE_7 == 'ALL' || $row->BODY_STYLE_8 == 'ALL' || $row->BODY_STYLE_9 == 'ALL' || $row->BODY_STYLE_10 == 'ALL'){
									$body_style = 'ALL';
								}else if(strpos($row->BODY_STYLE_1, 'NONE') !== false || strpos($row->BODY_STYLE_2, 'NONE') !== false || strpos($row->BODY_STYLE_3, 'NONE') !== false || strpos($row->BODY_STYLE_4, 'NONE') !== false || strpos($row->BODY_STYLE_5, 'NONE') !== false || strpos($row->BODY_STYLE_6, 'NONE') !== false || strpos($row->BODY_STYLE_7, 'NONE') !== false || strpos($row->BODY_STYLE_8, 'NONE') !== false || strpos($row->BODY_STYLE_9, 'NONE') !== false || strpos($row->BODY_STYLE_10, 'NONE') !== false){
									$body_style = 'NONE';
								}else{
									$body_styles = array();
									if($row->BODY_STYLE_1){
										array_push($body_styles, $row->BODY_STYLE_1);
									}
									if($row->BODY_STYLE_2){
										array_push($body_styles, $row->BODY_STYLE_2);
									}
									if($row->BODY_STYLE_3){
										array_push($body_styles, $row->BODY_STYLE_3);
									}
									if($row->BODY_STYLE_4){
										array_push($body_styles, $row->BODY_STYLE_4);
									}
									if($row->BODY_STYLE_5){
										array_push($body_styles, $row->BODY_STYLE_5);
									}
									if($row->BODY_STYLE_6){
										array_push($body_styles, $row->BODY_STYLE_6);
									}
									if($row->BODY_STYLE_7){
										array_push($body_styles, $row->BODY_STYLE_7);
									}
									if($row->BODY_STYLE_8){
										array_push($body_styles, $row->BODY_STYLE_8);
									}
									if($row->BODY_STYLE_9){
										array_push($body_styles, $row->BODY_STYLE_9);
									}
									if($row->BODY_STYLE_10){
										array_push($body_styles, $row->BODY_STYLE_10);
									}
									$body_style = implode(',', $body_styles);
								}
								$insert_data = array(
									'DEALER_ID' => $row->DEALER_ID,
									'ITEM_NAME' => $row->ITEM_NAME,
									'ITEM_PRICE' => $row->PRICE,
									'ITEM_DESCRIPTION' => $row->ITEM_DESCRIPTION,
									'MODELS' => $model,
									'BODY_STYLES' => $body_style,
									'AD_TYPE' => $row->AD_TYPE,
									'SEPARATOR_BELOW' => $row->SEPARATOR_BELOW,
									'SEPARATOR_ABOVE' => $row->SEPARATOR_ABOVE,
									'OG_OR_AD' => $row->OG_OR_AD,
									'RE_ORDER' => $row->RE_ORDER,
									'SEPARATOR_SPACES' => $row->SEPARATOR_SPACES,
								);
								DB::table('addendum_defaults')->insertGetId($insert_data);
							}
							break; 
						case 'backgrounds':
							$exist_row = DB::table('backgrounds')->
								where('BG_BASE', $row->bg_base)->
								where('BG_LAYOUT', $row->bg_layout)->
								where('BG_COLOR', $row->bg_color)->
								where('BG_URL', $row->bg_url)->get();
							if(!count($exist_row)){
								$insert_data = array(
									'BG_BASE' => $row->bg_base,
									'BG_LAYOUT' => $row->bg_layout,
									'BG_COLOR' => $row->bg_color,
									'BG_URL' => $row->bg_url
								);
								DB::table('backgrounds')->insertGetId($insert_data);
							}
							break; 
						case 'body_style':
							$exist_row = DB::table('body_style')->
								where('DEALER_ID', $row->DEALER_ID)->
								where('BODY_STYLE', $row->BODY_STYLE)->
								where('ACTIVE', $row->ACTIVE)->
								where('MAKE', $row->MAKE)->get();
							if(!count($exist_row)){
								$insert_data = array(
									'DEALER_ID' => $row->DEALER_ID,
									'BODY_STYLE' => $row->BODY_STYLE,
									'ACTIVE' => $row->ACTIVE,
									'MAKE' => $row->MAKE
								);
								DB::table('body_style')->insertGetId($insert_data);
							}
							break;
						case 'count_table':
							$update_data = array(
								'old_vehicle' => $row->old_vehicle,
								'last_30_v' => $row->last_30_v,
								'last_30' => $row->last_30,
								'most_aday' => $row->most_aday,
								'print_today' => $row->print_today,
								'old_addendum' => $row->old_addendum,
								'printed' => $row->printed
							);
							DB::table('count_table')->where('id',1)->update($update_data);
							break; 
						case 'dealer_dim':
							$exist_row = DB::table('dealer_dim')->
								where('ACTIVE', $row->Active)->
								where('OWNER', $row->OWNER)->
								where('DEALER_GROUP', $row->DEALERGROUP)->
								where('DEALER_ID', $row->DEALER_ID)->
								where('DEALER_NAME', $row->DEALER_NAME)->
								where('DEALER_PHONE', $row->PHONE)->
								where('ACCOUNT_TYPE', $row->ACCOUNT_TYPE)->
								where('CLIENT_ID', $row->CLIENT_ID)->
								where('RECURE_ID', $row->RECURE_ID)->
								where('LINE_ID', $row->LINE_ID)->
								where('CRM_ID', $row->CRM_ID)->get();
							if(!count($exist_row)){
								$insert_data = array(
									'ACTIVE' => $row->Active,
									'OWNER' => $row->OWNER,
									'DEALER_GROUP' => $row->DEALERGROUP,
									'DEALER_ID' => $row->DEALER_ID,
									'DEALER_NAME' => $row->DEALER_NAME,
									'PRIMARY_CONTACT' => $row->PRIMARY_CONTACT,
									'PRIMARY_CONTACT_EMAIL' => $row->PRIMARY_CONTACT_EMAIL,
									'DEALER_LOGO' => $row->DEALER_LOGO,
									'DEALER_ADDRESS' => $row->DEALER_ADDRESS,
									'DEALER_CITY' => $row->DEALER_CITY,
									'DEALER_STATE' => $row->DEALER_STATE,
									'DEALER_ZIP' => $row->DEALER_ZIP,
									'DEALER_COUNTRY' => $row->DEALER_COUNTRY,
									'DEALER_PHONE' => $row->PHONE,
									'BILLING_DATE' => $row->BILLING_DATE,
									'BILLING_STREET' => $row->BILLING_STREET,
									'BILLING_CITY' => $row->BILLING_CITY,
									'BILLING_STATE' => $row->BILLING_STATE,
									'BILLING_ZIP' => $row->BILLING_ZIP,
									'BILLING_COUNTRY' => $row->BILLING_COUNTRY,
									'ACCOUNT_TYPE' => $row->ACCOUNT_TYPE,
									'CLIENT_ID' => $row->CLIENT_ID,
									'RECURE_ID' => $row->RECURE_ID,
									'LINE_ID' => $row->LINE_ID,
									'LAST30' => $row->LAST30,
									'REFERRED_BY' => $row->REFERRED_BY,
									'MAKE1' => $row->MAKE1,
									'MAKE2' => $row->MAKE2,
									'MAKE3' => $row->MAKE3,
									'MAKE4' => $row->MAKE4,
									'MAKE5' => $row->MAKE5,
									'LAT1' => $row->LAT1,
									'LNG1' => $row->LNG1,
									'SAVE_PDF' => $row->SAVE_PDF,
									'SMS_PREFIX' => $row->SMS_PREFIX,
									'LAST_DAY' => $row->LAST_DAY,
									'VEHICLES_NEW' => $row->vehicles_new,
									'CURRENT_NEW' => $row->current_new,
									'CURRENT_USED' => $row->current_used,
									'LAST_DATE' => $row->last_date,
									'PRINTED_NEW' => $row->printed_new,
									'PRINTED_USED' => $row->printed_used,
									'VEHICLES_USED' => $row->vehicles_used,
									'PLUS_TODAY_N' => $row->plus_today_n,
									'NEED_NEW' => $row->need_new,
									'NEED_USED' => $row->need_used,
									'PLUS_TODAY_U' => $row->plus_today_u,
									'LAST30_NEW' => $row->last30_new,
									'LAST30_USED' => $row->last30_used,
									'RE_ORDER' => $row->RE_ORDER,
									'CRM_ID' => $row->CRM_ID
								);
								DB::table('dealer_dim')->insertGetId($insert_data);
							}
							break;
						case 'dealer_inventory':
							$exist_row = DB::table('dealer_inventory')->
								where('DEALER_ID', $row->DEALER_ID)->
								where('VIN_NUMBER', $row->VIN_NUMBER)->
								where('STOCK_NUMBER', $row->STOCK_NUMBER)->
								where('YEAR', $row->YEAR)->
								where('MAKE', $row->MAKE)->
								where('MODEL', $row->MODEL)->
								where('BODYSTYLE', $row->BODYSTYLE)->
								where('MSRP', $row->MSRP)->
								where('NEW_USED', $row->NEW_USED)->get();
							if(!count($exist_row)){
								$insert_data = array();
								foreach($row as $key => $val){
									if($key != 'id'){
										$insert_data[$key] = $val;	
									}
								}
								DB::table('dealer_inventory')->insertGetId($insert_data);
							}
							break;
						case 'default_buyer_guide':
							$exist_row = DB::table('dealer_inventory');
							foreach($row as $key => $val){
								if($key != 'ID'){
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data == array();
								foreach ($row as $key => $val) {
									if($key != 'ID'){
										$insert_data[$key] = $val;
									}
								}
								DB::table('default_buyer_guide')->insertGetId($insert_data);
							}
							break;
						case 'infosheet':
							$exist_row = DB::table('infosheet');
							foreach($row as $key => $val){
								if($key != 'INFO_ID'){
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data == array();
								foreach ($row as $key => $val) {
									if($key != 'INFO_ID'){
										$insert_data[$key] = $val;
									}
								}
								DB::table('infosheet')->insertGetId($insert_data);
							}
							break;
						case 'label_price':
							$exist_row = DB::table('label_price');
							foreach($row as $key => $val){
								if($key != 'LID'){
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data == array();
								foreach ($row as $key => $val) {
									if($key != 'LID'){
										$insert_data[$key] = $val;
									}
								}
								DB::table('label_price')->insertGetId($insert_data);
							}
							break;
						case 'model_data':
							$exist_row = DB::table('options_model_list');
							foreach($row as $key => $val){
								if($key != 'MODEL_ID'){
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data == array();
								foreach ($row as $key => $val) {
									if($key != 'MODEL_ID'){
										$insert_data[$key] = $val;
									}
								}
								DB::table('options_model_list')->insertGetId($insert_data);
							}
							break;
						case 'pending_label':
							$exist_row = DB::table('pending_label');
							foreach($row as $key => $val){
								if($key != 'PLID'){
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data == array();
								foreach ($row as $key => $val) {
									if($key != 'PLID'){
										$insert_data[$key] = $val;
									}
								}
								DB::table('pending_label')->insertGetId($insert_data);
							}
							break;
						case 'sms_template':
							$exist_row = DB::table('sms_template');
							foreach($row as $key => $val){
								if($key != 'ID'){
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data == array();
								foreach ($row as $key => $val) {
									if($key != 'ID'){
										$insert_data[$key] = $val;
									}
								}
								DB::table('sms_template')->insertGetId($insert_data);
							}
							break;
						case 'stock_image':
							$exist_row = DB::table('stock_image');
							foreach($row as $key => $val){
								if($key != 'si_id'){
									$key = strtoupper($key);
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data == array();
								foreach ($row as $key => $val) {
									if($key != 'si_id'){
										$key = strtoupper($key);
										$insert_data[$key] = $val;
									}
								}
								DB::table('stock_image')->insertGetId($insert_data);
							}
							break;
						case 'template_builder':
							$exist_row = DB::table('template_builder');
							foreach($row as $key => $val){
								if($key != 'id'){
									$key = strtoupper($key);
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data = array();
								foreach ($row as $key => $val) {
									if($key != 'id'){
										$key = strtoupper($key);
										$insert_data[$key] = $val;
									}
								}
								$insert_data['SHOWLOGO'] = 1;
								DB::table('template_builder')->insertGetId($insert_data);
							}
							break;
						case 'template_data':
							$exist_row = DB::table('template_data');
							foreach($row as $key => $val){
								if($key != 'ID'){
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data = array();
								foreach ($row as $key => $val) {
									if($key != 'ID'){
										$insert_data[$key] = $val;
									}
								}
								DB::table('template_data')->insertGetId($insert_data);
							}
							break;
						case 'users':
							$exist_row = DB::table('users')->
								where('USER_ID', $row->USER_ID)->
								where('USER_TYPE', $row->USER_TYPE)->
								where('DEALER_ID', $row->DEALER_ID)->get();
							if(!count($exist_row)){
								$insert_data = array(
									'USER_ID' => $row->USER_ID,
									'USER_TYPE' => $row->USER_TYPE,
									'NAME' => $row->NAME,
									'DEALER_ID' => $row->DEALER_ID,
									'DEALER_GROUP' => $row->DEALERGROUP,
									'EMAIL' => $row->EMAIL,
									'USER_PHONE' => $row->USER_PHONE,
									'username' => $row->USERNAME,
									'password' => $row->PASSWORD,
									'CREATOR_ID' => $row->CREATOR_ID,
									'CREATE_DATE' => $row->CREATE_DATE,
									'LAST_LOGIN' => $row->LAST_LOGIN,
									'NEW_USED_BOTH' => $row->NEW_USED_BOTH,
									'PRINT_UNPRINT' => $row->PRINT_UNPRINT,
									'ACTIVE_INACTIVE' => $row->ACTIVE_INACTIVE,
									'LAST_ACTIVITY' => $row->LAST_ACTIVITY,
									'USER_IMAGE' => $row->USER_IMAGE,
									'CRM_ID' => $row->CRM_ID,
									'RESELLER_ADMIN' => $row->RESELLER_ADMIN,
									'SHIP_ADDRESS' => $row->SHIP_ADDRESS
								);
								DB::table('users')->insertGetId($insert_data);
							}
							break;
						case 'users_privilege':
							
							break;
						case 'vehicle_buyer_guide':
							$exist_row = DB::table('vehicle_buyer_guide');
							foreach($row as $key => $val){
								if($key != 'ID'){
									$key = strtoupper($key);
									$exist_row = $exist_row->where($key, $val);	
								}
							}
							$exist_row = $exist_row->get();
							if(!count($exist_row)){
								$insert_data = array();
								foreach ($row as $key => $val) {
									if($key != 'ID'){
										$key = strtoupper($key);
										$insert_data[$key] = $val;
									}
								}
								$insert_data['SHOWLOGO'] = 1;
								DB::table('vehicle_buyer_guide')->insertGetId($insert_data);
							}
							break;
						default:
							# code...
							break;
					}
				}
			}
		}
		return json_encode(array('status' => 'success', 'table_name' => $table_name));
	}

	/**
	 *
	 *
	 * Sending Mail Funtion
	 *
	 */
	public function _SendMail($subject, $from, $to, $html, $text){
		// Create the Transport
		$transport = (new Swift_SmtpTransport('smtp.mandrillapp.com', 587))
	  		->setUsername(Constants::getConstantInfoByKey('MANDRILL_USERNAME'))
	  		->setPassword(Constants::getConstantInfoByKey('MANDRILL_PASSWORD'));

  		// Create the Mailer using your created Transport
	  	$mailer = new Swift_Mailer($transport);
	  	// Create a message
		$message = (new Swift_Message($subject))
			->setFrom($from)
			->setTo($to)
			->setBody($html,'text/html')
			->addPart($text, 'text/plain');
		return $mailer->send($message);
	}

}