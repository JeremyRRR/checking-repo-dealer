<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',  'password', 'remember_token', 'PRINT_UNPRINT', 'NEW_USED_BOTH'
    ];

    /**
    * Get total number of user with dealer_id option
    *
    * @param $dealer_id = '' (string)
    * @return int
    */
    public static function getTotalUserNum($dealer_id = ''){
        if($dealer_id == ''){
            return Static::count();
        }else{
            return Static::where('DEALER_ID', $dealer_id)->count();
        }
    }

    /**
     * Get Total User Information
     *
     * @return Array
     */
    public static function getAllUsers(){
        return Static::get();
    }

    /**
     *
     * Get User Detail from User Id
     *
     * @param user_id
     * @return object
     */
    public static function getUser($user_id){
        return Static::where('USER_ID', $user_id)->first();
    }

    /**
    * Get Last Login User Information with dealer_id option
    *
    * @param $dealer_id = '' (string)
    * @return Object
    */
    public static function getLastLoginInfo($dealer_id = ''){
        $query = Static::orderBy('LAST_LOGIN','desc');
        if($dealer_id != ''){
            $query = $query->where('DEALER_ID', $dealer_id);
        }
        return $query->first();
    }


    /**
     *
     * Insert User
     *
     * @param insert_Array
     * @return inserted Id
     */
    public static function insertUser($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
    * Update User table
    *
    * @param User_id, Array
    * @return (int)id
    */
    public static function updateUser($user_id, $array){
        return Static::where('USER_ID', $user_id)->update($array);
    }

    /** 
     * Delete User Row
     *
     * @param User id
     */
    public static function deleteUser($user_id){
        Static::where('USER_ID', $user_id)->delete();
    }

    /**
     * Get All Dealer with Dealer_id
     *
     * @param dealer id
     * @return Array
     */
    public static function getAllDealers($dealer_id){
        return Static::where('DEALER_ID', $dealer_id)->get();
    }

    public function setImpersonating($id)
    {
        Session::put('impersonate', $id);
    }

    public function stopImpersonating()
    {
        Session::forget('impersonate');
    }

    public function isImpersonating()
    {
        return Session::has('impersonate');
    }
}
