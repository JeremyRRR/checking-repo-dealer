<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class VehiclePhoto extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "vehicle_photo";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'MAKE_ID', 'MODEL_ID', 'COLOR_ID', 'DEALER_ID', 'IMAGE', 'created_at', 'updated_at'
    ];

    /**
     *
     * Insert Vehicle Photo information
     *
     * @param array
     * @return inserted_ID
     */
    public static function insertVImage($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     *
     * update Vehicle Photo information
     *
     * @param _ID, array
     * @return updated_ID
     */
    public static function updateVImage($id, $update_array){
        return Static::where('_ID', $id)->update($update_array);
    }

    /**
     *
     * Get All VImage info
     */
    public static function getVImage($dealer_id = ''){
        $query = \DB::table('vehicle_photo as a')->select(\DB::raw('a.*, b.name as MAKE_NAME, c.name as MODEL_NAME, d.name as COLOR_NAME, false as edit_on'))
            ->leftJoin('make as b', 'b._ID', '=', 'a.MAKE_ID')
            ->leftJoin('model as c', 'c._ID', '=', 'a.MODEL_ID')
            ->leftJoin('color as d', 'd._ID', '=', 'a.COLOR_ID');
        if($dealer_id != ''){
            $query = $query->where('DEALER_ID', $dealer_id);
        }
        return $query->orderBy('a._ID', 'desc')->get();
    }

    /**
     *
     * Get VImage info by ID
     *
     * @param _ID
     * @return object
     */
    public static function getVImageByID($id){
        return \DB::table('vehicle_photo as a')->select(\DB::raw('a.*, b.name as MAKE_NAME, c.name as MODEL_NAME, d.name as COLOR_NAME, false as edit_on'))
            ->leftJoin('make as b', 'b._ID', '=', 'a.MAKE_ID')
            ->leftJoin('model as c', 'c._ID', '=', 'a.MODEL_ID')
            ->leftJoin('color as d', 'd._ID', '=', 'a.COLOR_ID')
            ->where('a._ID', $id)->first();
    }

    /**
     *
     * Remove Image info By ID
     */
    public static function removeVImageByID($id){
        return Static::where('_ID', $id)->delete();
    }
}