<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class BodyStyle extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "body_style";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'DEALER_ID',  'BODY_STYLE', 'ACTIVE', 'MAKE', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get all body style
     * 
     * @return array
     */
    public static function getAllBodyStyle(){
        return Static::groupBy('BODY_STYLE')->get();
    }

    /**
     * Get all Body Style from Dealer ID
     *
     * @param Dealer ID
     * @return array
     */
    public static function getBodyStyles($dealer_id){
        return Static::where('DEALER_ID',"'".$dealer_id."'")->orWhere('DEALER_ID',"''")->orWhere('BODY_STYLE','<>',"'ALL'")->groupBy('BODY_STYLE')->get();
    }
}