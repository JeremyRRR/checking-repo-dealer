<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Constants extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "constants";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'const_key', 'const_value', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get Vehicle information from vehicle id
     *
     * @param vehicle_id Int
     * @return object
     */
    public static function getConstantInfoByKey($const_key){
        $query = Static::where('const_key', $const_key)->first();
        $const_value = '';
        if($query){
            $const_value = $query->const_value;
        }
        return $const_value;
    }
}