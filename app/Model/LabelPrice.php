<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class LabelPrice extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "label_price";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'QUANTITY', 'PRICE', 'SHIPPING', 'created_at', 'updated_at'
    ];

    /**
     * Get All Label Price list
     *
     * @return Array
     */
    public static function getAllLabelPrice(){
    	return Static::orderBy('_ID', 'asc')->get();
    }

    /**
     *
     * Get Label Price Row from QUANTITY
     *
     * @param $quantity
     * @return Object
     */
    public static function getLabelPriceByQuantity($quantity){
        return Static::where('QUANTITY', $quantity)->first();
    }



}