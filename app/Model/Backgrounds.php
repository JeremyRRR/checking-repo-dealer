<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Backgrounds extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "backgrounds";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'BG_BASE', 'BG_LAYOUT', 'BG_COLOR', 'BG_URL', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get BG info from base style
     *
     * @param bg_base, bg_layout
     * @return array
     */
    public static function getBGInfoByBaseStyle($bg_base, $bg_layout){
        return Static::where('BG_BASE', $bg_base)->where('BG_LAYOUT', $bg_layout)->get();
    }
    /**
     *
     * Get BG info from base style
     *
     * @param bg_base, bg_layout
     * @return array
     */
    public static function getBGInfoByBaseStyleColor($bg_base, $bg_layout, $color){
        return Static::where('BG_BASE', $bg_base)->where('BG_LAYOUT', $bg_layout)->where('BG_COLOR', $color)->first();
    }
}