<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class VehicleBuyerGuide extends Model
{
	/**
     * The DB table name
     *
     * @var string
     */
    protected $table = "vehicle_buyer_guide";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID', 'VEHICLE_ID', 'DEFAULT_GUIDE', 'LABEL_ASIS', 'SC_ASIS', 'LABEL_IMPLIED', 'SC_IMPLIED', 'LABEL_WARRANTY', 'SC_WARRANTY', 'WARRANTY_TYPE', 'PARTS', 'LABOR', 'SYSTEMS_COVERED', 'DURATION', 'LABEL_WARRANTY_S', 'SC_WARRANTY_S', 'WARRANTY_TYPE_S', 'PARTS_S', 'LABOR_S', 'SYSTEMS_COVERED_S', 'DURATION_S', 'M_WARRANTY', 'UV_WARRANTY', 'OUV_WARRANTY', 'DEALER_ID', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get sms template by user ID
     *
     * @param user_id
     * @return object
     *
     */
    public static function getVehicleBuyerGuideByVehicleID($vid){
    	return Static::where('VEHICLE_ID', $vid)->first();
    }

    /**
     *
     * Insert Vehicle Buyer Guide
     *
     * @param insert fields array
     * @return vbg_id
     *
     */
    public static function insertVehicleBuyerGuide($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     *
     * Update Vehicle Buyer Guide
     *
     * @param vbg_id, update fields array
     * @return vbg_id
     */
    public static function updateVehicleBuyerGuide($id, $update_array){
        return Static::where('_ID',$id)->update($update_array);
    }
 }