<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class TemplateBuilder extends Model
{
    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "template_builder";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
        '_ID',  'DEALER_ID', 'TEMPLATE_NAME', 'TEMPLATE_CREATE', 'TEMPLATE_WIDTH', 'TEMPLATE_LAYOUT', 'BORDER', 'BACKGROUND', 'DEALER_LOGO', 'INFO_IMAGE', 'SHOWSTOCK', 'SHOWVIN', 'SHOWYEAR', 'SHOWMAKE', 'SHOWMODEL', 'SHOWCOLOR', 'SHOWTRIM', 'SHOWMILEAGE', 'STOCKDESC', 'VINDESC', 'YEARDESC', 'MAKEDESC', 'MODELDESC', 'COLORDESC', 'TRIMDESC', 'MILEAGEDESC', 'STOCKBOLD', 'VINBOLD', 'YEARBOLD', 'MAKEBOLD', 'MODELBOLD', 'COLORBOLD', 'TRIMBOLD', 'MILEAGEBOLD', 'VI_FONT', 'SHOWMSRP', 'SHOWSECTION', 'SHOWSUBTOTAL', 'SHOWTOTAL', 'MSRPDESC', 'SECTIONDESC', 'SUBTOTALDESC', 'TOTALDESC', 'MSRP_FONT', 'SECTION_FONT', 'SUBTOTAL_FONT', 'TOTAL_FONT', 'OPTION_FONT', 'OPDESC_FONT', 'MSRPBOLD', 'SECTIONBOLD', 'SUBTOTALBOLD', 'TOTALBOLD', 'OPTIONBOLD', 'OPDESCBOLD', 'TEMPLATE_FOR', 'ADDRESS', 'STOCK_SELECT', 'QR_TITLE', 'QR_TEXT', 'QR_URL', 'BAR_TITLE', 'BAR_TEXT', 'QR_FOOTER', 'BAR_FOOTER', 'BACKGROUND_IMAGE', 'QR_TEXT2', 'created_at', 'updated_at', 'LOGO_POSITION', 'VINFO_POSITION', 'OPTIONS_POSITION', 'SUBTOTAL_POSITION', 'TOTAL_POSITION', 'ADDRESS_POSITION', 'FOOTER_POSITION', 'FOOTER_HEIGHT', 'SHOWLOGO'];

    /**
     * Get template rows from dealer_id
     *
     * @param $dealer_id (int)
     * @return Object
     */
    public static function getTemplateBuilder($dealer_id, $type = 'Both'){
    	return Static::where(function($query) use($dealer_id){
            $query->where('DEALER_ID','')
                ->orWhere('DEALER_ID',$dealer_id);
        })->where(function($query) use($type){
            if($type == 'New'){
                $query->where('TEMPLATE_FOR','New')
                ->orWhere('TEMPLATE_FOR','Both');
            }elseif($type == 'Used'){
                $query->where('TEMPLATE_FOR','Used')
                ->orWhere('TEMPLATE_FOR','Both');
            }else{
                $query->where('TEMPLATE_FOR','New')
                 ->orWhere('TEMPLATE_FOR','Both')
                 ->orWhere('TEMPLATE_FOR','Use Later');
            }
        })->get();
    }

    /**
     *
     * Get template row from dealer_id, template_name
     *
     * @param dealer_id, template_name
     * @return Object
     */
    public static function getTemplateBuilderRow($dealer_id, $temp_name){
        return \DB::table('template_builder as a')->selectRaw('a.*, b.BG_URL as BORDER_URL')
        ->leftJoin('backgrounds as b', function($q){
            $q->on('b.BG_BASE', '=', 'a.TEMPLATE_WIDTH');
            $q->on('b.BG_LAYOUT', '=', 'a.TEMPLATE_LAYOUT');
            $q->on('b.BG_COLOR', '=', 'a.BORDER');
        })
        ->where(function($query) use($dealer_id){
            $query->where('DEALER_ID','')
                ->orWhere('DEALER_ID',$dealer_id);
        })->where('TEMPLATE_NAME', $temp_name)->first();
    }

    public static function getTemplateBuilderByDID($dealer_id){
        return Static::where('DEALER_ID', $dealer_id)->get();
    }

    public static function insertTemplateData($data){
        return Static::insertGetId($data);
    }

    public static function updateTemplate($id, $data){
        return Static::where('_ID', $id)->update($data);
    }

    public static function getTemplateDataByID($id){
        return Static::where('_ID', $id)->first();
    }


}