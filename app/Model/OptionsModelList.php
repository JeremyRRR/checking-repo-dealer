<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class OptionsModelList extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "options_model_list";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'MODEL',  'MAKE', 'ACTIVE', 'IMAGE', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get Vehicle information from vehicle id
     *
     * @param vehicle_id Int
     * @return array
     */
    public static function getAllMake(){
        return Static::select('MAKE')->groupBy('MAKE')->get();
    }

    /**
     *
     * Get Models from Dealer MAKES
     *
     * @param Dealer Models
     * @return array
     */
    public static function getModels($make1, $make2, $make3, $make4, $make5){
        $q = Static::select('MODEL');
        if(!empty($make1) && $make1 != ''){
            $q->where('MAKE', $make1);
        }
        if(!empty($make2) && $make2 != ''){
            $q->orWhere('MAKE', $make2);
        }
        if(!empty($make3) && $make3 != ''){
            $q->orWhere('MAKE', $make3);
        }
        if(!empty($make4) && $make4 != ''){
            $q->orWhere('MAKE', $make4);
        }
        if(!empty($make5) && $make5 != ''){
            $q->orWhere('MAKE', $make5);
        }
        return $q->groupBy('MODEL')->get();
    }
}