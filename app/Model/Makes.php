<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Makes extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "make";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'NAME', 'created_at', 'updated_at'
    ];

    public static function getAllMake(){
        return Static::get();
    }
}