<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class SMSTemplate extends Model
{
	/**
     * The DB table name
     *
     * @var string
     */
    protected $table = "sms_template";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID', 'TEMPLATE', 'COLOR', 'USER_ID', 'SMS_PREFIX', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'TOP_MARGIN', 'BOTTOM_MARGIN', 'LOGO', 'CUSTOM1_BG', 'SMS_CODE_CHECK', 'LOGO_CHECK', 'INFO_CHECK', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get sms template by user ID
     *
     * @param user_id
     * @return object
     *
     */
    public static function getSMSTemplateByUserID($user_id){
    	return Static::where('USER_ID', $user_id)->first();
    }
}