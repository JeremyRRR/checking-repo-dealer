<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Models extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "model";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'MAKE_ID', 'NAME', 'created_at', 'updated_at'
    ];

    
    public static function getAllModel(){
        return Static::get();
    }

    public static function getModelsByMAKE($make_id){
        return Static::where('MAKE_ID', $make_id)->get();
    }
}