<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Colors extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "color";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'NAME', 'HEX','created_at', 'updated_at'
    ];

    
    public static function getAllColor(){
        return Static::get();
    }

}