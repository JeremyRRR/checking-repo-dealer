<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class AddendumData extends Model
{
    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "addendum_data";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'VEHICLE_ID', 'ITEM_NAME', 'ITEM_DESCRIPTION', 'ITEM_PRICE', 'ACTIVE', 'DEALER_ID', 'CREATION_DATE', 'SEPARATOR_BELOW', 'SEPARATOR_ABOVE', 'OR_OR_AD', 'VIN_NUMBER', 'ORDER_BY', 'SEPARATOR_SPACES', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get Addendum Information from _ID
     * @param _ID
     * @return object
     */

    public static function getAddendumDataByID($id){
        return Static::where('_ID',$id)->first();
    }


    /**
     * Get Number of Addendum from dealer_id, vin_number
     *
     * @param $dealer_id (int), vin_number String
     * @return Int
     */
    public static function getNumberAddendumData($dealer_id, $vin_number){
    	return Static::where('DEALER_ID',$dealer_id)->where('VIN_NUMBER',$vin_number)->count();
    }

    /**
     * Get Number of Addendum from dealer_id, vin_number
     *
     * @param $dealer_id (int), vin_number String
     * @return Int
     */
    public static function getNumberAddendumDataByItemName($dealer_id, $vin_number, $item_name){
        return Static::where('DEALER_ID',$dealer_id)->where('VIN_NUMBER',$vin_number)->where('ITEM_NAME',$item_name)->count();
    }

    /**
     * Insert Addendum Data
     *
     * @param array
     * @return inserted _ID
     */
    public static function insertAddendumData($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     * Update Addendum Data
     *
     * @param _ID, array
     * @return updated _ID
     */
    public static function updateAddendumData($id, $update_array){
        return Static::where('_ID', $id)->update($update_array);
    }

    /**
     *
     * Get Sum of item_price from dealer_id, vin_number
     *
     * @param dealer_id, vin_number
     * @return object
     */
    public static function getSumItemPrice($dealer_id, $vin_number){
        return Static::select(\DB::raw('SUM(CAST(item_price AS DECIMAL(8,2))) as options'))
            ->where('DEALER_ID', $dealer_id)
            ->where('VIN_NUMBER', $vin_number)->first();
    }

    /**
     *
     * Get Options Data from dealer_id, vin_number
     *
     * @param dealer_id, vin_number, $order_by
     * @return array
     */
    public static function getOptionsForTable($dealer_id, $vin_number, $order_by){
        return Static::where('DEALER_ID', $dealer_id)
            ->where('VIN_NUMBER', $vin_number)->orderBy($order_by,'asc')->get();
    }

    /**
     *
     * Delete Addendum row from _ID
     *
     * @param _ID
     */
    public static function deleteAddendumData($id){
        return Static::where('_ID',$id)->delete();
    }

}