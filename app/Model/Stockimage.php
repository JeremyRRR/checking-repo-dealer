<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Stockimage extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "stock_image";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'SI_BASE', 'SI_LAYOUT', 'SI_IMAGE', 'SI_NAME', 'DEALER_ID', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get BG info from base style
     *
     * @param bg_base, bg_layout
     * @return array
     */
    public static function getSIInfoByBaseStyle($base, $layout){
        return Static::where('SI_BASE', $base)->where('SI_LAYOUT', $layout)->get();
    }
}