<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class UserPrivilege extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "users_privilege";

    /**
     * Get privilege information from User Type
     *
     * @param $user_type (string)
     * @return Object
     */
    public static function getPrivilegeByUserType($user_type){
    	return Static::where('USER_TYPE',$user_type)->first();
    }

}