<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class DefaultBuyerGuide extends Model
{
	/**
     * The DB table name
     *
     * @var string
     */
    protected $table = "default_buyer_guide";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID', 'DEALER_ID', 'DEFAULT_GUIDE', 'LABEL_ASIS', 'SC_ASIS', 'LABEL_IMPLIED', 'SC_IMPLIED', 'LABEL_WARRANTY', 'SC_WARRANTY', 'WARRANTY_TYPE', 'PARTS', 'LABOR', 'SYSTEMS_COVERED', 'DURATION', 'LABEL_WARRANTY_S', 'SC_WARRANTY_S', 'WARRANTY_TYPE_S', 'PARTS_S', 'LABOR_S', 'SYSTEMS_COVERED_S', 'DURATION_S', 'M_WARRANTY', 'UV_WARRANTY', 'OUV_WARRANTY', 'PHONE', 'EMAIL', 'COMPLAINTS', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get sms template by user ID
     *
     * @param user_id
     * @return object
     *
     */
    public static function getDefaultBuyerGuideByDealerID($dealer_id){
    	return Static::where('DEALER_ID', $dealer_id)->first();
    }

    /**
     *
     * Insert DefaultBuyerGuide
     *
     * @param array
     * @return Inserted_id
     */
    public static function InsertDefaultBuyerGuide($array){
        return Static::insertGetId($array);
    }

    /**
     *
     * Update DefaultBuyerGuide
     *
     * @param dbg_id, array
     * @return updated_id
     */
    public static function updateDefaultBuyerGuide($id, $array){
        return Static::where('_ID', $id)->update($array);
    }
 }