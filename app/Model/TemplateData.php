<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class TemplateData extends Model
{
    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "template_data";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'DEALER_ID', 'TEMPLATE_TYPE', 'USER_ID', 'RIGHT_MARGIN', 'LEFT_MARGIN', 'TOP_MARGIN', 'BOTTOM_MARGIN', 'ADDENDUM_STYLE', 'DEALER_LOGO', 'QR_INFO_ADDRESS', 'ADDENDUM_BACKGROUND', 'INFO_STYLE', 'INFO_BACKGROUND', 'INFO_LOGO', 'INFO_BORDER', 'TEXT_COLOR', 'PURECARS_URL', 'STANDARD_BACKGROUND', 'SLOGAN', 'QR_ADDRESS', 'QR_TITLE', 'QR_DESCRIPTION', 'QR_BRANDED_COLOR', 'VIN_BRANDED_COLOR', 'created_at', 'updated_at'
    ];

    /**
     * Get template row from dealer_id, TEMPLATE_TYPE
     *
     * @param $dealer_id (int), TEMPLATE_TYPE String
     * @return Object
     */
    public static function getTemplateData($dealer_id, $template_type){
    	return Static::where('DEALER_ID',$dealer_id)->where('TEMPLATE_TYPE',$template_type)->first();
    }

    /**
     * Insert Basic template Data
     *
     * @param array
     * @return inserted _ID
     */
    public static function insertTemplateData($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     * Update template Data
     *
     * @param array, dealer_id, type
     * @return updated _ID
     */
    public static function updateTemplateData($dealer_id, $type, $update_array){
        return Static::where('DEALER_ID',$dealer_id)->where('TEMPLATE_TYPE', $type)->update($update_array);
    }

}