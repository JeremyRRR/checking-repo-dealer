<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class PendingLabel extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "pending_label";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'RECURE_ID', 'LINE_ID', 'created_at', 'updated_at'
    ];

    /**
     *
     * Insert Pending Label Row
     *
     * @param insert_array
     * @return inserted id
     */
    public static function insertPendingLabel($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     *
     * Get Pending Labels from Recure ID
     *
     * @param Recure ID
     * @return Array
     */
    public static function getPendingLabels($recure_id){
        return Static::where('RECURE_ID', $recure_id)->get();
    }

    /**
     *
     * Delete Pending Label by Line ID
     *
     * @param LINE ID
     *
     */
    public static function deletePendingLabelByLineID($line_id){
        Static::where('LINE_ID', $line_id)->delete();
    }


}