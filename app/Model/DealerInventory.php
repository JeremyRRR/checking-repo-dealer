<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class DealerInventory extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "dealer_inventory";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'EDIT_STATUS',  'EDIT_DATE', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get Vehicle information from vehicle id
     *
     * @param vehicle_id Int
     * @return object
     */
    public static function getVehicleInfoByID($vid){
        return Static::where('_ID', $vid)->first();
    }

    /**
     * Get total number of dealer_inventory with New/Used, Stock Date, PRINT_STATUS, Status option
     *
     * @param $new_used (string), $stock_date = '' (date string), $print_status='' (int), Status = 1 (int)
     * @return int
     */
    public static function getCountInventoryByNU($new_used, $print_status = '', $stock_date = '', $status = 1){

    	$query = Static::where('NEW_USED', $new_used);
        if($status != ''){
            $query = $query->where('STATUS',$status);
        }
        if($print_status != ''){
            $query = $query->where('PRINT_STATUS', $print_status);
        }
        if($stock_date != ''){
            $query = $query->where('DATE_IN_STOCK', $stock_date);
        }
        return $query->count();
    }

    /**
     * 
     * Get total number of dealer_inventory for reselleradmin
     *
     */
    public static function getCountInventoryResellerAdmin($reseller_id, $date = '', $print_status = '', $status = 1){
        $query = Static::leftJoin('dealer_dim', 'dealer_dim.DEALER_ID', '=', 'dealer_inventory.DEALER_ID')
            ->where('dealer_dim.OWNER', $reseller_id);
            if($date != ''){
                $query = $query->where('dealer_inventory.DATE_IN_STOCK', $date);
            }
            if($print_status != ''){
                $query = $query->where('dealer_inventory.PRINT_STATUS', $print_status);   
            }
            if($status != ''){
                $query = $query->where('dealer_inventory.STATUS', $status);
            }
        return $query->count();
    }


    /**
     * 
     * Get total number of dealer_inventory for groupAdmin
     *
     */
    public static function getCountInventoryGroupAdmin($dealer_group, $date = '', $print_status = '', $status = 1){
        $query = Static::leftJoin('dealer_dim', 'dealer_dim.DEALER_ID', '=', 'dealer_inventory.DEALER_ID')
            ->where('dealer_dim.DEALER_GROUP', $dealer_group);
            if($date != ''){
                $query = $query->where('dealer_inventory.DATE_IN_STOCK', $date);
            }
            if($print_status != ''){
                $query = $query->where('dealer_inventory.PRINT_STATUS', $print_status);   
            }
            if($status != ''){
                $query = $query->where('dealer_inventory.STATUS', $status);
            }
        return $query->count();
    }

   

    /**
     * Get total number of dealer_inventory with New/Used, Print_date option
     *
     * @param $new_used (string), $start_date (string), $end_date (string)
     * @return int
     */
    public static function getCountInventoryByNUandPrintDate($new_used, $start_date, $end_date){
        return Static::where('NEW_USED', $new_used)->whereBetween('PRINT_DATE',[$start_date, $end_date])->count();
    }

    /**
     * Get total number of dealer_inventory with New/Used, Print_date option for reselleradmin
     *
     * @param $reseller_id, $start_date (string), $end_date (string)
     * @return int
     */
    public static function getCountInventorywithlast30GroupAdmin($dealer_group, $start_date, $end_date, $flag = 0){
        $query = Static::leftJoin('dealer_dim', 'dealer_dim.DEALER_ID', '=', 'dealer_inventory.DEALER_ID')
            ->where('dealer_dim.DEALER_GROUP', $dealer_group);
        if($flag == 0){
            $query = $query->whereBetween('PRINT_DATE',[$start_date, $end_date]);
        }else{
            $query = $query->whereBetween('INPUT_DATE',[$start_date, $end_date]);
        }

        return $query->count();
    }

    /**
     * Get total number of dealer_inventory with New/Used, Print_date option for GroupAdmin
     *
     * @param $reseller_id, $start_date (string), $end_date (string)
     * @return int
     */
    public static function getCountInventorywithlast30ResellerAdmin($reseller_id, $start_date, $end_date, $flag = 0){
        $query = Static::leftJoin('dealer_dim', 'dealer_dim.DEALER_ID', '=', 'dealer_inventory.DEALER_ID')
            ->where('dealer_dim.OWNER', $reseller_id);
        if($flag == 0){
            $query = $query->whereBetween('PRINT_DATE',[$start_date, $end_date]);
        }else{
            $query = $query->whereBetween('INPUT_DATE',[$start_date, $end_date]);
        }

        return $query->count();
    }


    
    /**
     * Get Vehicle Data with several options
     *
     * @param DEALER_ID, PRINT_UNPRINT, ACTIVE_INACTIVE, NEW_USED_BOTH, start, limit, sort
     * @return Array
     */
    public static function getVehicleData($dealer_id, $print_unprint, $active_inactive, $new_used_both, $filter, $start, $limit, $sortField, $order){
        $return_result = array();
        $query = Static::select(\DB::raw("*, CONVERT(REPLACE(`MSRP`, ',', ''), DECIMAL) AS `MSRP`"))->where('DEALER_ID', $dealer_id);
        if($print_unprint == 1){
            $query = $query->where(function($q){
                $q->where('PRINT_STATUS','1')->orWhere('PRINT_GUIDE','1');
            });
        }elseif($print_unprint == 2){
            $query = $query->where(function($q){
                $q->where('PRINT_STATUS','0')->where('PRINT_GUIDE','0');
            });
        }
        if($active_inactive == 1){
            $query = $query->where('STATUS','1');
        }elseif($active_inactive == 0){
            $query = $query->where('STATUS','0');
        }
        if($new_used_both == 'New'){
            $query = $query->where('NEW_USED', 'New');
        }elseif($new_used_both == 'Used'){
            $query = $query->where('NEW_USED', 'Used');
        }
        if($filter != ''){
            $filter_arr = explode(',', $filter);
            $query = $query->where(function($q) use ($filter_arr){
                $q->where('STOCK_NUMBER','like','%'.$filter_arr[0].'%')
                        ->orWhere('VIN_NUMBER','like','%'.$filter_arr[0].'%')
                        ->orWhere('YEAR','like','%'.$filter_arr[0].'%')
                        ->orWhere('MAKE','like','%'.$filter_arr[0].'%')
                        ->orWhere('MODEL','like','%'.$filter_arr[0].'%');  
            });
        }
        $query = $query->skip($start)->take($limit);

        if($sortField != ''){
            $query = $query->orderBy($sortField, $order);
        }
        $return_result = array_merge($return_result, $query->get()->toArray());
        $query = null;
        if($filter != ''){
            $filter_arr = explode(',', $filter);
            if(count($filter_arr) > 1){
                foreach($filter_arr as $index => $filter_item){
                    if($index > 0 && $index < 3){
                        $query = Static::where('DEALER_ID', $dealer_id);
                        if($print_unprint == 1){
                            $query = $query->where(function($q){
                                $q->where('PRINT_STATUS','1')->orWhere('PRINT_GUIDE','1');
                            });
                        }elseif($print_unprint == 2){
                            $query = $query->where(function($q){
                                $q->where('PRINT_STATUS','0')->where('PRINT_GUIDE','0');
                            });
                        }
                        if($active_inactive == 1){
                            $query = $query->where('STATUS','1');
                        }elseif($active_inactive == 0){
                            $query = $query->where('STATUS','0');
                        }
                        if($new_used_both == 'New'){
                            $query = $query->where('NEW_USED', 'New');
                        }elseif($new_used_both == 'Used'){
                            $query = $query->where('NEW_USED', 'Used');
                        }
                        if($filter_item != ''){
                            $query = $query->where(function($q) use ($filter_item){
                                $q->where('STOCK_NUMBER','like','%'.$filter_item.'%')
                                        ->orWhere('VIN_NUMBER','like','%'.$filter_item.'%')
                                        ->orWhere('YEAR','like','%'.$filter_item.'%')
                                        ->orWhere('MAKE','like','%'.$filter_item.'%')
                                        ->orWhere('MODEL','like','%'.$filter_item.'%');  
                            });
                        }
                        $query = $query->skip($start)->take($limit);

                        if($sortField != ''){
                            $query = $query->orderBy($sortField, $order);
                        }
                        $return_result = array_merge($return_result, $query->get()->toArray());
                        $query = null;
                    }
                }
            }
        }
        
        return $return_result;
    }

    /**
     * Get Total Number of Vehicle Data with several options
     *
     * @param DEALER_ID, PRINT_UNPRINT, ACTIVE_INACTIVE, NEW_USED_BOTH
     * @return int
     */
    public static function getNumVehicleData($dealer_id, $print_unprint, $active_inactive, $new_used_both, $filter){
        $query = Static::where('DEALER_ID', $dealer_id);
        if($print_unprint == 1){
            $query = $query->where(function($q){
                $q->where('PRINT_STATUS','1')->orWhere('PRINT_GUIDE','1');
            });
        }elseif($print_unprint == 2){
            $query = $query->where(function($q){
                $q->where('PRINT_STATUS','0')->where('PRINT_GUIDE','0');
            });
        }
        if($active_inactive == 1){
            $query = $query->where('STATUS','1');
        }elseif($active_inactive == 0){
            $query = $query->where('STATUS','0');
        }
        if($new_used_both == 'New'){
            $query = $query->where('NEW_USED', 'New');
        }elseif($new_used_both == 'Used'){
            $query = $query->where('NEW_USED', 'Used');
        }
        if($filter != ''){
            $filter_arr = explode(',', $filter);
            $query = $query->where(function($q) use ($filter_arr){
                $q = $q->where('STOCK_NUMBER','like','%'.$filter_arr[0].'%')
                        ->orWhere('VIN_NUMBER','like','%'.$filter_arr[0].'%')
                        ->orWhere('YEAR','like','%'.$filter_arr[0].'%')
                        ->orWhere('MAKE','like','%'.$filter_arr[0].'%')
                        ->orWhere('MODEL','like','%'.$filter_arr[0].'%');  
                foreach($filter_arr as $index => $filter_item){
                    if($index > 0){
                        if($filter_item != ''){
                            $q = $q->orWhere('STOCK_NUMBER','like','%'.$filter_item.'%')
                            ->orWhere('VIN_NUMBER','like','%'.$filter_item.'%')
                            ->orWhere('YEAR','like','%'.$filter_item.'%')
                            ->orWhere('MAKE','like','%'.$filter_item.'%')
                            ->orWhere('MODEL','like','%'.$filter_item.'%');    
                        }    
                    }
                }
                return $q;
            });
        }
        return $query->count();
    }



    /**
     *
     * Get Total Number of Printed Vehicle
     *
     * @return int
     */
    public static function getTotalPrintCount($dealer_id){
        $count = 0;
        $query = Static::where('DEALER_ID', $dealer_id);
        $count += $query->where('PRINT_STATUS',1)->count();
        $count += $query->where('PRINT_GUIDE', 1)->count();
        $count += $query->where('PRINT_INFO', 1)->count();
        return $count;
    }

    /**
     *
     * Get Vehicle Data from Dealer ID and Stock Number
     *
     * @param dealer_id, stock_number
     * @return object
     *
     */
    public static function getVehicleDataByDealerIDandStock($dealer_id, $stock_number){
        return Static::where('DEALER_ID', $dealer_id)->where('STOCK_NUMBER', $stock_number)->first();
    }

    /**
     *
     * Update Dealer Inventory Table
     *
     * @param $vehicle_id, array
     * @return _ID
     */
    public static function updateInventoryData($vid, $array){
        return Static::where('_ID', $vid)->update($array);
    }

    /**
     *
     * Insert Dealer Inventory Table
     *
     * @param array
     * @return _ID
     *
     */
    public static function insertInventoryData($array){
        return Static::insertGetId($array);
    }

    /**
     *
     * Delete Dealer Inventory Data that inserted from Excel
     *
     * @param dealer_id
     *
     */
    public static function deleteInventoryExcelData($dealer_id){
        Static::where('DEALER_ID', $dealer_id)->where('CREATED_BY','EXCEL')->delete();
    }
}