<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class InfoSheet extends Model
{
	/**
     * The DB table name
     *
     * @var string
     */
    protected $table = "infosheet";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'DEALER_ID', 'USER_ID', 'INFO_TYPE', 'QR_INFO_ADDRESS', 'INFO_STYLE', 'INFO_BACKGROUND', 'INFO_LOGO', 'INFO_BORDER', 'PURECARS_URL', 'INFO_NARROW_BG', 'INFO_DC1_BG', 'INFO_DC1C_BG', 'INFO_NARROW_NP_BG', 'INFO_PURE4_BG', 'INFO_DCS_BG', 'INFO_DC2_BG', 'INFO_DC2C_BG', 'INFO_DC3_BG', 'INFO_DC3C_BG', 'INFO_NP_CHECK', 'INFO_DC4_BG', '_ID', 'created_at', 'updated_at'
    ];

    /**
     *
     * Get Info sheet information from user_id, info_type
     *
     * @param user_id, info_type
     * @return object
     *
     */
    public static function getInfosheetInfo($user_id, $info_type){
    	return Static::where('USER_ID', $user_id)->where('INFO_TYPE', $info_type)->first();
    }

    /**
     *
     * Insert Infosheet Row
     *
     * @param insert_Array
     * @return inserted ID
     */
    public static function insertInfosheetInfo($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     *
     * Update Info sheet information from settings array where user_id, info_type
     *
     * @param user_id, info_type
     * @return updated_id
     *
     */
    public static function updateInfosheetInfo($user_id, $info_type, $update_array){
        return Static::where('USER_ID', $user_id)->where('INFO_TYPE', $info_type)->update($update_array);
    }
}
