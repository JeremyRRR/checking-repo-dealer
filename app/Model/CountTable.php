<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class CountTable extends Model
{

    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "count_table";

}