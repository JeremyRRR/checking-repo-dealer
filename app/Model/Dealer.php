<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Dealer extends Model
{
    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "dealer_dim";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID','ACTIVE','OWNER','DEALER_GROUP','DEALER_ID' ,'DEALER_NAME' ,'PRIMARY_CONTACT' ,'PRIMARY_CONTACT_EMAIL' ,'DEALER_LOGO' ,'DEALER_ADDRESS' ,'DEALER_CITY' ,'DEALER_STATE' ,'DEALER_ZIP' ,'DEALER_COUNTRY' ,'DEALER_PHONE' ,'BILLING_DATE' ,'BILLING_STREET' ,'BILLING_CITY' ,'BILLING_STATE' ,'BILLING_ZIP' ,'BILLING_COUNTRY' ,'ACCOUNT_TYPE' ,'CLIENT_ID' ,'RECURE_ID' ,'LINE_ID' ,'LAST30' ,'REFERRED_BY' ,'MAKE1' ,'MAKE2' ,'MAKE3' ,'MAKE4' ,'MAKE5' ,'LAT1' ,'LNG1' ,'SAVE_PDF' ,'SMS_PREFIX' ,'LAST_DAY' ,'VEHICLES_NEW' ,'CURRENT_NEW' ,'CURRENT_USED' ,'LAST_DATE' ,'PRINTED_NEW' ,'PRINTED_USED' ,'VEHICLES_USED' ,'PLUS_TODAY_N' ,'NEED_NEW' ,'NEED_USED' ,'PLUS_TODAY_U' ,'LAST30_NEW' ,'LAST30_USED' ,'RE_ORDER' ,'CRM_ID' ,'created_at' ,'updated_at'
    ];


    /**
     * Get dealer row from dealer_id
     *
     * @param $dealer_id (int)
     * @return Object
     */
    public static function getDealerByDeaderID($dealer_id){
    	return Static::where('DEALER_ID',$dealer_id)->first();
    }

    /**
     * Get dealer row from _ID
     *
     * @param _ID (int)
     * @return Object
     */
    public static function getDealerByID($id){
        return Static::where('_ID',$id)->first();
    }

    /**
     *
     * insert Dealer
     *
     * @param insert_Array
     * @return inserted ID
     */
    public static function insertDealerInfo($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     *
     * Update Dealer
     *
     * @param dealer id, array
     * @return updated ID
     */
    public static function updateDealerByID($dealer_id, $update_array){
        return Static::where('DEALER_ID', $dealer_id)->update($update_array);
    }

    /**
     * Get Dealer Data
     *
     * @param start, limit, sort, filter
     * @return Array
     */
    public static function getDealerData($filter, $start, $limit, $sortField, $order, $owner_id = '', $group_id = ''){
        $query = new Static;
        if($filter != ''){
            $query = $query->where(function($q) use ($filter){
                $q->where('DEALER_ID','like','%'.$filter.'%')
                ->orWhere('DEALER_NAME','like','%'.$filter.'%')
                ->orWhere('DEALER_CITY','like','%'.$filter.'%')
                ->orWhere('DEALER_STATE','like','%'.$filter.'%');
            });
        }
        if($owner_id != ''){
            $query = $query->where('OWNER', $owner_id);
        }
        if($group_id != ''){
            $query = $query->where('DEALER_GROUP', $group_id);
        }
        $query = $query->skip($start)->take($limit);
        if($sortField != ''){
            $query = $query->orderBy($sortField, $order);
        }
        return $query->get();
    }

    /**
     * Get Number of Data
     *
     * @param $filter
     * @return Integer
     */
    public static function getNumDealerData($filter, $owner_id = '', $group_id = ''){
        $query = new Static;
        if($filter != ''){
            $query = $query->where(function($q) use ($filter){
                $q->where('DEALER_ID','like','%'.$filter.'%')
                ->orWhere('DEALER_NAME','like','%'.$filter.'%')
                ->orWhere('DEALER_CITY','like','%'.$filter.'%')
                ->orWhere('DEALER_STATE','like','%'.$filter.'%');
            });
        }
        if($owner_id != ''){
            $query = $query->where('OWNER', $owner_id);
        }
        if($group_id != ''){
            $query = $query->where('DEALER_GROUP', $group_id);
        }
        return $query->count();
    }

}