<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class AddendumDefault extends Model
{
    /**
     * The DB table name
     *
     * @var string
     */
    protected $table = "addendum_defaults";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_ID',  'VEHICLE_ID', 'ITEM_NAME', 'ITEM_DESCRIPTION', 'ITEM_PRICE', 'DEALER_ID', 'SEPARATOR_BELOW', 'SEPARATOR_ABOVE', 'OG_OR_AD', 'SEPARATOR_SPACES', 'MODELS', 'BODY_STYLES', 'AD_TYPE', 'RE_ORDER','created_at', 'updated_at'
    ];

    /**
     * Get addendum Default Data from _ID
     *
     * @param $id
     * @return object
     */

    public static function getAdDefaultDataByID($id){
        return Static::where('_ID',$id)->first();
    }

    /**
     * Get Addendum Default Data from dealer_id, $new_used, model, style
     *
     * @param $dealer_id (int), vin_number String
     * @return Int
     */
    public static function getAdDefaultData($dealer_id, $new_used, $model, $style){
    	$query = Static::where('DEALER_ID',$dealer_id);
        $query = $query->where(function($q) use ($new_used) {
            $q->where('AD_TYPE',$new_used)->orWhere('AD_TYPE','Both');
        });
        $query = $query->where(function($q) use ($model, $style){
            $q->where(function($qq) use ($model){
                $qq->where('MODEL_1', $model)->orWhere('MODEL_1', 'All')
                ->orWhere('MODEL_2', $model)->orWhere('MODEL_2', 'All')
                ->orWhere('MODEL_3', $model)->orWhere('MODEL_3', 'All')
                ->orWhere('MODEL_4', $model)->orWhere('MODEL_4', 'All')
                ->orWhere('MODEL_5', $model)->orWhere('MODEL_5', 'All')
                ->orWhere('MODEL_6', $model)->orWhere('MODEL_6', 'All')
                ->orWhere('MODEL_7', $model)->orWhere('MODEL_7', 'All')
                ->orWhere('MODEL_8', $model)->orWhere('MODEL_8', 'All')
                ->orWhere('MODEL_9', $model)->orWhere('MODEL_9', 'All')
                ->orWhere('MODEL_10', $model)->orWhere('MODEL_10', 'All')
                ->orWhere('MODEL_11', $model)->orWhere('MODEL_11', 'All')
                ->orWhere('MODEL_12', $model)->orWhere('MODEL_12', 'All')
                ->orWhere('MODEL_13', $model)->orWhere('MODEL_13', 'All')
                ->orWhere('MODEL_14', $model)->orWhere('MODEL_14', 'All')
                ->orWhere('MODEL_15', $model)->orWhere('MODEL_15', 'All');
            })->where(function($qq) use ($style){
                $qq->where('BODY_STYLE_1', $style)->orWhere('BODY_STYLE_1', 'All')
                ->orWhere('BODY_STYLE_2', $style)->orWhere('BODY_STYLE_2', 'All')
                ->orWhere('BODY_STYLE_3', $style)->orWhere('BODY_STYLE_3', 'All')
                ->orWhere('BODY_STYLE_4', $style)->orWhere('BODY_STYLE_4', 'All')
                ->orWhere('BODY_STYLE_5', $style)->orWhere('BODY_STYLE_5', 'All')
                ->orWhere('BODY_STYLE_6', $style)->orWhere('BODY_STYLE_6', 'All')
                ->orWhere('BODY_STYLE_7', $style)->orWhere('BODY_STYLE_7', 'All');
            });
        });
        return $query->get();
    }

    /**
     *
     * Get Addendum Default Data from dealer_id, $new_used
     *
     * @param $dealer_id, $new_used, $re_order
     * @return array
     */
    public static function getAdDefaultDataForOption($dealer_id, $new_used, $re_order){
        $query = Static::where('DEALER_ID',$dealer_id);
        $query = $query->where(function($q) use ($new_used) {
            $q->where('AD_TYPE',$new_used)->orWhere('AD_TYPE','Both');
        });
        if($re_order == 1){
            $query = $query->orderBy('RE_ORDER','ASC');
        }else{
            $query = $query->orderBy('ITEM_NAME','ASC');
        }
        return $query->get();
    }

    /**
     * Insert Addendum Default
     *
     * @param array
     * @return inserted _ID
     */
    public static function insertAddendumDefault($insert_array){
        return Static::insertGetId($insert_array);
    }

    /**
     * Update Addendum Default
     *
     * @param array
     * @return updated _ID
     */
    public static function updateAddendumDefault($id, $update_array){
        return Static::where('_ID', $id)->update($update_array);
    }

    /**
     * Delete Addendum Default
     *
     * @param _ID
     */
    public static function deleteAddendumDefault($id){
        return Static::where('_ID', $id)->delete();
    }

    /**
     * Get Addendum Default Data
     *
     * @param DEALER_ID, ORDER_BY
     * @return array
     */
    public static function getOptionsForTable($dealer_id, $og_or_ad = '',  $order_by){
        $q =  Static::where('DEALER_ID', $dealer_id);
        if($og_or_ad != ''){
            $q->where('OG_OR_AD', $og_or_ad);
        }
        return $q->orderBy($order_by,'asc')->get();
    }

}